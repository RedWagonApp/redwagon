package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreateActivitySecond extends Fragment implements View.OnClickListener {
    private static final int GALLERY_REQUEST_CODE = 121, MULTIPLE_REQUEST_CODE = 122;
    EditText court_activity, activity_scl, event_time_zone_activity, event_country_activity, event_zip_activity, date_scl_activity, duration_activity, address_activity, cost_activity;
    private String court_activity_get = "", activity_scl_get = "",
            select_class_scl_get = "", event_time_zone_activity_get = "",
            event_country_activity_get = "",
            event_zip_activity_get = "",
            date_scl_activity_get = "",
            duration_activity_get = "",
            address_activity_get = "", dayOfWeek = "";
    private int mYear, mMonth, mDay, mHour, mMinute;
    Button save_activity;
    CircleImageView logo_img;
    LinearLayout upload_logo;
    TextView text_title, select_class_scl, not_found_txt;
    ImageView gallback;
    private Dialog coachdialog;
    RecyclerView coach_rv;
    private String from;
    RelativeLayout relativeLayout;
    LinearLayout layout1, layout2, layout3;
    private int count = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.create_activity, container, false);
        FindviewID(view);

        Bundle bundle = getArguments();

        if (bundle != null) {
            from = bundle.getString("from", "");
            activity_scl_get = bundle.getString("eventNameStr", "");
            event_time_zone_activity_get = bundle.getString("eventTimeZoneStr", "");
            event_country_activity_get = bundle.getString("eventCountryStr", "");
            event_zip_activity_get = bundle.getString("eventZipCodeStr", "");
            address_activity_get = bundle.getString("eventLocationStr", "");

        }

        text_title = view.findViewById(R.id.text_title);
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        save_activity.setOnClickListener(this);
        date_scl_activity.setOnClickListener(this);
        duration_activity.setOnClickListener(this);
        text_title.setText("When");

        return view;
    }

    private void FindviewID(View view) {

        save_activity = view.findViewById(R.id.save_activity);
        court_activity = view.findViewById(R.id.court_activity);
        activity_scl = view.findViewById(R.id.activity_scl);
        select_class_scl = view.findViewById(R.id.select_class_scl);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.linear_main_bar);

        date_scl_activity = view.findViewById(R.id.date_scl_activity);
        duration_activity = view.findViewById(R.id.duration_activity);
        address_activity = view.findViewById(R.id.address_activity);

        layout1 = view.findViewById(R.id.lay1);
        layout2 = view.findViewById(R.id.lay2);
        layout3 = view.findViewById(R.id.lay3);

        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.VISIBLE);
        layout3.setVisibility(View.GONE);
        //populateCountries();
//        select_class_scl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getCoachDialog();
//            }
//        });


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_activity) {
            validation();
        }
        if (v.getId() == R.id.date_scl_activity) {
            datePicker();
        }
        if (v.getId() == R.id.duration_activity) {
            opendurationDialog();
        }

    }


    private void validation() {

        court_activity_get = court_activity.getText().toString().trim();
        date_scl_activity_get = date_scl_activity.getText().toString().trim();
        duration_activity_get = duration_activity.getText().toString().trim();


        if (!date_scl_activity_get.equals("")) {
            if (!duration_activity_get.equals("")) {


                    DataParse();


            } else {
                duration_activity.setError("Please enter the Duration");
            }
        } else {
            date_scl_activity.setError("Please enter the Date/time");
        }

    }


    private void opendurationDialog() {

        // Create custom dialog object
        final Dialog dialog = new Dialog(getContext());
        // Include dialog.xml file
        dialog.setContentView(R.layout.durationdialog);
        // Set dialog title
        dialog.setTitle("Select " +
                " Duration");

        // set values for custom dialog components - text, image and button
        TimePicker simpleTimePicker = (TimePicker) dialog.findViewById(R.id.timepicker);
        simpleTimePicker.setIs24HourView(true);
        simpleTimePicker.setCurrentHour(new Integer(0));
        simpleTimePicker.setCurrentMinute(new Integer(0));

        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                duration_activity.setText(selectedTime);
            }
        });

        dialog.show();

        dialog.findViewById(R.id.set_duration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (duration_activity.getText().toString().equals("")) {

                    Toast.makeText(getContext(), "Please set Duration", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                }
            }
        });

    }

    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis() - 1000);
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                int day = calendar.get(Calendar.DAY_OF_WEEK);
                dayOfWeek = getDayofWeek(day);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = format.format(calendar.getTime());
                String selectedDate = strDate;
                timePicker(selectedDate);


            }
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void timePicker(final String selectedDate) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {


                        String selectedTime = String.format("%02d:%02d", hourOfDay, minute);

                        date_scl_activity.setText(selectedDate + "  " + selectedTime);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void DataParse() {
        Log.e("fsdfsdf", "DataParse: " + activity_scl_get);

        CostFragment fragment = new CostFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.crate_activity_scl, fragment);
        Bundle bundle = new Bundle();
        bundle.putString("eventNameStr", activity_scl_get);
        bundle.putString("eventTimeZoneStr", event_time_zone_activity_get);
        bundle.putString("eventCountryStr", event_country_activity_get);
        bundle.putString("eventZipCodeStr", event_zip_activity_get);
        bundle.putString("eventDateTimeStr", date_scl_activity_get);
        bundle.putString("eventDurationStr", duration_activity_get);
        bundle.putString("eventLocationStr", address_activity_get);
        bundle.putString("eventCourtStr", court_activity_get);
        bundle.putString("dayOfWeek", dayOfWeek);
        bundle.putString("select_class_scl_get", "null");
        bundle.putString("fromType", "Activities");

        fragment.setArguments(bundle);
        transaction.addToBackStack(null);
        transaction.commit();

    }


    public String getDayofWeek(int day) {
        switch (day) {
            case Calendar.MONDAY:
                dayOfWeek = "MON";
                break;

            case Calendar.TUESDAY:
                dayOfWeek = "TUE";
                break;

            case Calendar.WEDNESDAY:
                dayOfWeek = "WED";
                break;

            case Calendar.THURSDAY:
                dayOfWeek = "THU";
                break;

            case Calendar.FRIDAY:
                dayOfWeek = "FRI";
                break;

            case Calendar.SATURDAY:
                dayOfWeek = "SAT";
                break;

            case Calendar.SUNDAY:
                dayOfWeek = "SUN";
                break;
        }
        return dayOfWeek;
    }


}


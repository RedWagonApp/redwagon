package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ManagerAdapter;
import com.wagon.red_wagon.adapter.PreferenceDriverAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.DriverBody;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.model.PreferDriverModel;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class PreferenceDriver extends Fragment {
    RecyclerView pref_recycler;
    TextView no_result_tv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.preantdriver, container, false);

        FamilyDashboardActivity.title.setText("Preference Driver");

        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        no_result_tv = view.findViewById(R.id.no_result_tv);
        pref_recycler = view.findViewById(R.id.pref_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        pref_recycler.setLayoutManager(linearLayoutManager);
        ApiPreferDriver();


        return view;
    }

    private void ApiPreferDriver() {


        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<PreferDriverModel> getApi = mAPIService.usersDriverPreferenceList(Constans.getFamilyId(getActivity()));

        getApi.enqueue(new Callback<PreferDriverModel>() {
            @Override
            public void onResponse(Call<PreferDriverModel> call, retrofit2.Response<PreferDriverModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<DriverBody> bodyList = response.body().getBody();

                    if (bodyList.size() == 0) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        pref_recycler.setVisibility(View.GONE);
                    } else {
                        no_result_tv.setVisibility(View.GONE);
                        pref_recycler.setVisibility(View.VISIBLE);
                        PreferenceDriverAdapter preferenceDriverAdapter = new PreferenceDriverAdapter(getActivity(), bodyList);
                        pref_recycler.setAdapter(preferenceDriverAdapter);
                    }


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<PreferDriverModel> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }
}

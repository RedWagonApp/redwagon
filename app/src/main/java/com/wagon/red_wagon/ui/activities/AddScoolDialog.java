package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.NotificationResponse;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;

public class AddScoolDialog extends AppCompatActivity {
    Dialog driverDialog;
    NotificationResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_scool_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            response = (NotificationResponse) extras.getSerializable("data");
            requestDialog(response);
           /* if(response.getType().equals("joinFamily")) {

            }else if(response.getType().equals("joinCarPool")){
                requestDialog(response);
            }*/
            Log.e("Response", "onCreateView: " + response.getType());

        }
    }

    private void requestDialog(NotificationResponse response) {

        driverDialog = new Dialog(AddScoolDialog.this);
        driverDialog.setContentView(R.layout.addfamliy_dialog);

        TextView setmsg = driverDialog.findViewById(R.id.setmsg);
//        TextView drivername = driverDialog.findViewById(R.id.drivername);
//        drivername.setText(response.getDriverName1());
        if (response.getType().equals("join")) {
            setmsg.setText(response.getName());
        }else{
            setmsg.setText(response.getMsg());
        }
        TextView reject_btn = driverDialog.findViewById(R.id.reject_btn);
        TextView accept_btn = driverDialog.findViewById(R.id.accept_btn);

        accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (response.getType().equals("joinFamily")) {
                    addFamilyRequestAccept(response.getMeUserId(), response.getSenduserId(), "1");
                } else if (response.getType().equals("joinCarPool")) {
                    joinCarPoolChild(response.getCarPoolId(),response.getDriverId(),response.getChildId(),response.getParentId(),"1");

                } else if (response.getType().equals("join")) {

                    acceptRejectJoin(response.getDriverId(),response.getCarPoolId(), response.getUserId(), "1", response.getCarPoolName(), response.getDriverName1(),response.getRequestId());

                }
            }
        });
        reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (response.getType().equals("joinFamily")) {
                    addFamilyRequestAccept(response.getMeUserId(), response.getSenduserId(), "2");
                } else if (response.getType().equals("joinCarPool")) {
                    joinCarPoolChild(response.getCarPoolId(),response.getDriverId(),response.getChildId(),response.getParentId(),"2");
                }  else if (response.getType().equals("join")) {

                acceptRejectJoin(response.getDriverId(),response.getCarPoolId(), response.getUserId(), "2", response.getCarPoolName(), response.getDriverName1(),response.getRequestId());

            }
            }
        });
        driverDialog.show();
    }

    private void addFamilyRequestAccept(String getMeUserId, String getSenduserId, String type) {
        final Dialog dialog = AppUtils.showProgress(AddScoolDialog.this);
        APIService mAPIService = AppUtils.getAPIService(AddScoolDialog.this);
        Call<AddEvent> getApi = mAPIService.addFamilyRequestAccept(getSenduserId, getMeUserId, type);
//        Log.e("acceptRejectJoin", "acceptRejectJoin: " + getCarPoolId + "\n" + getUserId + "\n" + type);
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    finish();
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(AddScoolDialog.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(AddScoolDialog.this);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                call.cancel();
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }
    private void joinCarPoolChild(String carpoolid,String driverId,String childId,String parentId,String status) {
        final Dialog dialog = AppUtils.showProgress(AddScoolDialog.this);
        APIService mAPIService = AppUtils.getAPIService(AddScoolDialog.this);
        Call<AddEvent> getApi = mAPIService.joinCarPoolChildAccept(carpoolid, driverId, parentId, childId,status);
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    finish();

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(AddScoolDialog.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(AddScoolDialog.this);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }
    private void acceptRejectJoin(String driver,String getCarPoolId, String getUserId, String type, String getCarPoolName, String getDriverName1, String chatId) {
        final Dialog dialog = AppUtils.showProgress(AddScoolDialog.this);
        APIService mAPIService = AppUtils.getAPIService(AddScoolDialog.this);
        Call<AddEvent> getApi = mAPIService.addDriverPoolList(getUserId, getCarPoolId,driver, type, getCarPoolName, getDriverName1,chatId);
        //Log.e("acceptRejectJoin", "acceptRejectJoin: " + getCarPoolId + "\n" + getUserId + "\n" + type);
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    finish();
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(AddScoolDialog.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(AddScoolDialog.this);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

}

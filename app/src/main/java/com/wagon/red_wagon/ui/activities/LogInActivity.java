package com.wagon.red_wagon.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LogInActivity extends BaseActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 44;
    Button register;
    EditText email_address_login, password;
    String getEmail, getPasword;
    ProgressDialog myDialog;
    ImageView google_intigration, teamsnap, facebook_click;
    public FirebaseAuth firebaseAuth;
    public GoogleApiClient googleApiClient;
    public static final int RequestSignInCode = 7;
    public static final String TAG = "CalendarActivity";
    CallbackManager callbackManager;
    LoginButton loginButton;
    TextView singup_screen, forget_;
    int mStatusCode;
    FrameLayout fram_dash;
    // CheckBox radio_button;
    boolean cheack = false;
    LocationManager locationManager;
    boolean loggedOut;
    private String latitude = "", longitude = "", add;
    private double lat, longi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in__screen);
        firebaseAuth = FirebaseAuth.getInstance();

        findViewMethod();
        generateKeyHash();
        if (Constans.FCMToken(this).equals("")) {
            firebaseInstance();
        }
        loginButton = findViewById(R.id.loginButton);
        forget_ = findViewById(R.id.forget_);
        forget_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogInActivity.this, ForgetPassword.class));
            }
        });
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }
        loggedOut = AccessToken.getCurrentAccessToken() == null;
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();

                GraphRequestAsyncTask request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                        Log.d(TAG, user.optString("email"));
                        Log.d(TAG, user.optString("name"));
                        Log.d(TAG, user.optString("id"));

                        Constans.getDisplayName = user.optString("name");
                        Constans.getEmail = user.optString("email");
                        Constans.getFbGoogleId = user.optString("id");
                        Log.e(TAG, "onCompletedemail: " + Constans.getEmail);

                        loginService("social");

                    }
                }).executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e(TAG, "onError: " + exception);
            }
        });

        googleInt();


        register.setOnClickListener(this);
        facebook_click.setOnClickListener(this);
//        teamsnap.setOnClickListener(this);
        google_intigration.setOnClickListener(this);
        singup_screen.setOnClickListener(this);
    }

    private void googleInt() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(LogInActivity.this)
                .enableAutoManage(LogInActivity.this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }


    private void checkVaildation() {

        getEmail = email_address_login.getText().toString();
        getPasword = password.getText().toString();
        if (!getEmail.equals("")) {
        if (AppUtils.emailValidator(getEmail)) {
            if (!getPasword.equals("") && getPasword.length() > 5) {
                loginService("");
            } else {
                password.setError("Please enter the password ");
            }
        } else {
            email_address_login.setError("Please enter valid Email Address");
        }
        } else {
            email_address_login.setError("Please enter your Email Address");
        }

    }


    private void findViewMethod() {
        //radio_button = findViewById(R.id.radio_button);
        register = findViewById(R.id.log_btn);
        facebook_click = findViewById(R.id.facebook_click);
        email_address_login = findViewById(R.id.email_addres_login);
        password = findViewById(R.id.password);
        google_intigration = findViewById(R.id.google_intigration);
        singup_screen = findViewById(R.id.singup_screen);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.log_btn) {
            checkVaildation();
        } else if (view.getId() == R.id.google_intigration) {
            UserSignInMethod();
        } else if (view.getId() == R.id.facebook_click) {
            getUserDetail();
        } else if (view.getId() == R.id.singup_screen) {
            Intent intent = new Intent(LogInActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
    }

    private void UserSignInMethod() {
        Intent AuthIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(AuthIntent, RequestSignInCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestSignInCode) {
            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (googleSignInResult.isSuccess()) {
                GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();
                FirebaseUserAuth(googleSignInAccount);
            }
        }
    }

    private void FirebaseUserAuth(GoogleSignInAccount googleSignInAccount) {

        AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
        firebaseAuth.signInWithCredential(authCredential)
                .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> AuthResultTask) {

                        if (AuthResultTask.isSuccessful()) {
                            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                            Constans.getDisplayName = firebaseUser.getDisplayName();
                            Constans.getEmail = firebaseUser.getEmail();
                            Constans.uri = firebaseUser.getPhotoUrl();
                            Constans.getPhoneNumber = firebaseUser.getPhoneNumber();
                            Constans.getFbGoogleId = googleSignInAccount.getId();
                            loginService("social");
                            Log.d("firebaseuser", "=" + firebaseUser.getDisplayName());
                        } else {
                            Toast.makeText(LogInActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void UserSignOutFunction() {
        firebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        // Toast.makeText(LogInActivity.this, "Logout Successfully", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getUserDetail() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json, GraphResponse response) {
                        if (response.getError() != null) {
                            System.out.println("ERROR");
                        } else {
                            System.out.println("Success");
                            String jsonresult = String.valueOf(json);
                            System.out.println("JSON Result" + jsonresult);
                            Constans.getFbGoogleId = json.optString("id");
                            Constans.getDisplayName = json.optString("name");
                            Constans.getEmail = json.optString("email");
                            loginService("social");
                        }
                        Log.e("FaceBook Response :", response.toString());
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.v("LoginActivity", "" + exception);
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void loginService(final String social) {
        final Dialog dialog = AppUtils.showProgress(this);
        String url;
        if (social.equals("social")) {
            url = Constans.BASEURL + "api/socialLogin";
        } else {
            url = Constans.BASEURL + "api/login";
        }
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                UserSignOutFunction();
                LoginManager.getInstance().logOut();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("sfdfsdfs", "sdfdfs" + response);
                    String message = jsonObject.getString("message");
                    JSONObject jsonObject1 = jsonObject.getJSONObject("body");
                    Constans.Token = jsonObject1.getString("token");
                    Constans.FMID = jsonObject1.getString("id");
                    String phoneNumber = jsonObject1.getString("phoneNumber");
                    String type = jsonObject1.getString("gender");
                    JSONObject jsonObject2 = jsonObject1.getJSONObject("userDetail");
                    String pic = jsonObject2.getString("profileImage");
                    String userName = jsonObject1.getString("firstName");
                    String charityId = jsonObject1.getString("charityId");
                    String latitude = jsonObject2.getString("latitude");
                    String longitude = jsonObject2.getString("longitude");
                    Log.e("latitude", "onResponse: "+latitude);
                    Log.e("longitude", "onResponse: "+longitude);
                    SharedPreferences.Editor editor1 = getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor1.putString("Token", Constans.Token);
                    editor1.putString("PicUrl", pic);
                    editor1.putString("charityId", charityId);
                    editor1.putString("FMID", Constans.FMID);
                    editor1.putString("phoneNumber", phoneNumber);
                    editor1.putString("userName", userName);
                    editor1.putString("type", type);
                    editor1.putString("longitude", longitude);
                    editor1.putString("latitude", latitude);
                    Gson gson = new Gson();
                    String json = gson.toJson(jsonObject2);
                    editor1.putString("profileData", json);

                    Log.e("GetId", "ID==" + Constans.FMID);
                    switch (type) {
                        case "Manager":
                            Log.e("Manager", "ManagerManager" + type);
                            editor1.putString("userr", "Manager");
                            String schId = jsonObject1.getString("schId");
                            editor1.putString("SchlId", schId);
                            Intent intent2 = new Intent(LogInActivity.this, TwoOption.class);
                            intent2.putExtra("Coach", "2");
                            intent2.putExtra("text", "Coach");
                            startActivity(intent2);
                            finish();
                            break;
                        case "Family":
                        case "Driver": {
                            Log.e("Manager", "FamilyFamily" + type);
                            editor1.putString("userr", type);
                            Intent intent = new Intent(LogInActivity.this, FamilyDashboardActivity.class);
                            intent.putExtra("check", "1");
                            intent.putExtra("child", "0");
                            intent.putExtra("type", "2");
                            intent.putExtra("UserPic", Constans.PicUrl);
                            startActivity(intent);
                            finish();
                            break;
                        }
                        case "child": {
                            Log.e("Manager", "FamilyFamily" + type);

                            editor1.putString("userr", type);
                            Intent intent = new Intent(LogInActivity.this, FamilyDashboardActivity.class);
                            intent.putExtra("check", "1");
                            intent.putExtra("child", "1");
                            intent.putExtra("type", "2");
                            intent.putExtra("UserPic", Constans.PicUrl);
                            startActivity(intent);
                            finish();
                            break;
                        }
                        case "School": {
                            Log.e("Manager", "SchoolSchool" + type);
                            SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
                            editor.putString("userr", "School");
                            editor.putString("Coach", "1");
                            editor.putString("PicUrl", pic);
                            editor.apply();
                            Intent intent = new Intent(LogInActivity.this, SchoolDashboard.class);
                            intent.putExtra("Coach", "1");
                            intent.putExtra("type", "2");
                            intent.putExtra("text", "Club");
                            intent.putExtra("UserPic", Constans.PicUrl);
                            startActivity(intent);
                            finish();
                            break;
                        }
                    }
                    editor1.apply();
                    Log.e("Constans.Token", "Constans.Token" + Constans.Token);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Constans.Token", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                UserSignOutFunction();
                LoginManager.getInstance().logOut();
                try {
                    mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 401) {
                        // HTTP Status Code: 401 Unauthorized
                        AppUtils.sessionExpiredAlert(LogInActivity.this);
                    }
                } catch (NullPointerException e) {
                    Toast.makeText(LogInActivity.this, "Server Error", Toast.LENGTH_SHORT).show();

                }
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject obj = new JSONObject(res);
                        String message = obj.getString("message");
                        Log.e("objobjobj", "objobjobj" + message);
                        if (message.equals("User Not exists")) {

                            registerAlert();
                        } else {
                            Toast.makeText(LogInActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (social.equals("social")) {
                    params.put("fbGoogleId", Constans.getFbGoogleId);
                } else {
                    params.put("email", getEmail);
                    params.put("password", getPasword);
                    params.put("latitude", latitude);
                    params.put("longitude", longitude);
                    params.put("deviceToken", Constans.FCMToken(LogInActivity.this));
                    params.put("deviceType", "2");

                }
                Log.e(TAG, "getParams: " + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }


    private void generateKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.wagon.red_wagon",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "onCreate: " + e);

        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "onCreate wd: ", e);

        }
    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, 111);
        } else {
            Location LocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps != null) {
                lat = LocationGps.getLatitude();
                longi = LocationGps.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
            } else if (LocationNetwork != null) {
                lat = LocationNetwork.getLatitude();
                longi = LocationNetwork.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
            } else if (LocationPassive != null) {
                lat = LocationPassive.getLatitude();
                longi = LocationPassive.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
            } else {
                Toast.makeText(this, "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void firebaseInstance() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.e("firebaseInstace", "getInstanceId failed", task.getException());
                return;
            }
            String fcmToken = task.getResult().getToken();
            SharedPreferences.Editor editor1 = getSharedPreferences("Login", MODE_PRIVATE).edit();
            editor1.putString("FCMToken", fcmToken);
            editor1.apply();
            Log.e("FCMToken", fcmToken);

        });
    }


    private void registerAlert() {
        Dialog driverDialog = new Dialog(LogInActivity.this);
        driverDialog.setContentView(R.layout.register_dialog);
        driverDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        TextView reject_btn = driverDialog.findViewById(R.id.reject_btn);
        TextView accept_btn = driverDialog.findViewById(R.id.accept_btn);

        accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogInActivity.this, RegisterActivity.class);
                startActivity(intent);
                driverDialog.dismiss();
            }
        });
        reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                driverDialog.dismiss();
            }
        });
        driverDialog.show();
    }
}

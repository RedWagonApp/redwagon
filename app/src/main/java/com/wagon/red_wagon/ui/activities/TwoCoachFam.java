package com.wagon.red_wagon.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wagon.red_wagon.R;

public class TwoCoachFam extends BaseActivity implements View.OnClickListener {
    TextView text_title;
    LinearLayout family_set, coach_set;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.two_coach_fam );

        family_set = findViewById( R.id.family_set );
        coach_set = findViewById( R.id.coach_set );
        text_title = findViewById( R.id.text_title );

        text_title.setText( "What would you like to do Today" );
        coach_set.setOnClickListener( this );
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.coach_set) {
            Intent intent = new Intent( TwoCoachFam.this, SchoolDashboard.class);

            startActivity(intent);
        }
        else if (v.getId() == R.id.family_set) {
            Intent intent = new Intent( TwoCoachFam.this, FamilyDashboardActivity.class);
            intent.putExtra( "check","1" );
            startActivity(intent);
        }
    }
}
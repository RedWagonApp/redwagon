package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CoachRowAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.model.ProfieModel;
import com.wagon.red_wagon.model.UsersDetail;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class CreateFamilyActivity extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CoachRowAdapter.AdapterCallback, CountryAdapter.CountryCallback {
    private static final int GALLERY_REQUEST_CODE = 121, MULTIPLE_REQUEST_CODE = 122;
    private EditText court_activity, activity_scl, event_time_zone_activity, event_country_activity, event_zip_activity, date_scl_activity, duration_activity, cost_activity;
    private String court_activity_get = "", activity_scl_get = "",
            select_class_scl_get = "", event_time_zone_activity_get = "",
            event_country_activity_get = "",
            event_zip_activity_get = "",
            date_scl_activity_get = "",
            duration_activity_get = "",
            address_activity_get = "", cost_activity_get = "", dayOfWeek = "";

    private int mYear, mMonth, mDay, mHour, mMinute;
    private  Button save_activity;
    private String image_path = "";
    private CircleImageView logo_img;
    private LinearLayout upload_logo, upload_images, lnrImages;
    private CountryCodePicker teamCountryccp;
    private TextView text_title, select_class_scl, not_found_txt;
    private ImageView gallback;
    private Spinner timezone_sp;
    private Dialog coachdialog, countrydialog;
    private RecyclerView coach_rv, country_rv;
    private String from;
    private ArrayList<String> imagesArr;
    private CountryAdapter countryAdapter;
    private  RelativeLayout relativeLayout;
    private  AutoCompleteAdapter mAdapter;
    PlacesClient placesClient;
    private AutoCompleteTextView address_activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.create_family_activity, container, false);
        FindviewID(view);

        Bundle bundle = getArguments();

        if (bundle != null) {
            from = bundle.getString("from", "");

        }

        FamilyDashboardActivity.title.setText("Add Activity");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        save_activity.setOnClickListener(this);
        date_scl_activity.setOnClickListener(this);
        duration_activity.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        upload_images.setOnClickListener(this);
        timezone_sp.setOnItemSelectedListener(this);

        setUpAutoCompleteTextView();
        getProfileDetail();
        final CountryAdapter.CountryCallback countryCallback = (CountryAdapter.CountryCallback) this;
        event_country_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.countryDialog(getContext(), countryCallback);
            }
        });

        event_time_zone_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.timeZoneDialog(getContext(), countryCallback);
            }
        });
        address_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });
        event_time_zone_activity.setText(AppUtils.getCurrentTimeZone());
        return view;
    }

    private void FindviewID(View view) {
        upload_logo = view.findViewById(R.id.upload_logo);
        upload_images = view.findViewById(R.id.upload_images);
        logo_img = view.findViewById(R.id.logo_img);
        lnrImages = view.findViewById(R.id.lnrImages);
        save_activity = view.findViewById(R.id.save_activity);
        court_activity = view.findViewById(R.id.court_activity);
        timezone_sp = view.findViewById(R.id.timezone_sp);
        activity_scl = view.findViewById(R.id.activity_scl);
        select_class_scl = view.findViewById(R.id.select_class_scl);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.linear_main_bar);
        event_country_activity = view.findViewById(R.id.event_country_activity);
        event_time_zone_activity = view.findViewById(R.id.event_time_zone_activity);
        event_zip_activity = view.findViewById(R.id.event_zip_activity);
        date_scl_activity = view.findViewById(R.id.date_scl_activity);
        duration_activity = view.findViewById(R.id.duration_activity);
        address_activity = view.findViewById(R.id.address_activity);
        save_activity = view.findViewById(R.id.save_activity);
        cost_activity = view.findViewById(R.id.cost_activity);
        teamCountryccp = view.findViewById(R.id.team_country);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_activity) {
            activity_scl_get = activity_scl.getText().toString().trim();
            if (!activity_scl_get.equals("")) {
                validation();
            } else {
                activity_scl.setError("Please enter the Activity");
            }
        }
        if (v.getId() == R.id.date_scl_activity) {
            datePicker();
        }
        if (v.getId() == R.id.duration_activity) {
            opendurationDialog();
        }
        if (v.getId() == R.id.upload_images) {
            Matisse.from(getActivity())
                    .choose(MimeType.ofAll())
                    .countable(true)
                    .maxSelectable(3)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(new GlideEngine())
                    .showPreview(false) // Default is `true`
                    .forResult(MULTIPLE_REQUEST_CODE);
        }
        if (v.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);
        }

    }

    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());
    }


    private void getPlaceInfo(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);

        if (addresses.get(0).getPostalCode() != null) {
            String ZIP = addresses.get(0).getPostalCode();
            event_zip_activity.setText(ZIP);
            Log.e("ZIP CODE", ZIP);
        }

        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();

            Log.e("CITY", city);
        }

        if (addresses.get(0).getAdminArea() != null) {
            String state = addresses.get(0).getAdminArea();
            Log.e("STATE", state);
        }

        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            event_country_activity.setText(country);
            Log.e("COUNTRY", country);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        event_time_zone_activity_get = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void validation() {
        court_activity_get = court_activity.getText().toString().trim();
        event_country_activity_get = event_country_activity.getText().toString().trim();
        event_zip_activity_get = event_zip_activity.getText().toString().trim();
        date_scl_activity_get = date_scl_activity.getText().toString().trim();
        duration_activity_get = duration_activity.getText().toString().trim();
        address_activity_get = address_activity.getText().toString().trim();
        cost_activity_get = cost_activity.getText().toString().trim();
        event_time_zone_activity_get = event_time_zone_activity.getText().toString().trim();
        if (!event_time_zone_activity_get.equals("")) {
            if (!event_country_activity_get.equals("")) {
                if (!event_zip_activity_get.equals("")) {
                    if (!date_scl_activity_get.equals("")) {
                        if (!duration_activity_get.equals("")) {
                            if (!address_activity_get.equals("")) {

                                        DataParse();

                            } else {
                                court_activity.setError("Please enter the Court/Field etc");
                            }
                        } else {
                            duration_activity.setError("Please enter the Duration");
                        }
                    } else {
                        date_scl_activity.setError("Please enter the Date/time");
                    }
                } else {
                    event_zip_activity.setError("Please enter the Zipcode");
                }
            } else {
                event_country_activity.setError("Please enter the Country");
            }
        } else {
            Toast.makeText(getContext(), "Please select the Time Zone", Toast.LENGTH_SHORT).show();
        }
    }


    private void opendurationDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.durationdialog);
        dialog.setTitle("Select " + " Duration");
        TimePicker simpleTimePicker = (TimePicker) dialog.findViewById(R.id.timepicker);
        simpleTimePicker.setIs24HourView(true);
        simpleTimePicker.setCurrentHour(new Integer(0));
        simpleTimePicker.setCurrentMinute(new Integer(0));
        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                duration_activity.setText(selectedTime);
            }
        });
        dialog.show();
        dialog.findViewById(R.id.set_duration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (duration_activity.getText().toString().equals("")) {

                    Toast.makeText(getContext(), "Please set Duration", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis() - 1000);
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                int day = calendar.get(Calendar.DAY_OF_WEEK);

                dayOfWeek = getDayofWeek(day);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = format.format(calendar.getTime());
                String selectedDate = strDate;
                timePicker(selectedDate);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void timePicker(final String selectedDate) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {

            @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                date_scl_activity.setText(selectedDate + "  " + selectedTime);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void DataParse() {
        Log.e("fsdfsdf", "DataParse: " + activity_scl_get);

        RecurrenceFamilyFrag fragment = new RecurrenceFamilyFrag();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.fram_dash, fragment);
        Bundle bundle = new Bundle();
        bundle.putString("eventNameStr", activity_scl_get);
        bundle.putString("eventTimeZoneStr", event_time_zone_activity_get);
        bundle.putString("eventCountryStr", event_country_activity_get);
        bundle.putString("eventZipCodeStr", event_zip_activity_get);
        bundle.putString("eventDateTimeStr", date_scl_activity_get);
        bundle.putString("eventDurationStr", duration_activity_get);
        bundle.putString("eventLocationStr", address_activity_get);
        bundle.putString("select_class_scl_get", "school");
        bundle.putString("cost_activity_get", cost_activity_get);
        bundle.putString("court_activity_get", court_activity_get);
        bundle.putStringArrayList("imagesArr", imagesArr);
        bundle.putString("image_path", image_path);
        bundle.putString("dayOfWeek", dayOfWeek);
        bundle.putString("fromType", "Activities");
        fragment.setArguments(bundle);
//        transaction.commitAllowingStateLoss();
//        fragmentManager.executePendingTransactions();
        transaction.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(image_path);
            logo_img.setImageBitmap(bitmap);


        }

        if (requestCode == MULTIPLE_REQUEST_CODE && resultCode == RESULT_OK) {
            lnrImages.setVisibility(View.VISIBLE);
            imagesArr = (ArrayList<String>) Matisse.obtainPathResult(data);
            for (int i = 0; i < imagesArr.size(); i++) {
                Bitmap yourbitmap = BitmapFactory.decodeFile(imagesArr.get(i));

                ImageView imageView = new ImageView(getContext());
                imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(50, 50));
                imageView.setMaxHeight(50);
                imageView.setMaxWidth(50);
                imageView.setPadding(5, 0, 5, 0);
                imageView.setImageBitmap(yourbitmap);
                imageView.setAdjustViewBounds(true);
                lnrImages.addView(imageView);
                Log.e("Matisse", "mSelected: " + imagesArr);
            }
        }

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                address_activity.setText(place.getName() + ", " + place.getAddressComponents().asList().get(1).getName());

                try {
                    getPlaceInfo(place.getLatLng().latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }

    @Override
    public void onMethodCallback(String name, String id) {
        coachdialog.dismiss();
        select_class_scl_get = id;

        select_class_scl.setText(name);
    }

    private String getrealPathFromUrl(Uri uri) {
        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();
        return result;
    }


    @Override
    public void onSelectCountryCallback(String name) {
        AppUtils.dismissDialog();
        event_country_activity.setText(name);

    }

    @Override
    public void onSelectTimeZoneCallback(String name) {
        AppUtils.dismissDialog();
        event_time_zone_activity.setText(name);
    }

    public String getDayofWeek(int day) {
        switch (day) {
            case Calendar.MONDAY:
                dayOfWeek = "MON";
                break;

            case Calendar.TUESDAY:
                dayOfWeek = "TUE";
                break;

            case Calendar.WEDNESDAY:
                dayOfWeek = "WED";
                break;

            case Calendar.THURSDAY:
                dayOfWeek = "THU";
                break;

            case Calendar.FRIDAY:
                dayOfWeek = "FRI";
                break;

            case Calendar.SATURDAY:
                dayOfWeek = "SAT";
                break;

            case Calendar.SUNDAY:
                dayOfWeek = "SUN";
                break;
        }
        return dayOfWeek;
    }

    private void getProfileDetail() {
        final Dialog dialog1 = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());


        mAPIService.getUserProfile().enqueue(new Callback<ProfieModel>() {
            @Override
            public void onResponse(Call<ProfieModel> call, retrofit2.Response<ProfieModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog1.dismiss();
                if (response.code() == 200) {
                    UsersDetail usersDetail = response.body().getBody().getUsersDetail();
                    Calendar cal = Calendar.getInstance();
                    TimeZone tz = cal.getTimeZone();
                    Log.d("Time zone", "=" + tz.getDisplayName());
                    event_country_activity.setText(usersDetail.getCountry());
                    event_time_zone_activity.setText(tz.getID());
                    event_zip_activity.setText(usersDetail.getZipCode());
                    Log.e("sequrty_number", "onResponse: " + Constans.BASEURL + usersDetail.getProfileImage());
                } else if (response.code() == 400) {
                    dialog1.dismiss();
                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    Toast.makeText(getContext(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfieModel> call, Throwable t) {
                dialog1.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }

}


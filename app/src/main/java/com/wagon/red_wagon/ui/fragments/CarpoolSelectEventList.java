package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CarpoolEventsAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CarpoolSelectEventList extends Fragment implements View.OnClickListener {

    EditText search_et;
    TextView player_count, text_title;
    ImageView gallback;
    Button sendcarpoolnotif;
    RecyclerView events_rv;
    CarpoolEventsAdapter eventAdapter;
    LinearLayout create_activity_lay;
    public static String eventId = "";
    List<Body> responseBody;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.create_pool, container, false);
        findView(view);
        FamilyDashboardActivity.title.setText("Create a Car Pool");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        events_rv.setLayoutManager(linearLayoutManager);

        getEventList();
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                if (responseBody!=null && responseBody.size() != 0) {
                    filter(editable.toString());
                }
            }
        });
        return view;
    }

    private void findView(View view) {
        //arrayList = new ArrayList<>();

        search_et = view.findViewById(R.id.search_et);
        events_rv = view.findViewById(R.id.events_rv);
        create_activity_lay = view.findViewById(R.id.create_activity_lay);
        sendcarpoolnotif = view.findViewById(R.id.sendcarpoolnotif);
        sendcarpoolnotif.setOnClickListener(this);
        create_activity_lay.setOnClickListener(this);
    }

    private void getEventList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getPoolEvent();


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();

                try {

                    if (response.code() == 200) {

                        responseBody = response.body().getBody();

                        if (responseBody.size() != 0) {

                            eventAdapter = new CarpoolEventsAdapter(getActivity(), responseBody);
                            events_rv.setAdapter(eventAdapter);

                        }

                    } else if (response.code() == 400) {
                        if (!response.isSuccessful()) {
                            try {

                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");

                                Log.e("MessageM ", userMessage);
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    } else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                }
                catch (Exception e){

                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


    private void filter(String text) {
        try {
            List<Body> filterdList = new ArrayList<>();


            for (Body s : responseBody) {

                if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                    filterdList.add(s);

                }
            }
            eventAdapter.filterList(filterdList);
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sendcarpoolnotif) {
            if (!eventId.equals("")) {
                CreateCarPoolFrag eventList = new CreateCarPoolFrag();
                Bundle args = new Bundle();
                args.putString("eventId", eventId);
                eventList.setArguments(args);

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fram_dash, eventList);
                transaction.addToBackStack(null);
                transaction.commit();
            } else {
                Toast.makeText(getContext(), "Please Create or Select Activity", Toast.LENGTH_SHORT).show();
            }

        }
        if (v.getId() == R.id.create_activity_lay) {
            CreateFamilyActivity createEvent = new CreateFamilyActivity();
            Bundle args = new Bundle();
            args.putString("from", "Family");
            createEvent.setArguments(args);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fram_dash, createEvent, "Add Activity");
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }
}

package com.wagon.red_wagon.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
public class CongFamily extends Fragment {
    TextView text_title;
    LinearLayout add_childclick, referw;
    Button home_page;

    @Nullable

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.cong_famliy, container, false);
        add_childclick = view.findViewById(R.id.add_childclick);
        referw = view.findViewById(R.id.referw);

        FamilyDashboardActivity.title.setText("Congratulations");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);


        add_childclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new ChildProfileFrag(), "Child Profile");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        referw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Invite_Screen invite_screen = new Invite_Screen();
                Bundle args = new Bundle();
                args.putString("school_dashboard", "2");
                invite_screen.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, invite_screen, "Invite");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }


}

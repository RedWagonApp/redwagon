package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class ChildDetailsFrag extends Fragment {

    EditText grade, mobile_num, email_test;
    String dateof_birth_get = "", grade_get, mobile_num_get, email_test_get, im, first_name_child_get, child_get, last_name_child_get;
    TextView text_title, dateof_birth;
    Button next_childditails;
    Bundle args;
    Bitmap bitmap;
    String filePath;
    ImageView userimh;
    Calendar myCalendar;
    LinearLayout open_clndr;
    RelativeLayout linear_main_bar;
    CountryCodePicker ccp;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.child_details_frag, container, false);
        args = getArguments();
        FamilyDashboardActivity.title.setText("Child Profile");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        findviiew_Id(view);


//        try {
//            Bundle args = getArguments();
//
//            child_get = args.getString( "childd", "" );
//            if(child_get.equals( "1" )){
//                linear_main_bar.setVisibility( View.GONE );
//            }
//            else{
//                linear_main_bar.setVisibility( View.VISIBLE );
//            }
//        }
//        catch (Exception e){
//
//        }

        try {
            filePath = args.getString("filePath", "");
            first_name_child_get = args.getString("first_name_child_get", "");
            last_name_child_get = args.getString("surname_child_get", "");
            bitmap = StringToBitMap(im);
            userimh.setImageBitmap(bitmap);
            Log.e("first_name_child_get", "=" + bitmap);
        } catch (NullPointerException e) {

        }
//        text_title.setText("About " + Constans.child_name);
        next_childditails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("fragmentManager", "fragmentManager");
                validation();
            }
        });


        //datepicker

        myCalendar = Calendar.getInstance();


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        open_clndr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


        return view;
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateof_birth.setText(sdf.format(myCalendar.getTime()));
    }

    private void findviiew_Id(View view) {

        dateof_birth = view.findViewById(R.id.dateof_birth);
        grade = view.findViewById(R.id.grade);
        mobile_num = view.findViewById(R.id.mobile_num);
        email_test = view.findViewById(R.id.email_test);
        userimh = view.findViewById(R.id.userimh);
        next_childditails = view.findViewById(R.id.next_childditails);
        open_clndr = view.findViewById(R.id.open_clndr);
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        ccp = view.findViewById(R.id.ccp);


    }


    private void validation() {

        grade_get = grade.getText().toString().trim();
        dateof_birth_get = dateof_birth.getText().toString().trim();
        mobile_num_get = ccp.getSelectedCountryCode() + mobile_num.getText().toString().trim();
        email_test_get = email_test.getText().toString().trim();

//        if (!dateof_birth_get.equals("")) {

        if (!email_test_get.equals("")) {
            if (emailValidator(email_test_get)) {
                if (!mobile_num_get.equals("")) {
                    SeriveApi();
                } else {
                    mobile_num.setError("Please enter your mobile number");
                }
            } else {
                email_test.setError("Please enter valid email address");
            }
        } else {
            email_test.setError("Please enter the email address");
        }


       /* } else {
            dateof_birth.setError("Please enter the DOB");
        }*/
    }


    private void SeriveApi() {

        Log.e("fragmentManager", "fragmentManager");


        AboutChild child_dtails_frag = new AboutChild();
        Bundle args = new Bundle();
        args.putString("first_name_child_get", first_name_child_get);
        args.putString("surname_child_get", last_name_child_get);
        args.putString("bitmap", filePath);
        args.putString("dateof_birth_get", dateof_birth_get);
        args.putString("mobile_num_get", mobile_num_get);
        args.putString("email_test_get", email_test_get);
        child_dtails_frag.setArguments(args);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.child_details_frag, child_dtails_frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    public Bitmap StringToBitMap(String image) {
        try {
            byte[] encodeByte = Base64.decode(image, Base64.DEFAULT);
            InputStream inputStream = new ByteArrayInputStream(encodeByte);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

}

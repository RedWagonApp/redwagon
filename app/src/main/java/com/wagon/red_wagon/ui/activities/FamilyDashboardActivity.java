package com.wagon.red_wagon.ui.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.EventcarpoolFrag;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.NotificationResponse;
import com.wagon.red_wagon.ui.fragments.AboutCar;
import com.wagon.red_wagon.ui.fragments.CarpoolListFrag;
import com.wagon.red_wagon.ui.fragments.ChatsFragment;
import com.wagon.red_wagon.ui.fragments.ChildProfileFrag;
import com.wagon.red_wagon.ui.fragments.ConguratulationFrag;
import com.wagon.red_wagon.ui.fragments.CreateFamilyActivity;
import com.wagon.red_wagon.ui.fragments.Dashboard;
import com.wagon.red_wagon.ui.fragments.DriverZoneFrag;
import com.wagon.red_wagon.ui.fragments.FamilyScheduleList;
import com.wagon.red_wagon.ui.fragments.Invite_Screen;
import com.wagon.red_wagon.ui.fragments.Notification;
import com.wagon.red_wagon.ui.fragments.ProfileMangmentFrag;
import com.wagon.red_wagon.ui.fragments.RequestRide;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.LocationUpdaterService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;


public class FamilyDashboardActivity extends BaseActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "FamilyDashboardActivity";
    String user_name = "", check = "", child = "0", type = "12";
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    public static ImageView sidebar;
    CircleImageView imageset;
    DrawerLayout mDrawerLayout;
    View layout;
    public static TextView title;
    public static ImageView allfmshow;
    FrameLayout frameLayout;
    boolean isSideMain = true;
    BottomNavigationView navigation;
    TextView username;
    NotificationResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_);
        sidebar = findViewById(R.id.sidebar);
        layout = findViewById(R.id.bar);
        frameLayout = findViewById(R.id.fram_dash);
        title = findViewById(R.id.text_create);
        allfmshow = findViewById(R.id.allfmshow);
        Intent intent = getIntent();
        mDrawerLayout = findViewById(R.id.mDrawerLayout);
        Intent serviceIntent = new Intent(getApplicationContext(), LocationUpdaterService.class);
        startService(serviceIntent);
        validateAppCode();
        createDynamicLink();
        Intent intent1 = getIntent();
        String action = intent1.getAction();
        String type = intent1.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain" .equals(type)) {
                String sharedText = intent1.getStringExtra(Intent.EXTRA_TEXT);
                Log.e("Inerntget", sharedText);
            }
        }
        sidebar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {

                if (isSideMain) {
                    mDrawerLayout.openDrawer(Gravity.START);
                } else {
                    onBackPressed();
                }

            }
        });
        isVisible(true);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);
        navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        NavigationView navView = (NavigationView) findViewById(R.id.navigation_view);
        View header = navView.getHeaderView(0);
        imageset = (CircleImageView) header.findViewById(R.id.imagesetuser);
        username = (TextView) header.findViewById(R.id.username);
        response = (NotificationResponse) intent.getSerializableExtra("data");

        if (Constans.getUserName(FamilyDashboardActivity.this) != null) {
            user_name = Constans.getUserName(FamilyDashboardActivity.this);
            username.setText(user_name);
        }
        if (Constans.getProfilePic(FamilyDashboardActivity.this) != null) {
            Glide.with(FamilyDashboardActivity.this).load(Constans.BASEURL + Constans.getProfilePic(FamilyDashboardActivity.this)).placeholder(R.drawable.logo).into(imageset);
        }
        try {

            check = intent.getStringExtra("check");
            type = intent.getStringExtra("type");
            Log.e(TAG, "onCreate: " + check);
            child = intent.getStringExtra("child");


        } catch (NullPointerException e) {
            Log.e(TAG, "NullPointerException: " + e);
        }

        if (!type.equals("1")) {
            Menu nav_Menu = navView.getMenu();
            nav_Menu.findItem(R.id.schoolgo).setVisible(false);
        }


        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @SuppressLint("WrongConstant")
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                // create a Fragment Object
                int itemId = menuItem.getItemId();

                if (itemId == R.id.logout) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FamilyDashboardActivity.this);
                    builder.setMessage("Are you sure you want to logout?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mDrawerLayout.closeDrawer(Gravity.START);
                                    logoutApi();

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();


                } else if (itemId == R.id.add_cc) {

                    ChildProfileFrag childProfile_frag = new ChildProfileFrag();
                    Bundle args = new Bundle();
                    args.putString("childd", "1");
                    childProfile_frag.setArguments(args);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, childProfile_frag, "Child Profile");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else if (itemId == R.id.add_drt) {

                    AboutCar about_car = new AboutCar();
                    Bundle args = new Bundle();
                    args.putString("about_car", "1");
                    about_car.setArguments(args);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, about_car, "About Car");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else if (itemId == R.id.my_stuff) {
                    navigation.getMenu().getItem(3).setChecked(true);

                    ProfileMangmentFrag profile_mangment = new ProfileMangmentFrag();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fram_dash, profile_mangment, "My Stuff");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else if (itemId == R.id.refer_wagon) {

                    Invite_Screen about_car = new Invite_Screen();
                    Bundle args = new Bundle();
                    args.putString("school_dashboard", "2");
                    about_car.setArguments(args);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, about_car, "Invite");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else if (itemId == R.id.schoolgo) {
                    Intent intent1 = new Intent(FamilyDashboardActivity.this, SchoolDashboard.class);
                    intent1.putExtra("Coach", "2");
                    intent1.putExtra("type", "1");
                    intent1.putExtra("text", "Coach");
                    startActivity(intent1);
                    finish();
                    mDrawerLayout.closeDrawer(Gravity.START);
                }
                return false;
            }
        });
        try {
            if (check.equals("2")) {
                isVisible(false);
                fragmentManager = getSupportFragmentManager();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new ConguratulationFrag(), "Congratulations");
                transaction.commit();

            } else if (check.equals("3")) {
                AboutCar about_car = new AboutCar();
                Bundle bundle = new Bundle();
                bundle.putString("value", "2");
                about_car.setArguments(bundle);
                fragmentManager = getSupportFragmentManager();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, about_car, "About Car");
                transaction.commit();
            } else {

                Dashboard dashboardFrag = new Dashboard();
                fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, dashboardFrag, "Home");
                if (response != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", response);
                    dashboardFrag.setArguments(bundle);
                }
                transaction.commit();


            }
        } catch (NullPointerException e) {

        }
    }

    public void createDynamicLink() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(FamilyDashboardActivity.this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.e(TAG, "getDynamicLink:" + deepLink);
                        }
                        if (deepLink != null) {
                            if (deepLink.getQueryParameter("userid") != null) {
                                String userid = deepLink.getQueryParameter("userid");

                                try {
                                    Uri uri = pendingDynamicLinkData.getLink();
                                    String permLink = uri.toString().split("\\?")[0];
                                    Log.e(TAG, "onSuccess: " + uri.getQueryParameter("userid"));
                                    EventcarpoolFrag eventcarpoolFrag = new EventcarpoolFrag();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("getId", uri.getQueryParameter("userid"));
                                    eventcarpoolFrag.setArguments(bundle);
                                    FragmentManager fragmentManager;
                                    fragmentManager = getSupportFragmentManager();
                                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                                    transaction.replace(R.id.fram_dash, eventcarpoolFrag, "Carpools");
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                } catch (NullPointerException e) {
                                    Toast.makeText(FamilyDashboardActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Log.d(TAG, "getDynamicLink: no link found");
//                            launchPlayStoreWithAppPackage();
                        }
                        // [END_EXCLUDE]
                    }
                })
                .addOnFailureListener(FamilyDashboardActivity.this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.action_home:
                allfmshow.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);
                isVisible(true);
                FragmentManager fm = getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new Dashboard(), "Home");
                transaction.commit();
                return true;

            case R.id.carpools:
                if (!(getCurrentFrag() instanceof CarpoolListFrag)) {
                    allfmshow.setVisibility(View.GONE);
                    CarpoolListFrag eventListFragment = new CarpoolListFrag();
                    fragmentManager = getSupportFragmentManager();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, eventListFragment, "Carpools");
                    transaction.commit();
                }
                return true;

            case R.id.notification:
                if (!(getCurrentFrag() instanceof Notification)) {
                    isVisible(true);
                    allfmshow.setVisibility(View.GONE);
                    layout.setVisibility(View.VISIBLE);
                    fragmentManager = getSupportFragmentManager();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, new Notification(), "Notifications");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                return true;


            case R.id.Profile:
                if (!(getCurrentFrag() instanceof ProfileMangmentFrag)) {
                    isVisible(true);
                    allfmshow.setVisibility(View.GONE);
                    layout.setVisibility(View.VISIBLE);
                    fragmentManager = getSupportFragmentManager();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, new ProfileMangmentFrag(), "My Stuff");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                return true;
        }

        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fram_dash);
        title.setText(fragment.getTag());
        if (fragment instanceof Dashboard) {
            isVisible(true);
            navigation.getMenu().getItem(0).setChecked(true);
        }

        if (fragment instanceof ChatsFragment) {
            allfmshow.setVisibility(View.VISIBLE);
        }
        if (fragment instanceof FamilyScheduleList) {
            allfmshow.setVisibility(View.VISIBLE);
        }
        if (fragment instanceof Invite_Screen) {
            isVisible(true);
        }

        if (fragment instanceof Notification) {
            navigation.getMenu().getItem(2).setChecked(true);
        }

        if (fragment instanceof CarpoolListFrag) {
            navigation.getMenu().getItem(1).setChecked(true);
        }

        if (fragment instanceof ProfileMangmentFrag) {
            navigation.getMenu().getItem(3).setChecked(true);
        }
        allfmshow.setVisibility(View.GONE);
    }

    public Fragment getCurrentFrag() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fram_dash);
        return fragment;
    }

    public void isVisible(boolean isMain) {
        isSideMain = isMain;
        if (!isMain) {
            sidebar.setBackground(getResources().getDrawable(R.drawable.bck_arrow));

        } else {
            sidebar.setBackground(getResources().getDrawable(R.drawable.menu_side));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fram_dash);
        if (fragment instanceof CreateFamilyActivity) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        if (fragment instanceof ChildProfileFrag) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (fragment instanceof RequestRide) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (fragment instanceof Dashboard) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void logoutApi() {
        final Dialog dialog = AppUtils.showProgress(FamilyDashboardActivity.this);
        APIService mAPIService = AppUtils.getAPIService(FamilyDashboardActivity.this);
        Call<AddEvent> getApi;
        getApi = mAPIService.logout(Constans.getFamilyId(FamilyDashboardActivity.this));
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    Constans.clearSp(FamilyDashboardActivity.this);
                    Intent intent2 = new Intent(FamilyDashboardActivity.this, LogInActivity.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent2);
                    finish();
                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(FamilyDashboardActivity.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(FamilyDashboardActivity.this);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(FamilyDashboardActivity.this, "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void validateAppCode() {
        String uriPrefix = getString(R.string.dynamic_links_uri_prefix);
        if (uriPrefix.contains("YOUR_APP")) {
            new AlertDialog.Builder(this)
                    .setTitle("Invalid Configuration")
                    .setMessage("Please set your Dynamic Links domain in app/build.gradle")
                    .setPositiveButton(android.R.string.ok, null)
                    .create().show();
        }
    }

    /*public static void launchPlayStoreWithAppPackage(Context context, String packageName) {
        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
        context.startActivity(i);
    }*/
}

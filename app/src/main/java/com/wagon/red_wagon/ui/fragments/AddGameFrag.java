package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.adapter.FlagsAdapter;
import com.wagon.red_wagon.adapter.SchoolAdapter;
import com.wagon.red_wagon.adapter.TeamB_Adapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.FlagDetail;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.model.TeamBModel;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class AddGameFrag extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, FlagsAdapter.AdapterCallback {
    private static final int GALLERY_REQUEST_CODE = 111;
    EditText event_name_game, event_time_zone_game, event_country_game, event_zip_game, event_date_time_game, event_duration_game, opponent_game, home_away_game, arrive_game;
    String event_name_game_get, flag_game_get = "Default", event_location_game_get, event_time_zone_game_get, event_country_game_get, event_zip_game_get, event_date_time_game_get, event_duration_game_get, opponent_game_get, home_away_game_get, arrive_game_get;
    Button save_game;
    ArrayList<TeamBModel> teamBData = new ArrayList();
    LinearLayout upload_logo;
    CircleImageView logo_img;
    TextView text_title, flag_game;
    ImageView gallback, flag_image, team_B;
    RecyclerView coach_rv;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String fromType, teamId = "0", oppId = "0", TeamName = "", TeamId = "", dayOfWeek = "";
    private String image_path = "";
    Dialog flagdialog;
    SchoolAdapter.AdapterCallback adapterCallback;
    TeamB_Adapter.AdapterCallback Callbackk;
    Dialog coachdialog;
    TextView not_found_txt;
    boolean teamA = false;
    AutoCompleteTextView event_location_game;
    AutoCompleteAdapter mAdapter;
    PlacesClient placesClient;
    ArrayList<String> weedaysArr = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_game, container, false);
        Bundle args = getArguments();

        if (!args.isEmpty()) {
            fromType = args.getString("fromType", "");
        }

        findViewID(view);
        setUpAutoCompleteTextView();
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Add Game");
        gallback = view.findViewById(R.id.gallback);
        flag_image = view.findViewById(R.id.flag_image);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        final CountryAdapter.CountryCallback countryCallback = new CountryAdapter.CountryCallback() {
            @Override
            public void onSelectCountryCallback(String name) {
                AppUtils.dismissDialog();
                event_country_game.setText(name);
            }

            @Override
            public void onSelectTimeZoneCallback(String name) {
                AppUtils.dismissDialog();
                event_time_zone_game.setText(name);
            }
        };
        event_country_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppUtils.countryDialog(getContext(), countryCallback);


            }
        });


        event_time_zone_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppUtils.timeZoneDialog(getContext(), countryCallback);

            }
        });

        event_location_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });
        save_game.setOnClickListener(this);
        event_date_time_game.setOnClickListener(this);
        event_duration_game.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        flag_game.setOnClickListener(this);
        event_name_game.setOnClickListener(this);
        team_B.setOnClickListener(this);
        event_time_zone_game.setText(AppUtils.getCurrentTimeZone());
        //populateAndUpdateTimeZone();

        adapterCallback = new SchoolAdapter.AdapterCallback() {
            @Override
            public void onMethodCallback(String name, String id, int position) {
                coachdialog.dismiss();
                if (teamA) {
                    event_name_game.setText(name);
                    teamId = id;
                    Log.e("postion", "onMethodCallback: " + position);
                    try {
                        teamBData.remove(position);
                    } catch (Exception e) {

                    }


                }

                Log.e("onMethodCallback", "onMethodCallback: " + id + "\n" + name);
            }
        };

        Callbackk = new TeamB_Adapter.AdapterCallback() {
            @Override
            public void onMethodCallback(String name, String id) {
                opponent_game.setText(name);
                coachdialog.dismiss();
            }
        };
        return view;
    }


    private void findViewID(View view) {
        try {
            logo_img = view.findViewById(R.id.logo_img);
            team_B = view.findViewById(R.id.team_B);
            upload_logo = view.findViewById(R.id.upload_logo);
            event_name_game = view.findViewById(R.id.event_name_game);
            event_location_game = view.findViewById(R.id.event_location_game);
            flag_game = view.findViewById(R.id.flag_game);
            event_time_zone_game = view.findViewById(R.id.event_time_zone_game);
            event_zip_game = view.findViewById(R.id.event_zip_game);
            event_date_time_game = view.findViewById(R.id.event_date_time_game);
            event_duration_game = view.findViewById(R.id.event_duration_game);
            event_country_game = view.findViewById(R.id.event_country_game);
            opponent_game = view.findViewById(R.id.opponent_game);
            home_away_game = view.findViewById(R.id.home_away_game);
            arrive_game = view.findViewById(R.id.arrive_game);
            save_game = view.findViewById(R.id.save_game);
            //timezone_sp = view.findViewById(R.id.event_time_zone_game);
            CountryCodePicker team_country_ccp = view.findViewById(R.id.team_country);


        } catch (NullPointerException r) {

        }

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        event_time_zone_game_get = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());

    }


    private void getPlaceInfo(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
       /* if (addresses.get(0).getAdminArea() != null) {
            String addressLine = addresses.get(0).getAdminArea();
            Log.e("ADDRESS ", addressLine);
            adress_school.setText(addressLine);
        }*/

        if (addresses.get(0).getPostalCode() != null) {
            String ZIP = addresses.get(0).getPostalCode();
            event_zip_game.setText(ZIP);
            Log.e("ZIP CODE", ZIP);
        }

        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();

            Log.e("CITY", city);
        }

        if (addresses.get(0).getAdminArea() != null) {
            String state = addresses.get(0).getAdminArea();
            Log.e("STATE", state);
        }

        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            event_country_game.setText(country);
            Log.e("COUNTRY", country);
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.save_game) {
            event_name_game_get = event_name_game.getText().toString().trim();
            event_location_game_get = event_location_game.getText().toString().trim();
            event_time_zone_game_get = event_time_zone_game.getText().toString().trim();
            event_country_game_get = event_country_game.getText().toString().trim();
            event_zip_game_get = event_zip_game.getText().toString().trim();
            event_date_time_game_get = event_date_time_game.getText().toString().trim();

            event_duration_game_get = event_duration_game.getText().toString().trim();
            opponent_game_get = opponent_game.getText().toString().trim();
            home_away_game_get = home_away_game.getText().toString().trim();
            arrive_game_get = arrive_game.getText().toString().trim();

            if (!event_name_game_get.equals("")) {
                if (!opponent_game_get.equals("")) {
                    if (!event_time_zone_game_get.equals("")) {
                        if (!event_country_game_get.equals("")) {
                            if (!event_zip_game_get.equals("")) {
                                if (!event_date_time_game_get.equals("")) {
                                    if (!event_duration_game_get.equals("")) {

                                        if (!event_location_game_get.equals("")) {
                                            if (!home_away_game_get.equals("")) {
                                                if (!arrive_game_get.equals("")) {
                                                    API_AddGame();
                                                } else {
                                                    Toast.makeText(getActivity(), "Please Select arrive", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(getActivity(), "Please Select home away", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(getActivity(), "Please Select Location", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), "Please Select Duration", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Please Select Date and Time", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Please Enter Zip Code", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Please Select Country", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please Select Time Zone", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please Enter Team B", Toast.LENGTH_SHORT).show();

                }
            } else {
                Toast.makeText(getActivity(), "Please Enter Team A", Toast.LENGTH_SHORT).show();
            }

        }

        if (v.getId() == R.id.event_date_time_game) {
            datePicker();
        }
        if (v.getId() == R.id.event_duration_game) {
            opendurationDialog();
        }
        if (v.getId() == R.id.flag_game) {
            getFlagList();
        }

        if (v.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

        }

        if (v.getId() == R.id.event_name_game) {
            teamA = true;
            getCoachDialog();

        }

        if (v.getId() == R.id.team_B) {
            teamA = false;
            coachdialog = new Dialog(getActivity());
            coachdialog.setContentView(R.layout.coach_dialog);
            coachdialog.setTitle("Game");
            coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
            not_found_txt = (TextView) coachdialog.findViewById(R.id.not_found_txt);
            not_found_txt.setText("Team not Found. Please create Team in Team Management Section.");

            getCoachDialog2(Callbackk);

            coachdialog.show();
        }

    }

    private void getCoachDialog2(TeamB_Adapter.AdapterCallback adapterCallback) {
        Log.e("teamBData", "getCoachDialog2: " + teamBData);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);
        TeamB_Adapter customAdapter = new TeamB_Adapter(getContext(), teamBData, adapterCallback);
        coach_rv.setAdapter(customAdapter);
    }

    private void getCoachDialog() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Game");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        not_found_txt = (TextView) coachdialog.findViewById(R.id.not_found_txt);
        not_found_txt.setText("Team not Found. Please create Team in Team Management Section.");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);

        getList(adapterCallback, dialog);


        coachdialog.show();
    }

    private void getList(SchoolAdapter.AdapterCallback adapterCallback, Dialog dialog) {

        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getTeamList(Constans.getFamilyId(getActivity()));


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                    } else {
                        not_found_txt.setVisibility(View.GONE);
                        coach_rv.setVisibility(View.VISIBLE);

                        teamBData.clear();
                        for (int i = 0; i < responseBody.size(); i++) {
                            TeamName = responseBody.get(i).getTeamName();
                            TeamId = responseBody.get(i).getTeamId();
                            Log.e("TeamName", "onResponse: " + TeamName);

                            TeamBModel teamBModel = new TeamBModel(TeamName, TeamId);
                            teamBData.add(teamBModel);
                        }


                        SchoolAdapter customAdapter = new SchoolAdapter(getContext(), responseBody, adapterCallback, "Game");
                        coach_rv.setAdapter(customAdapter);


                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                not_found_txt.setVisibility(View.VISIBLE);
                coach_rv.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void API_AddGame() {
        APIService mAPIService = AppUtils.getAPIService(getContext());
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String joined = AppUtils.convertToString(weedaysArr);
        MultipartBody.Part part = null;
//        File file = new File(image_path);
        File file = null;
        if (!image_path.equals("")) {
            try {
                file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            file = new File(image_path);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }
        RequestBody id1;
        if (Constans.getLoginType(getActivity()).equals("Manager")) {

            id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.getSchlID(getContext()));
        } else {


            id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.getFamilyId(getContext()));
        }

        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), event_name_game_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), event_location_game_get);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), event_time_zone_game_get);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), event_country_game_get);
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), event_zip_game_get);
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), event_duration_game_get);
        RequestBody id8 = RequestBody.create(MediaType.parse("text/plain"), event_date_time_game_get);
        RequestBody id9 = RequestBody.create(MediaType.parse("text/plain"), "2012-12-12");
        RequestBody id10 = RequestBody.create(MediaType.parse("text/plain"), "2012-12-12");
        RequestBody id11 = RequestBody.create(MediaType.parse("text/plain"), joined);
        RequestBody id12 = RequestBody.create(MediaType.parse("text/plain"), arrive_game_get);
        RequestBody id13 = RequestBody.create(MediaType.parse("text/plain"), flag_game_get);
        RequestBody id14 = RequestBody.create(MediaType.parse("text/plain"), opponent_game_get);
        RequestBody id15 = RequestBody.create(MediaType.parse("text/plain"), home_away_game_get);
        RequestBody id16 = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody id17 = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody id19 = RequestBody.create(MediaType.parse("text/plain"), "Games");
        RequestBody id18 = RequestBody.create(MediaType.parse("text/plain"), "s");
        RequestBody id20 = RequestBody.create(MediaType.parse("text/plain"), teamId);
        RequestBody id21 = RequestBody.create(MediaType.parse("text/plain"), event_name_game_get);
        RequestBody id22 = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody id23 = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody id24 = RequestBody.create(MediaType.parse("text/plain"), oppId);
        RequestBody id25 = RequestBody.create(MediaType.parse("text/plain"), "");
        Map<String, RequestBody> map = new HashMap<>();
        map.put("userId", id1);
        map.put("name", id2);
        map.put("address", id3);
        map.put("timeZone", id4);
        map.put("country", id5);
        map.put("Zipcode", id6);
        map.put("duration", id7);
        map.put("dateTime", id8);
        map.put("startDate", id9);
        map.put("endDate", id10);
        map.put("weekDay", id11);
        map.put("arriveEarly", id12);
        map.put("flagColor", id13);
        map.put("opponent", id14);
        map.put("homeAway", id15);
        map.put("cost", id16);
        map.put("classId", id17);
        map.put("type", id19);
        map.put("court", id18);
        map.put("teamId", id20);
        map.put("teamName", id21);
        map.put("coachId", id22);
        map.put("coachName", id23);
        map.put("opponentId", id24);
        map.put("dateRange", id25);
        mAPIService.addSchoolEvent(map, part).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                if (response.code() == 200) {
                    dialog.dismiss();
                    //   getActivity().getSupportFragmentManager().popBackStackImmediate(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getActivity().onBackPressed();
                } else {
                    if (response.code() == 400) {
                        dialog.dismiss();
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Connection Error! Please Try Again", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void getFlagList() {
        List<FlagDetail> flagsArr = new ArrayList<>();

        flagsArr.add(new FlagDetail("Default", R.drawable.red_flag));
        flagsArr.add(new FlagDetail("Lemon", R.drawable.yellow_flag));
        flagsArr.add(new FlagDetail("Blue", R.drawable.blue_flag));
        flagsArr.add(new FlagDetail("Lime", R.drawable.green_flag));
        flagsArr.add(new FlagDetail("Grape", R.drawable.purple_flag));
        flagsArr.add(new FlagDetail("Blackberry", R.drawable.gray_flag));
        flagsArr.add(new FlagDetail("Orange", R.drawable.orange_flag));

        flagdialog = new Dialog(getActivity());
        flagdialog.setContentView(R.layout.coach_dialog);
        flagdialog.setTitle("Select Flag");
        RecyclerView flag_rv = (RecyclerView) flagdialog.findViewById(R.id.coach_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        flag_rv.setLayoutManager(linearLayoutManager);
        FlagsAdapter customAdapter = new FlagsAdapter(getContext(), flagsArr, this);
        flag_rv.setAdapter(customAdapter);

        flagdialog.show();
    }

    private void opendurationDialog() {

        // Create custom dialog object
        final Dialog dialog = new Dialog(getContext());
        // Include dialog.xml file
        dialog.setContentView(R.layout.durationdialog);
        // Set dialog title
        dialog.setTitle("Select " +
                " Duration");

        // set values for custom dialog components - text, image and button
        TimePicker simpleTimePicker = (TimePicker) dialog.findViewById(R.id.timepicker);
        simpleTimePicker.setIs24HourView(true);
        simpleTimePicker.setCurrentHour(new Integer(0));
        simpleTimePicker.setCurrentMinute(new Integer(0));

        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                event_duration_game.setText(selectedTime);
            }
        });

        dialog.show();

        dialog.findViewById(R.id.set_duration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (event_duration_game.getText().toString().equals("")) {

                    Toast.makeText(getContext(), "Please set Duration", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                }
            }
        });

    }

    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        int day = calendar.get(Calendar.DAY_OF_WEEK);
                        dayOfWeek = getDayofWeek(day);
                        weedaysArr.clear();
                        weedaysArr = convertString(dayOfWeek);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String strDate = format.format(calendar.getTime());
                        String selectedDate = strDate;

                        timePicker(selectedDate);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private ArrayList<String> convertString(String weeks) {

        String[] elements = weeks.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);
        return listOfString;
    }

    private void timePicker(final String selectedDate) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                        event_date_time_game.setText(selectedDate + "  " + selectedTime);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {

            Uri uri = data.getData();
            image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //  Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(image_path);
            logo_img.setImageBitmap(bitmap);


        }
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                event_location_game.setText(place.getName() + ", " + place.getAddressComponents().asList().get(1).getName());
                try {
                    getPlaceInfo(place.getLatLng().latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }


    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    @Override
    public void onMethodCallback(String name, int img) {
        flagdialog.dismiss();
        flag_game_get = name;
        flag_game.setText(flag_game_get);
        flag_image.setImageResource(img);
    }

    public String getDayofWeek(int day) {
        switch (day) {
            case Calendar.MONDAY:
                dayOfWeek = "MON";
                break;

            case Calendar.TUESDAY:
                dayOfWeek = "TUE";
                break;

            case Calendar.WEDNESDAY:
                dayOfWeek = "WED";
                break;

            case Calendar.THURSDAY:
                dayOfWeek = "THU";
                break;

            case Calendar.FRIDAY:
                dayOfWeek = "FRI";
                break;

            case Calendar.SATURDAY:
                dayOfWeek = "SAT";
                break;

            case Calendar.SUNDAY:
                dayOfWeek = "SUN";
                break;
        }
        return dayOfWeek;
    }

}

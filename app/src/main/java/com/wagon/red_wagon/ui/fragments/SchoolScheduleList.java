package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.EventAdapterShow;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.CalendarActivity;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class SchoolScheduleList extends Fragment {

    RecyclerView eventRecyclerView;
    ArrayList<Event_List_model> arrayList;
    String Filter = "";
    ImageView gallback, schImg;
    CardView search_et;
    TextView no_result_tv, text_title, filter;
    ImageView showcal;
    String data = "", value = "";
    RelativeLayout linear_main_bar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.driver_list, container, false);
        arrayList = new ArrayList<>();
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        filter = view.findViewById(R.id.filter);
        filter.setVisibility(View.VISIBLE);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            data = bundle.getString("Type", "");
        }
//        data= getArguments().getString("Type","");
        if (data.equals("1")) {
            value = "1";
            text_title = view.findViewById(R.id.text_title);
            text_title.setText("Schedules");
            gallback = view.findViewById(R.id.gallback);
            schImg = view.findViewById(R.id.add_user_msg);
            schImg.setImageResource(R.drawable.white_cal);
            schImg.setVisibility(View.VISIBLE);
            gallback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });

            schImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(getActivity(), CalendarActivity.class).putExtra("from", "School"));
                }
            });

        } else {
            value = "2";
            linear_main_bar.setVisibility(View.GONE);
            FamilyDashboardActivity.title.setText("Schedules");
            FamilyDashboardActivity.allfmshow.setVisibility(View.VISIBLE);
            FamilyDashboardActivity.allfmshow.setImageResource(R.drawable.white_cal);
            ((FamilyDashboardActivity) getActivity()).isVisible(false);
            FamilyDashboardActivity.allfmshow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(getActivity(), CalendarActivity.class).putExtra("from", "School"));
                }
            });


        }
        getList("");
        eventRecyclerView = view.findViewById(R.id.driver_list_rv);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        showcal = view.findViewById(R.id.showcal);
        showcal.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        eventRecyclerView.setLayoutManager(linearLayoutManager);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), v);
                popup.getMenuInflater().inflate(R.menu.pop_up, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //  Toast.makeText(getContext(), "Some Text" + item.getTitle(), Toast.LENGTH_SHORT).show();
                        Filter = item.getTitle().toString();
                        getList(Filter);
                        return true;
                    }
                });
                popup.show();
            }
        });

        return view;
    }

    private void getList(String from) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        if (from.equals("Teams")) {
            getApi = mAPIService.getEventDetail(Constans.getFamilyId(getActivity()), "");
        } else {
            getApi = mAPIService.getEventDetail(Constans.getFamilyId(getActivity()), from);
        }

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        eventRecyclerView.setVisibility(View.GONE);
                    } else {
                        no_result_tv.setVisibility(View.GONE);
                        eventRecyclerView.setVisibility(View.VISIBLE);
                        EventAdapterShow eventAdapter = new EventAdapterShow(getActivity(), responseBody, from, value);
                        eventRecyclerView.setAdapter(eventAdapter);
                    }

                } else if (response.code() == 400) {
                    dialog.dismiss();
                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                no_result_tv.setVisibility(View.VISIBLE);
                eventRecyclerView.setVisibility(View.GONE);
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }
}

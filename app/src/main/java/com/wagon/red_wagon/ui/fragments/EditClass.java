package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CoachRowAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.ui.activities.SchoolDashboard.text_create;
import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class EditClass extends Fragment implements View.OnClickListener, CoachRowAdapter.AdapterCallback, AdapterView.OnItemSelectedListener {
    private  EditText email_user_class, mobile_class, exit_classse, select_class, name_user_class, duration_class, date_time, zip_code, end_date, time_zone,
            address_activity, cost_activity, country_name;
    private String email_user_class_get, mobile_class_get, exit_classse_get, select_class_get, end_date_get, duration_get, name_user_class_get;
    private Button add_class;
    private LinearLayout parent_lay;
    private CountryCodePicker country_ccp;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private  LinearLayout upload_logo;
    private CircleImageView logo_img;
    private String image_path = "";
    private  EditText courd_field;
    private TextView add_coach_txt, teac_coach, text_title;
    private int GALLERY_REQUEST_CODE = 222;
    private String dateTimeStr;
    private  String timeZoneStr;
    private String countryNameStr;
    private String zipCodeStr;
    private String addressStr;
    private String costStr, fromType = "";
    private  String classId = "";
    private  String managerId = "", managername = "";
    private  Dialog parentdialog, coachdialog;
    private  List<Body> bodyList;
    private  List<String> objects;
    private String imageStr;
    private RecyclerView coach_rv;
    ArrayAdapter idAdapter;
    ImageView gallback;
    RequestBody requestBody;
    MultipartBody.Part part;
    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (!bundle.isEmpty()) {
            classId = bundle.getString("classId", "");
            fromType = bundle.getString("fromType", "");
            Log.e("class", "onCreateView: " + fromType);
        }

        if (fromType.equals("show")) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.view_class, container, false);
            text_title = view.findViewById(R.id.text_title);
            text_title.setText("Show Class");


        } else {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.create_class, container, false);
            text_title = view.findViewById(R.id.text_title);
            text_title.setText("Edit Class");
            end_date = view.findViewById(R.id.end_date);
            duration_class = view.findViewById(R.id.duration_class);
            end_date.setOnClickListener(this);
            duration_class.setOnClickListener(this);
        }


        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        FindViewID(view);

        bodyList = new ArrayList<>();


        add_class.setOnClickListener(this);
        add_coach_txt.setOnClickListener(this);
        //teac_coach_sp.setOnClickListener(this);
        date_time.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        teac_coach.setOnClickListener(this);




        //teac_coach_sp.setOnItemSelectedListener( this );
        getCoachDetail(this);
        return view;
    }


    private static String displayTimeZone(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
            result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
        } else {
            result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
        }

        return result;

    }


    private void setSpinner() {
        //  final List<String> plantsList = new ArrayList<>( Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.simple_spinner_dropdown, objects) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };


    }

    private void FindViewID(View view) {

        exit_classse = view.findViewById(R.id.exit_classse);
        select_class = view.findViewById(R.id.select_class);
        teac_coach = view.findViewById(R.id.teac_coach);
        courd_field = view.findViewById(R.id.courd_field);

        add_class = view.findViewById(R.id.add_class);

        parent_lay = view.findViewById(R.id.parent_lay);
        country_ccp = view.findViewById(R.id.country_ccp);
        country_name = view.findViewById(R.id.country_name);
        logo_img = view.findViewById(R.id.logo_img);
        upload_logo = view.findViewById(R.id.upload_logo);
        date_time = view.findViewById(R.id.date_time);
        time_zone = view.findViewById(R.id.time_zone);
        zip_code = view.findViewById(R.id.zip_code);
        add_coach_txt = view.findViewById(R.id.add_coach_txt);
        add_class.setText("Update");
        address_activity = view.findViewById(R.id.address_activity);
        cost_activity = view.findViewById(R.id.cost_activity);


        final CountryAdapter.CountryCallback countryCallback = new CountryAdapter.CountryCallback() {
            @Override
            public void onSelectCountryCallback(String name) {
                AppUtils.dismissDialog();
                country_name.setText(name);
            }

            @Override
            public void onSelectTimeZoneCallback(String name) {
                AppUtils.dismissDialog();
                time_zone.setText(name);
            }
        };
        country_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fromType.equals("show")) {
                    AppUtils.countryDialog(getContext(), countryCallback);
                }
            }
        });

        time_zone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fromType.equals("show")) {
                    AppUtils.timeZoneDialog(getContext(), countryCallback);
                }
            }
        });
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_class) {

            exit_classse_get = exit_classse.getText().toString();
            //select_class_get = select_class.getText().toString();
            dateTimeStr = date_time.getText().toString();
            timeZoneStr = time_zone.getText().toString();
            countryNameStr = country_name.getText().toString();
            zipCodeStr = zip_code.getText().toString();
            addressStr = address_activity.getText().toString();
            costStr = cost_activity.getText().toString();
            duration_get = duration_class.getText().toString();
            end_date_get = end_date.getText().toString();

            if (!exit_classse_get.equals("")) {
                if (!duration_get.equals("")) {
                    if (!dateTimeStr.equals("")) {
                        if (!end_date_get.equals("")) {


                            if (!zipCodeStr.equals("")) {
                                if (!addressStr.equals("")) {

//                                    if (!image_path.equals("")) {
                                        if (!managerId.equals("")) {
                                            //getting the progressbar
                                            updateClassService();

                                        } else {
                                            Toast.makeText(getActivity(), "Please Select or Add Teacher", Toast.LENGTH_SHORT).show();
                                        }
                                }/* else {
                                        Toast.makeText(getActivity(), "Please Select Image", Toast.LENGTH_SHORT).show();
                                    }

                                } */ else {
                                    address_activity.setError("Please enter Address ");
                                }
                            } else {
                                zip_code.setError("Please enter Zip Code ");
                            }

                        } else {
                            end_date.setError("Please Select End Date");
                        }
                    } else {
                        date_time.setError("Please Select Date/Time");
                    }
                } else {
                    date_time.setError("Please Select Duration");
                }
            } else {
                Toast.makeText(getActivity(), "Please enter Class Name", Toast.LENGTH_SHORT).show();

            }
        }

        if (v.getId() == R.id.parent_lay) {
            parent_lay.setVisibility(View.VISIBLE);
        }

        if (v.getId() == R.id.date_time) {
            datePicker();
        }

        if (v.getId() == R.id.teac_coach) {
            if (!fromType.equals("show")) {
                getCoachDialog();
            }
        }

        if (v.getId() == R.id.duration_class) {
            if (!fromType.equals("show")) {
                opendurationDialog();
            }
        }


        if (v.getId() == R.id.add_coach_txt) {
            if (managerId.equals("")) {
                openParentDialog();
            } else {
                Toast.makeText(getActivity(), "Already added", Toast.LENGTH_SHORT).show();
            }

        }
        if (v.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

        }

    }

    private void opendurationDialog() {

        // Create custom dialog object
        final Dialog dialog = new Dialog(getContext());
        // Include dialog.xml file
        dialog.setContentView(R.layout.durationdialog);
        // Set dialog title
        dialog.setTitle("Select " +
                " Duration");

        // set values for custom dialog components - text, image and button
        TimePicker simpleTimePicker = (TimePicker) dialog.findViewById(R.id.timepicker);
        simpleTimePicker.setIs24HourView(true);
        simpleTimePicker.setCurrentHour(new Integer(0));
        simpleTimePicker.setCurrentMinute(new Integer(0));

        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                duration_class.setText(selectedTime);
            }
        });

        dialog.show();

        dialog.findViewById(R.id.set_duration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (duration_class.getText().toString().equals("")) {

                    Toast.makeText(getContext(), "Please set Duration", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                }
            }
        });

    }

    private void openParentDialog() {

        parentdialog = new Dialog(getActivity());
        parentdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        parentdialog.setCancelable(true);
        parentdialog.setContentView(R.layout.teacher_detail_dialog);

        name_user_class = parentdialog.findViewById(R.id.name_user_class);
        email_user_class = parentdialog.findViewById(R.id.email_user_class);
        mobile_class = parentdialog.findViewById(R.id.mobile_class);
        ImageView close_btn = (ImageView) parentdialog.findViewById(R.id.close_dialog);
        Button add_teacher_btn = (Button) parentdialog.findViewById(R.id.add_teacher_btn);

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentdialog.dismiss();
            }
        });
        add_teacher_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                name_user_class_get = name_user_class.getText().toString();
                email_user_class_get = email_user_class.getText().toString();
                mobile_class_get = mobile_class.getText().toString();

                if (!name_user_class_get.equals("")) {

                    if (!mobile_class_get.equals("")) {
                        //getting the progressbar
                        if (!email_user_class_get.equals("") && emailValidator(email_user_class_get)) {
                            select_class_get = select_class.getText().toString();

                            ApiService(name_user_class_get, email_user_class_get, mobile_class_get);
                        } else {
                            email_user_class.setError("Please enter Email Address");
                        }


                    } else {
                        mobile_class.setError("Please enter Mobile Number ");
                    }
                } else {
                    name_user_class.setError("Please enter Name");
                }


            }
        });

        parentdialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                logo_img.setImageBitmap(bitmap);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));

            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String strDate = format.format(calendar.getTime());
                        // String selectedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                        timePicker(strDate);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    private void timePicker(final String selectedDate) {


        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                        date_time.setText(selectedDate + "  " + selectedTime);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();

    }

    private void updateClassService() {
        final Dialog dialog = AppUtils.showProgress(getActivity());

        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> apiCallback;
        MultipartBody.Part part = null;
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(image_path);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }

        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), classId);
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), exit_classse_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), managerId);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), dateTimeStr);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), timeZoneStr);
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), "UK");
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), addressStr);
        RequestBody id8 = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody id9 = RequestBody.create(MediaType.parse("text/plain"), zipCodeStr);
        RequestBody id10 = RequestBody.create(MediaType.parse("text/plain"), courd_field.getText().toString());
        RequestBody id11 = RequestBody.create(MediaType.parse("text/plain"), duration_get);
        RequestBody id12 = RequestBody.create(MediaType.parse("text/plain"), end_date_get);

        Map<String, RequestBody> map = new HashMap<>();
        map.put("id", id1);
        map.put("classname", id2);
        map.put("managerId", id3);
        map.put("datetime", id4);
        map.put("timezone", id5);
        map.put("country", id6);
        map.put("address", id7);
        map.put("cost", id8);
        map.put("zip", id9);
        map.put("court", id10);
        map.put("duration", id11);
        map.put("enddate", id12);

        apiCallback = mAPIService.updateClassImage(map, part);
        apiCallback.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    EventListFragment createTeamFrag = new EventListFragment();
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.replace(R.id.crate_class_scl, createTeamFrag);

                    trans.commit();
                    //manager.popBackStack();


                    //   getActivity().getSupportFragmentManager().popBackStack();
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });
    }



    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    private void getCoachDialog() {
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Select Teacher/Coach");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);
        CoachRowAdapter customAdapter = new CoachRowAdapter(getContext(), bodyList, this, "");
        coach_rv.setAdapter(customAdapter);


        coachdialog.show();
    }

    private void getCoachDetail(final CoachRowAdapter.AdapterCallback callback) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());


        mAPIService.getCoachList(Constans.getFamilyId(getContext())).enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    bodyList = response.body().getBody();
                    if (bodyList.size() != 0) {

                        getList();


                    }


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


    private void ApiService(final String name, final String email, final String phnNum) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/managerSignUp";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {
//
                    JSONObject jsonObject = new JSONObject(response);
                    String masgg = jsonObject.getString("message");
                    JSONObject jsonObject1 = jsonObject.getJSONObject("body");
                    managerId = jsonObject1.getString("id");
                    teac_coach.setText(name);
                    parentdialog.dismiss();
                    Toast.makeText(getContext(), masgg, Toast.LENGTH_SHORT).show();

//                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
//                    editor.putString("userr", "Manager");
//                    editor.apply();
//                    Log.e("sdfdsfsdfsfs", "sdfsfs" + Constans.Token);
//                    fragmentManage = getActivity().getSupportFragmentManager();
//                    transaction = fragmentManage.beginTransaction();
//                    transaction.replace(R.id.add_manager_frame, new AllDone());
//                    transaction.addToBackStack(null);
//                    transaction.commit();

                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //    Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();

                Log.e("dsfdfsdf", "onErrorResponse: " + error.toString());
                try {
                    int code = error.networkResponse.statusCode;
                    if (code == 400) {

                    }
                } catch (NullPointerException e) {

                }

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        String masgg = obj.getString("message");
                        Log.e("objobjobj", "objobjobj" + masgg);
                        Toast.makeText(getActivity(), masgg, Toast.LENGTH_SHORT).show();

                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("firstName", name);
                params.put("email", email);
                params.put("phoneNumber", phnNum);
                params.put("schId", Constans.getFamilyId(getActivity()));
                params.put("gender", "Manager");
                Log.e("getParamsgetParams", "getParams: " + params);
                return params;

            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }

    @Override
    public void onResume() {
        super.onResume();
        text_create.setText("Add a Class");
    }

    @Override
    public void onMethodCallback(String name, String id) {

        coachdialog.dismiss();
        managerId = id;
        managername = name;
        teac_coach.setText(name);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        timeZoneStr = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi;

        getApi = mAPIService.getClassDetails(classId);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.body().getBody());
                dialog.dismiss();

                try {
                    if (response.code() == 200) {


                        name_user_class_get = response.body().getBody().getClassname();
                        managerId = response.body().getBody().getManagerId();
                        dateTimeStr = response.body().getBody().getdatetime();
                        timeZoneStr = response.body().getBody().getTimezone();
                        countryNameStr = response.body().getBody().getCountry();
                        addressStr = response.body().getBody().getAddress();
                        costStr = response.body().getBody().getCost();
                        zipCodeStr = response.body().getBody().getZip();
                        duration_get = response.body().getBody().getDuration();
                        end_date_get = response.body().getBody().getEndDate();
                        zipCodeStr = response.body().getBody().getZip();
                        imageStr = response.body().getBody().getProfileimage();
                        Log.e("daataaa", "onResponse: " + name_user_class_get + "\n" + imageStr);
                        exit_classse.setText(name_user_class_get);

                        country_name.setText(countryNameStr);
                        address_activity.setText(addressStr);
                        zip_code.setText(zipCodeStr);
                        end_date.setText(end_date_get);
                        duration_class.setText(duration_get);
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                        Date date = dateFormat.parse(dateTimeStr);
                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        String dateStr = formatter.format(date);
                        date_time.setText(dateStr);
                        courd_field.setText(response.body().getBody().getCourt());
                        time_zone.setText(timeZoneStr);

                        for (int i = 0; i < bodyList.size(); i++) {
                            if (bodyList.get(i).getId().toString().equals(managerId)) {
                                teac_coach.setText(bodyList.get(i).getFirstName());
                            }
                        }
                        Glide.with(getContext()).load(Constans.BASEURL + imageStr).into(logo_img);


                    } else if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            try {

                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");

                                Log.e("MessageM ", userMessage);
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        }

                    } else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


}


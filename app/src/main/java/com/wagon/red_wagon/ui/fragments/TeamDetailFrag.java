package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.TeamListAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class TeamDetailFrag extends Fragment {

    CardView add_team_lay, schedule_lay;
    RecyclerView teams_recyclerview;
    ArrayList<Event_List_model> arrayList;
    TextView team_count,sort_text,text_title;
    ImageView gallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.create_team, container, false);
        findView(view);
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Team Management");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                //getActivity().onBackPressed();
            }
        });
        return view;
    }

    private void findView(View view) {
        arrayList = new ArrayList<>();
        team_count = view.findViewById(R.id.team_count);
        add_team_lay = view.findViewById(R.id.add_team_lay);
        schedule_lay = view.findViewById(R.id.schedule_lay);
        sort_text = view.findViewById(R.id.sort_text);


        teams_recyclerview = view.findViewById(R.id.teams_recyclerview);
        getList("");
        add_team_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateTeamFrag createTeamFrag = new CreateTeamFrag();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_team, createTeamFrag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        sort_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getList("Sort");
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        teams_recyclerview.setLayoutManager(linearLayoutManager);


    }


    private void getList(String from) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        if(from.equals("Sort")){
            getApi = mAPIService.getSortTeamList(Constans.getFamilyId(getActivity()));
        }else {
            getApi = mAPIService.getTeamList(Constans.getFamilyId(getActivity()));
        }
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    if(responseBody.size() != 0) {
                        team_count.setText("Teams ("+responseBody.size()+")");
                    }
                    TeamListAdapter eventAdapter = new TeamListAdapter(getActivity(), responseBody,"2");
                    teams_recyclerview.setAdapter(eventAdapter);


                } else if (response.code() == 400) {
                    dialog.dismiss();
                    Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    Toast.makeText(getContext(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

}

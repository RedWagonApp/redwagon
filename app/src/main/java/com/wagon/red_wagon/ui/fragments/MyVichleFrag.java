package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.MyVichleAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Model_CarList;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class MyVichleFrag extends Fragment implements View.OnClickListener {
//    bank_details

    Button countinue_myvichle;
    RecyclerView recyler_vichle;
    TextView no_result_tv;
    ArrayList<Model_CarList> arrayList;
    String value = "";
    LinearLayout linear_main_bar;
    boolean isAvailable = false;
    MyVichleAdapter.VehicleCallback callback;
    MyVichleAdapter myVichle_adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_vichel_frag, container, false);
        linear_main_bar = view.findViewById(R.id.main_lay);
        countinue_myvichle = view.findViewById(R.id.countinue_myvichle);
        recyler_vichle = view.findViewById(R.id.recyler_vichel);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        FamilyDashboardActivity.title.setText("My Vehicles");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        callback = new MyVichleAdapter.VehicleCallback() {
            @Override
            public void makeAvailable(String id, String status, int pos) {
                changeAvailabilityStatus(id, status, pos);
            }
        };
        countinue_myvichle.setOnClickListener(this);
        if (getArguments().getString("From", "").equals("profile")) {
            countinue_myvichle.setVisibility(View.GONE);
        } else {
            countinue_myvichle.setVisibility(View.VISIBLE);
        }
        Bundle args = getArguments();
        value = args.getString("value", "");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyler_vichle.setLayoutManager(linearLayoutManager);
        Service("status");
        arrayList = new ArrayList<>();


        return view;
    }

    private void Service(String status) {
        if (arrayList != null && arrayList.size() != 0) {
            arrayList.clear();
        }
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/viewAllCar";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                try {

                    dialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("body");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        no_result_tv.setVisibility(View.GONE);
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String make = jsonObject1.getString("make");
                        String modal = jsonObject1.getString("modal");
                        String color = jsonObject1.getString("color");
                        String regNumber = jsonObject1.getString("regNumber");
                        String freeSeat = jsonObject1.getString("freeSeat");
                        String id = jsonObject1.getString("id");
                        String status = jsonObject1.getString("status");
                        Model_CarList model_carList = new Model_CarList(make, modal, color, regNumber, freeSeat, id, status);
                        arrayList.add(model_carList);
                    }
                    for (int i = 0; i < arrayList.size(); i++) {

                        if (arrayList.get(i).getStatus().equals("1")) {
                            isAvailable = true;
                        }
                    }
                    myVichle_adapter = new MyVichleAdapter(getActivity(), arrayList, callback);
                    recyler_vichle.setAdapter(myVichle_adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    no_result_tv.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception e) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.countinue_myvichle) {
            if (isAvailable) {
                DriverZoneFrag parent_zone_frag = new DriverZoneFrag();
                Bundle bundle = new Bundle();
                bundle.putString("value", value);
                parent_zone_frag.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, parent_zone_frag, "Preferred Pick Up Zones");
                transaction.addToBackStack(null);
                transaction.commit();
            } else {
                Toast.makeText(getContext(), "Make one vehicle available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void changeAvailabilityStatus(String id, String status, int pos) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi;
        getApi = mAPIService.carDetailsUpdateStatus(id, Constans.getFamilyId(getContext()), status);
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    Service("status");
                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

}


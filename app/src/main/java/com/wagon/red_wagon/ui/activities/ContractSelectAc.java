package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.Contract_SelectAdapter;

import java.util.ArrayList;

public class ContractSelectAc extends BaseActivity {
    ArrayList<String> userContract;
    ArrayList<String> userName;
    private final int REQUEST_CODE = 99;
    String phoneNo = "", name = "";
    RecyclerView recycler_contractl;
    Dialog dialog;
    TextView text_title;
    Button invite_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_select);
        dialog = new Dialog(this);
        text_title = findViewById(R.id.text_title);
        invite_btn = findViewById(R.id.invite_btn);
        ImageView gallback = findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recycler_contractl = findViewById(R.id.recycler_contract);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_contractl.setLayoutManager(linearLayoutManager);

        invite_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ContractSelectAc.this, "Players Invited Successfully!", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });
        getContacts();
    }

    public void getContacts() {
        dialog.show();
        userContract = new ArrayList<>();
        userName = new ArrayList<>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));


                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);

                    while (pCur.moveToNext()) {

                        phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        name = cur.getString(cur.getColumnIndex(
                                ContactsContract.Contacts.DISPLAY_NAME));
//                             email = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));


//                            Log.i("tag", "Phone Number: " + email);


                        userContract.add(phoneNo);
                        userName.add(name);


                        Log.i("tag", "Name: " + name);
                        Log.i("tag", "Phone Number: " + phoneNo);

                    }


                    Log.i("tag", "Name: " + name);
                    Log.i("tag", "Phone Number: " + phoneNo);


//                    list.add(sb);


                    pCur.close();

                }
            }
        }

        try {

        } catch (Exception e) {
            System.out.println(e);
        }
        if (cur != null) {
            cur.close();
        }

        ApiContact(userContract, userName);
    }

    private void ApiContact(ArrayList<String> userContract, ArrayList<String> userName) {
        dialog.dismiss();
        Contract_SelectAdapter contract_selectAdapter = new Contract_SelectAdapter(this, userContract, userName);
        recycler_contractl.setAdapter(contract_selectAdapter);
    }
}

package com.wagon.red_wagon.ui.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AttendancePlayerAdapter;
import com.wagon.red_wagon.model.AttendanceModel;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.PreviousDatesFrag;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class StudentAttendanceDetails extends Fragment implements AttendancePlayerAdapter.AttendanceCallBack, View.OnClickListener {


    private String teamId, eventId, teamName, dateRange, date;
    List<String> items = new ArrayList<>();
    RecyclerView players_recyclerview;
    AttendancePlayerAdapter.AttendanceCallBack callBack;
    TextView no_event;
    public static Button attandence;

    FragmentActivity context;
    ArrayList<String> playersId = new ArrayList();
    ArrayList<String> attendacestatus = new ArrayList();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.student_attandece_details, container, false);
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        TextView text_title = view.findViewById(R.id.text_title);
        ImageView add_user_msg = view.findViewById(R.id.add_user_msg);
        ImageView gallback = view.findViewById(R.id.gallback);
        no_event = view.findViewById(R.id.no_event);
        players_recyclerview = view.findViewById(R.id.players_recyclerview);
        attandence = view.findViewById(R.id.attandence);
        attandence.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        players_recyclerview.setLayoutManager(linearLayoutManager);
        context = getActivity();
        callBack = this;
        Bundle bundle = getArguments();
        if (bundle != null) {
            dateRange = bundle.getString("dateRange", "");
            teamId = bundle.getString("teamId", "");
            teamName = bundle.getString("teamName", "");
            eventId = bundle.getString("eventId", "");

            items = convertString(dateRange);
        }

        if (items.contains(date)) {
            no_event.setVisibility(View.GONE);
            players_recyclerview.setVisibility(View.VISIBLE);
            getList();
        } else {
            no_event.setVisibility(View.VISIBLE);
            players_recyclerview.setVisibility(View.GONE);
        }
        text_title.setText(date);

        add_user_msg.setVisibility(View.VISIBLE);
        add_user_msg.setImageResource(R.drawable.ic_view_week_black_24dp);
        add_user_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreviousDatesFrag eventListFragment = new PreviousDatesFrag();
                Bundle args = new Bundle();
                args.putString("fromType", "show");
                args.putString("dateRange", dateRange);
                args.putString("teamId", teamId);
                args.putString("teamName", teamName);
                args.putString("eventId", eventId);
                eventListFragment.setArguments(args);

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.school_dashbord_fram, eventListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        return view;
    }

    private ArrayList<String> convertString(String dateRange) {

        String[] elements = dateRange.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);

        return listOfString;
    }

    private void getList() {
        playersId.clear();
        attendacestatus.clear();
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getTeamPlayer(teamId);
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    if (responseBody.size() != 0) {
                        for (int i = 0; i < responseBody.size(); i++) {
                            playersId.add(responseBody.get(i).getId().toString());
                            attendacestatus.add(responseBody.get(i).getStatus());
                        }
                        AttendancePlayerAdapter playerAdapter = new AttendancePlayerAdapter(getActivity(), responseBody,playersId,attendacestatus, callBack);
                        players_recyclerview.setAdapter(playerAdapter);
                        //   setAttendance(playersId, attendacestatus);
                    }


                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onMethodCallback(ArrayList<String> ids, ArrayList<String> attstatus) {
       /* playersId = ids;
        attendacestatus = attstatus;*/
        if (ids.size() != 0 && attstatus.size() != 0) {
            setAttendance(ids, attstatus);
        }
    }

    @Override
    public void onClick(View v) {


    }

    private void setAttendance(ArrayList<String> ids, ArrayList<String> attstatus) {
        final Dialog dialog = AppUtils.showProgress((Activity) context);
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AttendanceModel> getApi;
        String attendanceStatus = AppUtils.convertToString(attstatus);
        String playerId = AppUtils.convertToString(ids);


        getApi = mAPIService.teamAttendance(teamId, playerId, eventId, attendanceStatus, date);
        getApi.enqueue(new Callback<AttendanceModel>() {
            @Override
            public void onResponse(@NotNull Call<AttendanceModel> call, @NotNull retrofit2.Response<AttendanceModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

           /*         assert response.body() != null;
                    List<AttendanceBody> responseBody = response.body().getBody();
                    Toast.makeText(getContext(), "Attendance Saved Successfully", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
*/
//                    getActivity().onBackPressed();
                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(@NotNull Call<AttendanceModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

}

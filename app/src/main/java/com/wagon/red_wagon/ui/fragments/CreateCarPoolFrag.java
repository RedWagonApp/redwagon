package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.DriverAllAdapter;
import com.wagon.red_wagon.adapter.RecommendedAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CreateCarPoolFrag extends Fragment implements View.OnClickListener, RecommendedAdapter.AddToPoolCallback {
    private RecyclerView driverList, RecommendedList;
    private Button savecarpools;
    private LinearLayoutManager linearLayoutManager;
    private TextView add_driver_txt;
    RecommendedAdapter.AddToPoolCallback callback;
    ArrayList<String> driverNameArr = new ArrayList<>();
    ArrayList<String> driverIdsArr = new ArrayList<>();
    ArrayList<String> genderArr = new ArrayList<>();
    ArrayList<String> driverImageArr = new ArrayList<>();
    ArrayList<String> driverPhone = new ArrayList<>();
    DriverAllAdapter driverAdapter;
    String eventId = "";
    EditText groupName, search_driver_et;
    List<Body> responseBody;
    RecommendedAdapter recommendedAdapter;
    Dialog dialog;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.create_carpool_lay, container, false);
        FindView(view);
        savecarpools.setOnClickListener(this);
        FamilyDashboardActivity.title.setText("Create a Pool");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        callback = (RecommendedAdapter.AddToPoolCallback) this;
        linearLayoutManager = new LinearLayoutManager(getActivity());
        RecommendedList.setLayoutManager(linearLayoutManager);
        RecommendedList.setNestedScrollingEnabled(true);
        driverList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        getDriverList();
        if (driverIdsArr.size() > 0) {
            driverIdsArr.clear();
            driverNameArr.clear();
            genderArr.clear();
            driverImageArr.clear();
            driverPhone.clear();
        } else {
            driverIdsArr.add(Constans.getFamilyId(getContext()));
            driverNameArr.add(Constans.getUserName(getContext()));
            genderArr.add(Constans.getLoginType(getContext()));
            driverImageArr.add(Constans.getProfilePic(getContext()));
            driverPhone.add(Constans.getphoneNumber(getContext()));

        }
        eventId = getArguments().getString("eventId", "");
        driverAdapter = new DriverAllAdapter(getActivity(), driverNameArr, driverImageArr);
        driverList.setAdapter(driverAdapter);
        search_driver_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input

                filter(editable.toString());
            }
        });
        return view;
    }

    private void FindView(View view) {
        driverList = view.findViewById(R.id.DriverList);
        add_driver_txt = view.findViewById(R.id.add_driver_txt);
        RecommendedList = view.findViewById(R.id.RecommendedList);
        savecarpools = view.findViewById(R.id.savecarpools);
        groupName = view.findViewById(R.id.pool_name);
        search_driver_et = view.findViewById(R.id.search_driver_et);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.savecarpools) {

            if (!groupName.getText().toString().equals("")) {
                if (driverIdsArr.size() != 0) {
                    addCarPool();
                } else {
                    Toast.makeText(getContext(), "Please Add Driver To Pool", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), "Please Enter Car Pool Group Name", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void getDriverList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getDriver(Constans.getlatitude(getActivity()),Constans.getlongitude(getActivity()));


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    responseBody = response.body().getBody();

                    if (responseBody.size() != 0) {

                        recommendedAdapter = new RecommendedAdapter(getActivity(), responseBody, callback);
                        RecommendedList.setAdapter(recommendedAdapter);

                    }

                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void filter(String text) {
        try {
            List<Body> filterdList = new ArrayList<>();


            for (Body s : responseBody) {

                if (s.getFirstName().toLowerCase().contains(text.toLowerCase())) {
                    filterdList.add(s);
                }
            }
            recommendedAdapter.filterList(filterdList);
        } catch (NullPointerException e) {

        }
    }

    private void addCarPool() {
        dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi;

        String driverIds = AppUtils.convertToString(driverIdsArr);
        String driverNames = AppUtils.convertToString(driverNameArr);
        String gender = AppUtils.convertToString(genderArr);
        String profImg = AppUtils.convertToString(driverImageArr);
        String driverPhn = AppUtils.convertToString(driverPhone);
        getApi = mAPIService.addCarpool(Constans.getFamilyId(getContext()), eventId, driverIds, driverNames, groupName.getText().toString().trim(), profImg, driverPhn, gender);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());

                if (response.code() == 200) {
//                    List<Body> responseBody = response.body().getBody();
                    Log.e("onResponse", "onResponse: "+response.body().getBody().getCarPoolId() );
//                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    getActivity().getSupportFragmentManager().popBackStack();
//                    getActivity().onBackPressed();
                    ApiGroupChat(response.body().getBody().getCarPoolId());

                } else if (response.code() == 400) {
                    dialog.dismiss();
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();
                call.cancel();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    @Override
    public void onAddDriver(List<Body> list, int pos, Button btn) {
        add_driver_txt.setVisibility(View.GONE);
        if (driverIdsArr.contains(list.get(pos).getId().toString())) {

            Toast.makeText(getContext(), "Already added", Toast.LENGTH_SHORT).show();
        } else if (driverIdsArr.size() > 3) {
            Toast.makeText(getContext(), "You cannot add more than 4 drivers", Toast.LENGTH_SHORT).show();

        } else {
            btn.setText("Added");
            driverIdsArr.add(list.get(pos).getId().toString());
            driverNameArr.add(list.get(pos).getFirstName());
            genderArr.add(list.get(pos).getGender());
            driverImageArr.add(list.get(pos).getProfileImage());
            driverPhone.add(list.get(pos).getPhoneNumber());
        }
        driverAdapter.notifyDataSetChanged();

    }
    private void ApiGroupChat(String carPoolId) {
       // final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<AddEvent> getApi;

        getApi = mAPIService.groupCarpoolChat(AppUtils.convertToString(driverIdsArr), groupName.getText().toString().trim(),carPoolId);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    getActivity().getSupportFragmentManager().popBackStack();
                    getActivity().onBackPressed();

                } else if (response.code() == 400) {
                    dialog.dismiss();
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }
}

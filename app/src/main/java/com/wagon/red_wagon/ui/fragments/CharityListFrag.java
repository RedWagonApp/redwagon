package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.MyChirtyListAdapter;
import com.wagon.red_wagon.model.ModelAllChirty;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharityListFrag extends Fragment {
    RecyclerView chirty_list;
    LinearLayout addheere;
    Button skip;

    List<ModelAllChirty> modelAllChirties;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.chirty_list_frg, container, false);
        FamilyDashboardActivity.title.setText("Perfer to Donate to a Charity?");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        chirty_list = view.findViewById(R.id.chirty_list);
        addheere = view.findViewById(R.id.addheere);
        skip = view.findViewById(R.id.skip);
        signUpService();

        modelAllChirties = new ArrayList<>();


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        chirty_list.setLayoutManager(linearLayoutManager);


        addheere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new AddChirtyFrag(), "Add your Charity");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new CongFamily(), "Congratulations");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    private void signUpService() {

        final Dialog dialog = AppUtils.showProgress(getActivity());

        String url = Constans.BASEURL + "api/viewAllCharity";
        Log.e("urllll", "Constans.BASEURL+" + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "response" + response);

                try {
                    dialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("body");
                    Log.e("jsonArray", "jsonArray" + jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String charityName = jsonObject1.getString("charityName");
                        String charityUrl = jsonObject1.getString("profileImage");
                        String charityId = jsonObject1.getString("id");

                        Log.e("charityName", "charityName" + charityUrl);

                        ModelAllChirty modelAllChirty = new ModelAllChirty(charityName, charityUrl,charityId);
                        modelAllChirties.add(modelAllChirty);

                        MyChirtyListAdapter myChirtyList_adapter = new MyChirtyListAdapter(getActivity(), modelAllChirties);
                        chirty_list.setAdapter(myChirtyList_adapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {
                    dialog.dismiss();
                    int mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 401) {
                        AppUtils.sessionExpiredAlert(getContext());
                    }
                } catch (NullPointerException e) {

                }

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

//                params.put("charityName", name_charty_get);
//                params.put("charityAccountName", name_acount_chirty_get);
//                params.put("charityBankName", bank_name_chirty_get);
//                params.put("charityAcccountNo", bankcode_chirty_get);
//                params.put("image", "image-1571815019.png");

                Log.e("params", "params" + params);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Log.e("bearer", "bearer" + "\n" + bearer);
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }

}

package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ActivityCoachAdapter;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class EditActivityFrag extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CountryAdapter.CountryCallback, ActivityCoachAdapter.AdapterCallback {
    private static final int GALLERY_REQUEST_CODE = 121, MULTIPLE_REQUEST_CODE = 122;
    private EditText court_activity, activity_scl, event_time_zone_activity, event_country_activity,
            event_zip_activity, date_scl_activity, duration_activity, cost_activity;
    private String court_activity_get = "",
            activity_scl_get = "",
            select_class_id_get = "",
            event_time_zone_activity_get = "",
            event_country_activity_get = "",
            event_zip_activity_get = "",
            date_scl_activity_get = "",
            duration_activity_get = "",
            address_activity_get = "",
            cost_activity_get = "";
    private Dialog coachdialog;
    private RecyclerView coach_rv;
    EditText coach_activity, team_activity;
    private CheckBox monImg, tueImg, wedImg, thuImg, friImg, satImg, sunImg;
    private TextView monTxt, tueTxt, wedTxt, thuTxt, friTxt, satTxt, sunTxt;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button save_activity;
    private String image_path = "";
    private CircleImageView logo_img;
    private LinearLayout upload_logo, uploadImg, lnrImages;
    private CountryCodePicker teamCountryccp;
    private TextView end_eventdate, start_dateeven, text_title, select_class_scl, not_found_txt;

    private ImageView gallback;

    private EditText timezone_spp;
    private String teamId = "0", coachId = "0";
    private String Id;
    private ArrayAdapter idAdapter;
    private String startDate = "", endDate = "", viewType = "", weeks = "", value = "", dayOfWeek = "", dateRange = "";
    private View view;
    private List<Body> responseBody;
    private CountryAdapter.CountryCallback countryCallback;
    ArrayList<String> imagesArr;
    RelativeLayout linear_main_bar;
    AutoCompleteAdapter mAdapter;
    PlacesClient placesClient;
    AutoCompleteTextView address_activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        countryCallback = (CountryAdapter.CountryCallback) this;
        ;
        if (!bundle.isEmpty()) {
            Id = bundle.getString("id", "");
            viewType = bundle.getString("viewType", "");
            value = bundle.getString("value", "");

        }


        if (viewType.equals("show")) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.view_activity, container, false);
            FindviewID(view);
            text_title = view.findViewById(R.id.text_title);
            linear_main_bar = view.findViewById(R.id.linear_main_bar);
            text_title.setText("Show Activity");
            teamCountryccp = view.findViewById(R.id.team_country);
            if (value.equals("2")) {
                linear_main_bar.setVisibility(View.GONE);
            } else {
                linear_main_bar.setVisibility(View.VISIBLE);
            }
            viewIds();


        } else {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.edit_activity, container, false);
            FindviewID(view);
            text_title = view.findViewById(R.id.text_title);
            text_title.setText("Edit Activity");
            uploadImg.setOnClickListener(this);
        }


        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        save_activity.setOnClickListener(this);
        date_scl_activity.setOnClickListener(this);
        duration_activity.setOnClickListener(this);
        upload_logo.setOnClickListener(this);

        team_activity.setOnClickListener(this);
        coach_activity.setOnClickListener(this);

        save_activity.setText("Next");
        setUpAutoCompleteTextView();

        getList();
        event_time_zone_activity.setText(AppUtils.getCurrentTimeZone());

        event_country_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.countryDialog(getContext(), countryCallback);
            }
        });

        event_time_zone_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!viewType.equals("show")) {
                    AppUtils.timeZoneDialog(getContext(), countryCallback);
                }
            }
        });
        address_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });

        return view;
    }


    private void viewIds() {
        start_dateeven = view.findViewById(R.id.start_dateevene);
        end_eventdate = view.findViewById(R.id.end_eventdatee);
        lnrImages = view.findViewById(R.id.lnrImages);
        uploadImg = view.findViewById(R.id.upload_images);
        monImg = view.findViewById(R.id.mon_img);
        tueImg = view.findViewById(R.id.tue_img);
        wedImg = view.findViewById(R.id.wed_img);
        thuImg = view.findViewById(R.id.thu_img);
        friImg = view.findViewById(R.id.fri_img);
        satImg = view.findViewById(R.id.sat_img);
        sunImg = view.findViewById(R.id.sun_img);
        monTxt = view.findViewById(R.id.mon_txt);
        tueTxt = view.findViewById(R.id.tue_txt);
        wedTxt = view.findViewById(R.id.wed_txt);
        thuTxt = view.findViewById(R.id.thu_txt);
        friTxt = view.findViewById(R.id.fri_txt);
        satTxt = view.findViewById(R.id.sat_txt);
        sunTxt = view.findViewById(R.id.sun_txt);
    }

    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());

    }


    private void getPlaceInfo(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
       /* if (addresses.get(0).getAdminArea() != null) {
            String addressLine = addresses.get(0).getAdminArea();
            Log.e("ADDRESS ", addressLine);
            adress_school.setText(addressLine);
        }*/

        if (addresses.get(0).getPostalCode() != null) {
            String ZIP = addresses.get(0).getPostalCode();
            event_zip_activity.setText(ZIP);
            Log.e("ZIP CODE", ZIP);
        }

        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();

            Log.e("CITY", city);
        }

        if (addresses.get(0).getAdminArea() != null) {
            String state = addresses.get(0).getAdminArea();
            Log.e("STATE", state);
        }

        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            event_country_activity.setText(country);
            Log.e("COUNTRY", country);
        }
    }

    private void FindviewID(View view) {
        upload_logo = view.findViewById(R.id.upload_logo);
        uploadImg = view.findViewById(R.id.upload_images);
        logo_img = view.findViewById(R.id.logo_img);

        save_activity = view.findViewById(R.id.save_activity);
        court_activity = view.findViewById(R.id.court_activity);
        lnrImages = view.findViewById(R.id.lnrImages);
        activity_scl = view.findViewById(R.id.activity_scl);
        select_class_scl = view.findViewById(R.id.select_class_scl);
        event_time_zone_activity = view.findViewById(R.id.event_time_zone_activity);
        event_country_activity = view.findViewById(R.id.event_country_activity);
        event_zip_activity = view.findViewById(R.id.event_zip_activity);
        date_scl_activity = view.findViewById(R.id.date_scl_activity);
        duration_activity = view.findViewById(R.id.duration_activity);
        address_activity = view.findViewById(R.id.address_activity);
        save_activity = view.findViewById(R.id.save_activity);
        cost_activity = view.findViewById(R.id.cost_activity);
        teamCountryccp = view.findViewById(R.id.team_country);
        team_activity = view.findViewById(R.id.team_activity);
        coach_activity = view.findViewById(R.id.coach_activity);


        event_time_zone_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
//        if (bundle != null) {
//
//            fromType = bundle.getString( "fromType", "" );
//            if (fromType.equals( "edit" )) {
//                Body body = (Body) bundle.getSerializable( "body" );
//                exit_classse.setText( body.getClassname() );
//
//                date_time.setText( body.getDateTime() );
//                time_zone.setText( body.getTimeZone() );
//                country_name.setText( body.getCountry() );
//                address_activity.setText( body.getAddress() );
//                cost_activity.setText( body.getCost() );
//                zip_code.setText( body.getZipcode() );
//                courd_field.setSelection( 1 );
//            }
//
//        }


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_activity) {
            activity_scl_get = activity_scl.getText().toString().trim();
            if (!activity_scl_get.equals("")) {

                validation();

            } else {
                activity_scl.setError("Please enter the Activity");
            }


        }

        if (v.getId() == R.id.coach_activity) {
            getCoachDialog("Coach");
        }
        if (v.getId() == R.id.team_activity) {
            getCoachDialog("Team");
        }

        if (v.getId() == R.id.date_scl_activity) {
            datePicker();
        }
        if (v.getId() == R.id.duration_activity) {
            opendurationDialog();
        }
        if (v.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

        }

        if (v.getId() == R.id.upload_images) {
            Matisse.from(getActivity())
                    .choose(MimeType.ofAll())
                    .countable(true)
                    .maxSelectable(3)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(new GlideEngine())
                    .showPreview(false) // Default is `true`
                    .forResult(MULTIPLE_REQUEST_CODE);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        event_time_zone_activity_get = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void validation() {

        court_activity_get = court_activity.getText().toString().trim();
        event_country_activity_get = event_country_activity.getText().toString().trim();
        event_zip_activity_get = event_zip_activity.getText().toString().trim();
        date_scl_activity_get = date_scl_activity.getText().toString().trim();
        duration_activity_get = duration_activity.getText().toString().trim();
        address_activity_get = address_activity.getText().toString().trim();
        cost_activity_get = cost_activity.getText().toString().trim();
        event_time_zone_activity_get = event_time_zone_activity.getText().toString().trim();

        if (!select_class_id_get.equals("")) {
            if (!event_time_zone_activity_get.equals("")) {
                if (!event_country_activity_get.equals("")) {
                    if (!event_zip_activity_get.equals("")) {
                        if (!date_scl_activity_get.equals("")) {
                            if (!duration_activity_get.equals("")) {
                                if (!address_activity_get.equals("")) {
                                    if (!court_activity_get.equals("")) {
                                        if (!cost_activity_get.equals("")) {
                                            //
                                            DataParse();
//                                            } else {
//                                                Toast.makeText(getActivity(), "Please upload image", Toast.LENGTH_SHORT).show();
//                                            }

                                        } else {
                                            cost_activity.setError("Please enter the cost");
                                        }
                                    } else {
                                        court_activity.setError("Please enter the Court/Filed");
                                    }
                                } else {
                                    court_activity.setError("Please enter the Court/Field etc");
                                }
                            } else {
                                duration_activity.setError("Please enter the Duration");
                            }
                        } else {
                            date_scl_activity.setError("Please enter the Date/time");
                        }
                    } else {
                        event_zip_activity.setError("Please enter the Zipcode");
                    }
                } else {
                    event_country_activity.setError("Please enter the Country");
                }
            } else {
                Toast.makeText(getContext(), "Please select the Time Zone", Toast.LENGTH_SHORT).show();
            }
        } else {
            select_class_scl.setError("Please enter the Class");
        }
    }


    private void opendurationDialog() {

        // Create custom dialog object
        final Dialog dialog = new Dialog(getContext());
        // Include dialog.xml file
        dialog.setContentView(R.layout.durationdialog);
        // Set dialog title
        dialog.setTitle("Select " +
                " Duration");

        // set values for custom dialog components - text, image and button
        TimePicker simpleTimePicker = (TimePicker) dialog.findViewById(R.id.timepicker);
        simpleTimePicker.setIs24HourView(true);
        simpleTimePicker.setCurrentHour(new Integer(0));
        simpleTimePicker.setCurrentMinute(new Integer(0));

        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                duration_activity.setText(selectedTime);
            }
        });

        dialog.show();

        dialog.findViewById(R.id.set_duration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (duration_activity.getText().toString().equals("")) {

                    Toast.makeText(getContext(), "Please set Duration", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                }
            }
        });

    }

    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        int day = calendar.get(Calendar.DAY_OF_WEEK);
                        dayOfWeek = getDayofWeek(day);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String strDate = format.format(calendar.getTime());
                        String selectedDate = strDate;
                        timePicker(selectedDate);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void timePicker(final String selectedDate) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {


                        String selectedTime = String.format("%02d:%02d", hourOfDay, minute);

                        date_scl_activity.setText(selectedDate + "  " + selectedTime);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void DataParse() {
        Log.e("fsdfsdf", "DataParse: " + activity_scl_get);

        EditReccurenceFrag fragment = new EditReccurenceFrag();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.crate_activity_scl, fragment);
        Bundle bundle = new Bundle();
        bundle.putString("eventNameStr", activity_scl_get);
        bundle.putString("eventTimeZoneStr", event_time_zone_activity_get);
        bundle.putString("eventCountryStr", event_country_activity_get);
        bundle.putString("eventZipCodeStr", event_zip_activity_get);
        bundle.putString("eventDateTimeStr", date_scl_activity_get);
        bundle.putString("eventDurationStr", duration_activity_get);
        bundle.putString("eventLocationStr", address_activity_get);
        bundle.putString("select_class_scl_get", select_class_id_get);
        bundle.putString("cost_activity_get", cost_activity_get);
        bundle.putString("court_activity_get", court_activity_get);
        bundle.putString("image_path", image_path);
        bundle.putString("startDate", startDate);
        bundle.putString("endDate", endDate);
        bundle.putString("id", Id);
        bundle.putString("weeks", weeks);
        bundle.putString("dateRange", dateRange);

        bundle.putString("fromType", "Activities");

        fragment.setArguments(bundle);

        transaction.commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                logo_img.setImageBitmap(bitmap);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        if (requestCode == MULTIPLE_REQUEST_CODE && resultCode == RESULT_OK) {
            lnrImages.setVisibility(View.VISIBLE);
            imagesArr = (ArrayList<String>) Matisse.obtainPathResult(data);
            for (int i = 0; i < imagesArr.size(); i++) {
                Bitmap yourbitmap = BitmapFactory.decodeFile(imagesArr.get(i));
                ImageView imageView = new ImageView(getContext());
                imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(50, 50));
                imageView.setMaxHeight(50);
                imageView.setMaxWidth(50);
                imageView.setPadding(5, 0, 5, 0);
                imageView.setImageBitmap(yourbitmap);
                lnrImages.addView(imageView);
                Log.e("Matisse", "mSelected: " + imagesArr);
            }
        }
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                address_activity.setText(place.getName() + ", " + place.getAddressComponents().asList().get(1).getName());

                try {
                    getPlaceInfo(place.getLatLng().latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    private void getList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi;

        getApi = mAPIService.getEventDetails(Id);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());

                dialog.dismiss();
                try {
                    if (response.code() == 200) {


                        activity_scl_get = response.body().getBody().getName();
                        event_time_zone_activity_get = response.body().getBody().getTimeZone();
                        event_country_activity_get = response.body().getBody().getCountry();
                        event_zip_activity_get = response.body().getBody().getZipcode();
                        date_scl_activity_get = response.body().getBody().getDateTime();
                        duration_activity_get = response.body().getBody().getDuration();
                        address_activity_get = response.body().getBody().getAddress();
                        select_class_id_get = response.body().getBody().getClassId();
                        cost_activity_get = response.body().getBody().getCost();
                        court_activity_get = response.body().getBody().getCourt();
                        String imageStr = response.body().getBody().getProfileImage();
                        startDate = response.body().getBody().getStartDate();
                        endDate = response.body().getBody().getEndDate();
                        weeks = response.body().getBody().getWeekDay();
                        dateRange = response.body().getBody().getDateRange();

                        if (viewType.equals("show")) {
                            convertString(weeks);
                            if (!startDate.equals("") && !endDate.equals("")) {

                                start_dateeven.setText(startDate);
                                end_eventdate.setText(endDate);
                            }
                        }

                        if (response.body().getBody().getEventImage() != null) {
                            if (!response.body().getBody().getEventImage().equals("")) {
                                String images = response.body().getBody().getEventImage();
                                ArrayList<String> items = new ArrayList<String>(Arrays.asList(images.split(",")));
                                for (int i = 0; i < items.size(); i++) {

                                    ImageView imageView = new ImageView(getContext());
                                    imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(80, 80));
                                    imageView.setMaxHeight(60);
                                    imageView.setMaxWidth(60);
                                    imageView.setPadding(5, 0, 5, 0);
                                    Glide.with(getActivity()).load(Constans.BASEURL + items.get(i)).into(imageView);
                                    lnrImages.addView(imageView);

                                    Log.e("Matisse", "mSelected: " + items);
                                }
                            }
                        }


                        Glide.with(getContext()).load(Constans.BASEURL + imageStr).into(logo_img);
                        activity_scl.setText(activity_scl_get);
                        event_country_activity.setText(event_country_activity_get);
                        event_zip_activity.setText(event_zip_activity_get);
                        duration_activity.setText(duration_activity_get);
                        address_activity.setText(address_activity_get);
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                        Date date = null;

                        try {
                            date = dateFormat.parse(date_scl_activity_get);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm"); //If you need time just put specific format for time like 'HH:mm:ss'
                        String dateStr = formatter.format(date);
                        date_scl_activity.setText(dateStr);
                        cost_activity.setText(cost_activity_get);
                        court_activity.setText(court_activity_get);
                        event_time_zone_activity.setText(event_time_zone_activity_get);

                        coach_activity.setText(response.body().getBody().getCoachName() != null ? response.body().getBody().getCoachName() : "");
                        team_activity.setText(response.body().getBody().getTeamName() != null ? response.body().getBody().getTeamName() : "");
                        coachId = response.body().getBody().getCoachId() != null ? response.body().getBody().getCoachId() : "0";
                        teamId = response.body().getBody().getTeamId() != null ? response.body().getBody().getTeamId() : "0";


                    } else if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            try {

                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");

                                Log.e("MessageM ", userMessage);
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        }

                    } else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private ArrayList<String> convertString(String weeks) {

        String[] elements = weeks.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);

        if (listOfString.contains("MON")) {
            monImg.setChecked(true);
            monTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("TUE")) {
            tueImg.setChecked(true);
            tueTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("WED")) {
            wedImg.setChecked(true);
            wedTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("THU")) {
            thuImg.setChecked(true);
            thuTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("FRI")) {
            friImg.setChecked(true);
            friTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("SAT")) {
            satImg.setChecked(true);
            satTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("SUN")) {
            sunImg.setChecked(true);
            sunTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }

        return listOfString;
    }

    @Override
    public void onSelectCountryCallback(String name) {
        AppUtils.dismissDialog();
        event_country_activity.setText(name);
    }

    @Override
    public void onSelectTimeZoneCallback(String name) {
        AppUtils.dismissDialog();
        event_time_zone_activity.setText(name);
    }


    private void getCoachDialog(String isTeam) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Class");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        not_found_txt = (TextView) coachdialog.findViewById(R.id.not_found_txt);
        not_found_txt.setText("No " + isTeam + " Result Found");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);

        getList(this, dialog, isTeam);


        coachdialog.show();
    }

    private void getList(final ActivityCoachAdapter.AdapterCallback callback, final Dialog dialog, String isTeam) {

        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        if (isTeam.equals("Coach")) {
            getApi = mAPIService.getCoachList(Constans.getFamilyId(getActivity()));
        } else {
            getApi = mAPIService.getTeamList(Constans.getFamilyId(getActivity()));
        }

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                    } else {
                        not_found_txt.setVisibility(View.GONE);
                        coach_rv.setVisibility(View.VISIBLE);
                        ActivityCoachAdapter customAdapter = new ActivityCoachAdapter(getContext(), responseBody, callback, isTeam);
                        coach_rv.setAdapter(customAdapter);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                not_found_txt.setVisibility(View.VISIBLE);
                coach_rv.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    @Override
    public void onMethodCallback(String coachName, String coachid, String teamName, String teamid) {
        coachdialog.dismiss();
        team_activity.setText(teamName);
        teamId = teamid;
        coachId = coachid;
        coach_activity.setText(coachName);


    }

    public String getDayofWeek(int day) {
        switch (day) {
            case Calendar.MONDAY:
                dayOfWeek = "MON";
                break;

            case Calendar.TUESDAY:
                dayOfWeek = "TUE";
                break;

            case Calendar.WEDNESDAY:
                dayOfWeek = "WED";
                break;

            case Calendar.THURSDAY:
                dayOfWeek = "THU";
                break;

            case Calendar.FRIDAY:
                dayOfWeek = "FRI";
                break;

            case Calendar.SATURDAY:
                dayOfWeek = "SAT";
                break;

            case Calendar.SUNDAY:
                dayOfWeek = "SUN";
                break;
        }
        return dayOfWeek;
    }

}

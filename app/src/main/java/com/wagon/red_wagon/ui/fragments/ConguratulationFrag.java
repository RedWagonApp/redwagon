package com.wagon.red_wagon.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;

public class ConguratulationFrag extends Fragment {
    LinearLayout add_childclick, refer_invirte;
    TextView text_title, textchange;
    ImageView gallback;
    RadioButton radio_button1, radio_button2;
    boolean click = true;
    Button home_page;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.conguratulation_frag, container, false);
        FamilyDashboardActivity.title.setText("Congratulations");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        refer_invirte = view.findViewById(R.id.refer_invirte);
        add_childclick = view.findViewById(R.id.add_childclickk);
        textchange = view.findViewById(R.id.textchange);
        radio_button1 = view.findViewById(R.id.radio_button1);
        radio_button2 = view.findViewById(R.id.radio_button2);
        home_page = view.findViewById(R.id.home_page);
        home_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dashboard dashboardFrag = new Dashboard();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fram_dash, dashboardFrag, "Home").commit();

            }
        });

        add_childclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (click == true) {
                    AboutCar about_car = new AboutCar();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    Bundle args = new Bundle();
                    args.putString("value", "1");
                    about_car.setArguments(args);
                    transaction.replace(R.id.fram_dash, about_car, "About Car");
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, new ChildProfileFrag(), "Child");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }


//                Intent intent=new Intent(getActivity(), FamilyDashboardActivity.class);
//                getActivity().startActivity(intent);
            }
        });

        radio_button1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    radio_button1.setChecked(true);
                    radio_button2.setChecked(false);
                    textchange.setText("Register as a Driver");
                    click = true;
                }
            }
        });
        radio_button2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    radio_button1.setChecked(false);
                    radio_button2.setChecked(true);
                    textchange.setText("Add Child");
                    click = false;
                }
            }
        });

        refer_invirte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Invite_Screen invoice_frag = new Invite_Screen();
                Bundle bundle = new Bundle();
                bundle.putString("school_dashboard", "2");
                invoice_frag.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, invoice_frag, "Invite");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return view;
    }

}

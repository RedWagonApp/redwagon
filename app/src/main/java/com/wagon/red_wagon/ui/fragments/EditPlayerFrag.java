package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CoachRowAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class EditPlayerFrag extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CoachRowAdapter.AdapterCallback {
    private static final int GALLERY_REQUEST_CODE = 122;
    private EditText first_name_plyr, last_name_plyr, email_plyr, phone_plyr, edit_gender_,  dateofbirth, jersey_num, postion_plyr, maneger_addplyr;
    private String fromType = "", Gander_get, first_name_plyr_get, last_name_plyr_get, email_plyr_get, phone_plyr_get, edit_gender_get, address_plyr_get, dateofbirth_get="", jersey_num_get, postion_plyr_get, maneger_addplyr_get;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Spinner gander_spinner;
    private Button save_playr;
    String[] gender = {"Gender", "Male", "Female", "Other"};
    TextView getgander, teac_coach_manegaer;
    private String teamId = "";
    private List<Body> bodyList;
    Dialog coachdialog;
    RecyclerView coach_rv;
    TextView text_title;
    String managerId = "", managername = "", playerId="",value="";
    TextView text_tittel;
    ImageView gallback;
    LinearLayout upload_logo;
    CircleImageView logo_img;
    private String image_path = "", url = "";
    View view;
    AutoCompleteTextView address_plyr;
    AutoCompleteAdapter mAdapter;
    PlacesClient placesClient;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        fromType = bundle.getString("fromType", "");
        value = bundle.getString("value", "");
        if (!getArguments().isEmpty()) {
            playerId = getArguments().getString("playerId", "");
        }

        if (fromType.equals("show")) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.view_player, container, false);
            findviewID(view);
            text_title = view.findViewById(R.id.text_title);
            text_title.setText("Show Player");
            gander_spinner.setEnabled(false);
        } else {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_player, container, false);
            findviewID(view);
            text_title = view.findViewById(R.id.text_title);
            text_title.setText("Edit Player");
        }
        jersey_num = view.findViewById(R.id.jersey_num);
        postion_plyr = view.findViewById(R.id.postion_plyr);
        if(value.equals("0")){
            jersey_num.setVisibility(View.GONE);
            postion_plyr.setVisibility(View.GONE);
        }
        else{
            jersey_num.setVisibility(View.VISIBLE);
            postion_plyr.setVisibility(View.VISIBLE);
        }

        setUpAutoCompleteTextView();

        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        bodyList = new ArrayList<>();
        save_playr.setOnClickListener(this);
        dateofbirth.setOnClickListener(this);
        //   teac_coach_manegaer.setOnClickListener(this);
        gander_spinner.setOnItemSelectedListener(this);
        logo_img = view.findViewById(R.id.logo_img);
        upload_logo = view.findViewById(R.id.upload_logo);
        upload_logo.setOnClickListener(this);


        ArrayAdapter<CharSequence> langAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_text, gender);
        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        gander_spinner.setAdapter(langAdapter);
        getList();
        address_plyr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });
        return view;
    }
    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());

    }


    private void findviewID(View view) {

        first_name_plyr = view.findViewById(R.id.first_name_plyr);
        last_name_plyr = view.findViewById(R.id.last_name_plyr);
        email_plyr = view.findViewById(R.id.email_plyr);
        phone_plyr = view.findViewById(R.id.phone_plyr);
        edit_gender_ = view.findViewById(R.id.edit_gender_);
        phone_plyr = view.findViewById(R.id.phone_plyr);
        address_plyr = view.findViewById(R.id.address_plyr);
        dateofbirth = view.findViewById(R.id.dateofbirth);
        save_playr = view.findViewById(R.id.save_playr);
        gander_spinner = view.findViewById(R.id.gander_spinner);
        save_playr.setText("Update");
        dateofbirth.setVisibility(View.GONE);
        //   teac_coach_manegaer = view.findViewById(R.id.teac_coach_manegaer);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_playr) {
            first_name_plyr_get = first_name_plyr.getText().toString();
            last_name_plyr_get = last_name_plyr.getText().toString();
            email_plyr_get = email_plyr.getText().toString();
            phone_plyr_get = phone_plyr.getText().toString();
            address_plyr_get = address_plyr.getText().toString();
            dateofbirth_get = dateofbirth.getText().toString();
            jersey_num_get = jersey_num.getText().toString();
            postion_plyr_get = postion_plyr.getText().toString();
            if (!first_name_plyr_get.equals("")) {
                if (!last_name_plyr_get.equals("")) {
                    if (!email_plyr_get.equals("") && AppUtils.emailValidator(email_plyr_get)) {
                        if (!phone_plyr_get.equals("")) {
                            if (!edit_gender_get.equals("Gender") || !edit_gender_get.equals("")) {
                                if (!address_plyr_get.equals("")) {
                                    //if (!dateofbirth_get.equals("")) {
                                        if (!jersey_num_get.equals("")) {
                                            if (!postion_plyr_get.equals("")) {


                                                Log.e("addplayer", "addplayer: ");


                                                ApiUpdatePlayer();
                                                /*} else {
                                                    Toast.makeText(getActivity(), "Please select image", Toast.LENGTH_SHORT).show();

                                                }
*/
                                            } else {
                                                postion_plyr.setError("Please enter the Position");
                                            }
                                        } else {
                                            jersey_num.setError("Please enter the jersey number");
                                        }
                                    /*} else {
                                        dateofbirth.setError("Please enter the DOB");
                                    }*/
                                } else {
                                    address_plyr.setError("Please enter the address");
                                }
                            } else {

                                Toast.makeText(getActivity(), "Please enter the gander", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            phone_plyr.setError("Please enter the mobile number");
                        }
                    } else {
                        email_plyr.setError("Please enter the email address");
                    }
                } else {
                    last_name_plyr.setError("Please enter the last name");
                }
            } else {
                first_name_plyr.setError("Please enter the first name");
            }

        }
        if (v.getId() == R.id.dateofbirth) {
            datePicker();
        }

        if (v.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

        }
    }

    private void ApiUpdatePlayer() {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        Call<AddEvent> mAPIService;
        MultipartBody.Part fileToUpload = null;
        Map<String, RequestBody> map = new HashMap<>();
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            fileToUpload = MultipartBody.Part.createFormData("image", "", requestBody);
          /*  RequestBody image = RequestBody.create(MediaType.parse("text/plain"), url);
            map.put("image", image);*/
        }

        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), teamId);
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), first_name_plyr_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), last_name_plyr_get);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), email_plyr_get);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), phone_plyr_get);
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), edit_gender_get);
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), address_plyr_get);
        RequestBody id8 = RequestBody.create(MediaType.parse("text/plain"), dateofbirth_get);
        RequestBody id9 = RequestBody.create(MediaType.parse("text/plain"), jersey_num_get);
        RequestBody id10 = RequestBody.create(MediaType.parse("text/plain"), postion_plyr_get);
        RequestBody id11 = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody id12 = RequestBody.create(MediaType.parse("text/plain"), playerId);


        map.put("id", id12);
        map.put("teamId", id1);
        map.put("firstName", id2);
        map.put("lastName", id3);
        map.put("email", id4);
        map.put("phoneNumber", id5);
        map.put("gender", id6);
        map.put("address", id7);
        map.put("birthday", id8);
        map.put("jerseyNumber", id9);
        map.put("position", id10);
        map.put("manager", id11);
//        if ( !image_path.equals("")) {
        mAPIService = AppUtils.getAPIService(getContext()).updatePlayerDetails(map, fileToUpload);
        /*} else {
            mAPIService = AppUtils.getAPIService(getContext()).updatePlayerDetails(map);
        }*/
        mAPIService.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }


            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });

    }


    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String strDate = format.format(calendar.getTime());
                        String selectedDate = strDate;
                        dateofbirth.setText(selectedDate);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        edit_gender_get = gender[position];
//        if (position == 0) {
//            getgander.setTextColor(getResources().getColor(R.color.gray));
//        } else {
//            getgander.setTextColor(getResources().getColor(R.color.black));
//        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onMethodCallback(String name, String id) {

        coachdialog.dismiss();
        managerId = id;
        managername = name;
        //  teac_coach_manegaer.setText(name);
    }

    private void getList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi;

        getApi = mAPIService.getPlayerDetails(playerId);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    if (response.body().getBody().getProfileImage() != null) {
                        url = response.body().getBody().getProfileImage();
                        Glide.with(getContext()).load(Constans.BASEURL + url).into(logo_img);
                    }
                    first_name_plyr_get = response.body().getBody().getFirstName();
                    last_name_plyr_get = response.body().getBody().getLastName();
                    email_plyr_get = response.body().getBody().getEmail();
                    phone_plyr_get = response.body().getBody().getPhoneNumber();
                    address_plyr_get = response.body().getBody().getAddress();
                    dateofbirth_get = response.body().getBody().getBirthday();
                    jersey_num_get = response.body().getBody().getJerseyNumber();
                    postion_plyr_get = response.body().getBody().getPosition();
                    teamId = response.body().getBody().getTeamId();
                    String gender_get = response.body().getBody().getGender();

                    if (gender_get != null) {
                        if (gender_get.equals("Male") || gender_get.equals("male")) {
                            gander_spinner.setSelection(1);
                        } else if (gender_get.equals("Female") || gender_get.equals("female")) {
                            gander_spinner.setSelection(2);
                        } else {
                            gander_spinner.setSelection(3);
                        }
                    }

                    first_name_plyr.setText(first_name_plyr_get);
                    last_name_plyr.setText(last_name_plyr_get);
                    email_plyr.setText(email_plyr_get);
                    phone_plyr.setText(phone_plyr_get);
                    address_plyr.setText(address_plyr_get);
                    dateofbirth.setText(dateofbirth_get);
                    jersey_num.setText(jersey_num_get);
                    postion_plyr.setText(postion_plyr_get);


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();
                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                logo_img.setImageBitmap(bitmap);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                address_plyr.setText(place.getAddress());


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }

    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }


}

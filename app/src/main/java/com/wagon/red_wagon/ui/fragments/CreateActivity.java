package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CoachRowAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class CreateActivity extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CoachRowAdapter.AdapterCallback, CountryAdapter.CountryCallback {
    private static final int GALLERY_REQUEST_CODE = 121, MULTIPLE_REQUEST_CODE = 122;
    EditText court_activity, activity_scl, event_time_zone_activity, event_country_activity, event_zip_activity, date_scl_activity, duration_activity, cost_activity;
    private String court_activity_get = "", activity_scl_get = "",
            select_class_scl_get = "", event_time_zone_activity_get = "",
            event_country_activity_get = "",
            event_zip_activity_get = "",
            date_scl_activity_get = "",
            duration_activity_get = "",
            address_activity_get = "", cost_activity_get = "";

    private int mYear, mMonth, mDay, mHour, mMinute;
    Button save_activity;
    private String image_path = "";
    CircleImageView logo_img;
    LinearLayout upload_logo, upload_images, lnrImages;
    CountryCodePicker teamCountryccp;
    TextView text_title, select_class_scl, not_found_txt;
    ImageView gallback;
    Spinner timezone_sp;
    private Dialog coachdialog, countrydialog;
    RecyclerView coach_rv, country_rv;
    private String from;
    ArrayList<String> imagesArr;
    CountryAdapter countryAdapter;
    RelativeLayout relativeLayout;
    LinearLayout layout1, layout2, layout3;
    private int count = 0;
    AutoCompleteTextView address_activity;
    PlacesClient placesClient;
    AutoCompleteAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.create_activity, container, false);
        FindviewID(view);

        Bundle bundle = getArguments();

        if (bundle != null) {
            from = bundle.getString("from", "");

        }
//        Locale localCountry = Locale.getDefault();
//        event_country_activity.setText(localCountry.getDisplayCountry());

        text_title = view.findViewById(R.id.text_title);
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        save_activity.setOnClickListener(this);
        date_scl_activity.setOnClickListener(this);
        duration_activity.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        upload_images.setOnClickListener(this);
        timezone_sp.setOnItemSelectedListener(this);
        text_title.setText("Activity Address");
        setUpAutoCompleteTextView();

        final CountryAdapter.CountryCallback countryCallback = (CountryAdapter.CountryCallback) this;
        event_country_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.countryDialog(getContext(), countryCallback);
            }
        });
        event_time_zone_activity.setText(AppUtils.getCurrentTimeZone());
        event_time_zone_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.timeZoneDialog(getContext(), countryCallback);
            }
        });
        address_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });
        getschoolprofile();
        return view;
    }

    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());

    }


    private void getPlaceInfo(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);


        if (addresses.get(0).getPostalCode() != null) {
            String ZIP = addresses.get(0).getPostalCode();
            event_zip_activity.setText(ZIP);
            Log.e("ZIP CODE", ZIP);
        }
        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();
            Log.e("CITY", city);
        }
        if (addresses.get(0).getAdminArea() != null) {
            String state = addresses.get(0).getAdminArea();
            Log.e("STATE", state);
        }
        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            event_country_activity.setText(country);
            Log.e("COUNTRY", country);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                address_activity.setText(place.getName() + ", " + place.getAddressComponents().asList().get(1).getName());


                try {
                    getPlaceInfo(place.getLatLng().latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }

    private void FindviewID(View view) {
        upload_logo = view.findViewById(R.id.upload_logo);
        upload_images = view.findViewById(R.id.upload_images);
        logo_img = view.findViewById(R.id.logo_img);
        lnrImages = view.findViewById(R.id.lnrImages);
        save_activity = view.findViewById(R.id.save_activity);
        court_activity = view.findViewById(R.id.court_activity);
        timezone_sp = view.findViewById(R.id.timezone_sp);
        activity_scl = view.findViewById(R.id.activity_scl);
        select_class_scl = view.findViewById(R.id.select_class_scl);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.linear_main_bar);
        event_country_activity = view.findViewById(R.id.event_country_activity);
        event_time_zone_activity = view.findViewById(R.id.event_time_zone_activity);
        event_zip_activity = view.findViewById(R.id.event_zip_activity);
        date_scl_activity = view.findViewById(R.id.date_scl_activity);
        duration_activity = view.findViewById(R.id.duration_activity);
        address_activity = view.findViewById(R.id.address_activity);
        save_activity = view.findViewById(R.id.save_activity);
        cost_activity = view.findViewById(R.id.cost_activity);
        teamCountryccp = view.findViewById(R.id.team_country);
        layout1 = view.findViewById(R.id.lay1);
        layout2 = view.findViewById(R.id.lay2);
        layout3 = view.findViewById(R.id.lay3);

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_activity) {
            activity_scl_get = activity_scl.getText().toString().trim();
            event_time_zone_activity_get = event_time_zone_activity.getText().toString().trim();
            event_zip_activity_get = event_zip_activity.getText().toString().trim();
            event_country_activity_get = event_country_activity.getText().toString().trim();
            address_activity_get = address_activity.getText().toString().trim();
            if (!activity_scl_get.equals("")) {
                if (!event_time_zone_activity_get.equals("")) {
                    if (!event_country_activity_get.equals("")) {
                        if (!event_zip_activity_get.equals("")) {
                            if (!address_activity_get.equals("")) {
                                DataParse();
                            } else {
                                event_zip_activity.setError("Please enter the Address");
                            }
                        } else {
                            event_zip_activity.setError("Please enter the Zipcode");
                        }
                    } else {
                        event_country_activity.setError("Please enter the Country");
                    }
                } else {
                    Toast.makeText(getContext(), "Please select the Time Zone", Toast.LENGTH_SHORT).show();
                }
            } else {
                activity_scl.setError("Please enter the Activity");
            }
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        //  event_time_zone_activity_get = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void DataParse() {
        Log.e("DataParse", "DataParse: " + activity_scl_get);
        CreateActivitySecond fragment = new CreateActivitySecond();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.crate_activity_scl, fragment);
        Bundle bundle = new Bundle();
        bundle.putString("eventNameStr", activity_scl_get);
        bundle.putString("eventTimeZoneStr", event_time_zone_activity_get);
        bundle.putString("eventCountryStr", event_country_activity_get);
        bundle.putString("eventZipCodeStr", event_zip_activity_get);
        bundle.putString("eventLocationStr", address_activity_get);
        bundle.putString("fromType", "Activities");
        fragment.setArguments(bundle);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onMethodCallback(String name, String id) {
        coachdialog.dismiss();
        select_class_scl_get = id;
        select_class_scl.setText(name);
    }


    @Override
    public void onSelectCountryCallback(String name) {
        AppUtils.dismissDialog();
        event_country_activity.setText(name);
        event_country_activity_get = name;

    }

    @Override
    public void onSelectTimeZoneCallback(String name) {
        AppUtils.dismissDialog();
        event_time_zone_activity.setText(name);
        event_time_zone_activity_get = name;
    }

    private void getschoolprofile() {
        event_zip_activity.setText(Constans.getprofileData(getContext()).getZipcodee());
        event_country_activity.setText(Constans.getprofileData(getContext()).getCountry());
        address_activity.setText(Constans.getprofileData(getContext()).getAddress());
    }


}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class AddManger extends Fragment implements View.OnClickListener {
    EditText manager_name, manager_emaile, manager_mobile, manager_description;
    String getArg = "", manager_name_get, manager_email_get, manager_mobile_get, manager_description_get, image_path = "", result = "";

    TextView text_title, gellary_clickm;
    Button next_add_manager;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    private static final int GALLERY_REQUEST_CODE = 1;
    CircleImageView imagesetup_m;
    ImageView gallback;
    private static final int CAMERA_PIC_REQUEST = 144;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_maneger, container, false);
        findview(view);

        text_title.setText("Add Coach");
        next_add_manager.setOnClickListener(this);
//        imagesetup_m.setOnClickListener(this);
        gellary_clickm.setOnClickListener(this);
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;


    }

    private void findview(View view) {

        text_title = view.findViewById(R.id.text_title);
        next_add_manager = view.findViewById(R.id.next_add_manager);
        manager_name = view.findViewById(R.id.manager_name);
        manager_emaile = view.findViewById(R.id.mamager_email);
        manager_mobile = view.findViewById(R.id.manager_mobile);
        gellary_clickm = view.findViewById(R.id.gellary_clickm);
        manager_description = view.findViewById(R.id.manager_description);
        imagesetup_m = view.findViewById(R.id.imagesetup_m);


        manager_description.setScroller(new Scroller(getContext()));
        manager_description.setMaxLines(40);
        manager_description.setVerticalScrollBarEnabled(true);
        manager_description.setMovementMethod(new ScrollingMovementMethod());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_add_manager:

                Validation();

                break;
            case R.id.gellary_clickm:

                pickFromGallery();

                break;

        }
    }

    private void pickFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }


    private void Validation() {
        manager_name_get = manager_name.getText().toString().trim();
        manager_email_get = manager_emaile.getText().toString().trim();
        manager_mobile_get = manager_mobile.getText().toString().trim();
        manager_description_get = manager_description.getText().toString().trim();
        if (!manager_name_get.equals("")) {
            if (!manager_email_get.equals("") && emailValidator(manager_email_get)) {
                if (!manager_mobile_get.equals("")) {
                    if (!manager_description.equals("")) {
                        ApiManager();
                    } else {
                        manager_mobile.setError("Please enter description");
                    }
                } else {
                    manager_mobile.setError("Please enter a Mobile number");
                }
            } else {
                manager_emaile.setError("Please enter the email");
            }
        } else {
            manager_name.setError("Please enter a Name");
        }

    }




    private void ApiManager() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        MultipartBody.Part part = null;
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(image_path);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }

        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.getFamilyId(getActivity()));
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), manager_name_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), manager_email_get);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), manager_mobile_get);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), "Manager");
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), manager_description_get);
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), Constans.FCMToken(getActivity()));
        RequestBody id8 = RequestBody.create(MediaType.parse("text/plain"), "2");

        Map<String, RequestBody> params = new HashMap<>();
        params.put("schId", id1);
        params.put("firstName", id2);
        params.put("email", id3);
        params.put("phoneNumber", id4);
        params.put("gender", id5);
        params.put("description", id6);
        params.put("deviceToken", id7);
        params.put("deviceType", id8);

        Log.e("ApiAddTeam", "ApiAddTeam: " + params);
        mAPIService.addmanger(params, part).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    getArg = getArguments().getString("school_dashboard", "");
                    if (getArg.equals("2")) {
                        Toast.makeText(getContext(), "Password sent in your email", Toast.LENGTH_SHORT).show();
                        Log.e("sdfdsfsdfsfs", "sdfsfs" + Constans.Token);

                        fragmentManager = getActivity().getSupportFragmentManager();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.add_manager_frame, new AllDone());
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        getActivity().onBackPressed();
                    }
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            try {
                Uri uri = data.getData();
                image_path = getPath(uri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                imagesetup_m.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getPath(Uri uri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(getActivity(), uri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();

        } catch (SecurityException ignored) {

        }
        return result;

    }


}

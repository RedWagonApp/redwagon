package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CarpoolListAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CarpoolListFrag extends Fragment implements View.OnClickListener {

    CardView add_carpool_lay;
    RecyclerView carpool_recyclerview;
    ArrayList<Event_List_model> arrayList;
    String teamId = "";
    TextView pool_count, text_title,no_result_tv;
    ImageView gallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.car_pool_list_lay, container, false);
        findView(view);
        add_carpool_lay.setOnClickListener(this);
        FamilyDashboardActivity.title.setText("Carpools");

        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        getCarPoolList();
        return view;
    }

    private void findView(View view) {
        arrayList = new ArrayList<>();

        add_carpool_lay = view.findViewById(R.id.add_carpool_lay);
        no_result_tv = view.findViewById(R.id.no_result_tv);

        carpool_recyclerview = view.findViewById(R.id.carpool_recyclerview);
        pool_count = view.findViewById(R.id.pool_count);
     /*   try {
            Bundle bundle = getArguments();
            teamId = bundle.getString("teamId", "");
        } catch (NullPointerException e) {

        }*/


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        carpool_recyclerview.setLayoutManager(linearLayoutManager);
//        CarpoolListAdapter playerAdapter = new CarpoolListAdapter(getActivity());
//        carpool_recyclerview.setAdapter(playerAdapter);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_carpool_lay) {
            CarpoolSelectEventList eventList = new CarpoolSelectEventList();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fram_dash, eventList, "Create a Car Pool");
            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    private void getCarPoolList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        getApi = mAPIService.getCarpool(Constans.getFamilyId(getContext()));
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    if(responseBody.size()!=0){
                        no_result_tv.setVisibility(View.GONE);
                        carpool_recyclerview.setVisibility(View.VISIBLE);
                        if (responseBody.size() != 0) {
                            pool_count.setText("Pools (" + responseBody.size() + ")");
                        }
                        CarpoolListAdapter playerAdapter = new CarpoolListAdapter(getActivity(), responseBody,"2");
                        carpool_recyclerview.setAdapter(playerAdapter);
                    }
                    else{
                        no_result_tv.setVisibility(View.VISIBLE);
                        carpool_recyclerview.setVisibility(View.GONE);
                    }



                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getContext());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onResume", "onStart: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "onResume: ");
        //getList();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.e("onResume", "onAttach: ");
    }


}

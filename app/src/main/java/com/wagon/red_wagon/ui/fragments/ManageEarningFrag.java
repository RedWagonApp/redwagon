package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardInputWidget;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageEarningFrag extends Fragment implements View.OnClickListener {
    private Button addmoneywallet, trasfer_click, addchirty;
    private String card_number = "", stripeToken = "";
    private Dialog amountDialog;
    private String stripe_token = "", paymentget;
    private int exp_date, exp_month;
    private EditText payment, cardname, paymentbank;
    private CardInputWidget mCardInputWidget;
    private TextView wallet_amount;
    Call<AddEvent> getApi;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.earning_frag, container, false);
        FamilyDashboardActivity.title.setText("Manage Earnings");

        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        findviewID(view);
        getUsersAmount();
        addmoneywallet.setOnClickListener(this);
        trasfer_click.setOnClickListener(this);
        addchirty.setOnClickListener(this);
        return view;
    }

    private void findviewID(View view) {
        addmoneywallet = view.findViewById(R.id.addmoneywallet);
        wallet_amount = view.findViewById(R.id.wallet_amount);
        trasfer_click = view.findViewById(R.id.trasfer_click);
        addchirty = view.findViewById(R.id.addchirtd);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.addmoneywallet) {
            Payemetmethod();
        }
        if (v.getId() == R.id.trasfer_click) {
            TransferToBank("1");
        }
        if (v.getId() == R.id.addchirtd) {
            TransferToBank("2");
        }

    }

    private void TransferToBank(String value) {
        amountDialog = new Dialog(getContext());
        amountDialog.setContentView(R.layout.transferbank);
        amountDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button transferbutton = amountDialog.findViewById(R.id.transferbutton);
        paymentbank = amountDialog.findViewById(R.id.paymentbank);
        transferbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentget = paymentbank.getText().toString();
                if (!paymentget.equals("")) {

                    ApiTransferBank(paymentget, value);

                } else {
                    paymentbank.setError("Please enter a Amount");

                }
            }
        });

        amountDialog.show();
    }

    private void ApiTransferBank(String ammount, String value) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        if (value.equals("1")) {
            getApi = mAPIService.usersFundTransfers(Constans.getFamilyId(getActivity()), ammount);
        } else {
            getApi = mAPIService.charityFundTransfers(Constans.getFamilyId(getActivity()), ammount, Constans.getCharityId(getActivity()));
        }
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {

                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        paymentbank.setText("");
                        if (value.equals("1")) {
                            Toast.makeText(getActivity(), "Transfer to Bank Successfully", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        } else {
                            Toast.makeText(getActivity(), "Transfer to Charity Successfully", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        }
                        amountDialog.dismiss();

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Payemetmethod() {
        amountDialog = new Dialog(getContext());
        amountDialog.setContentView(R.layout.stripe_card_info);
        amountDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button split = amountDialog.findViewById(R.id.split);
        payment = amountDialog.findViewById(R.id.payment);
        cardname = amountDialog.findViewById(R.id.cardname);
        mCardInputWidget = amountDialog.findViewById(R.id.card_input_widget);

        split.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String paymentget = payment.getText().toString().trim();
                String cardnameget = cardname.getText().toString().trim();

                if (!cardnameget.equals("")) {
                    if (!paymentget.equals("")) {
                        ApiService(stripeToken, paymentget);
                    } else {
                        payment.setError("please enter a valid amount");
                    }
                } else {
                    cardname.setError("Please enter a name");

                }


                mCardInputWidget.setCardInputListener(new CardInputListener() {
                    @Override
                    public void onFocusChange(String focusField) {
                    }

                    @Override
                    public void onCardComplete() {
                    }

                    @Override
                    public void onExpirationComplete() {
                    }

                    @Override
                    public void onCvcComplete() {
                    }

                    @Override
                    public void onPostalCodeComplete() {
                    }
                });


                try {
                    final Card cardToSave = mCardInputWidget.getCard();
                    exp_date = cardToSave.getExpYear();
                    exp_month = cardToSave.getExpMonth();
                    card_number = cardToSave.getNumber();
                    String name = cardToSave.getName();

                    Log.d("card_number", "card_number: " + exp_date);
                    Log.d("card_number", "card_number: " + exp_month);
                    Log.d("card_number", "card_number: " + card_number);
                    Log.d("card_number", "card_number: " + name);
                    if (cardToSave == null) {
                        Toast.makeText(getActivity(), "Invalid Card Data", Toast.LENGTH_LONG).show();
                        Log.e("card_number", "payDone: Invalid Card Data");

                        // mErrorDialogHandler.showError("Invalid Card Data");
                    }
                    //final Card card = new Card(cardNumber, cardDate[0], cardDate[1], cvcValue);
// Remember to validate the card object before you use it to save time.
                    else {
                        if (!cardToSave.validateCard()) {
                            // Do not continue token creation.
                            Toast.makeText(getActivity(), "Invalid Card Data", Toast.LENGTH_LONG).show();
                            Log.e("card_number", "onCreate: error");

                        } else {
                            Log.e("card_number", "carrddddddddd: " + cardToSave.getNumber());
                            Stripe stripe = new Stripe(getActivity(), getString(R.string.publishablekey));
                            stripe.createToken(cardToSave, new TokenCallback() {
                                public void onSuccess(Token token) {
                                    Log.e("card_number", "onSuccess: " + token);
                                    stripeToken = token.getId();
                                    Log.e("card_number", "onSuccess: " + stripeToken);


                                    // Send token to your server
                                }

                                public void onError(Exception error) {
                                    // Show localized error message
                                    Log.e("card_number", "onError: " + error);
                                }
                            });
                        }
                    }
                } catch (
                        Exception e) {
                    System.out.println(e);
                }
            }

        });
        amountDialog.show();
    }

    private void ApiService(String stripeToken, String paymentget) {


        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());

        mAPIService.addSameAmountPayment(Constans.getFamilyId(getActivity()), paymentget, stripeToken).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {

                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        payment.setText("");
                        Toast.makeText(getActivity(), "Amount Successfully Added to wallet", Toast.LENGTH_SHORT).show();
                        amountDialog.dismiss();
                        getActivity().onBackPressed();

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void getUsersAmount() {


        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        mAPIService.getUsersAmount(Constans.getFamilyId(getActivity())).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        String amount = response.body().getBody().getAmount();
//                        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);
                        ;
                        wallet_amount.setText("$" + amount + ".00");

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });


    }


}

package com.wagon.red_wagon.ui.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.fragments.AddManger;
import com.wagon.red_wagon.ui.fragments.AttendenceList;
import com.wagon.red_wagon.ui.fragments.ChatsFragment;
import com.wagon.red_wagon.ui.fragments.CostFragment;
import com.wagon.red_wagon.ui.fragments.EditActivityFrag;
import com.wagon.red_wagon.ui.fragments.EditPlayerFrag;
import com.wagon.red_wagon.ui.fragments.EventListFragment;
import com.wagon.red_wagon.ui.fragments.Invite_Screen;
import com.wagon.red_wagon.ui.fragments.InvoiceFrag;
import com.wagon.red_wagon.ui.fragments.RecurrenceFrag;
import com.wagon.red_wagon.ui.fragments.SchoolProfFrag;
import com.wagon.red_wagon.ui.fragments.SchoolScheduleList;
import com.wagon.red_wagon.ui.fragments.ShowChatTeam;
import com.wagon.red_wagon.ui.fragments.ShowManagersFrag;
import com.wagon.red_wagon.ui.fragments.TeamDetailFrag;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class SchoolDashboard extends BaseActivity implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
    LinearLayout schooldash;
    public static ImageView sidebar;
    DrawerLayout mDrawerLayout;
    LinearLayout game_school_coach, create_activity_sch, viewschedule_sch, game_school, student_lay, team_manage, classes_lay, chat_lay, attendence_lay;

    FragmentManager fragmentManager;
    public static TextView text_create;
    CircleImageView imageset;
    Socket socket;
    String Data = "0", textget = "", value = "", type = "12";
    TextView username;
    LinearLayout maneger_lay, coach_dash;
    NavigationView navView;
    boolean isSideMain = true;
    public static TextView title;
    BottomNavigationView navigation;
    public static final String PROGRESS_UPDATE = "progress_update";
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_school_dashboard);

        title = findViewById(R.id.text_create);
        FindViewID();
        schooldash = findViewById(R.id.schooldash);
        coach_dash = findViewById(R.id.coach_dash);
        getschoolprofile();

        Intent intent = getIntent();
        Data = intent.getStringExtra("Coach");
        type = intent.getStringExtra("type");
        Log.e("onCreate", "onCreate: " + type);


        try {
            if (Data.equals("2")) {

                textget = "Coach";
                value = "2";
                navView = (NavigationView) findViewById(R.id.navigation_view_school);
                Menu nav_Menu = navView.getMenu();
                nav_Menu.findItem(R.id.admaneger).setVisible(false);
                title.setText("Coach");
            } else {

                textget = "Club";
                value = "1";
                title.setText("Club");
            }
        } catch (NullPointerException e) {

        }
        schooldash.setVisibility(View.VISIBLE);
        coach_dash.setVisibility(View.GONE);

        create_activity_sch.setOnClickListener(this);
        game_school.setOnClickListener(this);
        viewschedule_sch.setOnClickListener(this);
        student_lay.setOnClickListener(this);
        team_manage.setOnClickListener(this);
        /* classes_lay.setOnClickListener(this);*/
        attendence_lay.setOnClickListener(this);
        chat_lay.setOnClickListener(this);
        maneger_lay.setOnClickListener(this);

        sidebar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });
        navigation = (BottomNavigationView) findViewById(R.id.bottom_navigations_school);

        navigation.setOnNavigationItemSelectedListener(this);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        navView = (NavigationView) findViewById(R.id.navigation_view_school);
        View header = navView.getHeaderView(0);
        imageset = (CircleImageView) header.findViewById(R.id.imagesetuser);
        username = (TextView) header.findViewById(R.id.username);

        try {
            if (type.equals("1")) {


            } else {
                navView = (NavigationView) findViewById(R.id.navigation_view_school);
                Menu nav_Menu = navView.getMenu();
                nav_Menu.findItem(R.id.famgo).setVisible(false);
            }
            Log.e("ConstansConstans", "onCreate: " + Constans.getUserName(SchoolDashboard.this));
            if (Constans.getUserName(SchoolDashboard.this) != null) {
                String user_name = Constans.getUserName(SchoolDashboard.this);
                username.setText(user_name);

            }
            if (Constans.getProfilePic(SchoolDashboard.this) != null) {
                Glide.with(SchoolDashboard.this).load(Constans.BASEURL + Constans.getProfilePic(SchoolDashboard.this)).placeholder(R.drawable.logo).into(imageset);


            }

        } catch (NullPointerException e) {

        }


        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @SuppressLint("WrongConstant")
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
// create a Fragment Object
                int itemId = menuItem.getItemId();

                if (itemId == R.id.log_school) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(SchoolDashboard.this);
                    builder.setMessage("Are you sure you want to logout?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mDrawerLayout.closeDrawer(Gravity.START);
                                    logoutApi();

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }
                if (itemId == R.id.Invoice_click) {
                    InvoiceFrag invoice_frag = new InvoiceFrag();
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.school_dashbord_fram, invoice_frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                }

                if (itemId == R.id.admaneger) {
                    AddManger invoice_frag = new AddManger();
                    Bundle bundle = new Bundle();
                    bundle.putString("school_dashboard", "1");
                    invoice_frag.setArguments(bundle);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.school_dashbord_fram, invoice_frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                }
                if (itemId == R.id.refer_wagon) {
                    Invite_Screen invoice_frag = new Invite_Screen();
                    Bundle bundle = new Bundle();
                    bundle.putString("school_dashboard", "1");
                    invoice_frag.setArguments(bundle);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.school_dashbord_fram, invoice_frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                }
                if (itemId == R.id.team_chat) {

                    ShowChatTeam schedule_list_frag = new ShowChatTeam();
                    Bundle args = new Bundle();
                    args.putString("fromType", "1");
                    schedule_list_frag.setArguments(args);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.school_dashbord_fram, schedule_list_frag);
                    transaction.addToBackStack(Constans.fromType);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                }
                if (itemId == R.id.famgo) {
                    Intent intent1 = new Intent(SchoolDashboard.this, FamilyDashboardActivity.class);
                    intent1.putExtra("check", "1");
                    intent1.putExtra("type", "1");
                    intent.putExtra("child", "0");
                    startActivity(intent1);
                    finish();

                    mDrawerLayout.closeDrawer(Gravity.START);
                }

                if (itemId == R.id.addplyer) {
                    EventListFragment eventListFragment = new EventListFragment();
                    Bundle args = new Bundle();
                    Constans.fromType = "Student";
                    args.putString("fromType", "Student");

                    eventListFragment.setArguments(args);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.school_dashbord_fram, eventListFragment);
                    transaction.addToBackStack(Constans.fromType);
                    transaction.commit();

                    mDrawerLayout.closeDrawer(Gravity.START);
                }
                return false;
            }
        });


    }

    public void isVisible(boolean isMain) {
        isSideMain = isMain;
        if (!isMain) {
            sidebar.setBackground(getResources().getDrawable(R.drawable.bck_arrow));

        } else {
            sidebar.setBackground(getResources().getDrawable(R.drawable.menu_side));
        }
    }


    private void FindViewID() {
        sidebar = findViewById(R.id.sidebar);
        sidebar.setBackground(getResources().getDrawable(R.drawable.menu_side));
        mDrawerLayout = findViewById(R.id.container);
        game_school = findViewById(R.id.game_school);
        create_activity_sch = findViewById(R.id.create_activity_sch);
        viewschedule_sch = findViewById(R.id.viewschedule);
        student_lay = findViewById(R.id.student_lay);
        team_manage = findViewById(R.id.team_manage);
        /* classes_lay = findViewById(R.id.classes_lay);*/
        text_create = findViewById(R.id.text_create);
        attendence_lay = findViewById(R.id.attendence_lay);
        chat_lay = findViewById(R.id.chat_lay);
        maneger_lay = findViewById(R.id.maneger_lay);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.create_activity_sch) {
            EventListFragment eventListFragment = new EventListFragment();
            Bundle args = new Bundle();
            Constans.fromType = "Activities";
            args.putString("fromType", "Activities");
            args.putString("fromType", value);
            eventListFragment.setArguments(args);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.school_dashbord_fram, eventListFragment);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();
        }

        if (v.getId() == R.id.student_lay) {
            EventListFragment eventListFragment = new EventListFragment();
            Bundle args = new Bundle();
            Constans.fromType = "Student";
            args.putString("fromType", "Student");
            eventListFragment.setArguments(args);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.school_dashbord_fram, eventListFragment);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();
        }

        if (v.getId() == R.id.team_manage) {
            TeamDetailFrag eventListFragment = new TeamDetailFrag();
            Bundle args = new Bundle();
            Constans.fromType = "Team";
            args.putString("fromType", "Team");
            eventListFragment.setArguments(args);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.school_dashbord_fram, eventListFragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }
        if (v.getId() == R.id.game_school) {

            EventListFragment eventListFragment = new EventListFragment();
            Bundle args = new Bundle();
            Constans.fromType = "Games";
            args.putString("fromType", "Games");
            eventListFragment.setArguments(args);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.school_dashbord_fram, eventListFragment);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();

        }
        if (v.getId() == R.id.viewschedule) {

            SchoolScheduleList schedule_list_frag = new SchoolScheduleList();
            Bundle args = new Bundle();
            Constans.fromType = "Schedule";
            args.putString("fromType", "Schedule");
            args.putString("Type", "1");
            schedule_list_frag.setArguments(args);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.school_dashbord_fram, schedule_list_frag, "Schedules");
            transaction.addToBackStack(null);
            transaction.commit();

        }

        if (v.getId() == R.id.chat_lay) {
            Bundle bundle = new Bundle();
            ChatsFragment chatlistfrag = new ChatsFragment();
            Constans.fromType = "Chat";
            bundle.putString("value", "2");
            bundle.putString("userid", "0");
            chatlistfrag.setArguments(bundle);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.school_dashbord_fram, chatlistfrag);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();
        }

        if (v.getId() == R.id.attendence_lay) {
            // CreateClass schedule_list_frag = new CreateClass();
            AttendenceList listFragment = new AttendenceList();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Constans.fromType = "Attendence";
            Bundle args = new Bundle();
            args.putString("fromType", "Activities");
            listFragment.setArguments(args);
            transaction.replace(R.id.school_dashbord_fram, listFragment);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();


        }
        if (v.getId() == R.id.maneger_lay) {
            // CreateClass schedule_list_frag = new CreateClass();
            ShowManagersFrag listfrag = new ShowManagersFrag();
            Constans.fromType = "Manager";
            Bundle args = new Bundle();
            args.putString("fromType", value);
            listfrag.setArguments(args);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.school_dashbord_fram, listfrag);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.home_school_side:

                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }
                }
                return true;

            case R.id.payment_school:
                if (!(getCurrentFrag() instanceof InvoiceFrag)) {
                    InvoiceFrag invoice_frag = new InvoiceFrag();
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.school_dashbord_fram, invoice_frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                return true;


            case R.id.Profilesc:
                if (!(getCurrentFrag() instanceof SchoolProfFrag)) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.school_dashbord_fram, new SchoolProfFrag())
                            .addToBackStack(Constans.fromType)
                            .commit();
                }
                return true;
            case R.id.meassge_school:
                if (!(getCurrentFrag() instanceof ChatsFragment)) {
                    Bundle bundle = new Bundle();
                    ChatsFragment chatlistfrag = new ChatsFragment();
                    Constans.fromType = "Chat";
                    bundle.putString("value", "2");
                    bundle.putString("userid", "0");
                    chatlistfrag.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.school_dashbord_fram, chatlistfrag)
                            .addToBackStack(Constans.fromType)
                            .commit();
                }
                return true;

        }

        return false;
    }


    @Override
    public void onBackPressed() {
        text_create.setText(textget);
        navigation.getMenu().getItem(0).setChecked(true);
        super.onBackPressed();


    }
    public Fragment getCurrentFrag() {
        Fragment frag = getSupportFragmentManager().findFragmentById(R.id.school_dashbord_fram);
        return frag;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.crate_activity_scl);
        Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.frag_frame_eve);
        Fragment fragment2 = getSupportFragmentManager().findFragmentById(R.id.frame_team);

        if (fragment instanceof CostFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        if (fragment1 instanceof EditActivityFrag) {
            fragment1.onActivityResult(requestCode, resultCode, data);
        }
        if (fragment1 instanceof RecurrenceFrag) {
            fragment1.onActivityResult(requestCode, resultCode, data);
        }
        if (fragment1 instanceof EditPlayerFrag) {
            fragment1.onActivityResult(requestCode, resultCode, data);
        }
        if (fragment2 instanceof EditPlayerFrag) {
            fragment2.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getschoolprofile() {
        final Dialog dialog = AppUtils.showProgress(SchoolDashboard.this);
        APIService mAPIService = AppUtils.getAPIService(SchoolDashboard.this);
        Call<AddEvent> getApi;
        getApi = mAPIService.getschoolprofile(Constans.getFamilyId(SchoolDashboard.this));
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body().getBody());
                    editor.putString("profileData", json);
                    editor.apply();
                    /*event_zip_activity.setText( response.body().getBody().getZipcodee());
                    event_country_activity.setText( response.body().getBody().getCountry());
                    address_activity.setText( response.body().getBody().getAddress());*/

                    Log.e("getschoolprofile", "onResponse: " + response.body().getBody().getZipcodee());
                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(SchoolDashboard.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(SchoolDashboard.this);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(SchoolDashboard.this, "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void logoutApi() {
        final Dialog dialog = AppUtils.showProgress(SchoolDashboard.this);
        APIService mAPIService = AppUtils.getAPIService(SchoolDashboard.this);
        Call<AddEvent> getApi;
        getApi = mAPIService.logout(Constans.getFamilyId(SchoolDashboard.this));
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    Constans.clearSp(SchoolDashboard.this);
                    Intent intent2 = new Intent(SchoolDashboard.this, LogInActivity.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent2);
                    finish();
                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(SchoolDashboard.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(SchoolDashboard.this);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(SchoolDashboard.this, "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

}
package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

public class ViewManagerFrag extends Fragment {
    String ManagerId = "", fromType = "", agr = "", result = "", image_path = "";
    TextView text_title, gellary_clickm;
    ImageView gallback;

    CircleImageView imagesetup_m;
    private static final int GALLERY_REQUEST_CODE = 1;
    EditText manager_name, manager_emaile, manager_mobile, manager_description;
    View view;
    Button managersave_btn;
    String id = "", manager_name_get, manager_email_get, manager_mobile_get, manager_description_get;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (!bundle.isEmpty()) {
            ManagerId = bundle.getString("id", "");
            agr = bundle.getString("edit", "");
        }
        if (agr.equals("")) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.viewmanger, container, false);
        } else {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.edit_manager, container, false);
        }
        manager_name = view.findViewById(R.id.manager_namem);
        manager_emaile = view.findViewById(R.id.mamager_emailm);
        manager_mobile = view.findViewById(R.id.manager_mobilem);
        manager_description = view.findViewById(R.id.manager_descriptionm);
        imagesetup_m = view.findViewById(R.id.imagesetup_mm);
        gellary_clickm = view.findViewById(R.id.gellary_clickme);
        managersave_btn = view.findViewById(R.id.managersave_btn);


        fromType = bundle.getString("fromType", "");
        Log.e("argsargs", "onCreateView: " + ManagerId);
        text_title = view.findViewById(R.id.text_title);
        text_title.setText(fromType);

        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        gellary_clickm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
            }
        });
        managersave_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager_name_get = manager_name.getText().toString().trim();
                manager_mobile_get = manager_mobile.getText().toString().trim();
                manager_description_get = manager_description.getText().toString().trim();
                if (!manager_name_get.equals("")) {
                    if (!manager_mobile_get.equals("")) {
                        if (!manager_description.equals("")) {
//                            if (!image_path.equals("")) {
                                update_mangaer();

                          /*  } else {

                                Toast.makeText(getActivity(), "Please upload profile image", Toast.LENGTH_SHORT).show();
                            }*/

                        } else {

                            manager_mobile.setError("Please enter description");
                        }

                    } else {
                        manager_mobile.setError("Please enter a Mobile number");
                    }

                } else {
                    manager_name.setError("Please enter a Name");
                }

            }
        });


        getManagerDetails();
        return view;
    }

    private void pickFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {


            try {
                Uri uri = data.getData();
                image_path = getPath(uri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                imagesetup_m.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }

    private String getPath(Uri uri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(getActivity(), uri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();

        } catch (SecurityException ignored) {

        }
        return result;

    }

    private void getManagerDetails() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/getManagerDetails";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("body");
                    id = jsonObject1.getString("id");
                    JSONObject jsonObject2 = jsonObject1.getJSONObject("usersDetail");

                    manager_name.setText(jsonObject1.getString("firstName"));
                    manager_emaile.setText(jsonObject1.getString("email"));
                    manager_mobile.setText(jsonObject1.getString("phoneNumber"));
                    manager_description.setText(jsonObject1.getString("description"));

                    Glide.with(getActivity()).load(Constans.BASEURL + jsonObject2.getString("profileImage")).into(imagesetup_m);

                    Log.e("iddididid", "onResponse: " + id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userId", ManagerId);

                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getActivity()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }

    private void update_mangaer() {


        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        dialog.show();

        //  Call<AddEvent> mAPIService;
        MultipartBody.Part fileToUpload = null;
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(image_path);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            fileToUpload = MultipartBody.Part.createFormData("image", "", requestBody);
        }

        /*File file = new File(image_path);

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);*/

        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), manager_name_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), manager_mobile_get);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), manager_description_get);


        Map<String, RequestBody> map = new HashMap<>();
        map.put("userId", id1);
        map.put("firstName", id2);
        map.put("phoneNumber", id3);
        map.put("description", id4);

        mAPIService.updateProfileManager(map, fileToUpload).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());

                if (response.code() == 200) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Profile updated successfully", Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack();
                    ShowManagersFrag listfrag = new ShowManagersFrag();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_frame_man, listfrag)
                            .commit();
                } else {
                    if (response.code() == 400) {
                        dialog.dismiss();
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


}

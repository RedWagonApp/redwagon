package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.SchoolAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class InstituteFrag extends Fragment implements View.OnClickListener {
    private Button continiue_instude;
    private EditText school, contactname, cityname, statename, zipecode, Contractnumber;
    private  String school_get, contactname_get, cityname_get, statename_get, zipecode_get, Contractnumber_get;

    private LinearLayout nolisted;
    TextView not_found_txt;
    RecyclerView coach_rv;
    Dialog coachdialog;
    SchoolAdapter.AdapterCallback adapterCallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.instude_and_instude_frag, container, false);
        FamilyDashboardActivity.title.setText("Designated School/Institute");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);


        findViewId(view);


        continiue_instude.setOnClickListener(this);
        school.setOnClickListener(this);

        adapterCallback = new SchoolAdapter.AdapterCallback() {
            @Override
            public void onMethodCallback(String name, String id,int pos) {
                coachdialog.dismiss();
                school.setText(name);
            }
        };

        return view;
    }

    private void findViewId(View view) {
        continiue_instude = view.findViewById(R.id.continiue_instude);
        school = view.findViewById(R.id.school);
        contactname = view.findViewById(R.id.contactname);
        cityname = view.findViewById(R.id.cityname);
        statename = view.findViewById(R.id.statename);
        zipecode = view.findViewById(R.id.zipecode);
        Contractnumber = view.findViewById(R.id.Contractnumber);
        nolisted = view.findViewById(R.id.nolisted);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.continiue_instude) {
            Validation();

        }
        if (view.getId() == R.id.school) {
            getCoachDialog();

        }
    }

    private void getCoachDialog() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Class");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        not_found_txt = (TextView) coachdialog.findViewById(R.id.not_found_txt);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);

        getList(adapterCallback, dialog);


        coachdialog.show();
    }


        private void getList(final SchoolAdapter.AdapterCallback callback, final Dialog dialog) {

        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getSchool();


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                    } else {
                        not_found_txt.setVisibility(View.GONE);
                        coach_rv.setVisibility(View.VISIBLE);
                        SchoolAdapter customAdapter = new SchoolAdapter(getContext(), responseBody, adapterCallback, "Student");
                        coach_rv.setAdapter(customAdapter);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                not_found_txt.setVisibility(View.VISIBLE);
                coach_rv.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void Validation() {

        school_get = school.getText().toString().trim();
//        contactname_get= contactname.getText().toString().trim();
//        cityname_get= cityname.getText().toString().trim();
//        statename_get= statename.getText().toString().trim();
//        zipecode_get= zipecode.getText().toString().trim();
//        Contractnumber_get= Contractnumber.getText().toString().trim();

        if (!school_get.equals("")) {
            signUpService();
//            if (!contactname_get.equals("") && contactname_get.length() >= 2) {
//
//                if (!cityname_get.equals("") && cityname_get.length()>=2) {
//
//
//                    if (!statename_get.equals("")&&statename_get.length()>=2) {
//
//                        if (!zipecode_get.equals("")&&zipecode_get.length()>=2) {
//
//                            if (!Contractnumber_get.equals("")&&Contractnumber_get.length()>=2) {
//
//
//
//                                signUpService();
//
//                            } else {
//
//                                Contractnumber.setError("Please enter a Contract number");
//                            }
//                        } else {
//                            zipecode.setError("Please enter Zipe Code");
//                        }
//
//                    } else {
//                        statename.setError("Please enter State Name");
//                    }
//                } else {
//                    cityname.setError("Please enter City Name");
//                }
//            } else {
//                contactname.setError("Please enter the Contactname");
//            }
        } else {
            Toast.makeText(getActivity(), "Please select school", Toast.LENGTH_SHORT).show();
        }


    }

    private void signUpService() {


        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fram_dash, new CharityListFrag(), "Perfer to Donate to a Charity?");
        transaction.addToBackStack(null);
        transaction.commit();

    }
}

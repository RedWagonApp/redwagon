package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class RecurrenceFrag extends Fragment implements View.OnClickListener, OnSelectDateListener {
    String select_class_scl_get = "0", cost_activity_get = "0";
    Button save__recursee;
    ImageView start_ever, enddate_, gallback;
    CheckBox monImg, tueImg, wedImg, thuImg, friImg, satImg, sunImg;
    TextView text_title, monTxt, tueTxt, wedTxt, thuTxt, friTxt, satTxt, sunTxt, start_dateeven, end_eventdate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String court_activity_get = "", eventNameStr = "", teamNameStr = "", coachName = "", eventLocationStr, eventTimeZoneStr, eventCountryStr, eventZipCodeStr, eventDateTimeStr, eventDurationStr;
    Calendar startDate = Calendar.getInstance();
    ArrayList<String> weedaysArr = new ArrayList<>();
    ArrayList<String> datesArr = new ArrayList<>();
    ArrayList<String> uriArr = new ArrayList<>();
    APIService mAPIService;
    private String fromType;
    private static final int GALLERY_REQUEST_CODE = 1;
    private ArrayList<String> imagesArr = new ArrayList<>();
    private String image_path = "", teamId = "0", coachId = "0";
    String selectedDate = "", selectedEndDate = "", dayOfWeek = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.recurrence_frag, container, false);
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Recurrence");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        Bundle args = getArguments();

        eventNameStr = args.getString("eventNameStr", "");
        eventLocationStr = args.getString("eventLocationStr", "");
        eventTimeZoneStr = args.getString("eventTimeZoneStr", "");
        eventCountryStr = args.getString("eventCountryStr", "");
        eventZipCodeStr = args.getString("eventZipCodeStr", "");
        eventDateTimeStr = args.getString("eventDateTimeStr", "");
        eventDurationStr = args.getString("eventDurationStr", "");
        court_activity_get = args.getString("court_activity_get", "");
        image_path = args.getString("image_path", "");
        teamNameStr = args.getString("teamName", "");
        coachName = args.getString("coachName", "");
        teamId = args.getString("teamId", "");
        coachId = args.getString("coachId", "");
        imagesArr = (ArrayList<String>) args.getStringArrayList("imagesArr");
        uriArr = (ArrayList<String>) args.getStringArrayList("uriArr");
        dayOfWeek = args.getString("dayOfWeek", "");

        fromType = args.getString("fromType", "");
        //activity
        select_class_scl_get = args.getString("select_class_scl_get", "");
        cost_activity_get = args.getString("cost_activity_get", "");


        if (!fromType.equals("Activities")) {
            LinearLayout linearLayout = view.findViewById(R.id.linear_main_bar);
            linearLayout.setVisibility(View.GONE);
        }
        save__recursee = view.findViewById(R.id.save__recurse);
        monImg = view.findViewById(R.id.mon_img);
        tueImg = view.findViewById(R.id.tue_img);
        wedImg = view.findViewById(R.id.wed_img);
        thuImg = view.findViewById(R.id.thu_img);
        friImg = view.findViewById(R.id.fri_img);
        satImg = view.findViewById(R.id.sat_img);
        sunImg = view.findViewById(R.id.sun_img);
        start_ever = view.findViewById(R.id.start_ever);
        enddate_ = view.findViewById(R.id.enddate_);

        monTxt = view.findViewById(R.id.mon_txt);
        tueTxt = view.findViewById(R.id.tue_txt);
        wedTxt = view.findViewById(R.id.wed_txt);
        thuTxt = view.findViewById(R.id.thu_txt);
        friTxt = view.findViewById(R.id.fri_txt);
        satTxt = view.findViewById(R.id.sat_txt);
        sunTxt = view.findViewById(R.id.sun_txt);
        start_dateeven = view.findViewById(R.id.start_dateeven);
        end_eventdate = view.findViewById(R.id.end_eventdate);
        try {
            selectedDate = reFormatDate(eventDateTimeStr);
            selectedEndDate = reFormatDate(eventDateTimeStr);
            start_dateeven.setText(selectedDate);
            end_eventdate.setText(selectedEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        weedaysArr = convertString(dayOfWeek);
        enddate_.setOnClickListener(this);
        start_ever.setOnClickListener(this);
        save__recursee.setOnClickListener(this);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        monImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weekChecked(monImg, monTxt);
            }
        });

        tueImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weekChecked(tueImg, tueTxt);
            }
        });

        wedImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weekChecked(wedImg, wedTxt);
            }
        });

        thuImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weekChecked(thuImg, thuTxt);
            }
        });

        friImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weekChecked(friImg, friTxt);
            }
        });

        satImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weekChecked(satImg, satTxt);
            }
        });

        sunImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weekChecked(sunImg, sunTxt);
            }
        });

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.enddate_) {

            openDaysPicker();

        }
        if (v.getId() == R.id.save__recurse) {
            if (weedaysArr.isEmpty()) {
                Toast.makeText(getActivity(), "Please select weekdays", Toast.LENGTH_SHORT).show();
            } else if (selectedDate.equals("")) {
                Toast.makeText(getContext(), "Please select start date", Toast.LENGTH_SHORT).show();
            } else if (selectedEndDate.equals("")) {
                Toast.makeText(getContext(), "Please select end date", Toast.LENGTH_SHORT).show();
            } else {
                API_AddGame();
            }
        }


    }

    private void API_AddGame() {

        final Dialog dialog = AppUtils.showProgress(getActivity());

        APIService mAPIService = AppUtils.getAPIService(getContext());
        String joined = AppUtils.convertToString(weedaysArr);
        if (datesArr.size() == 0) {
            datesArr.add(end_eventdate.getText().toString());
        }
        String dates = AppUtils.convertToString(datesArr);

//        File file = new File(image_path);
//        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);

        MultipartBody.Part part = null;
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(image_path);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }
        RequestBody id1;
        if (Constans.getLoginType(getActivity()).equals("Manager")) {
            id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.getSchlID(getContext()));
        } else {
            id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.getFamilyId(getContext()));
        }


        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), eventNameStr);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), eventLocationStr);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), eventTimeZoneStr);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), eventCountryStr);
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), eventZipCodeStr);
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), eventDurationStr);
        RequestBody id8 = RequestBody.create(MediaType.parse("text/plain"), eventDateTimeStr);
        RequestBody id9 = RequestBody.create(MediaType.parse("text/plain"), start_dateeven.getText().toString());
        RequestBody id10 = RequestBody.create(MediaType.parse("text/plain"), end_eventdate.getText().toString());
        RequestBody id11 = RequestBody.create(MediaType.parse("text/plain"), joined);
        RequestBody id12 = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody id13 = RequestBody.create(MediaType.parse("text/plain"), "blue");
        RequestBody id14 = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody id15 = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody id16 = RequestBody.create(MediaType.parse("text/plain"), cost_activity_get);
        RequestBody id17 = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody id18 = RequestBody.create(MediaType.parse("text/plain"), Constans.fromType);
        RequestBody id19 = RequestBody.create(MediaType.parse("text/plain"), court_activity_get);
        RequestBody id20 = RequestBody.create(MediaType.parse("text/plain"), teamId);
        RequestBody id21 = RequestBody.create(MediaType.parse("text/plain"), teamNameStr);
        RequestBody id22 = RequestBody.create(MediaType.parse("text/plain"), coachId);
        RequestBody id23 = RequestBody.create(MediaType.parse("text/plain"), coachName);
        RequestBody id24 = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody id25 = RequestBody.create(MediaType.parse("text/plain"), dates);

        Map<String, RequestBody> map = new HashMap<>();
        map.put("userId", id1);
        map.put("name", id2);
        map.put("address", id3);
        map.put("timeZone", id4);
        map.put("country", id5);
        map.put("Zipcode", id6);
        map.put("duration", id7);
        map.put("dateTime", id8);
        map.put("startDate", id9);
        map.put("endDate", id10);
        map.put("weekDay", id11);
        map.put("arriveEarly", id12);
        map.put("flagColor", id13);
        map.put("opponent", id14);
        map.put("homeAway", id15);
        map.put("cost", id16);
        map.put("classId", id17);
        map.put("type", id18);
        map.put("court", id19);
        map.put("teamId", id20);
        map.put("teamName", id21);
        map.put("coachId", id22);
        map.put("coachName", id23);
        map.put("opponentId", id24);
        map.put("dateRange", id25);

        mAPIService.addSchoolEvent(map, part).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {


                    addEventImages(response.body().getBody().getId().toString());
                    /*} else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //getActivity().onBackPressed();
                        EventListFragment createTeamFrag = new EventListFragment();
                        FragmentManager manager = getActivity().getSupportFragmentManager();
                        FragmentTransaction trans = manager.beginTransaction();
                        trans.replace(R.id.frag_frame_eve, createTeamFrag);
                        trans.commit();
                        if (fromType.equals("Activities")) {
                            getActivity().getSupportFragmentManager().popBackStackImmediate(2, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        } else {
                            getActivity().onBackPressed();
                        }

                    }*/
                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }


                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });

    }

    private void addEventImages(String id) {
        final Dialog dialog = AppUtils.showProgress(getActivity());

        APIService mAPIService = AppUtils.getAPIService(getContext());
        String type_img;
        RequestBody txt = null;
        MultipartBody.Part[] imagesParts;
        if (imagesArr != null && imagesArr.size() != 0) {
            imagesParts = new MultipartBody.Part[imagesArr.size()];
            type_img = AppUtils.convertToString(uriArr);
            for (int index = 0; index < imagesArr.size(); index++) {
                Log.d(TAG, "requestUploadSurvey: survey image " + index + "  " + imagesArr.get(index));
                File file = new File(imagesArr.get(index));

                RequestBody body = RequestBody.create(MediaType.parse("image/*"), file);

                imagesParts[index] = MultipartBody.Part.createFormData("image", file.getName(), body);
            }
        } else {
            imagesParts = new MultipartBody.Part[1];
            type_img = "1";
            MultipartBody.Part part = null;
            if (!image_path.equals("")) {
                try {
                    File file = new File(image_path);
                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    imagesParts[0] = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else {

                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                imagesParts[0] = MultipartBody.Part.createFormData("image", "", requestBody);
            }

        }

        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), type_img);

        Map<String, RequestBody> map = new HashMap<>();
        map.put("id", id1);
        map.put("type_img", id2);
        mAPIService.eventImage(map, imagesParts).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    //  Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    //getActivity().onBackPressed();
                    EventListFragment createTeamFrag = new EventListFragment();
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.replace(R.id.frag_frame_eve, createTeamFrag);

                    trans.commit();

                    if (fromType.equals("Activities")) {
                        getActivity().getSupportFragmentManager().popBackStackImmediate(2, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    } else {
                        getActivity().onBackPressed();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }


                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });

    }


    private void openDaysPicker() {

        Calendar min = Calendar.getInstance();
        Calendar max = toCalendar(selectedDate);
        min.add(Calendar.DATE, -1);
        List<Calendar> selectedDate = new ArrayList<>();
        selectedDate.add(max);
        // min.add(Calendar.DATE, -1);
        DatePickerBuilder daysBuilder = new DatePickerBuilder(getActivity(), this)
                .setPickerType(CalendarView.RANGE_PICKER)
                .setDate(Calendar.getInstance())
                .setHeaderColor(R.color.appclrcodew)
                .setSelectionColor(R.color.appclrcodew)
                .setTodayLabelColor(R.color.appclrcodew)
                .setDialogButtonsColor(R.color.appclrcodew)
                .setNavigationVisibility(View.VISIBLE)
                .setMinimumDate(max)
                .setSelectedDays(selectedDate)
                .setDisabledDays(AppUtils.getDisabledDays(weedaysArr));


        daysBuilder.build().show();


    }

    private String weekChecked(CheckBox checkBox, TextView view) {
        if (checkBox.isChecked()) {

            view.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
            if (!weedaysArr.contains(view.getText().toString())) {
                weedaysArr.add(view.getText().toString());
            }

        } else {
            if (weedaysArr.contains(view.getText().toString())) {
                weedaysArr.remove(view.getText().toString());
            }

            view.setTextColor(getActivity().getResources().getColor(R.color.gray));

        }
        return view.getText().toString();
    }

    private String reFormatDate(String dateIn) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:mm");
        Date dateObj = sdf.parse(dateIn);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(dateObj);
    }

    private ArrayList<String> convertString(String weeks) {

        String[] elements = weeks.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);

        if (listOfString.contains("MON")) {
            monImg.setChecked(true);
            monTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("TUE")) {
            tueImg.setChecked(true);
            tueTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("WED")) {
            wedImg.setChecked(true);
            wedTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("THU")) {
            thuImg.setChecked(true);
            thuTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("FRI")) {
            friImg.setChecked(true);
            friTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("SAT")) {
            satImg.setChecked(true);
            satTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }
        if (listOfString.contains("SUN")) {
            sunImg.setChecked(true);
            sunTxt.setTextColor(getActivity().getResources().getColor(R.color.appclrcodew));
        }

        return listOfString;
    }

    @Override
    public void onSelect(List<Calendar> calendar) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (int i = 0; i < calendar.size(); i++) {
            Log.e(TAG, "onSelect: " + calendar.get(i).getTime());
            String newDate = format.format(calendar.get(i).getTime());
            datesArr.add(newDate);
            selectedEndDate = newDate;
            end_eventdate.setText(selectedEndDate);
        }

    }

    private Date stringToDate(String dateStr) {

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = format.parse(dateStr);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Calendar toCalendar(String dateStr) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(stringToDate(dateStr));
        return cal;
    }


}

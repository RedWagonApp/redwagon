package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ChildAmountAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ChildFundAmountFrag extends Fragment implements View.OnClickListener {

    RecyclerView childamountrecycler;
    TextView no_result_tv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.child_found_amount, container, false);
        FamilyDashboardActivity.title.setText("Child Fund Amount");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        childamountrecycler = view.findViewById(R.id.childamountrecycler);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        childamountrecycler.setLayoutManager(linearLayoutManager);
        GetChildListAmount();
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.trasfer_click) {


        }
    }

    private void GetChildListAmount() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi = mAPIService.GetChildListAmount(Constans.getFamilyId(getActivity()));

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    if(responseBody.size()!=0){
                        no_result_tv.setVisibility(View.GONE);
                        childamountrecycler.setVisibility(View.VISIBLE);
                        ChildAmountAdapter childAmountAdapter = new ChildAmountAdapter(getActivity(), responseBody);
                        childamountrecycler.setAdapter(childAmountAdapter);
                    }
                    else{
                        no_result_tv.setVisibility(View.VISIBLE);
                        childamountrecycler.setVisibility(View.GONE);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }
}
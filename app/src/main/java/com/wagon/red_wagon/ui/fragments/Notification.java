package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.NotificationAdapter;
import com.wagon.red_wagon.model.NotificationBody;
import com.wagon.red_wagon.model.NotificationModel;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class Notification extends Fragment {
    RecyclerView recycler_notification;
    TextView no_result_tv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.notification, container, false);
        recycler_notification = view.findViewById(R.id.recycler_notification);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        FamilyDashboardActivity.title.setText("Notifications");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

      /*  TextView text_title = view.findViewById(R.id.text_title);
        text_title.setText("Notifications");
        ImageView gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });*/
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycler_notification.setLayoutManager(linearLayoutManager);
        getNotificationList();

        return view;
    }

    private void getNotificationList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<NotificationModel> getApi;

        getApi = mAPIService.getNotification(Constans.getFamilyId(getActivity()));


        getApi.enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, retrofit2.Response<NotificationModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();

                try {

                    if (response.code() == 200) {

                        List<NotificationBody> responseBody = response.body().getBody();

                        if (responseBody.size() != 0) {
                            no_result_tv.setVisibility(View.GONE);
                            recycler_notification.setVisibility(View.VISIBLE);
                            NotificationAdapter notificationAdapter = new NotificationAdapter(getActivity(), responseBody);
                            recycler_notification.setAdapter(notificationAdapter);

                        }
                        else{
                            no_result_tv.setVisibility(View.VISIBLE);
                            recycler_notification.setVisibility(View.GONE);
                        }

                    } else if (response.code() == 400) {
                        if (!response.isSuccessful()) {
                            try {

                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");

                                Log.e("MessageM ", userMessage);
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    } else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

}

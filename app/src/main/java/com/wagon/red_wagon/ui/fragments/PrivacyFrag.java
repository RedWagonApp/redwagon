package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.activities.LogInActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;

public class PrivacyFrag extends Fragment {
    Button submit, cancel;
    RadioButton radio_button;
    String famly_rg_get, surname_rg_get, email_rg_get, password_rg_get, phone_number_rg_get, filePath, ccp;
    TextView text_title;
    int mStatusCode;
    APIService mAPIService;
    String type, userName;
    Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.privacy_frag, container, false);
            mAPIService = AppUtils.getAPIService(getContext());

        text_title = view.findViewById(R.id.text_title);
        submit = view.findViewById(R.id.submit);
        cancel = view.findViewById(R.id.cancel);
        radio_button = view.findViewById(R.id.radio_button);
        text_title.setText("Family Registration");


        Bundle args = getArguments();

        famly_rg_get = args.getString("firstName", "");
        surname_rg_get = args.getString("surname_rg_get", "");
        email_rg_get = args.getString("email", "");
        phone_number_rg_get = args.getString("phone_number_rg_get", "");
        password_rg_get = args.getString("password_rg_get", "");
        ccp = args.getString("ccp", "");
        filePath = args.getString("filePath", "");

        radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!radio_button.isSelected()) {
                    radio_button.setChecked(true);
                    radio_button.setSelected(true);

                } else {


                    radio_button.setChecked(false);
                    radio_button.setSelected(false);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radio_button.isSelected()) {

                    ApiService();

                } else {
                    Toast.makeText(getActivity(), "Please accepet the checks to be carried out", Toast.LENGTH_SHORT).show();

                }


            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LogInActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }

    public void ApiService() {
        dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/signUp";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    JSONObject jsonObject1 = jsonObject.getJSONObject("message");
                    String email = jsonObject1.getString("email");
                    String token = jsonObject1.getString("token");
                    String id = jsonObject1.getString("id");
                    type = jsonObject1.getString("gender");
                    userName = jsonObject1.getString("firstName");
                    String phoneNumber = jsonObject1.getString("phoneNumber");

                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor.putString("Token", token);
                    editor.putString("FMID", id);
                    editor.putString("type", type);
                    editor.putString("userName", userName);
                    editor.apply();
                    uploadPic();
                    // Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    //next screen open


                    Log.e("jsonObject", "jsonObject" + email + "\n" + phoneNumber);

                } catch (JSONException e) {

                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                try {
                    mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 400) {
                        Toast.makeText(getActivity(), "User already exists", Toast.LENGTH_SHORT).show();

                    }
                    if (mStatusCode == 401) {
                        Toast.makeText(getContext(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                    }
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception r) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email_rg_get);
                params.put("password", password_rg_get);
                params.put("phoneNumber", ccp + phone_number_rg_get);
                params.put("firstName", famly_rg_get);
                params.put("userName", surname_rg_get);
                params.put("gender","Family");
                params.put("fbGoogleId", Constans.getFbGoogleId);
                params.put("deviceType","2");
                params.put("deviceToken",Constans.FCMToken(getActivity()));
                Log.e("params", "params" + params);
                return params;

            }

//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("Content-Type", "application/json; charset=UTF-8");
//                        params.put("token", Constans.Token);
//                        return params;
//                    }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    private void uploadPic() {


//        File file = new File(filePath);
//
//        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);


        MultipartBody.Part part  = null;
//        File file = new File(filePath);
//
//        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
        if (!filePath.equals("")) {
            try {
                File file = new File(filePath);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(filePath);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }
        mAPIService.prolieUplod(part).

                enqueue(new Callback<AddEvent>() {
                    @Override
                    public void onResponse( Call<AddEvent> call,  retrofit2.Response<AddEvent> response) {

                        dialog.dismiss();
                       try {
                           String img = response.body().getMessage();
                           SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
                           editor.putString("PicUrl", img);
                           editor.apply();
                           FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                           FragmentTransaction transaction = fragmentManager.beginTransaction();
                           transaction.replace(R.id.parivacyfrag, new ParentResgisterFrag());
                           transaction.addToBackStack(null);
                           transaction.commit();
                       }
                       catch (Exception e){

                       }
                    }

                    @Override
                    public void onFailure(Call<AddEvent> call, Throwable t) {
                        dialog.dismiss();
                        Log.e("Failure", "onFailure: ", t);
                    }
                });
    }


}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AttendenceListAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class AttendenceList extends Fragment implements View.OnClickListener {

    RecyclerView attendence_list_rv;
    ArrayList<Event_List_model> arrayList;
    String fromType = "";
    TextView no_result_tv, text_title;
    ImageView gallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.attendence_list_lay, container, false);

//        text_title.setText("Add Manager");
        findView(view);
        Bundle bundle = getArguments();
        try {

            fromType = bundle.getString("fromType", "");
        } catch (NullPointerException e) {

        }
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Attendence");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    private void findView(View view) {
        arrayList = new ArrayList<>();
        attendence_list_rv = view.findViewById(R.id.attendence_list_rv);
        no_result_tv = view.findViewById(R.id.no_result_tv);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        attendence_list_rv.setLayoutManager(linearLayoutManager);


    }

    @Override
    public void onClick(View v) {
    }

    private void getList(final String from) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi = null;
        if (from.equals("Classes")) {
            if (Constans.getLoginType(getActivity()).equals("Manager")) {
                getApi = mAPIService.getclassList(Constans.getSchlID(getActivity()));
            } else {
                getApi = mAPIService.getclassList(Constans.getFamilyId(getActivity()));
            }

        } else {
            if (Constans.getLoginType(getActivity()).equals("Manager")) {
                getApi = mAPIService.getEventDetail(Constans.getSchlID(getActivity()), from);
            } else {
                getApi = mAPIService.getEventDetail(Constans.getFamilyId(getActivity()), from);
            }

        }

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                try {
                    if (response.code() == 200) {

                        List<Body> responseBody = response.body().getBody();

                        if (responseBody.size() == 0) {
                            no_result_tv.setVisibility(View.VISIBLE);
                            attendence_list_rv.setVisibility(View.GONE);
                        } else {
                            no_result_tv.setVisibility(View.GONE);
                            attendence_list_rv.setVisibility(View.VISIBLE);
                            AttendenceListAdapter mAdapter = new AttendenceListAdapter(getActivity(), responseBody, from);
                            attendence_list_rv.setAdapter(mAdapter);

                        }
                    } else if (response.code() == 400) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        attendence_list_rv.setVisibility(View.GONE);
                        if (!response.isSuccessful()) {

                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");

                                Log.e("MessageM ", userMessage);
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                    else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                no_result_tv.setVisibility(View.VISIBLE);
                attendence_list_rv.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "onResume: ");
        getList(fromType);
    }


}


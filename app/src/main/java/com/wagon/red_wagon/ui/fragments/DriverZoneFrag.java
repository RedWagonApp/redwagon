package com.wagon.red_wagon.ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class DriverZoneFrag extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    Circle mapCircle;
    List arrayList = new ArrayList();
    private static final int REQUEST_CODE_PERMISSIONS = 121;
    private Button continue_myvichle;
    private double Latitude, Longitude;

    private static final int REQUEST_LOCATION = 1;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private String latitude, longitude, add, value = "";
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private double lat, longi;
    private LatLng sydney;
    TextView text_title;
    LatLng latLng;

    private static View view;
    Body responseBody;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.pickup_zone, container, false);
        } catch (IllegalArgumentException e) {

        }

        FamilyDashboardActivity.title.setText("Preferred Pick Up Zones");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);


        requestLocationPermission();


        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        //Check gps is enable or not

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Write Function To enable gps

            OnGPS();

            //GPS is already on then
        } else {
            //GPS is already On then
            getLocation();
            //GPS is already On then
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        Bundle args = getArguments();
        value = args.getString("value", "");

        SupportMapFragment mapFragment;
        mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        configureCameraIdle();


        continue_myvichle = view.findViewById(R.id.continue_myvichle);

        continue_myvichle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("latlatlat", "=" + Constans.lat);
                Log.e("Constans.longg", "=" + Constans.longg);
                if (Constans.longg == 0) {
                    Toast.makeText(getActivity(), "Try again", Toast.LENGTH_SHORT).show();
                } else {
                    // ApiService(Constans.lat, Constans.longg);
                    latlongservice(Constans.lat, Constans.longg);
                }


            }
        });

        return view;

    }

    private void requestLocationPermission() {

        boolean foreground = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (foreground) {
            boolean background = ActivityCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;

            if (background) {
                // handleLocationUpdates();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSIONS) {

            boolean foreground = false, background = false;

            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    //foreground permission allowed
                    if (grantResults[i] >= 0) {
                        foreground = true;
                        Toast.makeText(getApplicationContext(), "Foreground location permission allowed", Toast.LENGTH_SHORT).show();
                        continue;
                    } else {
                        Toast.makeText(getApplicationContext(), "Location Permission denied", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }

                if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                    if (grantResults[i] >= 0) {
                        foreground = true;
                        background = true;
                        Toast.makeText(getApplicationContext(), "Background location location permission allowed", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Background location location permission denied", Toast.LENGTH_SHORT).show();
                    }

                }
            }

//            if (foreground) {
//                if (background) {
//                    handleLocationUpdates();
//                } else {
//                    handleForegroundLocationUpdates();
//                }
//            }
        }
    }

    private void ApiService(final double lat, final double longg) {
        final Dialog dialog = AppUtils.showProgress(getActivity());

        String url = Constans.BASEURL + "api/carlatlogUpdate";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);

                try {
                    dialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    String meassage = jsonObject.getString("message");
                    // Toast.makeText(getActivity(), , Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                try {
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception e) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(longg));
                params.put("location", Constans.location);
                Log.e("params", "params" + params);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        try {
            mMap.setOnCameraIdleListener(onCameraIdleListener);
            sydney = new LatLng(lat, longi);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.setMyLocationEnabled(true);
            CameraPosition camPos = new CameraPosition.Builder()
                    .target(new LatLng(lat, longi))
                    .zoom(1)
                    .tilt(10)
                    .build();
            CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
            mMap.animateCamera(camUpd3);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));

//
//                if(mapCircle!=null){
//                    mapCircle.remove();
//                }


        } catch (SecurityException e) {

        }
    }


    private void configureCameraIdle() {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                latLng = mMap.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(getActivity());

                try {
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        String locality = addressList.get(0) ==null?"":addressList.get(0).getAddressLine(0);
                        String country = addressList.get(0).getCountryName();
                        if (!locality.isEmpty() && !country.isEmpty())
                            //esutText.setText(locality + "  " + country);
                            Log.e("locality", "" + locality + "\n" + country);

                        Log.e(" latLng.longitude", "" + latLng.latitude + "\n" + latLng.longitude);
                        Constans.lat = latLng.latitude;
                        Constans.location = locality;
                        Constans.longg = latLng.longitude;

                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                CircleOptions circleOptions = new CircleOptions()
//                        .center(latLng)
//                        .radius(5000)
//                        .strokeWidth(2)
//                        .strokeColor(Color.parseColor("#ADC02026"))
//                        .fillColor(Color.parseColor("#ADC02026"));

                // Supported formats are: #RRGGBB #AARRGGBB
                //   #AA is the alpha, or amount of transparency
                Log.e("circleOptions", "onCameraIdle: " + latLng);
//                if (mapCircle != null) {
//                    mapCircle.remove();
//                }
//                mapCircle = mMap.addCircle(circleOptions);


            }
        };

    }


    private void getLocation() {

        //Check Permissions again

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location LocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps != null) {
                lat = LocationGps.getLatitude();
                longi = LocationGps.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                //showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else if (LocationNetwork != null) {
                lat = LocationNetwork.getLatitude();
                longi = LocationNetwork.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                // showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else if (LocationPassive != null) {
                lat = LocationPassive.getLatitude();
                longi = LocationPassive.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                //showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else {
                Toast.makeText(getActivity(), "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }

            //Thats All Run Your App


        }

    }

    private void OnGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        try {
            Fragment fragment = (getFragmentManager().findFragmentById(R.id.mapFragment));
            if (fragment != null) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .remove(fragment)
                        .commit();
            }
        } catch (Exception e) {

        }
    }

    public void latlongservice(double lat, double longg) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<AddEvent> getApi;
//        Constans.lat = latLng.latitude;
//        Constans.location = locality;
//        Constans.longg = latLng.longitude;

        getApi = mAPIService.carlatlogUpdate(String.valueOf(Constans.lat), String.valueOf(Constans.longg), Constans.location);

        Log.e("onResponse", "lf;d-== " + Constans.lat + "\n" + Constans.longg);
//        2020-01-30 11:31:34.838 18799-18799/com.wagon.red_wagon E/ latLng.longitude: 30.244099416415192
//        75.85084620863199

        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    responseBody = response.body().getBody();
                    Log.e("responseBody", "onResponse: " + responseBody.getLatitude());
                    Log.e("getLongitude", "onResponse: " + responseBody.getLongitude());
                    SharedPreferences.Editor editor1 = getContext().getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor1.putString("userr", "Driver");
                    editor1.apply();
                    if (value.equals("2")) {
                        Intent intent = new Intent(getActivity(), FamilyDashboardActivity.class);
                        intent.putExtra("check", "4");
                        startActivity(intent);
                    } else {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fram_dash, new BankDetailsFrag(), "Bank Details");
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    Toast.makeText(getActivity(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();

                Log.e("Failure", "onFailure: ", t);
            }
        });
    }

}

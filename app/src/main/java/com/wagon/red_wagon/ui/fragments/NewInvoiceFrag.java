package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewInvoiceFrag extends Fragment implements View.OnClickListener {
    private Button send_invoice;
    private EditText due_date, invoice_title, amount_invoice, descaption_invoce;
    private TextView recipients_invoice;
    String recipients_invoice_get, due_date_get, invoice_title_get, amount_invoice_get, descaption_invoce_get;
    private TextView text_title;
    private ImageView gallback;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private ArrayList<String> emaillist;
    private ArrayList<String> arrayListName=new ArrayList<>();
    private ToggleButton toggleButton1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.new_invoice, container, false);
        text_title = view.findViewById(R.id.text_title);
        toggleButton1 = view.findViewById(R.id.toggleButton1);
        text_title.setText("New Invoices");

        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        try {
            Bundle args = getArguments();

            if (!args.isEmpty()) {
                emaillist = (ArrayList<String>) args.getStringArrayList("emaillist");
                arrayListName = (ArrayList<String>) args.getStringArrayList("arrayListName");
                Log.e("onCreateView", "arrayListName: " + arrayListName);
                Log.e("onCreateView", "emaillist: " + emaillist);
            }
        } catch (NullPointerException e) {

        }
        FindViewID(view);
        send_invoice.setOnClickListener(this);
        recipients_invoice.setOnClickListener(this);
        due_date.setOnClickListener(this);
        recipients_invoice.setOnClickListener(this);


        return view;
    }

    private void FindViewID(View view) {
        recipients_invoice = view.findViewById(R.id.recipients_invoice);
        send_invoice = view.findViewById(R.id.send_invoice);
        due_date = view.findViewById(R.id.due_date);
        invoice_title = view.findViewById(R.id.invoice_title);
        amount_invoice = view.findViewById(R.id.amount_invoice);
        descaption_invoce = view.findViewById(R.id.descaption_invoce);


    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.recipients_invoice) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.new_invoice_fram, new RecipientsFrag());

            transaction.commit();
        }
        if (v.getId() == R.id.due_date) {
            datePicker();
        }
        if (v.getId() == R.id.send_invoice) {
            try {
                due_date_get = due_date.getText().toString();
                invoice_title_get = invoice_title.getText().toString();
                amount_invoice_get = amount_invoice.getText().toString();
                descaption_invoce_get = descaption_invoce.getText().toString();

                if (arrayListName.size() != 0) {
                    if (!due_date_get.equals("")) {
                        if (!invoice_title_get.equals("")) {
                            if (!amount_invoice_get.equals("")) {
                                SendInoviceService();
                            } else {
                                amount_invoice.setError("");
                            }
                        } else {
                            invoice_title.setError("");
                        }
                    } else {
                        due_date.setError("");
                    }
                } else {
                    Toast.makeText(getActivity(), "Please select Player", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {

            }
        }
    }

    private void SendInoviceService() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi;

        getApi = mAPIService.invoiceEmail(Constans.getFamilyId(getActivity()), AppUtils.convertToString(emaillist), amount_invoice_get, due_date_get, invoice_title_get, AppUtils.convertToString(arrayListName));


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        Toast.makeText(getActivity(), "Email sent successfully", Toast.LENGTH_SHORT).show();
                        // getActivity().getSupportFragmentManager().popBackStack();
                        InvoiceFrag new_invoice_frag = new InvoiceFrag();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.new_invoice_fram, new_invoice_frag);

                        transaction.commit();

                    }
                } catch (Exception w) {

                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void datePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String strDate = format.format(calendar.getTime());
                        String selectedDate = strDate;
                        due_date.setText(selectedDate);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }
}
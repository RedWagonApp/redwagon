package com.wagon.red_wagon.ui.fragments;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class ChildProfileFrag extends Fragment implements View.OnClickListener {
    ImageView imageset_child;
    Button next_childprofile;
    EditText surname_child, first_name_child;
    String surname_child_get, first_name_child_get, filePath = "", result, child_get;

    TextView gallery_clickchild, text_title;
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CAMERA_PIC_REQUEST = 144;
    LinearLayout linear_main_bar;
    Bitmap bitmap;
    Uri imageUri;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.child_profile_frag, container, false);
        FamilyDashboardActivity.title.setText("Child Profile");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        findView_Id(view);
        next_childprofile.setOnClickListener(this);
        imageset_child.setOnClickListener(this);
        gallery_clickchild.setOnClickListener(this);
        return view;
    }

    private void findView_Id(View view) {
        next_childprofile = view.findViewById(R.id.next_childprofile);
        surname_child = view.findViewById(R.id.surname_child);
        first_name_child = view.findViewById(R.id.first_name_child);
        gallery_clickchild = view.findViewById(R.id.gellary_clickchild);
        text_title = view.findViewById(R.id.text_title);
        imageset_child = view.findViewById(R.id.imageset_child);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.gellary_clickchild:
                // Go to galary all.
                pickFromGallery();
                break;

            case R.id.next_childprofile:
                validation();

                break;
            case R.id.imageset_child:
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
                imageUri = getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);

                break;


        }
    }

    private void pickFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }

    private void validation() {
        first_name_child_get = first_name_child.getText().toString().trim();
        surname_child_get = surname_child.getText().toString().trim();
        if (!first_name_child_get.equals("")) {
            if (!surname_child_get.equals("")) {
                Constans.child_name = first_name_child_get;
                ChildDetailsFrag child_dtails_frag = new ChildDetailsFrag();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Bundle args = new Bundle();
                args.putString("first_name_child_get", first_name_child_get);
                args.putString("surname_child_get", surname_child_get);
                args.putString("filePath", filePath);
                child_dtails_frag.setArguments(args);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, child_dtails_frag);
                transaction.addToBackStack(null);
                transaction.commit();
            } else {
                surname_child.setError("Please enter the Surname");
            }
        } else {
            first_name_child.setError("Please enter the First Name");
        }

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_PIC_REQUEST) {
            try {
                imageset_child.setImageURI(imageUri);
                filePath = ImageUtils.getCompressedBitmap(getPath(imageUri));
            } catch (NullPointerException e) {
                Log.e("NullPointerException", "==v+" + e);
            }
        }

        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {


            Uri uri = data.getData();
            filePath = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageset_child.setImageBitmap(bitmap);

        }

    }

    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    private String getPath(Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(getActivity(), contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();

        } catch (SecurityException ignored) {

        }
        return result;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        first_name_child.setText("");
        surname_child.setText("");
        filePath = "";
    }
}

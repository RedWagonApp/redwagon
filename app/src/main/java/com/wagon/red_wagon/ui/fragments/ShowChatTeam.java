package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.TeamChatAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class  ShowChatTeam extends Fragment implements TeamChatAdapter.RecipientsCallBack {
    private RecyclerView reyallmaneger;
    private ImageView gallback, add_user_msg;
    private TextView titleTxt, no_result_tv;
    public static Button add_teamlist;
    private RelativeLayout linear_main_bar;
    private EditText search_et;
    private List<Body> responseBody;

    private TeamChatAdapter recipients_adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.showall_manegar, container, false);

        titleTxt = view.findViewById(R.id.text_title);
        add_teamlist = view.findViewById(R.id.add_teamlist);
        add_teamlist.setVisibility(View.GONE);
        add_user_msg = view.findViewById(R.id.add_user_msg);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        titleTxt.setText("New Chat");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });
        search_et = view.findViewById(R.id.search_et);
        reyallmaneger = view.findViewById(R.id.allmaneger);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        reyallmaneger.setLayoutManager(linearLayoutManager);
        GetTeam(this);
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input

                filter(editable.toString());
            }
        });

        return view;
    }

    private void filter(String text) {

           ArrayList<Body> filterdList = new ArrayList<>();
            if (responseBody.size() != 0) {
                for (Body s : responseBody) {

                    Log.e("getManagerName", "filter: "+s.getTeamName());
                    if (s.getTeamName().toLowerCase().contains(text.toLowerCase())) {
                        filterdList.add(s);
                    }
                }
            }

            recipients_adapter.filterList(filterdList);


    }

    private void GetTeam(final TeamChatAdapter.RecipientsCallBack showallTeam) {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<GetEvent> getApi;

        getApi = mAPIService.getTeamList(Constans.getFamilyId(getActivity()));
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    responseBody = response.body().getBody();
                    if (responseBody.size() != 0) {
                        no_result_tv.setVisibility(View.GONE);
                        reyallmaneger.setVisibility(View.VISIBLE);
                        recipients_adapter = new TeamChatAdapter(getActivity(), responseBody, showallTeam);
                        reyallmaneger.setAdapter(recipients_adapter);
                    } else {
                        no_result_tv.setVisibility(View.VISIBLE);
                        reyallmaneger.setVisibility(View.GONE);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        reyallmaneger.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


    private void addGroup(String arrayplyerId, String gropname) {
        Log.e("addGroup", "addGroup: " + arrayplyerId);
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());

        mAPIService.addGroup(arrayplyerId).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {

                        Toast.makeText(getActivity(), "Group Created Successfully", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onMethodCallback(String TeamId, String gropname) {

        Removeduplicate(TeamId, gropname);
        //  Log.e("arrayplyerId", "onClick: " + arrayplyerId);

    }

    private void Removeduplicate(String TeamId, String gropname) {

        addGroup(TeamId, gropname);
    }
}
package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.PreviousAttendaceAdapter;
import com.wagon.red_wagon.model.AttendanceBody;
import com.wagon.red_wagon.model.AttendanceModel;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class PreviousAttendaceFrag extends Fragment {


    private String teamId, eventId, teamName, date;
    List<String> items = new ArrayList<>();
    RecyclerView recyclerview;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.previous_attendance_lay, container, false);


        TextView text_title = view.findViewById(R.id.text_title);
        ImageView gallback = view.findViewById(R.id.gallback);
        ImageView add_user_msg = view.findViewById(R.id.add_user_msg);
        add_user_msg.setVisibility(View.GONE);
        recyclerview = view.findViewById(R.id.recyclerview);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(linearLayoutManager);
        Bundle bundle = getArguments();
        if (bundle != null) {
            date = bundle.getString("date", "");
            teamId = bundle.getString("teamId", "");
            eventId = bundle.getString("eventId", "");
            text_title.setText(date);
            getList(date);

        }


        return view;
    }

    private ArrayList<String> convertString(String dateRange) {

        String[] elements = dateRange.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);

        return listOfString;
    }

    private void getList(String date) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AttendanceModel> getApi;
        getApi = mAPIService.getAttendanceTeam(teamId, eventId, date);
        getApi.enqueue(new Callback<AttendanceModel>() {
            @Override
            public void onResponse(@NotNull Call<AttendanceModel> call, @NotNull retrofit2.Response<AttendanceModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    int count = 0;
                    assert response.body() != null;
                    List<AttendanceBody> responseBody = response.body().getBody();

                    if (responseBody.size() != 0) {
                        ArrayList<String> getPlayerName = new ArrayList<String>();
                        ArrayList<String> getProfileImage = new ArrayList<String>();
                        ArrayList<String> getAttendanceStatus = new ArrayList<String>();
                        for (int i = 0; i < responseBody.size(); i++) {
                            count = i;
                            getPlayerName.add(responseBody.get(i).getPlayerName());
                            getProfileImage.add(responseBody.get(i).getProfileImage());

                            Log.e("getAttendanceStatus", "onResponse: " + count);
                        }
                        getAttendanceStatus.add(responseBody.get(count).getAttendanceStatus());
                        HashSet<String> hashSet1 = new HashSet<String>();
                        HashSet<String> hashSet2 = new HashSet<String>();
                        HashSet<String> hashSet3 = new HashSet<String>();
                        hashSet1.addAll(getPlayerName);
                        hashSet2.addAll(getProfileImage);
                        hashSet3.addAll(getAttendanceStatus);
                        getPlayerName.clear();
                        getProfileImage.clear();
                        getAttendanceStatus.clear();
                        getPlayerName.addAll(hashSet1);
                        getProfileImage.addAll(hashSet2);
                        getAttendanceStatus.addAll(hashSet3);

                        Log.e("getAttendanceStatus", "onResponse: " + getAttendanceStatus.get(0));

                        PreviousAttendaceAdapter playerAdapter = new PreviousAttendaceAdapter(getActivity(), getPlayerName, getProfileImage, getAttendanceStatus);
                        recyclerview.setAdapter(playerAdapter);

                    }

                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(@NotNull Call<AttendanceModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

}

package com.wagon.red_wagon.ui.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.PreviousDatesAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PreviousDatesFrag extends Fragment {


    private String teamId, eventId, teamName, dateRange;
    List<String> items = new ArrayList<>();
    RecyclerView recyclerview;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.previous_attendance_lay, container, false);


        TextView text_title = view.findViewById(R.id.text_title);
        ImageView gallback = view.findViewById(R.id.gallback);
        ImageView add_user_msg = view.findViewById(R.id.add_user_msg);
        add_user_msg.setVisibility(View.GONE);
        recyclerview = view.findViewById(R.id.recyclerview);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        Bundle bundle = getArguments();
        if (bundle != null) {
            dateRange = bundle.getString("dateRange", "");
            teamId = bundle.getString("teamId", "");
            teamName = bundle.getString("teamName", "");
            eventId = bundle.getString("eventId", "");
            text_title.setText(teamName);
            items = convertString(dateRange);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(linearLayoutManager);
        PreviousDatesAdapter previousDatesAdapter = new PreviousDatesAdapter(getActivity(), items, teamId, eventId);
        recyclerview.setAdapter(previousDatesAdapter);

        return view;
    }

    private ArrayList<String> convertString(String dateRange) {

        String[] elements = dateRange.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);

        return listOfString;
    }

}

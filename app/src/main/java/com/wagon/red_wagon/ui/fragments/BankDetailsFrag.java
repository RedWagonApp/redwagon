package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class BankDetailsFrag extends Fragment implements View.OnClickListener {
    Button continue_bankdetails;
    EditText name_acount, nameof_bank, bank_code, account_number, reacount_number;
    String name_acount_get, nameof_bank_get, bank_code_get, account_number_get, reacount_number_get;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.bank_details_frag, container, false);
        FamilyDashboardActivity.title.setText("Bank Details");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        Findview_Id(view);

        continue_bankdetails.setOnClickListener(this);


        return view;
    }

    private void Findview_Id(View view) {
        continue_bankdetails = view.findViewById(R.id.continue_bankdetails);
        name_acount = view.findViewById(R.id.name_acount);
        nameof_bank = view.findViewById(R.id.nameof_bank);
        bank_code = view.findViewById(R.id.bank_code);
        account_number = view.findViewById(R.id.account_number);
        reacount_number = view.findViewById(R.id.reacount_number);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.continue_bankdetails) {

            Validition();
//
//            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//            FragmentTransaction transaction = fragmentManager.beginTransaction();
//            transaction.replace(R.id.bank_framedetails, new InstituteFrag());
//            transaction.addToBackStack(null);
//            transaction.commit();

        }
    }


    private void Validition() {

        name_acount_get = name_acount.getText().toString().trim();
        nameof_bank_get = nameof_bank.getText().toString().trim();
        bank_code_get = bank_code.getText().toString().trim();
        account_number_get = account_number.getText().toString().trim();
        reacount_number_get = reacount_number.getText().toString().trim();


        if (!name_acount_get.equals("")) {
            if (!nameof_bank_get.equals("")) {

                if (!bank_code_get.equals("")) {


                    if (!account_number_get.equals("")) {

                        if (!reacount_number_get.equals("")) {

                            if (account_number_get.equals(reacount_number_get)) {


                                signUpService();

                            } else {

                                Toast.makeText(getActivity(), "Account number does not match", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            reacount_number.setError("Please enter the Reaccount Number");
                        }

                    } else {
                        account_number.setError("Please enter the Account Number");
                    }
                } else {
                    bank_code.setError("Please enter the Bank Code");
                }
            } else {
                nameof_bank.setError("Please enter the Bank Name");
            }
        } else {
            name_acount.setError("Please enter the Name Account");
        }


    }

    private void signUpService() {
        final Dialog dialog = AppUtils.showProgress(getActivity());

        String url = Constans.BASEURL + "api/backDetails";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.e("response", "response" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String meassage = jsonObject.getString("message");
                    Toast.makeText(getActivity(), "Bank Details Added", Toast.LENGTH_SHORT).show();

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, new InstituteFrag(), "Designated School/Institute");
                    transaction.addToBackStack(null);
                    transaction.commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                try {
                    dialog.dismiss();

                    int mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 401) {
                        AppUtils.sessionExpiredAlert(getContext());
                    }
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception e) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("accountName", name_acount_get);
                params.put("bankName", nameof_bank_get);
                params.put("backCode", bank_code_get);
                params.put("accountNumber", account_number_get);

                Log.e("params", "params" + params);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }
}

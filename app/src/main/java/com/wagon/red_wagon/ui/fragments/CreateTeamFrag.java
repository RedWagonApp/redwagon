package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CoachRowAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

public class CreateTeamFrag extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CoachRowAdapter.AdapterCallback {
    private static final int GALLERY_REQUEST_CODE = 122;
    Button save_team;
    EditText teamName, teamSport, teamCountry, team_coach;
    CountryCodePicker teamCountryccp;
    String teamNameStr = "", teamSportStr = "", teamTimeZoneStr = "", teamCountryStr = "", teamCoachStr = "";
    LinearLayout upload_logo;
    CircleImageView logo_img;
    private String image_path = "", coachID = "";
    EditText teamTimeZone;
    TextView text_title, not_found_txt;
    ImageView gallback;
    RecyclerView coach_rv;
    Dialog coachdialog;
    RequestBody id1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_new_team, container, false);

        findviewId(view);
        getschoolprofile();
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Create New Team");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        save_team.setOnClickListener(this);
        team_coach.setOnClickListener(this);

        return view;
    }

    private void findviewId(View view) {

        save_team = view.findViewById(R.id.save_team);
        teamName = view.findViewById(R.id.team_name);
        teamSport = view.findViewById(R.id.team_sport);
        teamTimeZone = view.findViewById(R.id.team_zone);
        teamCountry = view.findViewById(R.id.country_name_txt);
        teamCountryccp = view.findViewById(R.id.team_country);
        team_coach = view.findViewById(R.id.team_coach);
        logo_img = view.findViewById(R.id.logo_img);
        upload_logo = view.findViewById(R.id.upload_logo);
        upload_logo.setOnClickListener(this);


        final CountryAdapter.CountryCallback countryCallback = new CountryAdapter.CountryCallback() {
            @Override
            public void onSelectCountryCallback(String name) {
                AppUtils.dismissDialog();
                teamCountry.setText(name);
            }

            @Override
            public void onSelectTimeZoneCallback(String name) {
                AppUtils.dismissDialog();
                teamTimeZone.setText(name);
            }
        };
        teamCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.countryDialog(getContext(), countryCallback);
            }
        });

        teamTimeZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.timeZoneDialog(getContext(), countryCallback);
            }
        });
        teamTimeZone.setText(AppUtils.getCurrentTimeZone());

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                logo_img.setImageBitmap(bitmap);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }


    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    @Override
    public void onClick(View view) {


        if (view == save_team) {
            validation();
        }

        if (view == team_coach) {
            getCoachDialog();
        }

        if (view.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

        }
    }

    private void validation() {
        teamNameStr = teamName.getText().toString().trim();
        teamSportStr = teamSport.getText().toString().trim();
        teamCountryStr = teamCountry.getText().toString().trim();
        teamTimeZoneStr = teamTimeZone.getText().toString().trim();
        teamCoachStr = team_coach.getText().toString().trim();

        if (!teamNameStr.equals("")) {
            if (!teamCoachStr.equals("")) {
                if (!teamSportStr.equals("")) {
                    if (!teamTimeZoneStr.equals("")) {
                        if (!teamCountryStr.equals("")) {
                            ApiAddTeam();
                        } else {
                            Toast.makeText(getActivity(), "Please Select Country", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please Select Time Zone", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please Select Duration", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Please Select Coach", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Please Enter Team Name", Toast.LENGTH_SHORT).show();
        }
    }


    private void ApiAddTeam() {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        Call<AddEvent> mAPIService;
        MultipartBody.Part fileToUpload = null;
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(image_path);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            fileToUpload = MultipartBody.Part.createFormData("image", "", requestBody);
        }


        if (Constans.getLoginType(getActivity()).equals("Manager")) {
            id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.getSchlID(getContext()));
        } else {
            id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.getFamilyId(getContext()));
        }

        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), teamNameStr);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), teamSportStr);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), teamTimeZoneStr);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), teamCountryStr);
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), coachID);
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), teamCoachStr);


        Map<String, RequestBody> map = new HashMap<>();
        map.put("userId", id1);
        map.put("teamName", id2);
        map.put("sport", id3);
        map.put("timeZone", id4);
        map.put("country", id5);
        map.put("managerId", id6);
        map.put("managerName", id7);
        if (image_path != null || !image_path.equals("")) {
            mAPIService = AppUtils.getAPIService(getContext()).addTeam(map, fileToUpload);
        } else {
            mAPIService = AppUtils.getAPIService(getContext()).addTeam(map);
        }
        mAPIService.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    AddRoasterFrag addRoasterFrag = new AddRoasterFrag();
                    Bundle args = new Bundle();
                    args.putString("teamId", response.body().getBody().getId().toString());
                    addRoasterFrag.setArguments(args);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_addteam, addRoasterFrag);
                    transaction.commit();

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }


            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });

    }

    private void getCoachDialog() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Class");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        not_found_txt = (TextView) coachdialog.findViewById(R.id.not_found_txt);
        not_found_txt.setText("Coaches not Found");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);

        getList(this, dialog);


        coachdialog.show();
    }

    private void getList(final CoachRowAdapter.AdapterCallback callback, final Dialog dialog) {

        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        if (Constans.getLoginType(getActivity()).equals("Manager")) {
            getApi = mAPIService.getCoachList(Constans.getSchlID(getActivity()));
        } else {
            getApi = mAPIService.getCoachList(Constans.getFamilyId(getActivity()));
        }


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                    } else {
                        not_found_txt.setVisibility(View.GONE);
                        coach_rv.setVisibility(View.VISIBLE);
                        CoachRowAdapter customAdapter = new CoachRowAdapter(getContext(), responseBody, callback, "");
                        coach_rv.setAdapter(customAdapter);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                not_found_txt.setVisibility(View.VISIBLE);
                coach_rv.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    @Override
    public void onMethodCallback(String name, String id) {
        coachdialog.dismiss();
        team_coach.setText(name);
        coachID = id;

    }

    private void getschoolprofile() {

        //     event_zip_activity.setText(Constans.getprofileData(getContext()).getZipcodee());
        teamCountry.setText(Constans.getprofileData(getContext()).getCountry());
        // address_activity.setText(Constans.getprofileData(getContext()).getAddress());
    }
}

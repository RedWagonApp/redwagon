package com.wagon.red_wagon.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.CoachDashboardAc;

public class DashboardCoach extends Fragment implements View.OnClickListener {
    LinearLayout  viewschedule_sch, game_school, chat_lay, attendence_lay;
    FragmentManager fragmentManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=LayoutInflater.from(container.getContext()).inflate(R.layout.dashboard_coach,container,false);
        CoachDashboardAc.title.setText("Manager");
        ((CoachDashboardAc) getActivity()).isVisible(true);
        FindViewID(view);
/*
       *//* game_school.setOnClickListener(this);
        viewschedule_sch.setOnClickListener(this);

        attendence_lay.setOnClickListener(this);
        chat_lay.setOn*//*ClickListener(this);*/

        return view;
    }

    private void FindViewID(View view) {
       
       /* game_school = view.findViewById(R.id.game_coach);
       
        viewschedule_sch = view.findViewById(R.id.viewschedule_coach);


        *//* classes_lay = view.findViewById(R.id.classes_lay);*//*

        attendence_lay = view.findViewById(R.id.attendence_lay_coach);
        chat_lay = view.findViewById(R.id.chat_lay_coach);
*/
    }

    @Override
    public void onClick(View v) {
      
        

      /*  if (v.getId() == R.id.game_coach) {

            EventListFragment eventListFragment = new EventListFragment();
            Bundle args = new Bundle();
            Constans.fromType = "Games";
            args.putString("fromType", "Games");
            eventListFragment.setArguments(args);

            fragmentManager =getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash_coach, eventListFragment);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();

        }
        if (v.getId() == R.id.viewschedule_coach) {
            SchoolScheduleList schedule_list_frag = new SchoolScheduleList();
            Bundle args = new Bundle();
            Constans.fromType = "Schedule";
            args.putString("fromType", "Schedule");
            fragmentManager =getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash_coach, schedule_list_frag);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();

        }

       *//* if (v.getId() == R.id.classes_lay) {
            // CreateClass schedule_list_frag = new CreateClass();
            EventListFragment schedule_list_frag = new EventListFragment();
            Bundle args = new Bundle();
            Constans.fromType = "Classes";
            args.putString("fromType", "Classes");
            schedule_list_frag.setArguments(args);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash_coach, schedule_list_frag);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();
        }*//*
        if (v.getId() == R.id.chat_lay_coach) {
            // CreateClass schedule_list_frag = new CreateClass();
            ChatsFragment chatlistfrag = new ChatsFragment();
            Constans.fromType = "Chat";
            fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash_coach, chatlistfrag);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();
        }

        if (v.getId() == R.id.attendence_lay_coach) {
            // CreateClass schedule_list_frag = new CreateClass();
            AttendaceSelectionFrag listfrag = new AttendaceSelectionFrag();
            Constans.fromType = "Attendence";
            fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash_coach, listfrag);
            transaction.addToBackStack(Constans.fromType);
            transaction.commit();
        }
*/
    }
    }


package com.wagon.red_wagon.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.utils.Constans;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    LinearLayout familyclick, driveclick, school_section;
    ImageView gallback;
    TextView text_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__screen);

        findview_id();
        text_title.setText("Registration");
//        gallback.setVisibility(View.VISIBLE);

        familyclick.setOnClickListener(this);
        driveclick.setOnClickListener(this);
        school_section.setOnClickListener(this);
        gallback.setOnClickListener(this);

    }

    public void findview_id() {
        gallback = findViewById(R.id.gallback);
        familyclick = findViewById(R.id.familyclick);
        driveclick = findViewById(R.id.driveclick);
        school_section = findViewById(R.id.school_section);
        text_title = findViewById(R.id.text_title);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.driveclick:
                Constans.userFind = "Family";
                Intent intent = new Intent(RegisterActivity.this, FamilyRegisterActivity.class);
                intent.putExtra("value", "2");
                startActivity(intent);
                break;

            case R.id.familyclick:
                Constans.userFind = "Family";
                Intent intent2 = new Intent(RegisterActivity.this, FamilyRegisterActivity.class);
                intent2.putExtra("value", "1");
                startActivity(intent2);
                break;

            case R.id.school_section:
                Intent intent3 = new Intent(RegisterActivity.this, SchoolRegisterActivity.class);
                startActivity(intent3);
                break;
            case R.id.gallback:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}


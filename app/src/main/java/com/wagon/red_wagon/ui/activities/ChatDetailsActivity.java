package com.wagon.red_wagon.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.ViewCompat;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ChatDetailsListAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.ChatAppMsgDTO;
import com.wagon.red_wagon.model.NotificationSimple;
import com.wagon.red_wagon.model.UserMessage;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatDetailsActivity extends BaseActivity {
    private static final int GALLERY_REQUEST_CODE = 010;
    public static String KEY_FRIEND = "FRIEND";
    long stopTime = 0;
    ChatAppMsgDTO userMessage;
    public ChatDetailsListAdapter mAdapter;
    Handler handler;
    private RecyclerView recyclerView;
    private ActionBar actionBar;
    //    private Friend friend;
    private List<AddEvent> items = new ArrayList<>();
    private View parent_view;
    TextView titleTxt, timer_txt;
    ImageView gallback, btn_gallery;
    EmojiconEditText emojiconEditText;
    EmojiconTextView textView;
    ImageView emojiImageView;
    ImageView submitButton;
    LinearLayout mainLay;
    RelativeLayout btn_send_lay;
    EmojIconActions emojIcon;
    LinearLayout live_score_lay;
    List<ChatAppMsgDTO> msgDtoList = new ArrayList<ChatAppMsgDTO>();
    static Socket socket;
    Chronometer chronometer;
    TextView period;
    ImageView start_btn, timer;
    ImageView pause_btn;
    EditText team_a_et;
    EditText team_b_et;
    private String image_path = "";
    boolean isVisible = false;
    int usersec;
    String UserId2 = "", UserId = "", chat_id = "", Room = "", Userchatname = "";
    ArrayList<UserMessage> arrayList;
    Intent intent;
    ArrayList<String> strings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_details);
        UserId = Constans.getFamilyId(this);
        arrayList = new ArrayList();
        Log.e("UserId", "onCreate: " + UserId);
        try {
            iniComponen();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        parent_view = findViewById(android.R.id.content);

        titleTxt = findViewById(R.id.text_title);
        gallback = findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ViewCompat.setTransitionName(parent_view, KEY_FRIEND);
        intent = getIntent();
        String name = intent.getExtras().getString("name");
//        UserId = intent.getExtras().getString("UserId");

        UserId2 = intent.getExtras().getString("UserId2");
        chat_id = intent.getExtras().getString("chat_id");
        Room = intent.getExtras().getString("Room");
        if (UserId2.equals("maneger")) {
            UserId2 = String.valueOf(Constans.userin);
        }
        Log.e("UserId2", "onCreate: " + UserId2);
        Log.e("chat_id", "onCreate: " + chat_id);
        Log.e("Room", "onCreate: " + Room);
        titleTxt.setText(name);

        emojIcon = new EmojIconActions(this, mainLay, emojiconEditText, emojiImageView);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard", "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard", "Keyboard closed");
            }
        });


        btn_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);


            }
        });
        start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chronometer.setBase(SystemClock.elapsedRealtime() + stopTime);
                chronometer.start();
                start_btn.setVisibility(View.GONE);
                pause_btn.setVisibility(View.VISIBLE);


            }
        });
        pause_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopTime = chronometer.getBase() - SystemClock.elapsedRealtime();
                chronometer.stop();
                start_btn.setVisibility(View.VISIBLE);
                pause_btn.setVisibility(View.GONE);

            }
        });

        timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isVisible) {
                    isVisible = false;
                    live_score_lay.setVisibility(View.VISIBLE);
                } else {
                    isVisible = true;
                    live_score_lay.setVisibility(View.GONE);
                }

            }
        });

        period.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ChatDetailsActivity.this);
                builder.setTitle("Period");
                final String[] time = {"1st", "2nd", "Half", "3rd", "4th"};
                builder.setItems(time, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        period.setText(time[which]);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


        try {


//if you are using a phone device you should connect to same local network as your laptop and disable your pubic firewall as well
//Build Connection using URI, Query(optional), forceNew(optional), reconnection(optional), transports
            socket = IO.socket(Constans.BASEURL);

            //create connection


// emit the event join along side with the nickname
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", UserId);

            socket.emit("connect_user", jsonObject);

            socket.connect();
            Log.e("joinjoin", "joinjoin" + socket.toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.e("URISyntaxException", "joinjoin" + e);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            socket = IO.socket(Constans.BASEURL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("chat_id", chat_id);
            socket.emit("chat_all", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }


//Listeners chat all get history

        socket.on("chat_all", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {


                if (msgDtoList.size() != 0) {
                    msgDtoList.clear();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        JSONArray data = (JSONArray) args[0];
                        for (int i = 0; i < data.length(); i++) {
                            try {
                                JSONObject jsonObject = data.getJSONObject(i);

                                String message = jsonObject.getString("message");
                                String user_id = jsonObject.getString("user_id");
                                String name = jsonObject.getString("name");
                                String type = jsonObject.getString("type");

                                if (user_id.equals(UserId)) {
                                    if (type.equals("1")) {
                                        userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_SENT, message, "", "");
                                    } else if (type.equals("2")) {
                                        userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_IMAGE_SEND, "", message, "");
                                    }
                                } else {
                                    if (type.equals("1")) {
                                        userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_RECEIVED, message, "", name);
                                    } else if (type.equals("2")) {
                                        userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_IMAGE_RECIEVED, "", message, name);
                                    }
                                }

                                msgDtoList.add(userMessage);

                                //
                                Log.e("message", "call: " + msgDtoList.size());
                                //  mAdapter.notifyDataSetChanged();

                                mAdapter = new ChatDetailsListAdapter(ChatDetailsActivity.this, msgDtoList);
                                mAdapter.notifyDataSetChanged();

                                recyclerView.setAdapter(mAdapter);

                                // Stuff that updates the UI


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
                //new message set adapter
                socket.on("new_message", new Emitter.Listener() {
                    @Override
                    public void call(final Object... args) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                JSONObject data = (JSONObject) args[0];
                                Log.e("newmasesw", "JSONObject: " + data);


                                try {
                                    String message = data.getString("message");
                                    Log.e("newmasesw", "run: " + message);
                                    String user_id = data.getString("user_id");
                                    String type = data.getString("type");
                                    String name = data.getString("name");
                                    ;
                                    if (name.equals("1")) {
                                        Userchatname = "";
                                    } else {
                                        Userchatname = name;
                                    }
                                    Log.e("user_id", "run: " + user_id);


                                    if (user_id.equals(UserId)) {
                                        if (type.equals("1")) {
                                            userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_SENT, message, "", "");
                                        } else if (type.equals("2")) {
                                            userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_IMAGE_SEND, "", message, "");
                                        }
                                    } else {
                                        if (type.equals("1")) {
                                            userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_RECEIVED, message, "", Userchatname);
                                        } else if (type.equals("2")) {
                                            userMessage = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_IMAGE_RECIEVED, "", message, Userchatname);
                                        }
                                    }
                                    msgDtoList.add(userMessage);

                                    //
                                    Log.e("message", "call: " + msgDtoList.size());
                                    //  mAdapter.notifyDataSetChanged();

                                    mAdapter = new ChatDetailsListAdapter(ChatDetailsActivity.this, msgDtoList);


                                    recyclerView.setAdapter(mAdapter);


                                } catch (
                                        JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                });


            }
        });

        socket.connect();

    }


    public void iniComponen() throws JSONException {
        recyclerView = (RecyclerView) findViewById(R.id.listview);
        btn_gallery = findViewById(R.id.btn_gallery);
        btn_send_lay = findViewById(R.id.btn_send_lay);
        emojiconEditText = findViewById(R.id.message_edittext);
        emojiImageView = findViewById(R.id.emojicon_icon);
        mainLay = findViewById(R.id.mainLay);
        live_score_lay = findViewById(R.id.live_score_lay);
        chronometer = findViewById(R.id.timer_txt);
        period = findViewById(R.id.period);
        start_btn = findViewById(R.id.start_btn);
        pause_btn = findViewById(R.id.pause_btn);
        team_a_et = findViewById(R.id.team_a_et);
        team_b_et = findViewById(R.id.team_b_et);
        timer = findViewById(R.id.timer);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

//message send the user button

        btn_send_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msgContent = emojiconEditText.getText().toString();

                /*if (isVisible) {
                    msgContent = "Team A : " + team_a_et.getText().toString() + "\n" + "Team B : " + team_b_et.getText().toString() + "\n" + period.getText().toString()" , "+stopTime;
                }*/
                if (Room.equals("1")) {


                    if (!msgContent.equals("")) {
//                    ChatAppMsgDTO msgDto = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_SENT, msgContent, "");
//                    msgDtoList.add(msgDto);
//                    int newMsgPosition = msgDtoList.size() - 1;
//                    mAdapter.notifyItemInserted(newMsgPosition);
//                    recyclerView.scrollToPosition(newMsgPosition);
                        String message = StringEscapeUtils.escapeJava(msgContent);
                        try {
                            socket = IO.socket(Constans.BASEURL);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                        try {
                            JSONObject jsonData = new JSONObject();
                            jsonData.put("user_id", UserId);
                            jsonData.put("user2id", UserId2);
                            jsonData.put("message", message);
                            jsonData.put("message_type", "1");
                            socket.emit("send_message", jsonData);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        emojiconEditText.setText("");
                        socket.connect();
                    }
                } else {
                    if (!msgContent.equals("")) {

                        String message = StringEscapeUtils.escapeJava(msgContent);
                        strings = (ArrayList<String>) getIntent().getSerializableExtra("User_idarry");
                        String otherId = AppUtils.convertToString(strings);
                        Log.e("User_idarry", "User_idarry: " + otherId);
                        try {
                            socket = IO.socket(Constans.BASEURL);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                        try {
                            JSONObject jsonData = new JSONObject();
                            jsonData.put("user_id", UserId);
                            jsonData.put("roomid", chat_id);
                            jsonData.put("message", message);
                            jsonData.put("otherId", otherId);
                            jsonData.put("message_type", "1");

                            socket.emit("groupchat", jsonData);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        emojiconEditText.setText("");
                        socket.connect();
                    }
                }


            }
        });


    }

    public void sendImage(String path) {
        String ext = path.substring(path.lastIndexOf(".") + 1);
        /*ChatAppMsgDTO msgDto = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_IMAGE_SEND, "", encodeImage(path));
        msgDtoList.add(msgDto);
        int newMsgPosition = msgDtoList.size() - 1;
        mAdapter.notifyItemInserted(newMsgPosition);
        recyclerView.scrollToPosition(newMsgPosition);*/
        String resizeImage = resizeBase64Image(encodeImage(path));
        try {
            socket = IO.socket(Constans.BASEURL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put("user_id", UserId);
            jsonData.put("user2id", UserId2);
            jsonData.put("message", resizeImage);
            jsonData.put("message_type", "2");
            jsonData.put("ext", ext);
            socket.emit("send_message", jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        emojiconEditText.setText("");
        socket.connect();
    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);
        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, 400, 400, false);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private String encodeImage(String path) {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        //Base64.de
        return encImage;
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private TextWatcher contentWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable etd) {
            if (etd.toString().trim().length() == 0) {
                btn_send_lay.setVisibility(View.GONE);
            } else {
                btn_send_lay.setVisibility(View.VISIBLE);
            }
            //carpools.setContent(etd.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.replace(R.id.chat_deatils_frag, new ChatsFragment());
//        transaction.commit();


    }


    @Override
    protected void onDestroy() {
        UpdateSettings("1");
        socket.disconnect();
        super.onDestroy();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                //logo_img.setImageBitmap(bitmap);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));

                sendImage(image_path);
                // setImage(image_path);

            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }
    private void UpdateSettings(String s) {

        APIService mAPIService = AppUtils.getAPIService(this);
        Call<NotificationSimple> getApi;

        getApi = mAPIService.UpdateSettings(Constans.getFamilyId(this),s);


        getApi.enqueue(new Callback<NotificationSimple>() {
            @Override
            public void onResponse(Call<NotificationSimple> call, retrofit2.Response<NotificationSimple> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());



            }

            @Override
            public void onFailure(Call<NotificationSimple> call, Throwable t) {


                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    private void setImage(String image) {
        String ext = image.substring(image.lastIndexOf("."));
        ChatAppMsgDTO msgDto = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_IMAGE_SEND, "", encodeImage(image), "");
        msgDtoList.add(msgDto);
        int newMsgPosition = msgDtoList.size() - 1;
        mAdapter.notifyItemInserted(newMsgPosition);
        recyclerView.scrollToPosition(newMsgPosition);

    }

    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(this, uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    private void addScroesDialog() {
//        Dialog scoresdialog = new Dialog(this);
//        setContentView(R.layout.score_dialog);
//       
        final Chronometer chronometer = findViewById(R.id.timer_txt);
        TextView period = findViewById(R.id.period);
        final ImageView start_btn = findViewById(R.id.start_btn);
        final ImageView pause_btn = findViewById(R.id.pause_btn);
        EditText team_a_et = findViewById(R.id.team_a_et);
        EditText team_b_et = findViewById(R.id.team_b_et);

        start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chronometer.setBase(SystemClock.elapsedRealtime() + stopTime);
                chronometer.start();
                start_btn.setVisibility(View.GONE);
                pause_btn.setVisibility(View.VISIBLE);


            }
        });
        pause_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopTime = chronometer.getBase() - SystemClock.elapsedRealtime();
                chronometer.stop();
                start_btn.setVisibility(View.VISIBLE);
                pause_btn.setVisibility(View.GONE);

            }
        });


    }


    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        socket.close();
    }


}

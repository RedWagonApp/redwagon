package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.NotificationResponse;
import com.wagon.red_wagon.ui.fragments.ScheduleTeamListFrag;

public class CalendarTeamDetailAcrivity extends AppCompatActivity {
    Dialog driverDialog;
    NotificationResponse response;
    String teamId = "", teamName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_team_activity);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Bundle extras = getIntent().getExtras();
        TextView text_title = findViewById(R.id.text_title);

        ImageView gallback = findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        teamId = extras.getString("teamId", "");
        teamName = extras.getString("teamName", "");
        if (teamName != null) {
            text_title.setText(teamName);
        }
        ScheduleTeamListFrag scheduleTeamListFrag = new ScheduleTeamListFrag();
        Bundle args = new Bundle();
        args.putString("teamId", teamId);
        scheduleTeamListFrag.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.team_frame, scheduleTeamListFrag, "Team")
                .commit();
    }
}

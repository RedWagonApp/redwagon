package com.wagon.red_wagon.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.fragments.SchoolRegFrag;


public class SchoolRegisterActivity extends BaseActivity implements View.OnClickListener {
    TextView text_title;
    LinearLayout school_section;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.school_reg_frag);
        findview_id();
        ImageView gallback = findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        text_title.setText("School Registration");

    }

    public void findview_id() {


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.school_reg_fram, new SchoolRegFrag());
        transaction.addToBackStack(null);
        transaction.commit();
        text_title = findViewById(R.id.text_title);


    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();

    }
}

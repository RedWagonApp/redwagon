package com.wagon.red_wagon.ui.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;

public class EnterCodeFrag extends Fragment implements View.OnClickListener, TextWatcher {
    TextView next_text, reaend_code, mTextField;
    String famly_rg_get, surname_rg_get, email_rg_get, password_rg_get, phone_number_rg_get, filePath, ccp;
    private EditText editTextone, editText_two, editTextthree, editTextfour, editTextfive, editTextsix;
    private FirebaseAuth mAuth;
    String mVerificationId, code, value = "";
    ImageView gallback;
    LinearLayout resend_layout;
    int mStatusCode;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private static final String TAG = "PhoneAuthActivity";
    APIService mAPIService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.enter_code_opt, container, false);
        mAuth = FirebaseAuth.getInstance();
        mAPIService = AppUtils.getAPIService(getContext());

        Bundle args = getArguments();

        famly_rg_get = args.getString("firstName", "");
        surname_rg_get = args.getString("surname_rg_get", "");
        email_rg_get = args.getString("email", "");
        phone_number_rg_get = args.getString("phone_number_rg_get", "");
        password_rg_get = args.getString("password_rg_get", "");
        ccp = args.getString("ccp", "");
        filePath = args.getString("filePath", "");
        value = args.getString("value", "");

        Log.e(TAG, "onCreateView: filePath" + filePath);
        sendVerificationCode(phone_number_rg_get, ccp);
        Timeer();
        resend_layout = view.findViewById(R.id.resend_layout);
        mTextField = view.findViewById(R.id.mTextField);
        gallback = view.findViewById(R.id.gallback);
        next_text = view.findViewById(R.id.next_text);
        editTextone = view.findViewById(R.id.editTextone);
        editText_two = view.findViewById(R.id.editTexttwoo);
        editTextthree = view.findViewById(R.id.editTextthree);
        editTextfour = view.findViewById(R.id.editTextfour);
        editTextfive = view.findViewById(R.id.editTextfive);
        editTextsix = view.findViewById(R.id.editTextsix);
        reaend_code = view.findViewById(R.id.reaend_code);

        next_text.setOnClickListener(this);
        reaend_code.setOnClickListener(this);
        editTextone.addTextChangedListener(this);
        editText_two.addTextChangedListener(this);
        editTextthree.addTextChangedListener(this);
        editTextfour.addTextChangedListener(this);
        editTextfive.addTextChangedListener(this);
        editTextsix.addTextChangedListener(this);
        next_text.setTextColor(getResources().getColor(R.color.gray));
        next_text.setClickable(false);
        next_text.setFocusable(false);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {


            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {

                Log.d(TAG, "onVerificationCompleted:" + credential);
//                signInWithPhoneAuthCredential(credential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // mPhoneNumberField.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {

                }
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {

                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };


        return view;
    }

    private void sendVerificationCode(String phoneNumber, String ccp) {

        Log.e("sendVerificationCode", "" + phoneNumber + "\n" + ccp);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+" + ccp + phoneNumber,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);

        String time = String.valueOf(TimeUnit.SECONDS);

        Log.e("TimeUnit", "-=" + time);


    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
//                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
                //  Servive();
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            //   Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };


    private void verifyVerificationCode(String code) {
        //creating the credential
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

            //signing the user
            signInWithPhoneAuthCredential(credential);
        } catch (IllegalArgumentException e) {

        }
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            Servive();

//                            startActivity(new Intent(PhoneAuthActivity.this, CalendarActivity.class));
//                            finish();
                        } else {

                            Toast.makeText(getActivity(), "Wrong OTP", Toast.LENGTH_SHORT).show();
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {

                            }
                        }
                    }
                });
    }

    private void Servive() {

        hideKeyboardFrom(getContext());
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        if (value.equals("1")) {
            PrivacyFrag enterCode_frag = new PrivacyFrag();
            Bundle args = new Bundle();
            args.putString("email", email_rg_get);
            args.putString("password_rg_get", password_rg_get);
            args.putString("ccp", ccp);
            args.putString("phone_number_rg_get", phone_number_rg_get);
            args.putString("firstName", famly_rg_get);
            args.putString("surname_rg_get", surname_rg_get);
            args.putString("filePath", filePath);

            enterCode_frag.setArguments(args);
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.otp_frame, enterCode_frag);
            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            ApiserviceDriver();
        }

    }

    private void ApiserviceDriver() {
        Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/signUp";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    JSONObject jsonObject1 = jsonObject.getJSONObject("message");
                    String email = jsonObject1.getString("email");
                    String token = jsonObject1.getString("token");
                    String id = jsonObject1.getString("id");
                    String type = jsonObject1.getString("gender");
                    String userName = jsonObject1.getString("firstName");
                    String phoneNumber = jsonObject1.getString("phoneNumber");

                    Log.e(TAG, "onResponse: " + message);
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();

                    editor.putString("Token", token);
                    editor.putString("FMID", id);
                    editor.putString("type", type);
                    editor.putString("userName", userName);
                    editor.apply();

                    uploadPic();
                    // Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    //next screen open


                    Log.e("jsonObject", "jsonObject" + email + "\n" + phoneNumber);

                } catch (JSONException e) {

                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                try {
                    mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 400) {
                        Toast.makeText(getActivity(), "User already exists", Toast.LENGTH_SHORT).show();

                    }
                    if (mStatusCode == 401) {
                        AppUtils.sessionExpiredAlert(getContext());
                    }
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception r) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email_rg_get);
                params.put("password", password_rg_get);
                params.put("phoneNumber", ccp + phone_number_rg_get);
                params.put("firstName", famly_rg_get);
                params.put("userName", surname_rg_get);
                params.put("gender", "Driver");
                params.put("fbGoogleId", Constans.getFbGoogleId);
                Log.e("params", "params" + params);
                return params;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.next_text) {

            Validitaion();
//            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//            FragmentTransaction transaction = fragmentManager.beginTransaction();
//            transaction.replace(R.id.otp_frame, new ParentResgisterFrag());
//            transaction.addToBackStack(null);
//            transaction.commit();


        } else if (view.getId() == R.id.reaend_code) {
            sendVerificationCode(phone_number_rg_get, ccp);
            resend_layout.setVisibility(View.GONE);
            mTextField.setVisibility(View.VISIBLE);

            Timeer();
        }
    }

    private void Validitaion() {
        if (!editTextone.equals("")) {
            if (!editText_two.equals("")) {

                if (!editTextthree.equals("")) {
                    if (!editTextfour.equals("")) {
                        if (!editTextfive.equals("")) {
                            if (!editTextsix.equals("")) {
                                //done
                                code = editTextone.getText().toString().trim() + editText_two.getText().toString().trim() + editTextthree.getText().toString().trim() + editTextfour.getText().toString().trim() + editTextfive.getText().toString().trim() + editTextsix.getText().toString().trim();
                                Log.e("MyEnter_Code", code);

                                verifyVerificationCode(code);
                            } else {
                                editTextsix.setError("");
                            }
                        } else {
                            editTextfive.setError("");
                        }
                    } else {
                        editTextfour.setError("");
                    }

                } else {
                    editTextthree.setError("");
                }
            } else {
                editText_two.setError("");
            }
        } else {
            editTextone.setError("");
        }
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 1) {
            if (editTextone.length() == 1) {
                editText_two.requestFocus();
            }

            if (editText_two.length() == 1) {
                editTextthree.requestFocus();
            }
            if (editTextthree.length() == 1) {
                editTextfour.requestFocus();
            }
            if (editTextfour.length() == 1) {
                editTextfive.requestFocus();
            }
            if (editTextfive.length() == 1) {
                editTextsix.requestFocus();
                next_text.setTextColor(getResources().getColor(R.color.white));
                next_text.setClickable(true);
                next_text.setFocusable(true);
            }

        } else if (editable.length() == 0) {
            if (editText_two.length() == 0) {
                editTextone.requestFocus();

            } else if (editTextthree.length() == 0) {
                editText_two.requestFocus();
            } else if (editTextfour.length() == 0) {
                editTextthree.requestFocus();
            } else if (editTextfive.length() == 0) {
                editTextfour.requestFocus();
            } else if (editTextsix.length() == 0) {
                next_text.setTextColor(getResources().getColor(R.color.gray));
                next_text.setClickable(false);
                next_text.setFocusable(false);
                editTextfive.requestFocus();
            }


        }


    }

    public void Timeer() {
        new CountDownTimer(50000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTextField.setText("Did not get the code? Resend in a " + millisUntilFinished / 1000 + " secs");
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {

                resend_layout.setVisibility(View.VISIBLE);
                mTextField.setVisibility(View.GONE);
            }

        }.start();
    }

    public void hideKeyboardFrom(Context context) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void uploadPic() {
        Dialog dialog = AppUtils.showProgress(getActivity());

        MultipartBody.Part part = null;
//        File file = new File(image_path);
        File file = null;
        if (!filePath.equals("")) {
            try {
                file = new File(filePath);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }


        mAPIService.prolieUplod(part).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {

                dialog.dismiss();

                String img = response.body().getMessage();

                SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
                editor.putString("PicUrl", img);
                editor.apply();


                Intent intent = new Intent(getActivity(), FamilyDashboardActivity.class);

                intent.putExtra("check", "3");
                intent.putExtra("type", "1");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);

            }
        });
    }
}



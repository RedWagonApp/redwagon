package com.wagon.red_wagon.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.SchoolDashboard;
import com.wagon.red_wagon.utils.Constans;

public class AllDone extends Fragment {
    Button home_page;
    TextView text_title, club_text;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.all_done, container, false);

        text_title = view.findViewById(R.id.text_title);
        club_text = view.findViewById(R.id.club_text);

        text_title.setText("All Done");
        club_text.setText(Constans.clubOrgranization_get + " is set up on Red Wagon.");
        home_page = view.findViewById(R.id.home_page);
        home_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), SchoolDashboard.class);
                intent.putExtra("Coach", "1");
                intent.putExtra("text", "Club");
                intent.putExtra("type", "2");
                intent.putExtra("UserPic", Constans.PicUrl);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();

            }
        });


        return view;
    }
}

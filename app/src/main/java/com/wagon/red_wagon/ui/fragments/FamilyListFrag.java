package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.FamilyAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FamilyListFrag extends Fragment {

    private RecyclerView family_reyclerview;
    private TextView no_result_tv, add_family_txt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View layout = inflater.inflate(R.layout.family_list_lay, container, false);
        return layout;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        family_reyclerview = view.findViewById(R.id.family_reyclerview);
        add_family_txt = view.findViewById(R.id.add_family_txt);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        FamilyDashboardActivity.title.setText("Family");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        family_reyclerview.setLayoutManager(linearLayoutManager);

        add_family_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFamilyFrag familyFrag = new AddFamilyFrag();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, familyFrag, "Add Family Member");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        GetChildFamilyList();
    }


    private void GetChildFamilyList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi = null;

        getApi = mAPIService.GetChildFamilyList(Constans.getFamilyId(getActivity()));
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                try {
                    if (response.code() == 200) {

                        List<Body> responseBody = response.body().getBody();

                        if (responseBody.size() == 0) {
                            no_result_tv.setVisibility(View.VISIBLE);
                            family_reyclerview.setVisibility(View.GONE);
                        } else {
                            no_result_tv.setVisibility(View.GONE);
                            family_reyclerview.setVisibility(View.VISIBLE);
                            FamilyAdapter mAdapter = new FamilyAdapter(getActivity(), responseBody);
                            family_reyclerview.setAdapter(mAdapter);

                        }
                    } else if (response.code() == 400) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        family_reyclerview.setVisibility(View.GONE);
                        if (!response.isSuccessful()) {

                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");

                                Log.e("MessageM ", userMessage);
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                    else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                no_result_tv.setVisibility(View.VISIBLE);
                family_reyclerview.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ManagerAdapter;
import com.wagon.red_wagon.adapter.Recipients_Adapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ShowManagersFrag extends Fragment {
    RecyclerView event_reyclerview;
    TextView text_title, createeveet_, no_result_tv;
    ImageView gallback;
    public static Button ok_invoice;
    Recipients_Adapter.RecipientsCallBack recipientsCallBack;
    EditText search_et;
    CardView cardview;
    String value = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.viewmanagers, container, false);
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Coaches");
        try {
            Bundle bundle = getArguments();
            value = bundle.getString("fromType", "");
        } catch (NullPointerException e) {

        }

        no_result_tv = view.findViewById(R.id.no_result_tv);
        gallback = view.findViewById(R.id.gallback);
        event_reyclerview = view.findViewById(R.id.event_reyclerview);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        event_reyclerview.setLayoutManager(linearLayoutManager);
        if (value.equals("2")) {
            getCoachDetail(Constans.getSchlID(getActivity()));
        } else {
            getCoachDetail(Constans.getFamilyId(getActivity()));
        }
        return view;
    }

    private void getCoachDetail(String schlID) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());


        mAPIService.getCoachList(schlID).enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    List<Body> bodyList = response.body().getBody();

                    if (bodyList.size() == 0) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        event_reyclerview.setVisibility(View.GONE);
                    } else {
                        no_result_tv.setVisibility(View.GONE);
                        event_reyclerview.setVisibility(View.VISIBLE);
                        ManagerAdapter customAdapter = new ManagerAdapter(getActivity(), bodyList, value);
                        event_reyclerview.setAdapter(customAdapter);
                    }
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

}
package com.wagon.red_wagon.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


public class SecurityFrag extends Fragment implements View.OnClickListener {
    EditText password_newe, password_olde, password_conf;
    String password_new_get, password_old_get, password_conf_get;
    Button save_changepass;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.security_frag, container, false);
        FamilyDashboardActivity.title.setText("Change Password");

        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        findView(view);
        save_changepass.setOnClickListener(this);
        return view;
    }

    private void findView(View view) {

        password_newe = view.findViewById(R.id.password_new);
        password_olde = view.findViewById(R.id.password_old);
        password_conf = view.findViewById(R.id.password_conf);
        save_changepass = view.findViewById(R.id.save_changepass);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_changepass) {

            password_new_get = password_newe.getText().toString();
            password_old_get = password_olde.getText().toString();
            password_conf_get = password_conf.getText().toString();
            if (!password_old_get.equals("")) {
                if (!password_new_get.equals("")) {

                    if (!password_conf_get.equals("")) {
                        if (password_conf_get.equals(password_new_get)) {
                            Sevice();
                        } else {
                            password_conf.setError("Password not match");
                        }
                    } else {
                        password_conf.setError("Please enter the Confirm password");
                    }
                } else {
                    password_newe.setError("Please enter the New password");
                }
            } else {
                password_olde.setError("Please enter the Old password");

            }
        }
    }

    private void Sevice() {


        String url = Constans.BASEURL + "api/changePassword";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Toast.makeText(getActivity(), "Password updated successfully", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                } catch (JSONException e) {

                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error.networkResponse.statusCode == 401) {
                    AppUtils.sessionExpiredAlert(getContext());
                }
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id",Constans.getFamilyId(getActivity()));
                params.put("newPassword", password_new_get);
                params.put("oldPassword", password_old_get);

                Log.e("params", "params" + params);
                return params;

            }

//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("Content-Type", "application/json; charset=UTF-8");
//                        params.put("token", Constans.Token);
//                        return params;
//                    }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }
}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;
import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class SchoolRegFrag extends Fragment implements View.OnClickListener {
    private TextView text_title;
    private Button next_Schoolrestger, next_restgerManagers;
    private FragmentManager fragmentManage;
    private FragmentTransaction transaction;
    private int number = 0;
    AutoCompleteTextView adress_school;
    private EditText email_school, pasword_school, clubOrgranization, activtiy_school, zipe_code_school, phonenumbner_school, website_school, county_school;
    private String filePath = "", email_school_get, pasword_school_get, clubOrgranization_get, activtiy_school_get, adress_school_get, zipe_code_school_get, phonenumbner_school_get, website_school_get, county_school_get;
    private static final int GALLERY_REQUEST_CODE = 111;
    private static final int CAMERA_PIC_REQUEST = 144;
    private CircleImageView imageset;
    private TextView click_glry;
    private String result, PicUrl = "";
    private Dialog dialog;
    private Uri uri;
    AutoCompleteAdapter mAdapter;

    PlacesClient placesClient;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.school_reg_frag, container, false);
        findview(view);
        text_title.setText("School Registration");
        next_Schoolrestger.setOnClickListener(this);
        next_restgerManagers.setOnClickListener(this);

        String country = getApplicationContext().getResources().getConfiguration().locale.getDisplayCountry();
        county_school.setText(country);
        final CountryAdapter.CountryCallback countryCallback = new CountryAdapter.CountryCallback() {
            @Override
            public void onSelectCountryCallback(String name) {
                AppUtils.dismissDialog();
                county_school.setText(name);
            }

            @Override
            public void onSelectTimeZoneCallback(String name) {
            }
        };
        county_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.countryDialog(getContext(), countryCallback);
            }
        });


        setUpAutoCompleteTextView();
        adress_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });
        return view;
    }

    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());

    }


    private void getPlaceInfo(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
       /* if (addresses.get(0).getAdminArea() != null) {
            String addressLine = addresses.get(0).getAdminArea();
            Log.e("ADDRESS ", addressLine);
            adress_school.setText(addressLine);
        }*/


        if (addresses.get(0).getPostalCode() != null) {
            String ZIP = addresses.get(0).getPostalCode();
            zipe_code_school.setText(ZIP);
            Log.e("ZIP CODE", ZIP);
        }

        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();

            Log.e("CITY", city);
        }

        if (addresses.get(0).getAdminArea() != null) {
            String state = addresses.get(0).getAdminArea();
            Log.e("STATE", state);
        }

        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            county_school.setText(country);
            Log.e("COUNTRY", country);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_Schoolrestger:
                Validaton();
                break;
            case R.id.gellary_clickfamily:
                pickFromGallery();

                break;

            case R.id.imageset:
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
                uri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                break;

        }
    }

    private void Validaton() {
        email_school_get = email_school.getText().toString().trim();
        pasword_school_get = pasword_school.getText().toString().trim();
        clubOrgranization_get = clubOrgranization.getText().toString().trim();
        activtiy_school_get = activtiy_school.getText().toString().trim();
        adress_school_get = adress_school.getText().toString().trim();
        zipe_code_school_get = zipe_code_school.getText().toString().trim();
        website_school_get = website_school.getText().toString().trim();
        phonenumbner_school_get = phonenumbner_school.getText().toString().trim();
        county_school_get = county_school.getText().toString().trim();

        if (!email_school_get.equals("") && emailValidator(email_school_get)) {
            if (!pasword_school_get.equals("")) {
                if (!clubOrgranization_get.equals("")) {
                    if (!activtiy_school_get.equals("")) {
                        if (!adress_school_get.equals("")) {
                            if (!zipe_code_school_get.equals("")) {
                                if (!website_school_get.equals("")) {
                                    if (!phonenumbner_school_get.equals("")) {
                                        if (!county_school_get.equals("")) {

                                            ApiService();

                                        } else {
                                            county_school.setError("Please enter a county");
                                        }
                                    } else {
                                        phonenumbner_school.setError("Please enter a Phone Number");
                                    }
                                } else {
                                    website_school.setError("Please enter a Website");
                                }
                            } else {
                                zipe_code_school.setError("Please enter a Zip Code");
                            }
                        } else {
                            adress_school.setError("Please enter a Address");
                        }
                    } else {
                        activtiy_school.setError("Please enter a Activtiy");
                    }
                } else {
                    clubOrgranization.setError("Please enter the Club Orgranization");
                }
            } else {
                pasword_school.setError("Please enter the Password");
            }
        } else {
            email_school.setError("Please enter the Email Address");
        }
    }


    private void ApiService() {
        dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/schoolsignUp";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                try {
                    Constans.clubOrgranization_get = clubOrgranization_get;
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("message");
                    Constans.Token = jsonObject1.getString("token");
                    Constans.FMID = jsonObject1.getString("id");

                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor.putString("Token", Constans.Token);
                    editor.putString("FMID", Constans.FMID);
                    editor.putString("userr", "School");
                    editor.putString("type", "School");
                    editor.putString("userName", clubOrgranization_get);
                    editor.putString("school_dashboard", "2");
                    editor.apply();

                    uploadPic();
                    Log.e("sdfdsfsdfsfs", "sdfsfs" + Constans.Token);


                } catch (JSONException e) {
                    dialog.dismiss();
                    Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("dsfdfsdf", "onErrorResponse: " + error.toString());
                dialog.dismiss();
                //Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();

                try {
                    int code = error.networkResponse.statusCode;
                    if (error.networkResponse.statusCode == 401) {
                        AppUtils.sessionExpiredAlert(getContext());
                    }
                } catch (NullPointerException e) {

                }

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        String masgg = obj.getString("message");
                        Log.e("objobjobj", "objobjobj" + masgg);
                        Toast.makeText(getActivity(), masgg, Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        e1.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email_school_get);
                params.put("password", pasword_school_get);
                params.put("phoneNumber", phonenumbner_school_get);
                params.put("nameClub", clubOrgranization_get);
                params.put("zipCode", zipe_code_school_get);
                params.put("mainActivity", activtiy_school_get);
                params.put("address", adress_school_get);
                params.put("website", website_school_get);
                params.put("country", county_school_get);
                params.put("gender", "School");
                params.put("deviceType", "2");
                params.put("deviceToken", Constans.FCMToken(getActivity()));
                params.put("fbGoogleId", Constans.getFbGoogleId);
                params.put("deviceToken", Constans.FCMToken(getActivity()));
                params.put("deviceType", "2");
                Log.e("getParamsgetParams", "getParams: " + params);
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }

    private void findview(View view) {
        text_title = view.findViewById(R.id.text_title);
        next_Schoolrestger = view.findViewById(R.id.next_Schoolrestger);
        next_restgerManagers = view.findViewById(R.id.next_restgerManagers);
        imageset = view.findViewById(R.id.imageset);
        click_glry = view.findViewById(R.id.gellary_clickfamily);
        imageset.setOnClickListener(this);
        click_glry.setOnClickListener(this);

        county_school = view.findViewById(R.id.county_school);
        website_school = view.findViewById(R.id.website_school);
        phonenumbner_school = view.findViewById(R.id.phonenumbner_school);
        zipe_code_school = view.findViewById(R.id.zipe_code_school);
        adress_school = view.findViewById(R.id.adress_school);
        activtiy_school = view.findViewById(R.id.activtiy_school);
        clubOrgranization = view.findViewById(R.id.clubOrgranization);
        pasword_school = view.findViewById(R.id.pasword_school);
        email_school = view.findViewById(R.id.email_school);


    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK

        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_PIC_REQUEST) {
            try {
                imageset.setImageURI(uri);
                filePath = getPath(uri);
            } catch (NullPointerException e) {
                Log.e("NullPointerException", "==v+" + e);
            }

        }

        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {


            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                filePath = getPath(uri);
                imageset.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                adress_school.setText(place.getName() + ", " + place.getAddressComponents().asList().get(1).getName());

                try {
                    getPlaceInfo(Objects.requireNonNull(place.getLatLng()).latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }

    private String getPath(Uri contentUri) {

        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(getContext(), contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();

        } catch (SecurityException ignored) {

        }
        return result;
    }

    private void uploadPic() {

        MultipartBody.Part part = null;
//        File file = new File(filePath);
//
//        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
        if (!filePath.equals("")) {
            try {
                File file = new File(filePath);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(filePath);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                part = MultipartBody.Part.createFormData("image", "", requestBody);
        }

        AppUtils.getAPIService(getContext()).prolieUplod(part).

                enqueue(new Callback<AddEvent>() {
                    @Override
                    public void onResponse(@NotNull Call<AddEvent> call, @NotNull retrofit2.Response<AddEvent> response) {
                        dialog.dismiss();

                        SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
                        PicUrl = response.body().getMessage();
                        editor.putString("PicUrl", PicUrl);
                        editor.apply();
                        AddManger add_manger = new AddManger();
                        Bundle bundle = new Bundle();
                        bundle.putString("school_dashboard", "2");
                        add_manger.setArguments(bundle);
                        fragmentManage = getActivity().getSupportFragmentManager();
                        transaction = fragmentManage.beginTransaction();
                        transaction.replace(R.id.school_reg_fram, add_manger);
                        transaction.commit();


                    }

                    @Override
                    public void onFailure(Call<AddEvent> call, Throwable t) {
                        dialog.dismiss();
                        Log.e("Failure", "onFailure: ", t);
                    }
                });
    }

}

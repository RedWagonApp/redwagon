package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Connectivity;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class EditCarDetails extends Fragment implements View.OnClickListener {
    String ID = "";
    EditText edit_details_make, edit_details_car_model, edit_details_car_clr, edit_details_reg_num, edit_details_free_seats;
    Button edit_details_save;
    TextView text_title;
    private String car_make_get, car_model_get, car_clr_get, rfg_car_get, number_seats_get;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.edit_cardetails, container, false);
        Bundle args = getArguments();

        findView(view);
        FamilyDashboardActivity.title.setText("Edit Details");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        ID = args.getString("Id", "");
        Log.e("IDDIDD", "IDD=" + ID);
        getCarDetails(ID);


        edit_details_save.setOnClickListener(this);


        return view;
    }

    private void findView(View view) {

        edit_details_make = view.findViewById(R.id.edit_details_make);
        edit_details_car_clr = view.findViewById(R.id.edit_details_car_clr);
        edit_details_reg_num = view.findViewById(R.id.edit_details_reg_num);
        edit_details_free_seats = view.findViewById(R.id.edit_details_free_seats);
        edit_details_car_model = view.findViewById(R.id.edit_details_car_model);
        edit_details_save = view.findViewById(R.id.edit_details_save);
    }

    private void getCarDetails(String caritem_item) {
        final Dialog dialog = AppUtils.showProgress(getActivity());

        APIService mAPIService = AppUtils.getAPIService(getContext());
        mAPIService.getCarDetail(caritem_item).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    edit_details_make.setText(response.body().getBody().getMake());
                    edit_details_car_clr.setText(response.body().getBody().getColor());
                    edit_details_reg_num.setText(response.body().getBody().getRegNumber());
                    edit_details_free_seats.setText(response.body().getBody().getFreeSeat());
                    edit_details_car_model.setText(response.body().getBody().getModal());

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    Toast.makeText(getContext(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }

//    private void Service(final String IDv) {
//
//
//        String url = Constans.BASEURL + "api/editCarList";
//        Log.e("urllll", "Constans.BASEURL+" + url);
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    Log.e("response", "response" + response);
//                    JSONObject jsonObject = new JSONObject(response);
//                    JSONObject jsonObject1 = jsonObject.getJSONObject("body");
//                    String make = jsonObject1.getString("make");
//                    String modal = jsonObject1.getString("modal");
//                    String color = jsonObject1.getString("color");
//                    String regNumber = jsonObject1.getString("regNumber");
//                    String freeSeat = jsonObject1.getString("freeSeat");
//                    edit_details_make.setText(make);
//                    edit_details_car_clr.setText(color);
//                    edit_details_reg_num.setText(regNumber);
//                    edit_details_free_seats.setText(freeSeat);
//                    edit_details_car_model.setText(modal);
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                NetworkResponse response = error.networkResponse;
//                if (error instanceof ServerError && response != null) {
//                    try {
//                        String res = new String(response.data,
//                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
//                        // Now you can use any deserializer to make sense of data
//                        JSONObject obj = new JSONObject(res);
//
//                        Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_SHORT).show();
//                    } catch (UnsupportedEncodingException e1) {
//                        // Couldn't properly decode data to string
//                        e1.printStackTrace();
//                    } catch (JSONException e2) {
//                        // returned data is not JSONObject?
//                        e2.printStackTrace();
//                    }
//                }
//                Log.e("response", "onErrorResponse" + error.toString());
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                params.put("id", IDv);
//
//                return params;
//
//            }
//
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String, String> params = new HashMap<String, String>();
//                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
//                Map<String, String> headersSys = null;
//                try {
//                    headersSys = super.getHeaders();
//                } catch (AuthFailureError authFailureError) {
//                    authFailureError.printStackTrace();
//                }
//                Map<String, String> headers = new HashMap<String, String>();
//                headersSys.remove("Authorization");
//                headers.put("Authorization", bearer);
//                headers.putAll(headersSys);
//                Log.e("headersSys", "sdsd" + headersSys);
//                return headers;
//
//            }
//
//        };
//        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//        requestQueue.add(stringRequest);
//
//    }

    private void UpdateCar() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/updateCarDetails";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                try {
                    Log.e("response", "response" + response);
                    JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);

                        Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", ID);
                params.put("make", car_make_get);
                params.put("modal", car_model_get);
                params.put("color", car_clr_get);
                params.put("regNumber", rfg_car_get);
                params.put("freeSeat", number_seats_get);

                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.edit_details_save) {


            Valdation();
        }

    }

    private void Valdation() {

        car_make_get = edit_details_make.getText().toString().trim();
        car_clr_get = edit_details_car_clr.getText().toString().trim();
        rfg_car_get = edit_details_reg_num.getText().toString().trim();
        number_seats_get = edit_details_free_seats.getText().toString().trim();
        car_model_get = edit_details_car_model.getText().toString().trim();

        if (!car_make_get.equals("")) {

            if (!car_model_get.equals("")) {

                if (!car_clr_get.equals("")) {


                    if (!rfg_car_get.equals("")) {


                        if (!number_seats_get.equals("")) {
                            if (!number_seats_get.equals("7") && !number_seats_get.equals("8") && !number_seats_get.equals("9") && !number_seats_get.equals("0")) {


                                if (Connectivity.isConnected(getContext())) {
                                    UpdateCar();
                                }
                            } else {
                                Toast.makeText(getActivity(), "enter a maximum 6 seats ", Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            edit_details_reg_num.setError("Please enter the Number of FREE Seats");
                        }
                    }

                } else {
                    edit_details_reg_num.setError("Please enter the REG Number");
                }
            } else {
                edit_details_car_model.setError("Please enter the Car Model");
            }
        } else {

            edit_details_make.setError("Please enter the Make");
        }
    }
}

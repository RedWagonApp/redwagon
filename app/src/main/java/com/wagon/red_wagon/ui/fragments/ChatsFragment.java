package com.wagon.red_wagon.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ChatsListAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.ModelUserChat;
import com.wagon.red_wagon.model.NotificationSimple;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class ChatsFragment extends Fragment {

    public static RecyclerView recyclerView;
    ArrayList<ModelUserChat> list;
    private LinearLayoutManager mLayoutManager;
    public ChatsListAdapter mAdapter;
    private ProgressBar progressBar;
    TextView titleTxt;
    ImageView gallback, add_user;
    View view;
    EditText search_et;
    public static Socket socket;
    List<Body> responseBody;
    FragmentManager fragmentManager;
    public static Activity activity;
    RelativeLayout linear_main_bar;
    ImageView allfmshow, add_user2;
    String value = "",userid="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_chat, container, false);
        UpdateSettings("1");
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        allfmshow = view.findViewById(R.id.allfmshow);
        add_user = view.findViewById(R.id.add_user_msgg);
        add_user2 = view.findViewById(R.id.add_user_msg);

        assert getArguments() != null;
        value = getArguments().getString("value", "");
        userid = getArguments().getString("userid", "");
        Log.e("useriduserid", "onCreateView: "+userid );
        if (value.equals("1")) {

            linear_main_bar.setVisibility(View.GONE);
            FamilyDashboardActivity.title.setText("Chat");
            ((FamilyDashboardActivity) getActivity()).isVisible(false);
            FamilyDashboardActivity.allfmshow.setVisibility(View.VISIBLE);
            FamilyDashboardActivity.allfmshow.setImageResource(R.drawable.newuser_msg);
            FamilyDashboardActivity.allfmshow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    socket.disconnect();

                    ShowSchoolManger invoice_frag = new ShowSchoolManger();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("value", "1");
                    invoice_frag.setArguments(bundle1);
                    fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.chat_frag, invoice_frag, "Chat");
                    transaction.addToBackStack(null);
                    transaction.commit();

                }
            });
        } else {
            linear_main_bar.setVisibility(View.VISIBLE);
            add_user2.setVisibility(View.VISIBLE);
        }
        activity = getActivity();
        list = new ArrayList<>();
        // activate fragment menu
        setHasOptionsMenu(true);
        titleTxt = view.findViewById(R.id.text_title);
        titleTxt.setText("Chat");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        add_user2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                socket.disconnect();

                ShowSchoolManger invoice_frag = new ShowSchoolManger();
                Bundle bundle1 = new Bundle();
                bundle1.putString("value", "2");
                invoice_frag.setArguments(bundle1);
                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.chat_frag, invoice_frag,"Chat");
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
//        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        search_et = (EditText) view.findViewById(R.id.search_et);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);

        Get_Players();
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));


        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input

                filter(editable.toString());
            }
        });


//        bindView();


        return view;
    }

    private void filter(String text) {

        ArrayList<ModelUserChat> filterdList = new ArrayList<>();
        if (list.size() != 0) {
            for (ModelUserChat s : list) {

                if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                    filterdList.add(s);
                }
            }

            mAdapter.filterList(filterdList);
        }
    }

//    public void bindView() {
//        try {
//            // mAdapter.notifyDataSetChanged();
//            progressBar.setVisibility(View.GONE);
//        } catch (Exception e) {
//        }
//
//    }

    @Override
    public void onDestroy() {
        //Remove the listener, otherwise it will continue listening in the background
        //We have service to run in the background
        socket.close();
        socket.disconnect();
        super.onDestroy();
    }

    private void Get_Players() {


        try {
            socket = IO.socket(Constans.BASEURL);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", Constans.getFamilyId(getActivity()));

            socket.emit("connect_user", jsonObject);

            socket.connect();
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("user_id", Constans.getFamilyId(getActivity()));
            socket.emit("chat_list", jsonObject1);

            socket.on("chat_list", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    try {
                        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
//

                                JSONArray data = (JSONArray) args[0];
                                if (list.size() > 0) {
                                    list.clear();
                                }

                                for (int i = 0; i < data.length(); i++) {

                                    try {

                                        Log.e("SizeSize", "call: " + data.length());
                                        JSONObject jsonObject = data.getJSONObject(i);

                                        String otheruser = jsonObject.getString("otheruser");
                                        String message = jsonObject.getString("message");
                                        String profile_image = jsonObject.getString("profile_image");
                                        String name = jsonObject.getString("name");
                                        String chat_id = jsonObject.getString("chat_id");
                                        String room = jsonObject.getString("room");
                                        Log.e("namename", "call: " + room);

                                        ModelUserChat modelUserChat = new ModelUserChat(message, profile_image, name, otheruser, chat_id, room,userid);

                                        list.add(modelUserChat);


                                    } catch (Exception e) {
                                        Log.e("Exception", e.toString());
                                    }

//                    recyclerView.setAdapter(mAdapter);
                                }
//                    mAdapter = new ChatsListAdapter(getContext(),responseBody);
                                mAdapter = new ChatsListAdapter(getActivity(), list);
                                recyclerView.setAdapter(mAdapter);

                            }
                        });

                    } catch (NullPointerException e) {
                        Log.e("Exception", e.toString());
                    }
                    // Stuff that updates the UI

                }
            });


        } catch (JSONException e) {

            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onResume() {
        super.onResume();
        socket.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        socket.connect();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        socket.disconnect();

    }
    private void UpdateSettings(String s) {

        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<NotificationSimple> getApi;

        getApi = mAPIService.UpdateSettings(Constans.getFamilyId(getActivity()),s);


        getApi.enqueue(new Callback<NotificationSimple>() {
            @Override
            public void onResponse(Call<NotificationSimple> call, retrofit2.Response<NotificationSimple> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());



            }

            @Override
            public void onFailure(Call<NotificationSimple> call, Throwable t) {


                Log.e("Failure", "onFailure: ", t);
            }
        });

    }
}
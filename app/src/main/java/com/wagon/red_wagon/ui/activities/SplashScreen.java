package com.wagon.red_wagon.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.NotificationResponse;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.Q)
public class SplashScreen extends AppCompatActivity {
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 111;
    String username = "";
    static String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CALENDAR,
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    PackageInfo info;
    NotificationResponse notificationResponse;
    LocationManager locationManager;
    String latitude, longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* switch (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                Log.e("uiMode", "onCreate: " + "yes");
                // Toast.makeText(this, "yes", Toast.LENGTH_SHORT).show();
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                Log.e("uiMode", "onCreate: " + "no");
                break;
        }*/

        firebaseInstance();


        try {
            info = getPackageManager().getPackageInfo("com.wagon.red_wagon", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        new Handler().postDelayed(new Runnable() {

                                      /*
                                       * Showing spalsh screen with a timer. This will be useful when you
                                       * want to show case your app ic_launcher / company
                                       */

                                      @Override
                                      public void run() {
//
                                          if (checkPermissions(SplashScreen.this)) {
                                              getLocation();
                                              openScreen();
                                          }


                                      }
                                  },
                3000);


    }

    private void openScreen() {
        Intent intent1 = getIntent();
        String action = intent1.getAction();
        String type1 = intent1.getType();

        if (Intent.ACTION_SEND.equals(action) && type1 != null) {
            if ("text/plain".equals(type1)) {
                String sharedText = intent1.getStringExtra(Intent.EXTRA_TEXT);
                Log.e("Inerntget", sharedText);
            }
        }
        SharedPreferences sharedPreferences = getSharedPreferences("Login", MODE_PRIVATE);
        username = sharedPreferences.getString("userr", "");
        Log.e("Hlo", "nnnnnnnnnn: " + username);
        onNewIntent(getIntent());
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("android_id", "android_id" + android_id);

        if (username.equals("")) {
            Intent iv = new Intent(SplashScreen.this, LogInActivity.class);
            startActivity(iv);
            ActivityCompat.finishAffinity(this);
//
        } else if (username.equals("Family") || username.equals("Driver")) {
            Bundle extras = getIntent().getExtras();
            Intent iv = new Intent(SplashScreen.this, FamilyDashboardActivity.class);

            if (extras != null) {
                if (extras.getString("type") != null) {
                    switch (extras.getString("type")) {
                        case "Request":
                            String requestId = extras.getString("requestId");
                            String name = extras.getString("name");
                            String pickAddress = extras.getString("pickAddress");
                            String dropAddress = extras.getString("dropAddress");
                            String seat = extras.getString("seat");
                            String userId = extras.getString("userId");
                            String driverId = extras.getString("driverId");
                            String type = extras.getString("type");
                            String carPoolId = extras.getString("carPoolId");
                            String driverName1 = extras.getString("driverName1");
                            String carPoolName = extras.getString("carPoolName");
                            String imageurl = extras.getString("imageurl");
                            double pickLat = Double.parseDouble(extras.getString("pickLatitude"));
                            double pickLong = Double.parseDouble(extras.getString("pickLongitude"));
                            double dropLat = Double.parseDouble(extras.getString("dropLatitude"));
                            double dropLong = Double.parseDouble(extras.getString("dropLongitude"));

                            LatLng pickLtLtng = new LatLng(pickLat, pickLong);
                            LatLng dropLtLtng = new LatLng(dropLat, dropLong);
                            Log.e("seatseat", "openScreen: "+seat );
                             notificationResponse = new NotificationResponse(driverId, userId, seat, pickAddress, requestId, dropAddress, type, name, carPoolId, driverName1, carPoolName,imageurl);
                            Log.e("onNewIntent", getIntent().getExtras().getString("type"));

                            break;
                        case "ride status":
                            if (extras.getString("statusrequest").equals("1")) {
                                String statusrequest = String.valueOf(extras.getInt("statusrequest"));
                                notificationResponse = new NotificationResponse("1", extras.getString("type"));
                            }

                            break;
                        case "completed":

                            String driverIdd = extras.getString("driverId");
                            String userIdd = extras.getString("userId");
                            String username = extras.getString("name");

                            notificationResponse = new NotificationResponse("1", driverIdd, userIdd, "completed", username);

                            break;
                        case "joinFamily":
                            String senduserId = extras.getString("userId1");
                            String meUserId = extras.getString("userId2");
                            String msg = extras.getString("msg");

                            Log.e("msg", "openScreen: " + msg);
                            notificationResponse = new NotificationResponse("1", meUserId, senduserId, "joinFamily", msg, "null");

                            break;

                        case "addFamily":
                            String msgg = extras.getString("msg");
                            String typee = extras.getString("type");
                            notificationResponse = new NotificationResponse("1", "", "", typee, msgg, "null");
                            break;

                        case "joinCarPool":
                            msg = extras.getString("msg");
                            type = extras.getString("type");
                            carPoolId = String.valueOf(extras.getString("carPoolId"));
                            driverId = String.valueOf(extras.getString("driverId"));
                            String childId = String.valueOf(extras.getString("childId"));
                            String parentId = String.valueOf(extras.getString("parentId"));
                            String requestStatus = String.valueOf(extras.getString("requestStatus"));
                            notificationResponse = new NotificationResponse(driverId, carPoolId, childId, parentId, msg, type, requestStatus);

                            break;

                        case "join":
                            msg = extras.getString("msg");
                            type = extras.getString("type");
                            carPoolId = String.valueOf(extras.getString("carPoolId"));
                            driverId = String.valueOf(extras.getString("driverId"));
                            String driverName = String.valueOf(extras.getString("driverName1"));
                            carPoolName = String.valueOf(extras.getString("carPoolName"));
                            String chatId = String.valueOf(extras.getString("chatId"));

                            userId = String.valueOf(extras.getString("userId"));
                            notificationResponse = new NotificationResponse(driverId, userId, "", "", chatId, "", type, msg, carPoolId, driverName, carPoolName);

                            //  notificationResponse = new NotificationResponse(userId, carPoolId, driverName, carPoolName, msg, type, userId);

                            break;
                        case "addJoinCarPool":
                            msgg = extras.getString("msg");
                            typee = extras.getString("type");
                            notificationResponse = new NotificationResponse("1", "", "", typee, msgg, "null");
//
                            break;
                            case "Chat":
                            msgg = extras.getString("msg");
                            typee = extras.getString("type");
                                userId = extras.getString("userId");
                            notificationResponse = new NotificationResponse("1", userId, "", typee, msgg, "null");
//
                            break;
                    }

                }
            }

            iv.putExtra("data", (Serializable) notificationResponse);
            iv.putExtra("check", "1");
            iv.putExtra("type", "2");
            iv.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(iv);
            ActivityCompat.finishAffinity(this);
        } else if (username.equals("School")) {
            Intent intent = new Intent(SplashScreen.this, SchoolDashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("Coach", "1");
            intent.putExtra("type", "2");
            startActivity(intent);
            ActivityCompat.finishAffinity(this);
        } else if (username.equals("Manager")) {
            Intent intent = new Intent(SplashScreen.this, SchoolDashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("Coach", "2");
            intent.putExtra("type", "1");
            startActivity(intent);
            ActivityCompat.finishAffinity(this);
        } else if (username.equals("child")) {
//            Log.e("Manager", "FamilyFamily" + type);

            Intent intent = new Intent(SplashScreen.this, FamilyDashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("check", "1");
            intent.putExtra("type", "2");
            startActivity(intent);
            ActivityCompat.finishAffinity(this);
        }

    }


    public static boolean checkPermissions(Activity activity) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[],
                                           int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0) {
                    String permissionsDenied = "";
                    for (String per : permissionsList) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            permissionsDenied += "\n" + per;


                        }

                    }
                    openScreen();
                } else {
                    ActivityCompat.finishAffinity(this);
                }
                return;
            }
        }
    }


    private void firebaseInstance() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.e("firebaseInstace", "getInstanceId failed", task.getException());
                return;
            }

            String fcmToken = task.getResult().getToken();

            SharedPreferences.Editor editor1 = getSharedPreferences("Login", MODE_PRIVATE).edit();
            editor1.putString("FCMToken", fcmToken);
            editor1.apply();
            Log.e("FCMToken", fcmToken);

        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                SplashScreen.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                SplashScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
                Log.e("Your Location: " ,  "Latitude: " + latitude + "\n" + "Longitude: " + longitude);
            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

package com.wagon.red_wagon.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.ContractSelectAc;

import java.util.ArrayList;

public class AddRoasterFrag extends Fragment implements View.OnClickListener {

    private static final int PICK_CONTACT = 1;
    Button skip_btn;
    TextView addNewTeam, addRosterManually, text_title, student_btn;
    String teamId;
    ImageView contat_img, gallback;
    ArrayList<String> userContract;
    String name, phonenumber, displayName = "";
    private final int REQUEST_CODE = 99;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.add_rooster_lay, container, false);
        findviewId(view);
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Add Your Roster");

        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        skip_btn.setOnClickListener(this);
        addNewTeam.setOnClickListener(this);
        addRosterManually.setOnClickListener(this);
        contat_img.setOnClickListener(this);
        student_btn.setOnClickListener(this);

        return view;
    }


    private void findviewId(View view) {

        skip_btn = view.findViewById(R.id.skip_btn);
        addNewTeam = view.findViewById(R.id.another_team_btn);
        addRosterManually = view.findViewById(R.id.add_manual_btn);
        contat_img = view.findViewById(R.id.contat_img);
        student_btn = view.findViewById(R.id.student_btn);
        Bundle bundle = getArguments();
        if (bundle != null) {
            teamId = bundle.getString("teamId", "");
        }
    }

    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.skip_btn) {
            TeamDetailFrag createTeamFrag = new TeamDetailFrag();
            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.replace(R.id.frame_team, createTeamFrag);
            trans.commit();
//            getActivity().getSupportFragmentManager().popBackStackImmediate(1, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            //manager.popBackStack();
        }

        if (v.getId() == R.id.another_team_btn) {

            ShowallPlayer showallPlayer = new ShowallPlayer();
            Bundle bundle = new Bundle();
            bundle.putString("from", "Player");
            bundle.putString("teamId", teamId);
            showallPlayer.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.roster_fragment, showallPlayer);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        if (v.getId() == R.id.add_manual_btn) {
            AddRosterManuallyFrag createTeamFrag = new AddRosterManuallyFrag();
            Bundle bundle = new Bundle();
            bundle.putString("teamId", teamId);
            createTeamFrag.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.roster_fragment, createTeamFrag);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        if (v.getId() == R.id.student_btn) {
            ShowallPlayer showallPlayer = new ShowallPlayer();
            Bundle bundle = new Bundle();
            bundle.putString("teamId", teamId);
            bundle.putString("from", "Students");
            showallPlayer.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.roster_fragment, showallPlayer);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        if (v.getId() == R.id.contat_img) {
            Intent intent = new Intent(getActivity(), ContractSelectAc.class);
            startActivity(intent);

        }


    }


}

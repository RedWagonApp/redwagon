package com.wagon.red_wagon.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;

public class ChildCongFrag extends Fragment {
    TextView text_title;
    LinearLayout click_refer;
    ImageView gallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.child_congratulation, container, false);
        click_refer = view.findViewById(R.id.click_refer);

        FamilyDashboardActivity.title.setText("Congratulations");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        click_refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.cong_child, new Invite_Screen());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return view;
    }
}

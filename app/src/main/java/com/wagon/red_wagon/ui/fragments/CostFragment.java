package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ActivityCoachAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

public class CostFragment extends Fragment implements View.OnClickListener, ActivityCoachAdapter.AdapterCallback {
    private static final int GALLERY_REQUEST_CODE = 121, MULTIPLE_REQUEST_CODE = 122;
    EditText coach_activity, team_activity, cost_activity;
    private String court_activity_get = "", activity_scl_get = "",
            select_class_scl_get = "", event_time_zone_activity_get = "",
            event_country_activity_get = "",
            event_zip_activity_get = "",
            date_scl_activity_get = "",
            duration_activity_get = "",
            address_activity_get = "", cost_activity_get = "", dayOfWeek = "";

    Button save_activity;
    private String image_path = "", teamId = "0", coachId = "0";
    CircleImageView logo_img;
    LinearLayout upload_logo, upload_images, lnrImages;
    TextView text_title, select_class_scl, not_found_txt;
    ImageView gallback;
    private String from;
    ArrayList<String> imagesArr;
    ArrayList<String> uriArr = new ArrayList<>();
    RelativeLayout relativeLayout;
    LinearLayout layout1, layout2, layout3;
    private Dialog coachdialog;
    private RecyclerView coach_rv;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_activity, container, false);
        FindviewID(view);

        Bundle args = getArguments();

        if (args != null) {
            from = args.getString("from", "");
            activity_scl_get = args.getString("eventNameStr", "");
            event_time_zone_activity_get = args.getString("eventTimeZoneStr", "");
            event_country_activity_get = args.getString("eventCountryStr", "");
            event_zip_activity_get = args.getString("eventZipCodeStr", "");
            address_activity_get = args.getString("eventLocationStr", "");
            date_scl_activity_get = args.getString("eventDateTimeStr", "");
            duration_activity_get = args.getString("eventDurationStr", "");
            court_activity_get = args.getString("eventCourtStr", "");
            dayOfWeek = args.getString("dayOfWeek", "");

        }
        text_title = view.findViewById(R.id.text_title);
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        save_activity.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        upload_images.setOnClickListener(this);
        team_activity.setOnClickListener(this);
        coach_activity.setOnClickListener(this);
        text_title.setText("Cost");

        return view;
    }

    private void FindviewID(View view) {
        upload_logo = view.findViewById(R.id.upload_logo);
        upload_images = view.findViewById(R.id.upload_images);
        logo_img = view.findViewById(R.id.logo_img);
        lnrImages = view.findViewById(R.id.lnrImages);
        save_activity = view.findViewById(R.id.save_activity);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.linear_main_bar);
        cost_activity = view.findViewById(R.id.cost_activity);
        coach_activity = view.findViewById(R.id.coach_activity);
        team_activity = view.findViewById(R.id.team_activity);

        layout1 = view.findViewById(R.id.lay1);
        layout2 = view.findViewById(R.id.lay2);
        layout3 = view.findViewById(R.id.lay3);

        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.GONE);
        layout3.setVisibility(View.VISIBLE);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_activity) {
            validation();
        }
        if (v.getId() == R.id.coach_activity) {
            getCoachDialog("Coach");
        }
        if (v.getId() == R.id.team_activity) {
            getCoachDialog("Team");
        }

        if (v.getId() == R.id.upload_images) {
            Matisse.from(getActivity())
                    .choose(MimeType.ofAll())
                    .countable(true)
                    .maxSelectable(3)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                    .thumbnailScale(0.85f)
                    .imageEngine(new GlideEngine())
                    .showPreview(false) // Default is `true`

                    .forResult(MULTIPLE_REQUEST_CODE);
        }
        if (v.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);


        }

    }


    private void validation() {

        cost_activity_get = cost_activity.getText().toString().trim();



            if (!teamId.equals("0")) {

                DataParse();

            } else {
                team_activity.setError("Please select team");
            }


    }


    public void DataParse() {
        Log.e("fsdfsdf", "DataParse: " + activity_scl_get);

        RecurrenceFrag fragment = new RecurrenceFrag();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.crate_activity_scl, fragment);
        Bundle bundle = new Bundle();
        bundle.putString("eventNameStr", activity_scl_get);
        bundle.putString("eventTimeZoneStr", event_time_zone_activity_get);
        bundle.putString("eventCountryStr", event_country_activity_get);
        bundle.putString("eventZipCodeStr", event_zip_activity_get);
        bundle.putString("eventDateTimeStr", date_scl_activity_get);
        bundle.putString("eventDurationStr", duration_activity_get);
        bundle.putString("eventLocationStr", address_activity_get);
        bundle.putString("select_class_scl_get", "null");
        bundle.putString("cost_activity_get", cost_activity_get);
        bundle.putString("court_activity_get", court_activity_get);
        bundle.putStringArrayList("imagesArr", imagesArr);
        bundle.putStringArrayList("uriArr", uriArr);
        bundle.putString("image_path", image_path);
        bundle.putString("coachId", coachId);
        bundle.putString("teamId", teamId);
        bundle.putString("teamName", team_activity.getText().toString());
        bundle.putString("coachName", coach_activity.getText().toString());
        bundle.putString("dayOfWeek", dayOfWeek);
        bundle.putString("fromType", "Activities");
        fragment.setArguments(bundle);
        transaction.addToBackStack(null);
//        transaction.commitAllowingStateLoss();
//        fragmentManager.executePendingTransactions();
        transaction.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(image_path);
            logo_img.setImageBitmap(bitmap);


        }

        if (requestCode == MULTIPLE_REQUEST_CODE && resultCode == RESULT_OK) {
            lnrImages.setVisibility(View.VISIBLE);
            imagesArr = (ArrayList<String>) Matisse.obtainPathResult(data);


            for (int i = 0; i < imagesArr.size(); i++) {
                if (imagesArr.get(i).substring(imagesArr.get(i).lastIndexOf(".") + 1).equals("jpg")) {
                    uriArr.add("1");
                } else {
                    uriArr.add("2");
                }
                Bitmap yourbitmap = BitmapFactory.decodeFile(imagesArr.get(i));
                ImageView imageView = new ImageView(getContext());
                imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(50, 50));
                imageView.setMaxHeight(50);
                imageView.setMaxWidth(50);
                imageView.setPadding(5, 0, 5, 0);
                imageView.setImageBitmap(yourbitmap);
                lnrImages.addView(imageView);
                //   Log.e("Matisse", "mSelected: " + getMimeType(uriArr));
            }
        }


    }

    public ArrayList<String> getMimeType(ArrayList<Uri> uri) {
        ArrayList<String> mimeType = new ArrayList<>();

        for (int i = 0; i < uri.size(); i++) {
            if (uri.get(i).getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                ContentResolver cr = getActivity().getContentResolver();
                assert mimeType != null;
                mimeType.add(cr.getType(uri.get(i)));
            } else {
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.get(i)
                        .toString());
                mimeType.add(MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                        fileExtension.toLowerCase()));
            }
        }
        return mimeType;
    }

    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    private void getCoachDialog(String isTeam) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Class");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        not_found_txt = (TextView) coachdialog.findViewById(R.id.not_found_txt);
        not_found_txt.setText("No " + isTeam + " Found.");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);

        getList(this, dialog, isTeam);


        coachdialog.show();
    }

    private void getList(final ActivityCoachAdapter.AdapterCallback callback, final Dialog dialog, String isTeam) {

        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        if (isTeam.equals("Coach")) {
            getApi = mAPIService.getCoachList(Constans.getFamilyId(getActivity()));
        } else {
            getApi = mAPIService.getTeamList(Constans.getFamilyId(getActivity()));
        }

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                    } else {
                        not_found_txt.setVisibility(View.GONE);
                        coach_rv.setVisibility(View.VISIBLE);
                        ActivityCoachAdapter customAdapter = new ActivityCoachAdapter(getContext(), responseBody, callback, isTeam);
                        coach_rv.setAdapter(customAdapter);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                not_found_txt.setVisibility(View.VISIBLE);
                coach_rv.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    @Override
    public void onMethodCallback(String coachName, String coachid, String teamName, String teamid) {
        coachdialog.dismiss();
        team_activity.setText(teamName);
        teamId = teamid;
        coachId = coachid;
        coach_activity.setText(coachName);
    }
}

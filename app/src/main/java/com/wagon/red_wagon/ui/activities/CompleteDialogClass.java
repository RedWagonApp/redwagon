package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.NotificationResponse;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;

public class CompleteDialogClass extends AppCompatActivity {
    Dialog driverDialog, ratingDialog;
    NotificationResponse notfresponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_dialog_class);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            notfresponse = (NotificationResponse) extras.getSerializable("data");
            if (notfresponse != null) {
                if (notfresponse.getType().equals("completed")) {
                    requestDialog(notfresponse);
                } /*else  if (notfresponse.getType().equals("completed")){
                    ratingDialog(notfresponse);
                }*/
            }

            //Log.e("Response", "onCreateView: " + notfresponse.getType());

        }
    }

    private void requestDialog(NotificationResponse response) {
        driverDialog = new Dialog(CompleteDialogClass.this);
        driverDialog.setContentView(R.layout.drop_alert);
        driverDialog.setCanceledOnTouchOutside(false);
        ImageView bestrank = driverDialog.findViewById(R.id.bestrank);
        ImageView midiumrank = driverDialog.findViewById(R.id.midiumrank);
        ImageView failrating = driverDialog.findViewById(R.id.failrating);
        TextView username = driverDialog.findViewById(R.id.username);
        TextView leave_rating = driverDialog.findViewById(R.id.leave_rating);

        leave_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        username.setText(response.getName() + " has been safely dropped");
        bestrank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usersDriverRating(response.getUserId(), response.getDriverId(), "3");

            }
        });
        midiumrank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usersDriverRating(response.getUserId(), response.getDriverId(), "2");

            }
        });
        failrating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usersDriverRating(response.getUserId(), response.getDriverId(), "1");

            }
        });

        driverDialog.show();
    }

    private void usersDriverRating(String userId, String driverId, String rateing) {
        final Dialog dialog = AppUtils.showProgress(CompleteDialogClass.this);
        APIService mAPIService = AppUtils.getAPIService(CompleteDialogClass.this);
        Call<AddEvent> getApi;

        getApi = mAPIService.usersDriverRating(driverId, userId, rateing);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    finish();
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(CompleteDialogClass.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(CompleteDialogClass.this);                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();

                Log.e("Failure", "onFailure: ", t);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

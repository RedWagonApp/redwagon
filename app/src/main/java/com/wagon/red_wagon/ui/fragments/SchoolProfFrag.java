package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class SchoolProfFrag extends Fragment {
    TextView text_title;
    FragmentTransaction transaction;
    int number = 0;
    private EditText email_school, pasword_school, clubOrgranization, activtiy_school, adress_school, zipe_code_school, phonenumbner_school, website_school, county_school;
    CircleImageView imageset;
    String result;
    Dialog dialog;
    Uri uri;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.school_prof_lay, container, false);
        findview(view);
        text_title.setText("School Profile");
        ImageView gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        getProfileData();
        return view;
    }



    private void findview(View view) {
        text_title = view.findViewById(R.id.text_title);
        imageset = view.findViewById(R.id.imageset);
        county_school = view.findViewById(R.id.county_school);
        website_school = view.findViewById(R.id.website_school);
        phonenumbner_school = view.findViewById(R.id.phonenumbner_school);
        zipe_code_school = view.findViewById(R.id.zipe_code_school);
        adress_school = view.findViewById(R.id.adress_school);
        activtiy_school = view.findViewById(R.id.activtiy_school);
        clubOrgranization = view.findViewById(R.id.clubOrgranization);
        pasword_school = view.findViewById(R.id.pasword_school);
        email_school = view.findViewById(R.id.email_school);
    }


    private void getProfileData() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<AddEvent> getApi;
        getApi = mAPIService.getschoolprofile(Constans.getFamilyId(getActivity()));
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    Body body = response.body().getBody();
                    Glide.with(getContext()).load(Constans.BASEURL + body.getProfileImage()).into(imageset);
                    county_school.setText(body.getCountry());
                    //  website_school.setText(Constans.getprofileData(getContext()).getFirstName());
                    phonenumbner_school.setText(body.getPhoneNumber());
                    zipe_code_school.setText(body.getZipcodee());
                    adress_school.setText(body.getAddress());
                    clubOrgranization.setText(body.getFirstName());
                    email_school.setText(body.getEmail());

                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getActivity(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

}


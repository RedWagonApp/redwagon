package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AddChildInPoolAdapter;
import com.wagon.red_wagon.adapter.RemoveCarpoolChildAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.AddedChildBody;
import com.wagon.red_wagon.model.AddedChildModel;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddChildToPoolFrag extends Fragment implements View.OnClickListener {

    private RecyclerView child_rv, added_child_rv;
    private AddChildInPoolAdapter.AdapterCallback adapterCallback;
    private RemoveCarpoolChildAdapter.AdapterCallback removeadapterCallback;
    private TextView event_name, driver_name, timestatus, not_found_txt;
    private String driverId = "", eventID = "", carpoolid = "", driverName = "", profileImage = "", timestatusStr = "";
    private CircleImageView driver_img;
    LinearLayout added_child_lay;
    ArrayList<String> childIds = new ArrayList<>();
    // Button add_child_btn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.add_child_to_pool_lay, container, false);

        FamilyDashboardActivity.title.setText("Add Child to Pool");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        child_rv = view.findViewById(R.id.child_rv);
        driver_img = view.findViewById(R.id.driver_img);
        timestatus = view.findViewById(R.id.timestatus);
        event_name = view.findViewById(R.id.event_name);
        driver_name = view.findViewById(R.id.driver_name);
        added_child_lay = view.findViewById(R.id.added_child_lay);
        not_found_txt = view.findViewById(R.id.not_found_txt);
        added_child_rv = view.findViewById(R.id.added_child_rv);
        // add_child_btn = view.findViewById(R.id.add_child_btn);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        child_rv.setLayoutManager(linearLayoutManager);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        added_child_rv.setLayoutManager(linearLayoutManager1);
        Bundle bundle = getArguments();
        if (bundle != null) {
            driverId = bundle.getString("driverId");
            eventID = bundle.getString("eventID");
            driverName = bundle.getString("driverName");
            profileImage = bundle.getString("profileImage");
            timestatusStr = bundle.getString("date");
            carpoolid = bundle.getString("carpoolid");
            driver_name.setText(driverName);
            timestatus.setText(timestatusStr);
            Glide.with(getActivity()).load(Constans.BASEURL + profileImage).into(driver_img);
        }


        adapterCallback = new AddChildInPoolAdapter.AdapterCallback() {
            @Override
            public void onMethodCallback(String name, String id, Button v) {
                joinCarPoolChild(id, v);
                Log.e("onMethodCallback", "onMethodCallback: " + name + "\n" + id);

            }
        };

        removeadapterCallback = new RemoveCarpoolChildAdapter.AdapterCallback() {
            @Override
            public void onMethodCallback(String id) {
                ApiDelete(id);
                Log.e("onMethodCallback", "onMethodCallback: " + id + "\n" + id);

            }
        };

        getAddedList(removeadapterCallback);
    }

    @Override
    public void onClick(View v) {

    }


    private void getList(AddChildInPoolAdapter.AdapterCallback callback) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi = mAPIService.GetChildListNew(Constans.getFamilyId(getActivity()));

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    AddChildInPoolAdapter childTrackAdapter = new AddChildInPoolAdapter(getActivity(), responseBody, childIds, callback);
                    child_rv.setAdapter(childTrackAdapter);

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    private void getAddedList(RemoveCarpoolChildAdapter.AdapterCallback callback) {
        if (childIds.size() != 0) {
            childIds.clear();
        }
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddedChildModel> getApi = mAPIService.getJoinCarPoolChild(driverId);

        getApi.enqueue(new Callback<AddedChildModel>() {
            @Override
            public void onResponse(Call<AddedChildModel> call, retrofit2.Response<AddedChildModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();

                if (response.code() == 200) {

                    List<AddedChildBody> responseBody = response.body().getBody();
                    if (responseBody.size() != 0) {
                        added_child_lay.setVisibility(View.VISIBLE);
                        not_found_txt.setVisibility(View.GONE);
                        for (int i = 0; i < responseBody.size(); i++) {
                            childIds.add(String.valueOf(responseBody.get(i).getChildId()));
                        }
                        RemoveCarpoolChildAdapter removeCarpoolChildAdapter = new RemoveCarpoolChildAdapter(getActivity(), responseBody, "2", callback);
                        added_child_rv.setAdapter(removeCarpoolChildAdapter);
                    } else {
                        added_child_lay.setVisibility(View.GONE);
                        not_found_txt.setVisibility(View.VISIBLE);
                    }
                    getList(adapterCallback);
                } else if (response.code() == 400) {
                    added_child_lay.setVisibility(View.GONE);
                    not_found_txt.setVisibility(View.VISIBLE);
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddedChildModel> call, Throwable t) {
                added_child_lay.setVisibility(View.GONE);
                not_found_txt.setVisibility(View.VISIBLE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    private void joinCarPoolChild(String childId, final Button v) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi = mAPIService.joinCarPoolChildRequest(carpoolid, driverId, Constans.getFamilyId(getActivity()), childId);
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    v.setTextColor(getResources().getColor(R.color.gray));
                    v.setEnabled(false);
                    Toast.makeText(getContext(), "Request has been sent to driver.", Toast.LENGTH_SHORT).show();

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    private void ApiDelete(String id) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        Call<AddEvent> addEventCall;
        addEventCall = AppUtils.getAPIService(getContext()).deleteJoinCarPoolChild(id);


        addEventCall.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                if (response.code() == 200) {

                    getAddedList(removeadapterCallback);
                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

//                    EventListFragment createTeamFrag = new EventListFragment();
//                    FragmentManager manager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction trans = manager.beginTransaction();
//                    trans.replace(R.id.crate_activity_scl, createTeamFrag);
//
//                    trans.commit();
                    //manager.popBackStack();


                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

}
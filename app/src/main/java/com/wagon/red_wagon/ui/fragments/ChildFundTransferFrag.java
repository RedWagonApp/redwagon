package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardInputWidget;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChildFundTransferFrag extends Fragment implements View.OnClickListener {

    Button trasfer_clickbank, transfertowallet;
    EditText payment, cardname, amountEt;
    CardInputWidget mCardInputWidget;
    String card_number = "", stripeToken = "", childId = "", amount = "";
    private int exp_date, exp_month;
    TextView wallet_amount;
    private Dialog amountDialog;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.childfoundtransefr, container, false);
        FamilyDashboardActivity.title.setText("Child Fund Amount");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        findviewId(view);
        trasfer_clickbank.setOnClickListener(this);
        transfertowallet.setOnClickListener(this);
        assert getArguments() != null;
        childId = getArguments().getString("childId", "");
        amount = getArguments().getString("amount", "");
        wallet_amount.setText("$" + amount);
        return view;
    }

    private void findviewId(View view) {
        trasfer_clickbank = view.findViewById(R.id.trasfer_clickbank);
        transfertowallet = view.findViewById(R.id.transfertowallet);
        amountEt = view.findViewById(R.id.ammount);
        wallet_amount = view.findViewById(R.id.wallet_amount);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.trasfer_clickbank) {
            Apiservice(childId);
        }
        if (v.getId() == R.id.transfertowallet) {
            String amount = amountEt.getText().toString();
            if (!amount.equals("")) {
                ApiTransferTowallet(childId, amount);
            } else {
                amountEt.setError("Please enter amount");
            }

        }

    }

    private void ApiTransferTowallet(String childId, String amountEtget) {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());

        mAPIService.addFamilyToChildWallet(Constans.getFamilyId(getActivity()), amountEtget, childId).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {

                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        Toast.makeText(getActivity(), "Amount Added Successfully", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    Toast.makeText(getActivity(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void Apiservice(String ChildId) {
        amountDialog = new Dialog(getContext());
        amountDialog.setContentView(R.layout.stripe_card_info);
        amountDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button split = amountDialog.findViewById(R.id.split);
        payment = amountDialog.findViewById(R.id.payment);
        cardname = amountDialog.findViewById(R.id.cardname);
        mCardInputWidget = amountDialog.findViewById(R.id.card_input_widget);
        split.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String paymentget = payment.getText().toString().trim();
                String cardnameget = cardname.getText().toString().trim();

                if (!cardnameget.equals("")) {
                    if (!paymentget.equals("")) {
//                        Toast.makeText(activity, "Amount Added", Toast.LENGTH_SHORT).show();
                        addFamilyAmountPayment(ChildId, paymentget, stripeToken);


                    } else {
                        payment.setError("please enter valid amount");
                    }
                } else {
                    cardname.setError("Please enter name");

                }


                mCardInputWidget.setCardInputListener(new CardInputListener() {
                    @Override
                    public void onFocusChange(String focusField) {
                    }
                    @Override
                    public void onCardComplete() {
                    }
                    @Override
                    public void onExpirationComplete() {
                    }
                    @Override
                    public void onCvcComplete() {
                    }
                    @Override
                    public void onPostalCodeComplete() {
                    }
                });


                try {
                    final Card cardToSave = mCardInputWidget.getCard();
                    exp_date = cardToSave.getExpYear();
                    exp_month = cardToSave.getExpMonth();
                    card_number = cardToSave.getNumber();
                    String name = cardToSave.getName();

//                    Log.d("card_number", "card_number: " + exp_date);
//                    Log.d("card_number", "card_number: " + exp_month);
//                    Log.d("card_number", "card_number: " + card_number);
//                    Log.d("card_number", "card_number: " + name);
                    if (cardToSave == null) {
                        Toast.makeText(getActivity(), "Invalid Card Data", Toast.LENGTH_LONG).show();
                        Log.e("card_number", "payDone: Invalid Card Data");

                        // mErrorDialogHandler.showError("Invalid Card Data");
                    }
                    //final Card card = new Card(cardNumber, cardDate[0], cardDate[1], cvcValue);
// Remember to validate the card object before you use it to save time.
                    else {
                        if (!cardToSave.validateCard()) {
                            // Do not continue token creation.
                            Toast.makeText(getActivity(), "Invalid Card Data", Toast.LENGTH_LONG).show();
                            Log.e("card_number", "onCreate: error");

                        } else {
                            Log.e("card_number", "carrddddddddd: " + cardToSave.getNumber());
                            Stripe stripe = new Stripe(getActivity(), getActivity().getString(R.string.publishablekey));
                            stripe.createToken(cardToSave, new TokenCallback() {
                                public void onSuccess(Token token) {
                                    Log.e("card_number", "onSuccess: " + token);
                                    stripeToken = token.getId();
                                    Log.e("card_number", "onSuccess: " + stripeToken);


                                    // Send token to your server
                                }

                                public void onError(Exception error) {
                                    // Show localized error message
                                    Log.e("card_number", "onError: " + error);
                                }
                            });
                        }
                    }
                } catch (
                        Exception e) {
                    System.out.println(e);
                }
            }

        });
        amountDialog.show();
    }

    private void addFamilyAmountPayment(String childid, String payments, String stripeToken) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());

        mAPIService.addFamilyAmountPayment(Constans.getFamilyId(getActivity()), payments, childid, stripeToken).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {

                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        payment.setText("");
                        Toast.makeText(getActivity(), "Amount Added Successfully", Toast.LENGTH_SHORT).show();
                        amountDialog.dismiss();
                        getActivity().onBackPressed();

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    Toast.makeText(getActivity(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

}

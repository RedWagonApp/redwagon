package com.wagon.red_wagon.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ChildTrackAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.RideBody;
import com.wagon.red_wagon.model.RideModel;
import com.wagon.red_wagon.ui.activities.DriverDialogClass;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.DirectionsJSONParser;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

import static com.facebook.FacebookSdk.getApplicationContext;

public class OnGoingRideFrag extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LatLng sydney;
    private LocationManager locationManager;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private static final int REQUEST_CODE_PERMISSIONS = 121;
    private static final int REQUEST_LOCATION = 1;
    private double lat, longi;
    private String driverId, requestId, userId;
    private String latitude, longitude, from = "";
    View mapView;
    Button start_ride, complete_ride;
    ChildTrackAdapter.AdapterCallback adapterCallback;
    public static Socket socket;
    double drivLong = 0.0d, drivLat = 0.0d;
    Activity thiscontext;

    private Polyline mPolyline;
    LatLng pickupLtLng, dropLtLng;
    SupportMapFragment mapFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.ongoing_lay, container, false);
        FamilyDashboardActivity.title.setText("On Going Ride");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        thiscontext = ((FamilyDashboardActivity) getActivity());
        start_ride = view.findViewById(R.id.start_ride);
        complete_ride = view.findViewById(R.id.complete_ride);
        requestLocationPermission();

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
//GPS is already On then
            getLocation();
//GPS is already On then
        }
        mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.maptrackchild);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
            mapView = mapFragment.getView();
        } else {
            mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.maptrackchild);
        }
        complete_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rideComplete();
            }
        });

        start_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ridestart(  userId, driverId, requestId,"4","start ride");
            }
        });
        getRide();
        return view;

    }

    private void getLocation() {
//Check Permissions again
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location LocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps != null) {
                lat = LocationGps.getLatitude();
                longi = LocationGps.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

//showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else if (LocationNetwork != null) {
                lat = LocationNetwork.getLatitude();
                longi = LocationNetwork.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

// showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);

            } else if (LocationPassive != null) {
                lat = LocationPassive.getLatitude();
                longi = LocationPassive.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

//showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else {
                Toast.makeText(getActivity(), "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }

//Thats All Run Your App


        }

    }

    private void OnGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            mMap.setOnCameraIdleListener(onCameraIdleListener);
            sydney = new LatLng(lat, longi);
            mMap.setMyLocationEnabled(true);
            animateCamera(sydney);
            if (mapView != null &&
                    mapView.findViewById(Integer.parseInt("1")) != null) {

                View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                        locationButton.getLayoutParams();
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                layoutParams.setMargins(0, 30, 30, 0);
            }
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getContext(), Locale.getDefault());
            addresses = geocoder.getFromLocation(lat, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && addresses.size() != 0) {
                String address = addresses.get(0) == null ? "" : addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                Log.e("onMapReady", "onMapReady: " + address);
            }
        } catch (SecurityException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void animateCamera(LatLng ltLng) {
        CameraPosition camPos = new CameraPosition.Builder()
                .target(ltLng)
                .zoom(5)
                .tilt(10)
                .build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        mMap.animateCamera(camUpd3);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ltLng, 17));
    }

    private void requestLocationPermission() {

        boolean foreground = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (foreground) {
            boolean background = ActivityCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;

            if (background) {
// handleLocationUpdates();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            socket.disconnect();
            Fragment fragment = (getFragmentManager().findFragmentById(R.id.maptrackchild));
            if (fragment != null) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .remove(fragment)
                        .commit();

            }
        } catch (Exception e) {

        }
    }



    private void SocketCall(String childId) {

// Log.e("Socket Call", "SocketCall:" + lat + "\n" + longi);
        try {
            socket = IO.socket(Constans.BASEURL);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", childId);
            socket.emit("get_lat_long", jsonObject);
            socket.connect();
            socket.on("get_lat_long", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("ObjectObject", "call: " + args);
                    if (thiscontext != null) {
                        Objects.requireNonNull(thiscontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                JSONObject data = (JSONObject) args[0];

                                try {
                                    Log.e("JSONObject", "latitude" + data.getString("latitude"));
                                    Log.e("JSONObject", "longitude" + data.getString("longitude"));

                                    if (data.getString("latitude") != null && !data.getString("latitude").equals("")) {
                                        drivLat = Double.parseDouble(data.getString("latitude"));
                                        drivLong = Double.parseDouble(data.getString("longitude"));

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                if (drivLat != 0.0d && drivLong != 0.0d) {
                                    if (mMap != null) {
                                        mMap.clear();
                                    }
                                    dropLtLng = new LatLng(drivLat, drivLong);
                                    pickupLtLng = new LatLng(lat, longi);
                                    animateCamera(dropLtLng);
                                    if (pickupLtLng != null && dropLtLng != null) {
                                        String url = getDirectionsUrl(pickupLtLng, dropLtLng);
                                        FetchUrl FetchUrl = new FetchUrl();
                                        FetchUrl.execute(url);
                                    }
                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(drivLat, drivLong))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon)));
                                } else {
                                    Toast.makeText(getContext(), "Location not found", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                    } else {
                        Log.e("getPackageName", "nulll: " + (FamilyDashboardActivity) getActivity());
                    }
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

// For storing data from web service
            String data = "";

            try {
// Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

// Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

// Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

// Connecting to url
            urlConnection.connect();

// Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

// Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

// Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

// Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

// Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

// Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if (mPolyline != null) {
                    mPolyline.remove();
                }
                mPolyline = mMap.addPolyline(lineOptions);

            } else
                Toast.makeText(getApplicationContext(), "No route is found", Toast.LENGTH_LONG).show();
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String key = "key=" + thiscontext.getResources().getString(R.string.google_maps_key);
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + key;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        Log.e("url", url);
        return url;
    }

    private void getRide() {
        final Dialog dialog = AppUtils.showProgress(thiscontext);
        APIService mAPIService = AppUtils.getAPIService(thiscontext);
        Call<RideModel> getApi = mAPIService.oneWaylatlog(Constans.getFamilyId(thiscontext));

        getApi.enqueue(new Callback<RideModel>() {
            @Override
            public void onResponse(@NotNull Call<RideModel> call, @NotNull retrofit2.Response<RideModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    RideBody body = response.body().getBody();

                    if (String.valueOf(body.getUserId()).equals(Constans.getFamilyId(thiscontext))) {
                        SocketCall(String.valueOf(body.getDriverId()));
                    } else if (String.valueOf(body.getDriverId()).equals(Constans.getFamilyId(thiscontext))) {
                        start_ride.setVisibility(View.VISIBLE);
                        userId = String.valueOf(body.getUserId());
                        requestId = String.valueOf(body.getId());
//                        drivername = String.valueOf(body.getDriName());
                        driverId = String.valueOf(body.getDriverId());
                        dropLtLng = new LatLng(Double.parseDouble(body.getPickLatitude()), Double.parseDouble(body.getPickLongitude()));
                        pickupLtLng = new LatLng(lat, longi);
                        if (pickupLtLng != null && dropLtLng != null) {
                            String url = getDirectionsUrl(pickupLtLng, dropLtLng);
                            FetchUrl FetchUrl = new FetchUrl();
                            FetchUrl.execute(url);
                        }
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(pickupLtLng.latitude, pickupLtLng.longitude))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_flag)));
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(dropLtLng.latitude, dropLtLng.longitude))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_flag)));
                        animateCamera(pickupLtLng);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), "No ride found", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<RideModel> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    private void rideComplete() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi = mAPIService.rideComplete(userId, driverId, requestId, "3");

        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    getActivity().onBackPressed();

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    private void ridestart(String userId, String driverId, String requestId, String requestStatus,String notifType) {
        final Dialog dialog = AppUtils.showProgress(thiscontext);
        APIService mAPIService = AppUtils.getAPIService(thiscontext);
        Call<AddEvent> getApi;

        getApi = mAPIService.rideStart(userId, driverId, requestId, notifType, requestStatus);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    complete_ride.setVisibility(View.VISIBLE);
                    start_ride.setVisibility(View.GONE);
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(thiscontext, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(thiscontext);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();

                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AddChildInPoolAdapter;
import com.wagon.red_wagon.adapter.ChildCarpoolAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ChildListCarpoolFrag extends Fragment {
RecyclerView reclyer_child;
String getGetEventName="";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=LayoutInflater.from(container.getContext()).inflate(R.layout.childlistcarpool,container,false);

        FamilyDashboardActivity.title.setText("Child to Pool");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        reclyer_child=view.findViewById(R.id.reclyer_child);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        reclyer_child.setLayoutManager(linearLayoutManager);
        Bundle bundle = getArguments();
        if (bundle != null) {

            getGetEventName = bundle.getString("getGetEventName");

        }
        GetListChildCarpool();

        return view;
    }

    private void GetListChildCarpool() {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi = mAPIService.GetChildListNew(Constans.getFamilyId(getActivity()));

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    ChildCarpoolAdapter childTrackAdapter = new ChildCarpoolAdapter(getActivity(), responseBody,getGetEventName);
                    reclyer_child.setAdapter(childTrackAdapter);

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }
}

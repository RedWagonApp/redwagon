package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CoachRowAdapter;
import com.wagon.red_wagon.adapter.SchoolAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.model.School_Model;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;


public class AboutChild extends Fragment implements View.OnClickListener, CoachRowAdapter.AdapterCallback {
    Button butoon_add, butoon_finish;
    EditText class_child, techer_add, alergies_child, any_meical, school_about;
    String first_name_child_get, school_about_get, class_child_get, coach_child_get, alergies_child_get, any_meical_get, im, last_name_child_get;
    TextView text_title, not_found_txt, coach_child;
    String school_ID = "", dateof_birth_get = "", email_test_get, mobile_num_get, image_path = "";
    Bundle args;
    Bitmap bitmap;
    ImageView tacke_im;
    APIService mAPIService;
    RecyclerView coach_rv;
    Spinner school_about_sp;
    ArrayList arrayList;
    Dialog coachdialog;
    SchoolAdapter.AdapterCallback adapterCallback;
    List<Body> bodyList;
    List<String> objects;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.about_child, container, false);
        mAPIService = AppUtils.getAPIService(getContext());
        arrayList = new ArrayList();

        GetSchoolApi();


        args = getArguments();

        try {
            image_path = args.getString("bitmap", "");
            first_name_child_get = args.getString("first_name_child_get", "");
            last_name_child_get = args.getString("surname_child_get", "");

            dateof_birth_get = args.getString("dateof_birth_get", "");
            email_test_get = args.getString("email_test_get", "");
            mobile_num_get = args.getString("mobile_num_get", "");


            Log.e("first_name_child_get", "=" + bitmap);
        } catch (NullPointerException e) {

        }


        findview_ID(view);
        bodyList = new ArrayList<>();
        objects = new ArrayList<String>();
        objects.add("Select Coach/Field");
        objects.add("Field");
        objects.add("Court");

//        try {
//            Bundle args = getArguments();
//
//            child_get = args.getString( "childd", "" );
//            if(child_get.equals( "1" )){
//                linear_main_bar.setVisibility( View.GONE );
//            }
//            else{
//                linear_main_bar.setVisibility( View.VISIBLE );
//            }
//        }
//        catch (Exception e){
//
//        }


//        try {
//            im = args.getString( "bitmap", "" );
//            bitmap = StringToBitMap( im );
//            tacke_im.setImageBitmap( bitmap );
//            Log.e( "bitmap","="+im );
//        }
//        catch (NullPointerException e){
//
//        }
        butoon_finish.setOnClickListener(this);
        butoon_add.setOnClickListener(this);
        school_about.setOnClickListener(this);
        coach_child.setOnClickListener(this);


        adapterCallback = new SchoolAdapter.AdapterCallback() {
            @Override
            public void onMethodCallback(String name, String id, int position) {
                coachdialog.dismiss();
                school_about.setText(name);
                school_ID = id;
                Log.e("onMethodCallback", "onMethodCallback: " + name + "\n" + id);
            }
        };
        return view;
    }

    private void GetSchoolApi() {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/schoolList";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("body");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jasonvalue = jsonArray.getJSONObject(i);


                        String schoolname = jasonvalue.getString("firstName");
                        String Id = jasonvalue.getString("id");
                        School_Model school_model = new School_Model(schoolname, Id);
                        arrayList.add(school_model);
                    }


                } catch (Exception e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                try {


                    int mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 400) {
                        Toast.makeText(getContext(), "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();

                    } else if (mStatusCode == 401) {
                        AppUtils.sessionExpiredAlert(getContext());
                    }
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception e) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }

    private void findview_ID(View view) {

        school_about = view.findViewById(R.id.school_about);
        coach_child = view.findViewById(R.id.coach_child);

        alergies_child = view.findViewById(R.id.allergy_child);
        any_meical = view.findViewById(R.id.any_meical);
        butoon_add = view.findViewById(R.id.add_child);
        butoon_finish = view.findViewById(R.id.finish_child);
        tacke_im = view.findViewById(R.id.tacke_im);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.add_child:
                validation("another");
                break;

            case R.id.finish_child:
                validation("finish");
                break;

            case R.id.school_about:
                getschoolDialog();
                break;

                case R.id.coach_child:
                getCoachDialog();
                break;


        }

    }


    private void getCoachDetail(final CoachRowAdapter.AdapterCallback callback) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());


        mAPIService.getCoachList(Constans.getFamilyId(getContext())).enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    bodyList = response.body().getBody();
                    if (bodyList.size() != 0) {
                        setSpinner();
                        CoachRowAdapter customAdapter = new CoachRowAdapter(getContext(), bodyList, callback, "");
                        coach_rv.setAdapter(customAdapter);
                    }


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


    private void getschoolDialog() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("School");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        not_found_txt = (TextView) coachdialog.findViewById(R.id.not_found_txt);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);

        getList(adapterCallback, dialog);


        coachdialog.show();
    }

    private void getCoachDialog() {
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Select Teacher/Coach");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);
        getCoachDetail(this);
        coachdialog.show();
    }

    public void validation(String from) {


        school_about_get = school_about.getText().toString().trim();

        // class_child_get = class_child.getText().toString().trim();
        coach_child_get = coach_child.getText().toString().trim();
        alergies_child_get = alergies_child.getText().toString().trim();
        any_meical_get = any_meical.getText().toString().trim();


        if (!school_about_get.equals("")) {

            ApiService(from);

        } else {
            Toast.makeText(getActivity(), "Please enter school", Toast.LENGTH_SHORT).show();

        }

    }

    private void getList(final SchoolAdapter.AdapterCallback callback, final Dialog dialog) {

        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getSchool();


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                    } else {
                        not_found_txt.setVisibility(View.GONE);
                        coach_rv.setVisibility(View.VISIBLE);
                        SchoolAdapter customAdapter = new SchoolAdapter(getContext(), responseBody, callback, "Student");
                        coach_rv.setAdapter(customAdapter);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        not_found_txt.setVisibility(View.VISIBLE);
                        coach_rv.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                not_found_txt.setVisibility(View.VISIBLE);
                coach_rv.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


    private void ApiService(final String from) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        MultipartBody.Part part = null;
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {


            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }
        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), first_name_child_get);
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), last_name_child_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), dateof_birth_get);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), mobile_num_get);
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), email_test_get);
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), school_ID);
        RequestBody id8 = RequestBody.create(MediaType.parse("text/plain"), Constans.getFamilyId(getActivity()));
        RequestBody id9 = RequestBody.create(MediaType.parse("text/plain"), alergies_child_get);
        RequestBody id10 = RequestBody.create(MediaType.parse("text/plain"), any_meical_get);

        Map<String, RequestBody> map = new HashMap<>();
        map.put("firstName", id1);
        map.put("userName", id2);
        map.put("dob", id3);
        map.put("mobile", id5);
        map.put("email", id6);
        map.put("schoolId", id7);
        map.put("familyId", id8);
        map.put("alorgles", id9);
        map.put("medical", id10);
        Log.e("ApiService", "ApiService: " + map);
        mAPIService.ADD_CHILD_CALL(map, part).enqueue(new Callback<AddEvent>() {

            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    //getActivity().onBackPressed();
                    //Toast.makeText(getContext(), "Child Added", Toast.LENGTH_SHORT).show();
                    if (from.equals("finish")) {
                        getActivity().getSupportFragmentManager().popBackStackImmediate(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    } else {
                        getActivity().getSupportFragmentManager().popBackStackImmediate(1, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }

                }
                else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");

                                 Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    } else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                }


            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }

        });


    }


    @Override
    public void onMethodCallback(String name, String id) {
        coachdialog.dismiss();

        String managername = name;
        coach_child.setText(name);

    }

    private void setSpinner() {
        //  final List<String> plantsList = new ArrayList<>( Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.simple_spinner_dropdown, objects) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };


    }
}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class ParentResgisterFrag extends Fragment implements View.OnClickListener {
    //  parent_res_frame
    private EditText country_clik, home_teliphone, street, county, state, zipe_code, sequrty_number, driving_number;
    private String country_clikget = "", home_teliphone_get, flat_number_get, street_get, county_get, state_get, zipe_code_get, united_state_get, sequrty_number_get, driving_number_get, state_issued_get;
    private Button next_restger;
    private EditText state_issued;
    private String united_state;
    private CountryCodePicker ccpdd_coutry, state_issued_spinner;
    TextView text_title;
    AutoCompleteAdapter mAdapter;
    PlacesClient placesClient;
    AutoCompleteTextView flat_number;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.parent_resgister_frag, container, false);
        findview_Id(view);
        text_title.setText("Family Registration");
        ImageView gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        next_restger.setOnClickListener(this);
        final CountryAdapter.CountryCallback countryCallback = new CountryAdapter.CountryCallback() {
            @Override
            public void onSelectCountryCallback(String name) {
                AppUtils.dismissDialog();
                country_clik.setText(name);
            }

            @Override
            public void onSelectTimeZoneCallback(String name) {

            }
        };

        country_clik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.countryDialog(getContext(), countryCallback);
            }
        });

        setUpAutoCompleteTextView();
        flat_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });
        return view;

    }

    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());

    }



    private void getPlaceInfo(double lat, double lng) throws IOException {
        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lng, 1);
        Address address = addresses.get(0);


        if (addresses != null && addresses.size() > 0) {
            street.setText(addresses.get(0).getThoroughfare());
        }

        if (addresses.get(0).getPostalCode() != null) {
            String ZIP = addresses.get(0).getPostalCode();
            zipe_code.setText(ZIP);
            Log.e("ZIP CODE", ZIP);
        }

        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();

            Log.e("CITY", city);
        }

        if (addresses.get(0).getAdminArea() != null) {
            String stater = addresses.get(0).getAdminArea();
            state.setText(stater);
            Log.e("STATE", stater);
        }

        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            country_clik.setText(country);
            Log.e("COUNTRY", country);
        }
    }

    private void findview_Id(View view) {

        text_title = view.findViewById(R.id.text_title);
        next_restger = view.findViewById(R.id.next_restger);
        home_teliphone = view.findViewById(R.id.home_teliphone);
        flat_number = view.findViewById(R.id.flat_number);
        street = view.findViewById(R.id.street);
        county = view.findViewById(R.id.county);
        state = view.findViewById(R.id.state);
        zipe_code = view.findViewById(R.id.zipe_code);
        sequrty_number = view.findViewById(R.id.sequrty_number);
        driving_number = view.findViewById(R.id.driving_number);
        state_issued = view.findViewById(R.id.state_issued);
        ccpdd_coutry = view.findViewById(R.id.ccpdd_coutry);
        country_clik = view.findViewById(R.id.country_clik);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.next_restger) {
            Validation();
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                if (place.getAddressComponents().asList().get(0) != null) {
                    flat_number.setText(place.getAddressComponents().asList().get(0).getName());
                }
                try {
                    getPlaceInfo(Objects.requireNonNull(place.getLatLng()).latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }
    private void Validation() {

        home_teliphone_get = home_teliphone.getText().toString().trim();
        flat_number_get = flat_number.getText().toString().trim();
        street_get = street.getText().toString().trim();
        county_get = county.getText().toString().trim();
        state_get = state.getText().toString().trim();
        zipe_code_get = zipe_code.getText().toString().trim();
        sequrty_number_get = sequrty_number.getText().toString().trim();
        // united_state_get = united_state.getText().toString().trim();
        united_state_get = ccpdd_coutry.getSelectedCountryName();
        country_clikget = country_clik.getText().toString();
        Log.e("united_state", "united_state" + united_state);

        driving_number_get = driving_number.getText().toString().trim();
        state_issued_get = state_issued.getText().toString().trim();


        if (!home_teliphone_get.equals("")) {
            if (!flat_number_get.equals("")) {
                if (!street_get.equals("")) {
                    if (!county_get.equals("")) {
                        if (!state_get.equals("")) {
                            if (!zipe_code_get.equals("")) {
                                if (!country_clikget.equals("")) {
                                    if (!united_state_get.equals("")) {
                                        //   if (!sequrty_number_get.equals("")) {
                                        //  if (!driving_number_get.equals("")) {
                                        if (!state_issued_get.equals("")) {
                                            service();
                                        } else {
                                            state_issued.setError("Please enter a state issued");
                                        }
                                            /*} else {
                                                driving_number.setError("Please enter a driving number");
                                            }*/
                                       /* } else {
                                            sequrty_number.setError("Please enter a sequrty number");
                                        }*/
                                    } else {
                                        Toast.makeText(getActivity(), "Please Select Country", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    country_clik.setError("Please select a Country");
                                }
                            } else {
                                zipe_code.setError("Please enter a zip code");
                            }
                        } else {

                            state.setError("Please enter a state");
                        }
                    } else {
                        county.setError("Please enter a state country");
                    }
                } else {
                    street.setError("Please enter the street ");
                }
            } else {
                flat_number.setError("Please enter the flat number");
            }
        } else {
            home_teliphone.setError("Please enter the home telephone");
        }
    }

    private void service() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/UpdateName";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                try {
                    dialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    String meassage = jsonObject.getString("message");
//                    Toast.makeText(getActivity(), meassage, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor.putString("isCompleted", "yes");
                    editor.putString("userr", "Family");
                    editor.apply();
                    Intent intent = new Intent(getActivity(), FamilyDashboardActivity.class);
                    intent.putExtra("check", "2");
                    intent.putExtra("type", "2");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dialog.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception e) {

                }
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("homePhone", home_teliphone_get);
                params.put("flatNumber", flat_number_get);
                params.put("street", street_get);
                params.put("country", country_clikget);
                params.put("state", state_get);
                params.put("zipCode", zipe_code_get);
                params.put("unitedStates", united_state_get);
                params.put("securityNumber", sequrty_number_get);
                params.put("licenceNumber", driving_number_get);
                params.put("stateIssued", state_issued_get);
                params.put("county", county_get);

                Log.e("params", "params" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
}



package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.DatePicker;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.applandeo.materialcalendarview.utils.DateUtils;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class EditCarPoolDriverFrag extends Fragment implements View.OnClickListener, OnSelectDateListener {


    private RecyclerView carpool_recyclerview;
    private ArrayList<Event_List_model> arrayList;
    private String eventID = "", driverId = "", driverName = "", carpoolid = "";
    private TextView driver_name, text_title, dates_txt;
    private ImageView gallback;
    private Button select_date_btn, save_btn;
    private TextView pick_btn, drop_btn;
    private DatePicker manyDaysPicker;
    private DatePickerBuilder manyDaysBuilder;
    boolean isPick = false;
    private boolean isDrop = false;
    private ArrayList<String> weeksArr = new ArrayList<>();
    private ArrayList<String> selectedDates = new ArrayList<>();
    private String pickUpStatus = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.edit_carpool_driver, container, false);
        findView(view);

        FamilyDashboardActivity.title.setText("Edit");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        select_date_btn.setOnClickListener(this);
        save_btn.setOnClickListener(this);
        pick_btn.setOnClickListener(this);
        drop_btn.setOnClickListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            weeksArr = bundle.getStringArrayList("weeks");
            driverId = bundle.getString("driverId");
            eventID = bundle.getString("eventID");
            driverName = bundle.getString("driverName");
            carpoolid = bundle.getString("carpoolid");
            driver_name.setText(driverName);
        }
        return view;
    }

    private void findView(View view) {
        arrayList = new ArrayList<>();

        select_date_btn = view.findViewById(R.id.select_date_btn);
        save_btn = view.findViewById(R.id.save_btn);
        driver_name = view.findViewById(R.id.driver_name);
        dates_txt = view.findViewById(R.id.dates_txt);
        pick_btn = view.findViewById(R.id.pick_btn);
        drop_btn = view.findViewById(R.id.drop_btn);


     /*   try {
            Bundle bundle = getArguments();
            teamId = bundle.getString("teamId", "");
        } catch (NullPointerException e) {

        }*/


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.select_date_btn) {
            openManyDaysPicker();
        }

        if (v.getId() == R.id.save_btn) {
            if (!pickUpStatus.equals("")) {
                if (selectedDates.size() != 0) {
                    if (isPick && isDrop) {
                        pickUpStatus = "3";
                    }
                    addPickDrop();
                } else {
                    Toast.makeText(getContext(), "Please Select Dates", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), "Please Select Pick/Drop", Toast.LENGTH_SHORT).show();
            }


        }
        if (v.getId() == R.id.pick_btn) {
            isPick = !isPick;
            if (isPick) {
                pickUpStatus = "1";
                pick_btn.setBackgroundResource(R.drawable.buutonshape);
                pick_btn.setTextColor(getResources().getColor(R.color.white));
            } else {
                pickUpStatus = "";
                pick_btn.setBackgroundResource(R.drawable.whitebutton);
                pick_btn.setTextColor(getResources().getColor(R.color.black));
            }

        }
        if (v.getId() == R.id.drop_btn) {
            isDrop = !isDrop;
            if (isDrop) {
                pickUpStatus = "2";
                drop_btn.setBackgroundResource(R.drawable.buutonshape);
                drop_btn.setTextColor(getResources().getColor(R.color.white));
            } else {
                pickUpStatus = "";
                drop_btn.setBackgroundResource(R.drawable.whitebutton);
                drop_btn.setTextColor(getResources().getColor(R.color.black));
            }
        }
    }

    private void addPickDrop() {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<AddEvent> getApi;

        getApi = mAPIService.addRide(carpoolid, eventID, driverId, AppUtils.convertToString(selectedDates), pickUpStatus);
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    //  Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();


                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void openManyDaysPicker() {

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.add(Calendar.DATE, -1);
        // min.add(Calendar.DATE, -1);
        manyDaysBuilder = new DatePickerBuilder(getActivity(), this)
                .setPickerType(CalendarView.MANY_DAYS_PICKER)
                .setDate(Calendar.getInstance())
                .setHeaderColor(R.color.appclrcodew)
                .setSelectionColor(R.color.appclrcodew)
                .setTodayLabelColor(R.color.appclrcodew)
                .setDialogButtonsColor(R.color.appclrcodew)
                .setNavigationVisibility(View.VISIBLE)
                .setMinimumDate(min)
                .setDisabledDays(getDisabledDays());

        manyDaysPicker = manyDaysBuilder.build();


        manyDaysPicker.show();
    }

    private List<Calendar> getDisabledDays() {

        int weeks = 50;
        int week = 0;
        int i = 0;
        Calendar calendar;
        List<Calendar> calendars = new ArrayList<>();


        while (week < weeks * 7) {

            if (weeksArr.size() != 0) {
                if (!weeksArr.contains("MON")) {

                    calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_YEAR, (Calendar.MONDAY - calendar.get(Calendar.DAY_OF_WEEK) + 7 * i));
                    calendars.add(calendar);

                }
                if (!weeksArr.contains("TUE")) {
                    Calendar calendar1 = DateUtils.getCalendar();
                    calendar1.add(Calendar.DAY_OF_YEAR, (Calendar.TUESDAY - calendar1.get(Calendar.DAY_OF_WEEK) + 7 * i));
                    calendars.add(calendar1);
                }
                if (!weeksArr.contains("WED")) {
                    Calendar calendar2 = DateUtils.getCalendar();
                    calendar2.add(Calendar.DAY_OF_YEAR, (Calendar.WEDNESDAY - calendar2.get(Calendar.DAY_OF_WEEK) + 7 * i));
                    calendars.add(calendar2);
                }
                if (!weeksArr.contains("THU")) {
                    Calendar calendar3 = DateUtils.getCalendar();
                    calendar3.add(Calendar.DAY_OF_YEAR, (Calendar.THURSDAY - calendar3.get(Calendar.DAY_OF_WEEK) + 7 * i));
                    calendars.add(calendar3);
                }
                if (!weeksArr.contains("FRI")) {
                    Calendar calendar4 = DateUtils.getCalendar();
                    calendar4.add(Calendar.DAY_OF_YEAR, (Calendar.FRIDAY - calendar4.get(Calendar.DAY_OF_WEEK) + 7 * i));
                    calendars.add(calendar4);
                }
                if (!weeksArr.contains("SAT")) {
                    Calendar calendar5 = DateUtils.getCalendar();
                    calendar5.add(Calendar.DAY_OF_YEAR, (Calendar.SATURDAY - calendar5.get(Calendar.DAY_OF_WEEK) + 7 * i));
                    calendars.add(calendar5);
                }
                if (!weeksArr.contains("SUN")) {
                    Calendar calendar6 = DateUtils.getCalendar();
                    calendar6.add(Calendar.DAY_OF_YEAR, (Calendar.SUNDAY - calendar6.get(Calendar.DAY_OF_WEEK) + 7 * i));
                    calendars.add(calendar6);
                }
            }
            i += 1;
            week += 7;
        }

        return calendars;
    }

    @Override
    public void onSelect(List<Calendar> calendar) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);

        Log.e("DATE", "onSelect: " + calendar.get(0).getTime());
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");

        for (int i = 0; i < calendar.size(); i++) {
            String newDate = format.format(calendar.get(i).getTime());
            selectedDates.add(newDate);
        }

        dates_txt.setText(AppUtils.convertToString(selectedDates));

    }

}
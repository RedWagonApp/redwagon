package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AutoCompleteAdapter;
import com.wagon.red_wagon.adapter.CoachRowAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;
import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class AddPlayer extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CoachRowAdapter.AdapterCallback {
    private static final int GALLERY_REQUEST_CODE = 122;
    EditText first_name_plyr, last_name_plyr, email_plyr, phone_plyr, edit_gender_, dateofbirth, jersey_num, postion_plyr, maneger_addplyr;
    String Gander_get, first_name_plyr_get, last_name_plyr_get, email_plyr_get, phone_plyr_get, edit_gender_get, address_plyr_get, dateofbirth_get, jersey_num_get, postion_plyr_get, maneger_addplyr_get;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Spinner gander_spinner;
    Button save_playr;
    String[] gender = {"Gender", "Male", "Female", "Other"};
    TextView getgander, text_title;
    private String teamId = "0";
    List<Body> bodyList;
    Dialog coachdialog;
    RecyclerView coach_rv;
    String managerId = "0", managername = "";
    ImageView gallback;
    LinearLayout upload_logo;
    CircleImageView logo_img;
    private String image_path = "", manegerID = "0", value = "";
    AutoCompleteTextView address_plyr;
    PlacesClient placesClient;
    AutoCompleteAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_player, container, false);

        FindView_Id(view);

        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Add Player");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        bodyList = new ArrayList<>();
        save_playr.setOnClickListener(this);
        dateofbirth.setOnClickListener(this);
        //   teac_coach_manegaer.setOnClickListener(this);
        gander_spinner.setOnItemSelectedListener(this);
        logo_img = view.findViewById(R.id.logo_img);
        upload_logo = view.findViewById(R.id.upload_logo);
        upload_logo.setOnClickListener(this);

        ArrayAdapter<CharSequence> langAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_text, gender);
        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        gander_spinner.setAdapter(langAdapter);
        setUpAutoCompleteTextView();
        address_plyr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);

                // Initialize the AutocompleteSupportFragment.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getActivity());
                startActivityForResult(intent, 1);
            }
        });
        return view;
    }

    private void setUpAutoCompleteTextView() {
        String apiKey = getString(R.string.google_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());

    }


    private void FindView_Id(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            value = bundle.getString("value", "");
            teamId = bundle.getString("teamId", "");
            manegerID = bundle.getString("manegerID", "");
        }
        getgander = view.findViewById(R.id.getgander);
        first_name_plyr = view.findViewById(R.id.first_name_plyr);
        last_name_plyr = view.findViewById(R.id.last_name_plyr);
        email_plyr = view.findViewById(R.id.email_plyr);
        phone_plyr = view.findViewById(R.id.phone_plyr);
        edit_gender_ = view.findViewById(R.id.edit_gender_);
        phone_plyr = view.findViewById(R.id.phone_plyr);
        address_plyr = view.findViewById(R.id.address_plyr);
        dateofbirth = view.findViewById(R.id.dateofbirth);
        jersey_num = view.findViewById(R.id.jersey_num);
        postion_plyr = view.findViewById(R.id.postion_plyr);


        save_playr = view.findViewById(R.id.save_playr);
        gander_spinner = view.findViewById(R.id.gander_spinner);
        //   teac_coach_manegaer = view.findViewById(R.id.teac_coach_manegaer);
        if (value.equals("1")) {
            jersey_num.setVisibility(View.GONE);
            postion_plyr.setVisibility(View.GONE);
            jersey_num.setText("0");
            postion_plyr.setText("0");
        } else {
            jersey_num.setVisibility(View.VISIBLE);
            postion_plyr.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_playr) {
            first_name_plyr_get = first_name_plyr.getText().toString();
            last_name_plyr_get = last_name_plyr.getText().toString();
            email_plyr_get = email_plyr.getText().toString();
            phone_plyr_get = phone_plyr.getText().toString().trim();
            //edit_gender_get = getgander.getText().toString();

            address_plyr_get = address_plyr.getText().toString();
            dateofbirth_get = dateofbirth.getText().toString();
            jersey_num_get = jersey_num.getText().toString();
            postion_plyr_get = postion_plyr.getText().toString();


            if (!first_name_plyr_get.equals("")) {
                if (!last_name_plyr_get.equals("")) {
                    if (!email_plyr_get.equals("") && emailValidator(email_plyr_get)) {

                        if (!phone_plyr_get.equals("")) {
                            if (!edit_gender_get.equals("Gender")) {

                                if (!jersey_num_get.equals("")) {
                                    if (!postion_plyr_get.equals("")) {

                                        ApiPlayer();

                                    } else {
                                        postion_plyr.setError("Please enter the Position");
                                    }
                                } else {
                                    jersey_num.setError("Please enter the jersey number");
                                }


                            } else {

                                Toast.makeText(getActivity(), "Please enter the gender", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            phone_plyr.setError("Please enter the mobile number");
                        }
                    } else {
                        email_plyr.setError("Please enter the valid email address");
                    }
                } else {
                    last_name_plyr.setError("Please enter the last name");
                }
            } else {
                first_name_plyr.setError("Please enter the first name");
            }

        }
        if (v.getId() == R.id.dateofbirth) {
            datePicker();
        }

        if (v.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

        }
    }

    private void ApiPlayer() {
        if (teamId.equals("0")) {

             ParentInfomationFrag parentInfomationFrag = new ParentInfomationFrag();
            Bundle args = new Bundle();
            args.putString("teamId", teamId);
            args.putString("first_name_plyr_get", first_name_plyr_get);
            args.putString("last_name_plyr_get", last_name_plyr_get);
            args.putString("email_plyr_get", email_plyr_get);
            args.putString("phone_plyr_get", phone_plyr_get);
            args.putString("edit_gender_get", edit_gender_get);
            args.putString("address_plyr_get", address_plyr_get);
            args.putString("dateofbirth_get", dateofbirth_get);
            args.putString("jersey_num_get", jersey_num_get);
            args.putString("postion_plyr_get", postion_plyr_get);
            args.putString("manegerID", manegerID);
            args.putString("image_path", image_path);
            parentInfomationFrag.setArguments(args);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.add_plyer_fram, parentInfomationFrag);
            transaction.addToBackStack(null);
            transaction.commit();


        } else {


            ParentInfomationFrag parentInfomationFrag = new ParentInfomationFrag();
            Bundle args = new Bundle();
            args.putString("teamId", teamId);
            args.putString("first_name_plyr_get", first_name_plyr_get);
            args.putString("last_name_plyr_get", last_name_plyr_get);
            args.putString("email_plyr_get", email_plyr_get);
            args.putString("phone_plyr_get", phone_plyr_get);
            args.putString("edit_gender_get", edit_gender_get);
            args.putString("address_plyr_get", address_plyr_get);
            args.putString("dateofbirth_get", dateofbirth_get);
            args.putString("jersey_num_get", jersey_num_get);
            args.putString("postion_plyr_get", postion_plyr_get);
            args.putString("manegerID", manegerID);
            args.putString("image_path", image_path);
            parentInfomationFrag.setArguments(args);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.add_plyaer_, parentInfomationFrag);
            transaction.addToBackStack(null);
            transaction.commit();

//                        getActivity().onBackPressed();
        }


    }


    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String strDate = format.format(calendar.getTime());
                        String selectedDate = strDate;
                        dateofbirth.setText(selectedDate);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        edit_gender_get = gender[position];

//        if (position == 0) {
//            getgander.setTextColor(getResources().getColor(R.color.gray));
//        } else {
//            getgander.setTextColor(getResources().getColor(R.color.black));
//        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void getCoachDialog() {
        coachdialog = new Dialog(getActivity());
        coachdialog.setContentView(R.layout.coach_dialog);
        coachdialog.setTitle("Select Teacher/Coach");
        coach_rv = (RecyclerView) coachdialog.findViewById(R.id.coach_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        coach_rv.setLayoutManager(linearLayoutManager);
        getCoachDetail(this);

        coachdialog.show();
    }

    private void getCoachDetail(final CoachRowAdapter.AdapterCallback adapterCallback) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());


        mAPIService.getCoachList(Constans.getFamilyId(getContext())).enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    bodyList = response.body().getBody();
                    CoachRowAdapter customAdapter = new CoachRowAdapter(getContext(), bodyList, adapterCallback, "");
                    coach_rv.setAdapter(customAdapter);

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }


            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                logo_img.setImageBitmap(bitmap);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                address_plyr.setText(place.getAddress());


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }

    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }

    @Override
    public void onMethodCallback(String name, String id) {

        coachdialog.dismiss();
        managerId = id;
        managername = name;
        //  teac_coach_manegaer.setText(name);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}

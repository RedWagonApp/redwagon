package com.wagon.red_wagon.ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.RidesDriverAdapter;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.ModelRequest;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.DirectionsJSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

import static com.facebook.FacebookSdk.getApplicationContext;

public class RideShowDriverFrag extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private LatLng sydney;
    private LocationManager locationManager;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private static final int REQUEST_CODE_PERMISSIONS = 121;
    private static final int REQUEST_LOCATION = 1;
    private double picklat, droplat, picklong, droplong, lat, longi;
    private String pickloc, droploc, notes, latitude, longitude;
    public static Socket socket;
    ArrayList<ModelRequest> modelRequests;
    SupportMapFragment mapFragment;
    RecyclerView driverRecyclerView;
    String drivernote = "";
    LinearLayout driver_list_lay;
    TextView no_driver_txt;
    RidesDriverAdapter.onRequestCallback callback;
    private Polyline mPolyline;
    LatLng pickupLtLng, dropLtLng;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.request_ride_show_driver, container, false);
        FamilyDashboardActivity.title.setText("Request Ride");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        driverRecyclerView = view.findViewById(R.id.driverRecyclerView);
        driver_list_lay = view.findViewById(R.id.driver_list_lay);
        no_driver_txt = view.findViewById(R.id.no_driver_txt);
        modelRequests = new ArrayList<>();
        requestLocationPermission();

        Bundle bundle = getArguments();
        if (bundle != null) {
            droplat = bundle.getDouble("dropLt");
            droplong = bundle.getDouble("dropLng");
            picklat = bundle.getDouble("pickLt");
            picklong = bundle.getDouble("pickLng");
            pickloc = bundle.getString("pickLoc", "");
            droploc = bundle.getString("dropLoc", "");
            pickupLtLng = new LatLng(picklat, picklong);
            dropLtLng = new LatLng(droplat, droplong);

            drivernote = bundle.getString("driver_notes", "");
        }

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }
        callback = new RidesDriverAdapter.onRequestCallback() {
            @Override
            public void onRequest(String driverId, String seat) {
                RequestRide(driverId, seat);
            }
        };

        SupportMapFragment mapFragment;
        mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mapreequest);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        driverRecyclerView.setLayoutManager(linearLayoutManager);
        return view;

    }

    private void getLocation() {

//Check Permissions again

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location LocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps != null) {
                lat = LocationGps.getLatitude();
                longi = LocationGps.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

//showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else if (LocationNetwork != null) {
                lat = LocationNetwork.getLatitude();
                longi = LocationNetwork.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

// showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else if (LocationPassive != null) {
                lat = LocationPassive.getLatitude();
                longi = LocationPassive.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

//showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            } else {
                Toast.makeText(getActivity(), "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }

//Thats All Run Your App


        }

    }

    private void OnGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

// Add a marker in Sydney and move the camera
        try {
            mMap.setOnCameraIdleListener(onCameraIdleListener);
            sydney = new LatLng(lat, longi);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.setMyLocationEnabled(true);
            CameraPosition camPos = new CameraPosition.Builder()
                    .target(new LatLng(lat, longi))
                    .zoom(1)
                    .tilt(10)
                    .build();
            CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
            mMap.animateCamera(camUpd3);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));

            if (pickupLtLng != null && dropLtLng != null) {
                String url = getDirectionsUrl(pickupLtLng, dropLtLng);
                FetchUrl FetchUrl = new FetchUrl();
                FetchUrl.execute(url);
            }
            SocketCall(sydney.latitude, sydney.longitude);


        } catch (SecurityException e) {

        }
    }

    private void SocketCall(double lat, double longi) {

        Log.e("SocketCall", "SocketCall:" + lat + "\n" + longi);
        try {

            socket = IO.socket(Constans.BASEURL);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("latitude", lat);
            jsonObject.put("longitude", longi);
            jsonObject.put("user_id", Constans.getFamilyId(getActivity()));
            socket.emit("update_location", jsonObject);
            socket.connect();
            socket.on("nearByUser", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("ObjectObject", "call: " + args);
//                    JSONObject data = (JSONObject) args[0];
                    //  Log.e("JSONObject", "JSONObject"+data.toString() );
                }
            });

            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("user_id", Constans.getFamilyId(getActivity()));
            jsonObject2.put("page", "1");
            socket.emit("nearByUsersList", jsonObject2);
            socket.on("users", new Emitter.Listener() {
                @Override
                public void call(Object... args) {

                    try {

                        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                JSONObject object = (JSONObject) args[0];
                                JSONArray data = null;
                                try {
                                    data = object.getJSONArray("users");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                for (int i = 0; i < data.length(); i++) {

                                    try {

                                        Log.e("SizeSize", "call: " + data.length());
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        String name = jsonObject.getString("name");
                                        double latitude = Double.parseDouble(jsonObject.getString("latitude"));
                                        double longitude = Double.parseDouble(jsonObject.getString("longitude"));
                                        String profileImage = jsonObject.getString("profileImage");
                                        String driverId = String.valueOf(jsonObject.getInt("id"));
                                        String cost_mille = String.valueOf(jsonObject.getInt("cost_mille"));

                                        Log.e("longitude", "longitude: " + latitude);
                                        Log.e("longitude", "longitude: " + longitude);
                                        ModelRequest modelRequest = new ModelRequest(latitude, longitude, profileImage, name, driverId, cost_mille, "0");
                                        modelRequests.add(modelRequest);

                                    } catch (Exception e) {
                                        Log.e("Exception", e.toString());
                                    }
                                }
                                createMarker(modelRequests);

                                RidesDriverAdapter mAdapter = new RidesDriverAdapter(getActivity(), modelRequests, callback);
                                driverRecyclerView.setAdapter(mAdapter);
                            }
                        });
                    } catch (Exception e) {

                    }
                }


            });


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    private void requestLocationPermission() {

        boolean foreground = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (foreground) {
            boolean background = ActivityCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;

            if (background) {
// handleLocationUpdates();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        try {
            Fragment fragment = (getFragmentManager().findFragmentById(R.id.mapreequest));
            if (fragment != null) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .remove(fragment)
                        .commit();
            }
        } catch (Exception e) {

        }
    }

    public void createMarker(ArrayList<ModelRequest> modelRequests) {
        // Log.e("createMarker", "createMarker: " + latitude + "\n" + longitude);
        driver_list_lay.setVisibility(View.VISIBLE);
        if (modelRequests.size() == 0) {
            no_driver_txt.setVisibility(View.VISIBLE);
            driverRecyclerView.setVisibility(View.GONE);
        } else {
            no_driver_txt.setVisibility(View.GONE);
            driverRecyclerView.setVisibility(View.VISIBLE);
        }
        for (int i = 0; i < modelRequests.size(); i++) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(modelRequests.get(i).getLatitude(), modelRequests.get(i).getLongitude()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon)));

        }
        //
    }

    private void RequestRide(String driverId, String seat) {
        Log.e("RequestRidevvvvv", "RequestRide: " + seat);
        final Dialog dialog = AppUtils.showProgress(getActivity());

        APIService mAPIService = AppUtils.getAPIService(getActivity());
        mAPIService.requestRide(Constans.getFamilyId(getActivity()), driverId, pickloc, droploc, String.valueOf(picklong), String.valueOf(picklat), String.valueOf(droplong), String.valueOf(droplat), seat, drivernote).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    Log.e("requestRide", "onResponse: ");
                    Toast.makeText(getActivity(), "Request sent", Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack();
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    Toast.makeText(getActivity(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if (mPolyline != null) {
                    mPolyline.remove();
                }
                mPolyline = mMap.addPolyline(lineOptions);

            } else
                Toast.makeText(getApplicationContext(), "No route is found", Toast.LENGTH_LONG).show();
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String key = "key=" + getResources().getString(R.string.google_maps_key);
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + key;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        Log.e("url", url);
        return url;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }
}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.DriverAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.DriverModel;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class DriverListFrag extends Fragment implements View.OnClickListener {


    RecyclerView driver_list_rv;
    ArrayList<Event_List_model> arrayList;
    public static String eventId = "";
    TextView driver_name, text_title;
    ImageView gallback;
    Button select_date_btn;
    ArrayList<String> driverIds = new ArrayList<>();
    ArrayList<String> userIds = new ArrayList<>();
    ArrayList<String> driverNames = new ArrayList<>();
    ArrayList<String> weeks = new ArrayList<>();
    ArrayList<String> driverImage = new ArrayList<>();
    ArrayList<String> driverPhone = new ArrayList<>();
    ArrayList<String> gender = new ArrayList<>();
    List<Body> list = new ArrayList<>();
    Body response;
    String value = "", carpoolid = "", getEventName = "", weekDay, getCarPoolName = "";
    RelativeLayout linear_main_bar;
    DriverModel driverModel;
    List<DriverModel> driverList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.driver_list, container, false);
        findView(view);
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        FamilyDashboardActivity.title.setText("Drivers");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            value = bundle.getString("value", "");
            eventId = bundle.getString("eventId");
            driverIds = bundle.getStringArrayList("driverIds");
            driverNames = bundle.getStringArrayList("driverNames");
            weeks = bundle.getStringArrayList("weeks");
            gender = bundle.getStringArrayList("gender");
            carpoolid = bundle.getString("carpoolid");
            getEventName = bundle.getString("getEventName");
            driverImage = bundle.getStringArrayList("driverImage");
            driverPhone = bundle.getStringArrayList("driverPhone");
            getCarPoolName = bundle.getString("getCarPoolName");
            Log.e("carpoolid", "onCreateView: " + carpoolid);
            GetdetailEventApi(carpoolid);
        }
        linear_main_bar.setVisibility(View.GONE);
        FamilyDashboardActivity.allfmshow.setImageResource(R.drawable.newuser_msg);
        if (value.equals("1")) {
            FamilyDashboardActivity.allfmshow.setVisibility(View.GONE);
        } else {
            FamilyDashboardActivity.allfmshow.setVisibility(View.GONE);
        }
        return view;
    }
    private void findView(View view) {
        arrayList = new ArrayList<>();
        driver_name = view.findViewById(R.id.driver_name);
        driver_list_rv = view.findViewById(R.id.driver_list_rv);
    }
    private void GetdetailEventApi(String carpoolid) {
        userIds.clear();
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        getApi = mAPIService.getEventDriverList(carpoolid);
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    List<Body> responseBody = response.body().getBody();

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    driver_list_rv.setLayoutManager(linearLayoutManager);
                    if (driverList.size() > 0) {
                        driverList.clear();
                    }
                    if (responseBody.size() > 0) {
                        for (int i = 0; i < responseBody.size(); i++) {
                            userIds.add(responseBody.get(i).getUserId());
                            Log.e("getEventName", "onResponse: "+ responseBody.get(i).getEventName());
                            if (driverIds.contains(responseBody.get(i).getUserId())) {
                                driverModel = new DriverModel(carpoolid, responseBody.get(i).getUserId(), responseBody.get(i).getDriName(), weeks, responseBody.get(i).getProfileImage(), responseBody.get(i).getPickupStatus(), responseBody.get(i).getPickDate(), responseBody.get(i).getGender(), responseBody.get(i).getPhoneNumber(),getEventName);
                                driverList.add(driverModel);
                            }
                        }
                    }
                    for (int j = 0; j < driverIds.size(); j++) {
                        if (userIds == null) {
                            driverModel = new DriverModel(carpoolid, driverIds.get(j), driverNames.get(j), weeks, driverImage.get(j), "", "", gender.get(j), driverPhone.get(j),getEventName);
                        } else if (!userIds.contains(driverIds.get(j))) {

                            driverModel = new DriverModel(carpoolid, driverIds.get(j), driverNames.get(j), weeks, driverImage.get(j), "", "", gender.get(j), driverPhone.get(j),getEventName);
                            driverList.add(driverModel);
                        }
                    }
                    DriverAdapter driverAdapter = new DriverAdapter(getActivity(), driverList, value);
                    driver_list_rv.setAdapter(driverAdapter);

                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }



    @Override
    public void onClick(View v) {

    }


}

package com.wagon.red_wagon.ui.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.EventsAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.EventModel;
import com.wagon.red_wagon.model.RideBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class CalendarAct extends AppCompatActivity {
    private HorizontalCalendar horizontalCalendar;
    ArrayList<String> dates = new ArrayList<>();
    RecyclerView recyclerView;
    List<Body> responseBody;
    List<EventModel> eventModels = new ArrayList<>();
    String selectedDate, from = "";
    String selctedDate;
    TextView no_result_tv;
    boolean value = false;
    List<RideBody> body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calr_view);
        no_result_tv = findViewById(R.id.no_result_tv);
        recyclerView = findViewById(R.id.events_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            from = bundle.getString("from", "");

            if (from.equals("School")) {
                responseBody = (List<Body>) getIntent().getSerializableExtra("responseBody");
            } else {
                body = (List<RideBody>) getIntent().getSerializableExtra("responseBody");
            }
        }
        dates = getIntent().getStringArrayListExtra("dates");
        selectedDate = getIntent().getStringExtra("selectedDate");
        Log.e("Selecteddate", selectedDate);

        /* start 2 months ago from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -2);

        /* end after 2 months from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 2);

        // Default Date set to Today.
        final Calendar defaultSelectedDate = toCalendar(selectedDate);


        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .configure()
                .formatTopText("MMM")
                .formatMiddleText("dd")
                .formatBottomText("EEE")
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.LTGRAY, Color.WHITE)
                .colorTextMiddle(Color.LTGRAY, Color.parseColor("#ffd54f"))
                .end()
                .defaultSelectedDate(defaultSelectedDate)
                .addEvents(new CalendarEventsPredicate() {

                    Random rnd = new Random();

                    @Override
                    public List<CalendarEvent> events(Calendar date) {
                        List<CalendarEvent> events = new ArrayList<>();
                        //int count = rnd.nextInt(6);
                        Log.e("calendarToString", calendarToString(date));
                        for (int i = 0; i < dates.size(); i++) {
                            if (from.equals("School")) {
                                if (dates.get(i).equals(calendarToString(date))) {
                                    events.add(new CalendarEvent(getResources().getColor(R.color.white), "event"));
                                }
                            } else {
                                if (dates.get(i).equals(calendarFamilyString(date))) {
                                    events.add(new CalendarEvent(getResources().getColor(R.color.white), "event"));
                                }
                            }

                        }


                        return events;
                    }
                })
                .build();

        Log.e("gyfjgvyv", "onDateSelected: " + selctedDate);

        if (eventModels.size() != 0) {
            eventModels.clear();
        }

        if (from.equals("School")) {
            selctedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(stringToDate(selectedDate));
            setSchoolEvent(responseBody);
        } else {
            selctedDate = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(stringToDate(selectedDate));
            setFamilyEvent(body);
        }


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                String selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                //    Toast.makeText(CalendarAct.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
                Log.i("onDateSelected", selectedDateStr + " - Position = " + position);

                if (eventModels.size() != 0) {
                    eventModels.clear();
                }
                if (from.equals("School")) {
                    String selctedDate = calendarToString(date);
                    Log.e("hgfjygjgf", "onDateSelected: " + selctedDate);
                    if (responseBody != null) {
                        for (int i = 0; i < responseBody.size(); i++) {
                            Log.e("ehgfdgsdgsdfsfg", "onDateSelected: " + responseBody.get(i).getStartDate());
                           /* if(!responseBody.get(i).getDateRange().equals("")){

                            }*/
                            if (convertString(responseBody.get(i).getDateRange()).contains(selctedDate)) {

                                EventModel eventModel = new EventModel(responseBody.get(i).getName(), responseBody.get(i).getProfileImage(), responseBody.get(i).getTeamId());
                                eventModels.add(eventModel);
                            }
                            EventsAdapter mAdapter = new EventsAdapter(CalendarAct.this, eventModels, from);
                            recyclerView.setAdapter(mAdapter);
                        }
                    }
                } else {
                    String selctedDate = calendarFamilyString(date);
                    for (int i = 0; i < body.size(); i++) {
                        for (int j = 0; j < body.get(i).getPickDate().size(); j++) {
                            if (selctedDate.contains(body.get(i).getPickDate().get(j))) {
                                RideBody rideBody = body.get(i);
                                EventModel eventModel = new EventModel(body.get(i).getDriName(), body.get(i).getProfileImage(), body.get(i).getPhoneNumber(), body.get(i).getPickupStatus());
                                eventModels.add(eventModel);
                            }
                            EventsAdapter mAdapter = new EventsAdapter(CalendarAct.this, eventModels, from);
                            recyclerView.setAdapter(mAdapter);
                        }
                    }
                }

            }


        });

    }

    private String calendarToString(Calendar calendar) {


        calendar.add(Calendar.DATE, 0);

        Date date = calendar.getTime();

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        String date1 = format1.format(date);


        return date1;
    }

    private String calendarFamilyString(Calendar calendar) {


        calendar.add(Calendar.DATE, 0);

        Date date = calendar.getTime();

        SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");

        String date1 = format1.format(date);


        return date1;
    }

    private Date stringToDate(String dateStr) {

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        try {
            date = format.parse(dateStr);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Calendar toCalendar(String dateStr) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(stringToDate(dateStr));
        return cal;
    }

    public void setSchoolEvent(List<Body> responseBody) {
        if (responseBody != null) {
            if (responseBody.size() == 0) {
                no_result_tv.setText("No Event Found");
                no_result_tv.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                for (int i = 0; i < responseBody.size(); i++) {
                    Log.e("ehgfdgsdgsdfsfg", "onDateSelected: " + responseBody.get(i).getStartDate());
                     if (convertString(responseBody.get(i).getDateRange()).contains(selctedDate)) {
                        EventModel eventModel = new EventModel(responseBody.get(i).getName(), responseBody.get(i).getProfileImage(), responseBody.get(i).getTeamId());
                        eventModels.add(eventModel);
                        EventsAdapter mAdapter = new EventsAdapter(CalendarAct.this, eventModels, from);
                        recyclerView.setAdapter(mAdapter);
                        recyclerView.setVisibility(View.VISIBLE);
                        no_result_tv.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public void setFamilyEvent(List<RideBody> body) {

        if (body != null) {
            if (body.size() == 0) {
                no_result_tv.setText("No Event Found");
                no_result_tv.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                for (int i = 0; i < body.size(); i++) {
                    for (int j = 0; j < body.get(i).getPickDate().size(); j++) {
                        if (selctedDate.contains(body.get(i).getPickDate().get(j))) {
                            RideBody rideBody = body.get(i);
                            //value=true;
                            EventModel eventModel = new EventModel(body.get(i).getDriName(), body.get(i).getProfileImage(), body.get(i).getPhoneNumber(), body.get(i).getPickupStatus());
                            eventModels.add(eventModel);

                            EventsAdapter mAdapter = new EventsAdapter(CalendarAct.this, eventModels, from);
                            recyclerView.setAdapter(mAdapter);
                            recyclerView.setVisibility(View.VISIBLE);
                            no_result_tv.setVisibility(View.GONE);
                        }

                    }
                }
            }
        }
    }

    private String dateTimeToString(String dateStr) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dest = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            Log.d("Exception", e.getMessage());
        }
        String result = dest.format(date);
        return result;
    }

    private ArrayList<String> convertString(String date) {

        String[] elements = date.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);
        return listOfString;

    }
}
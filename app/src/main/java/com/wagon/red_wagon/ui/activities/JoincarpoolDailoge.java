package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.NotificationResponse;

public class JoincarpoolDailoge extends AppCompatActivity {
    Dialog driverDialog;
    NotificationResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joincarpool_dailoge);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            response = (NotificationResponse) extras.getSerializable("data");

            requestDialog(response);
            Log.e("Response", "onCreateView: " + response.getType());

        }
    }

    private void requestDialog(NotificationResponse response) {

        driverDialog = new Dialog(JoincarpoolDailoge.this);
        driverDialog.setContentView(R.layout.request_joindailoge);
        driverDialog.setCanceledOnTouchOutside(false);
        TextView carpoolname = driverDialog.findViewById(R.id.carpoolname);
        TextView drivername = driverDialog.findViewById(R.id.drivername);
        drivername.setText(response.getMsg());
        carpoolname.setText(response.getCarPoolName());
        TextView reject_btn = driverDialog.findViewById(R.id.reject_btn);
        TextView accept_btn = driverDialog.findViewById(R.id.accept_btn);

        accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // acceptRejectJoin(response.getCarPoolId(), response.getUserId(), "1", response.getCarPoolName(), response.getDriverName1());

            }
        });
        reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  acceptRejectJoin(response.getCarPoolId(), response.getUserId(), "2", response.getCarPoolName(), response.getDriverName1());

            }
        });
        driverDialog.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}

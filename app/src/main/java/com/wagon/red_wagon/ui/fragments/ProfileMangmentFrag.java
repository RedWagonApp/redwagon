package com.wagon.red_wagon.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;

public class ProfileMangmentFrag extends Fragment implements View.OnClickListener {
    LinearLayout childamount, click_profile, manger_earning, manade_vichle, securty, emargancy, family_lay, preference_divider_view;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    TextView text_title;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.profile_manegment, container, false);
        FamilyDashboardActivity.title.setText("My Stuff");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        emargancy = view.findViewById(R.id.emargancy);
        securty = view.findViewById(R.id.securty);
        manade_vichle = view.findViewById(R.id.manade_vichle);
        click_profile = view.findViewById(R.id.click_profile);
        manger_earning = view.findViewById(R.id.manger_earning);
        family_lay = view.findViewById(R.id.family_lay);
        childamount = view.findViewById(R.id.childamount);
        preference_divider_view = view.findViewById(R.id.preference_divider_view);

        click_profile.setOnClickListener(this);
        manger_earning.setOnClickListener(this);
        manade_vichle.setOnClickListener(this);
        securty.setOnClickListener(this);
        emargancy.setOnClickListener(this);
        family_lay.setOnClickListener(this);
        childamount.setOnClickListener(this);
        preference_divider_view.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.click_profile) {


//`
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, new UpdateProfile(), "Update Profile");
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (view.getId() == R.id.manger_earning) {
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, new ManageEarningFrag(), "Manage Earnings");
            transaction.addToBackStack(null);
            transaction.commit();
//            Intent intent=new Intent(getActivity(), EarningAc.class);
//            startActivity(intent);

        } else if (view.getId() == R.id.manade_vichle) {

            MyVichleFrag myVichleFrag = new MyVichleFrag();

            Bundle bundle = new Bundle();
            bundle.putString("From", "profile");
            myVichleFrag.setArguments(bundle);
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, myVichleFrag, "My Vehicles");
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (view.getId() == R.id.securty) {

            SecurityFrag securtyFrag = new SecurityFrag();


            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, securtyFrag, "Change Password");
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (view.getId() == R.id.emargancy) {

            EmergecyFrag emagacy_frag = new EmergecyFrag();


            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, emagacy_frag, "Set Fare for Emergency");
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (view.getId() == R.id.family_lay) {

            FamilyListFrag familyListFrag = new FamilyListFrag();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, familyListFrag, "Family");
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (view.getId() == R.id.childamount) {

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, new ChildFundAmountFrag(), "Child Found Amount");
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (view.getId() == R.id.preference_divider_view) {

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fram_dash, new PreferenceDriver(), "Preference Driver");
            transaction.addToBackStack(null);
            transaction.commit();

        }
    }
}

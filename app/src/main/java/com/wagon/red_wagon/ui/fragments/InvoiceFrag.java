package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.AdapterInvoice;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class InvoiceFrag extends Fragment implements View.OnClickListener {
    private Button create_invoice;
    private TextView text_title, creat_new_invoice;
    ImageView gallback;
    private RecyclerView invoice_recycler;
    private LinearLayout no_list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.invoices, container, false);
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Invoices");

        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        FindViewId(view);
        getList();
        create_invoice.setOnClickListener(this);
        creat_new_invoice.setOnClickListener(this);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        invoice_recycler.setLayoutManager(linearLayoutManager);


        return view;
    }

    private void FindViewId(View view) {

        invoice_recycler = view.findViewById(R.id.invoice_recycler);
        create_invoice = view.findViewById(R.id.create_invoice);
        no_list = view.findViewById(R.id.not_list);
        creat_new_invoice = view.findViewById(R.id.creat_new_invoice);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.creat_new_invoice) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.invoice_frag, new NewInvoiceFrag());
            transaction.addToBackStack(null);
            transaction.commit();
        }

        if (v.getId() == R.id.create_invoice) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.invoice_frag, new NewInvoiceFrag());
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private void getList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        getApi = mAPIService.getInvoice(Constans.getFamilyId(getActivity()));
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    if (responseBody.size() == 0) {
                        no_list.setVisibility(View.VISIBLE);
                        invoice_recycler.setVisibility(View.GONE);
                    } else {
                        no_list.setVisibility(View.GONE);
                        invoice_recycler.setVisibility(View.VISIBLE);
                        AdapterInvoice adapterInvoice = new AdapterInvoice(getActivity(), responseBody);
                        invoice_recycler.setAdapter(adapterInvoice);
                    }

                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }


}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Connectivity;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class AboutCar extends Fragment implements View.OnClickListener {
    private Button next_aboutcar, continue_about, continue_about2;
    private EditText car_make, car_model, car_clr, reg_car, number_seats;
    private String car_make_get, car_model_get, car_clr_get, reg_car_get, number_seats_get, value = "";
    TextView save_text;
    Spinner spinmake, spinmodel, spincolor;
    private ImageView gallback;
    RelativeLayout linear_main_bar;
    int mStatusCode = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.about_car, container, false);
        FamilyDashboardActivity.title.setText("About Car");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        findviewId(view);
        Bundle args = getArguments();
        value = args.getString("value", "");
/*
        try {
            Bundle args = getArguments();

            child_get = args.getString("about_car", "");
            if (child_get.equals("1")) {
                linear_main_bar.setVisibility(View.GONE);
            } else {
                TextView title = view .findViewById(R.id.title);
                gallback = view.findViewById(R.id.gallback);
                gallback.setOnClickListener(this);
                linear_main_bar.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

        }
       */

//        gallback.setVisibility(View.VISIBLE);

        continue_about.setOnClickListener(this);

        next_aboutcar.setOnClickListener(this);
        continue_about2.setOnClickListener(this);


        return view;
    }


    private void findviewId(View view) {
        next_aboutcar = view.findViewById(R.id.next_aboutcar);
        car_make = view.findViewById(R.id.car_make);
        car_model = view.findViewById(R.id.car_model);
        car_clr = view.findViewById(R.id.car_clr);
        reg_car = view.findViewById(R.id.reg_car);
        number_seats = view.findViewById(R.id.number_seats);
        continue_about2 = view.findViewById(R.id.continue_about2);
        continue_about = view.findViewById(R.id.continue_about);
        linear_main_bar = view.findViewById(R.id.linear_main_bar);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.continue_about) {
            Validitaion();


        }
        if (view.getId() == R.id.next_aboutcar) {
            Validitaion2();

        }
        if (view.getId() == R.id.continue_about2) {
            continue_about.setVisibility(View.VISIBLE);
            continue_about2.setVisibility(View.GONE);
            Validitaion();
            MyVichleFrag myVichle_frag = new MyVichleFrag();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("From", "car");
            bundle.putString("value", value);

            myVichle_frag.setArguments(bundle);
            transaction.replace(R.id.fram_dash, myVichle_frag, "My Vehicles");
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private void Validitaion2() {
        car_make_get = car_make.getText().toString().trim();
        car_model_get = car_model.getText().toString().trim();
        car_clr_get = car_clr.getText().toString().trim();
        reg_car_get = reg_car.getText().toString().trim();
        number_seats_get = number_seats.getText().toString().trim();


        if (!car_make_get.equals("")) {

            if (!car_model_get.equals("")) {

                if (!car_clr_get.equals("")) {


                    if (!reg_car_get.equals("")) {


                        if (!number_seats_get.equals("")) {
                            if (!number_seats_get.equals("7") && !number_seats_get.equals("8") && !number_seats_get.equals("9") && !number_seats_get.equals("0")) {


                                if (Connectivity.isConnected(getContext())) {
                                    Api_Service2();
                                }

                            } else {
                                Toast.makeText(getActivity(), "enter a maximum 6 seats ", Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            number_seats.setError("Please enter the Number of FREE Seats");
                        }
                    } else {
                        reg_car.setError("Please enter the REG Number");
                    }
                } else {

                    car_clr.setError("Please enter the Car Color");
                }
            } else {
                car_model.setError("Please enter the Car Model");
            }

        } else {

            car_make.setError("Please enter the Car Name");
        }
    }

    private void Api_Service2() {

        final Dialog dialog = AppUtils.showProgress(getActivity());

        String url = Constans.BASEURL+"api/carDetails";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String meassage = jsonObject.getString("message");
                    //   Toast.makeText(getActivity(), "Car Added Successfully", Toast.LENGTH_SHORT).show();
                    continue_about.setVisibility(View.GONE);
                    continue_about2.setVisibility(View.VISIBLE);
                    car_make.setText("");
                    car_model.setText("");
                    car_clr.setText("");
                    reg_car.setText("");
                    number_seats.setText("");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    mStatusCode = error.networkResponse.statusCode;
                } catch (NullPointerException e) {

                }
                if (mStatusCode == 400) {
                    Toast.makeText(getContext(), "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();

                } else if (mStatusCode == 401) {
                    AppUtils.sessionExpiredAlert(getContext());
                }

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());


            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("make", car_make_get);
                params.put("modal", car_model_get);
                params.put("color", car_clr_get);
                params.put("regNumber", reg_car_get);
                params.put("freeSeat", number_seats_get);
                params.put("gender", "Driver");
                Log.e("params", "params" + params);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }


    private void Validitaion() {
        car_make_get = car_make.getText().toString().trim();
        car_model_get = car_model.getText().toString().trim();
        car_clr_get = car_clr.getText().toString().trim();
        reg_car_get = reg_car.getText().toString().trim();
        number_seats_get = number_seats.getText().toString().trim();


        if (!car_make_get.equals("")) {
            if (!car_model_get.equals("")) {
                if (!car_clr_get.equals("")) {
                    if (!reg_car_get.equals("")) {
                        if (!number_seats_get.equals("")) {
                            if (!number_seats_get.equals("7") && !number_seats_get.equals("8") && !number_seats_get.equals("9") && !number_seats_get.equals("0")) {
                                if (Connectivity.isConnected(getContext())) {
                                    Api_Service();
                                }
                            } else {
                                Toast.makeText(getActivity(), "You can add maximum 6 seats ", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            number_seats.setError("Please enter the Number of FREE Seats");
                        }
                    }
                } else {
                    reg_car.setError("Please enter the REG Number");
                }
            } else {
                car_model.setError("Please enter the Car Model");
            }
        } else {
            car_make.setError("Please enter the Car Color");
        }
    }

    private void Api_Service() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/carDetails";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                try {
                    dialog.dismiss();

                    JSONObject jsonObject = new JSONObject(response);
                    String meassage = jsonObject.getString("message");
//                    Toast.makeText(getActivity(), "Car Added Successfully", Toast.LENGTH_SHORT).show();
                    continue_about.setVisibility(View.GONE);
                    continue_about2.setVisibility(View.VISIBLE);

                    MyVichleFrag myVichle_frag = new MyVichleFrag();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("From", "car");
                    bundle.putString("value", value);
                    myVichle_frag.setArguments(bundle);
                    transaction.replace(R.id.fram_dash, myVichle_frag, "My Vehicles");
                    transaction.addToBackStack(null);
                    transaction.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dialog.dismiss();
                    int mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 400) {
                        Toast.makeText(getContext(), "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();

                    } else if (mStatusCode == 401) {
                        AppUtils.sessionExpiredAlert(getContext());
                    }
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (Exception e) {

                }
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("make", car_make_get);
                params.put("modal", car_model_get);
                params.put("color", car_clr_get);
                params.put("regNumber", reg_car_get);
                params.put("freeSeat", number_seats_get);
                params.put("gender", "Driver");
                Log.e("params", "params" + params);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }


}


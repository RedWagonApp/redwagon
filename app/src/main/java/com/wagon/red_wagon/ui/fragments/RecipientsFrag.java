package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.Recipients_Adapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class RecipientsFrag extends Fragment implements Recipients_Adapter.RecipientsCallBack {
    RecyclerView recipients_recycler;
    TextView text_title;
    public static CheckBox select_all;
    ImageView gallback;
    public static Button ok_invoice;
    Recipients_Adapter.RecipientsCallBack recipientsCallBack;
    List<Body> responseBody;
    Recipients_Adapter recipients_adapter;
    boolean isAllSelect = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.recipients, container, false);
        text_title = view.findViewById(R.id.text_title);
        select_all = view.findViewById(R.id.select_all);
        text_title.setText("Recipients");

        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        FindViewId(view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recipients_recycler.setLayoutManager(linearLayoutManager);

        Get_Players(this);


        return view;
    }

    private void Get_Players(final Recipients_Adapter.RecipientsCallBack callBack) {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getTeamPlyer(Constans.getFamilyId(getActivity()));
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                try {
                    if (response.code() == 200) {

                        responseBody = response.body().getBody();
                        recipients_adapter = new Recipients_Adapter(getActivity(), responseBody, callBack);
                        recipients_recycler.setAdapter(recipients_adapter);
                    } else if (response.code() == 400) {
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        }

                    } else if (response.code() == 401) {
                        AppUtils.sessionExpiredAlert(getActivity());
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }


    private void FindViewId(View view) {
        ok_invoice = view.findViewById(R.id.ok_invoice);
        recipients_recycler = view.findViewById(R.id.recipients_recycler);

    }

    @Override
    public void onMethodCallback(ArrayList<String> emaillist, ArrayList<String> arrayListName) {
        Log.e("onMethodCallback", "onMethodCallback: " + emaillist);
        ok_invoice.setVisibility(View.GONE);
        NewInvoiceFrag new_invoice_frag = new NewInvoiceFrag();
        Bundle args = new Bundle();
        args.putStringArrayList("emaillist", emaillist);
        args.putStringArrayList("arrayListName", arrayListName);
        new_invoice_frag.setArguments(args);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.replace(R.id.recipients_frag, new_invoice_frag);

        transaction.commit();
    }
}

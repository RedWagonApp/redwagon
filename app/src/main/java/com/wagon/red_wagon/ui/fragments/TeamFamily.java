package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ChildTrackAdapter;
import com.wagon.red_wagon.adapter.TeamListAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class TeamFamily extends Fragment implements ChildTrackAdapter.AdapterCallback {
    TextView titleTxt, no_result_tv, child_fillter;
    RecyclerView chil_team_rv;
    RecyclerView recycler_child;
    ChildTrackAdapter.AdapterCallback adapterCallback;
    ArrayList<String> childIdlist = new ArrayList<>();
    public static Dialog dialog1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.team_family, container, false);


        FamilyDashboardActivity.title.setText("Team");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        chil_team_rv = view.findViewById(R.id.chil_team_rv);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        dialog1 = new Dialog(getActivity());
        child_fillter = view.findViewById(R.id.child_fillter);
        dialog1.setContentView(R.layout.childlistdailoge);
        recycler_child = dialog1.findViewById(R.id.recycler_childs);
        child_fillter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (childIdlist.size() > 0) {
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    recycler_child.setLayoutManager(linearLayoutManager);
                    dialog1.show();
                }
            }
        });
        getList(this);
        adapterCallback = new ChildTrackAdapter.AdapterCallback() {
            @Override
            public void onMethodCallback(String name, String id) {
                dialog1.dismiss();
                Log.e("onMethodCallback", "onMethodCallback: " + name + "\n" + id);
            }
        };
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        chil_team_rv.setLayoutManager(linearLayoutManager);
        return view;
    }

    private void getList(ChildTrackAdapter.AdapterCallback callback) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi = mAPIService.GetChildListNew(Constans.getFamilyId(getActivity()));

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    Log.e("responseBody", "onResponse: " + responseBody.size());
                    if (responseBody.size() != 0) {


                        no_result_tv.setVisibility(View.GONE);
                        chil_team_rv.setVisibility(View.VISIBLE);
                        ChildTrackAdapter childTrackAdapter = new ChildTrackAdapter(getActivity(), responseBody, "1", callback);
                        recycler_child.setAdapter(childTrackAdapter);
                        for (int i = 0; i < responseBody.size(); i++) {
                            String idchild = String.valueOf(responseBody.get(i).getId());
                            childIdlist.add(idchild);
                            ApigetTeam(childIdlist);
                        }
                    } else {
                        no_result_tv.setVisibility(View.VISIBLE);
                        chil_team_rv.setVisibility(View.GONE);

                    }
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    private void ApigetTeam(ArrayList<String> childIdlist) {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        String userid = AppUtils.convertToString(childIdlist);
        Call<GetEvent> getApi = mAPIService.SearchTeams(userid);

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    if (responseBody.size() != 0) {
                        no_result_tv.setVisibility(View.GONE);
                        chil_team_rv.setVisibility(View.VISIBLE);
                        TeamListAdapter eventAdapter = new TeamListAdapter(getActivity(), responseBody, "1");
                        chil_team_rv.setAdapter(eventAdapter);
                    } else {

                        no_result_tv.setVisibility(View.VISIBLE);
                        chil_team_rv.setVisibility(View.GONE);

                    }


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Log.e("MessageM", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }


    @Override
    public void onMethodCallback(String name, String id) {
        if (childIdlist.size() > 0) {
            childIdlist.clear();
        }
        Log.e("onMethodCallback", "onMethodCallback: " + id);
        childIdlist.add(id);
        ApigetTeam(childIdlist);
        dialog1.show();
    }
}

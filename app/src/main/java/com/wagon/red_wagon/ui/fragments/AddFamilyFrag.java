package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class AddFamilyFrag extends Fragment implements View.OnClickListener {
    EditText family_name, family_emaile, family_mobile;
    String getArg = "", family_name_get, family_email_get, family_mobile_get, image_path = "", result = "";

    TextView text_title, gellary_clickm;
    Button next_add_family;

    FragmentTransaction transaction;
    private static final int GALLERY_REQUEST_CODE = 1;
    CircleImageView imagesetup_m;
    ImageView gallback;
    private static final int CAMERA_PIC_REQUEST = 144;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_family_lay, container, false);
        findview(view);
        next_add_family.setOnClickListener(this);
        FamilyDashboardActivity.title.setText("Add Family Member");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        return view;


    }

    private void findview(View view) {

        text_title = view.findViewById(R.id.text_title);
        next_add_family = view.findViewById(R.id.add_family);
        family_name = view.findViewById(R.id.family_name);
        family_emaile = view.findViewById(R.id.mamager_email);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_family:

                Validation();

                break;

        }
    }

    private void pickFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }


    private void Validation() {
        family_name_get = family_name.getText().toString().trim();
        family_email_get = family_emaile.getText().toString().trim();


        if (!family_name_get.equals("")) {
            if (!family_email_get.equals("") && emailValidator(family_email_get)) {
                Apifamily();
            } else {
                family_emaile.setError("Please enter the email");
            }
        } else {
            family_name.setError("Please enter a name");
        }

    }



    private void Apifamily() {

        final Dialog dialog = AppUtils.showProgress(getActivity());

        APIService mAPIService = AppUtils.getAPIService(getContext());

        mAPIService.addFamilyRequest(Constans.getFamilyId(getActivity()), family_name_get, family_email_get).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    Objects.requireNonNull(getActivity()).onBackPressed();
                } else if (response.code() == 400) {
//                    Toast.makeText(getActivity(), "Invaild User", Toast.LENGTH_SHORT).show();

                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }


            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {


            try {
                Uri uri = data.getData();
                image_path = getPath(uri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                imagesetup_m.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }

    private String getPath(Uri uri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(getActivity(), uri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();

        } catch (SecurityException ignored) {

        }
        return result;

    }


}

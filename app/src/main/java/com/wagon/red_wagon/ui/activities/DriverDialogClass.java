package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.NotificationResponse;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;

public class DriverDialogClass extends BaseActivity {

    Dialog driverDialog, ratingDialog;
    NotificationResponse notfresponse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blanl_activity);
        //getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            notfresponse = (NotificationResponse) extras.getSerializable("data");
            if (notfresponse != null) {
                if (notfresponse.getType().equals("Request")) {
                    requestDialog(notfresponse);
                } /*else  if (notfresponse.getType().equals("completed")){
                    ratingDialog(notfresponse);
                }*/
            }

            //Log.e("Response", "onCreateView: " + notfresponse.getType());

        }
    }

    private void requestDialog(NotificationResponse response) {
        driverDialog = new Dialog(DriverDialogClass.this);
        driverDialog.setContentView(R.layout.request_ridedailoge);
        driverDialog.setCanceledOnTouchOutside(false);
        TextView reject_btn = driverDialog.findViewById(R.id.reject_btn);
        ImageView userimag = driverDialog.findViewById(R.id.userimag);
        TextView accept_btn = driverDialog.findViewById(R.id.accept_btn);
        TextView makename = driverDialog.findViewById(R.id.makename);
        TextView passengers_number = driverDialog.findViewById(R.id.passengers_number);
        TextView from_location = driverDialog.findViewById(R.id.from_location);
        TextView to_location = driverDialog.findViewById(R.id.to_location);

        makename.setText(response.getName());
        passengers_number.setText("Passengers: " + response.getSeat());
        from_location.setText("From: " + response.getPickAddress());
        to_location.setText("To: " + response.getDropAddress());
        Glide.with(this).load(Constans.BASEURL + response.getImageurl()).into(userimag);

        accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                acceptReject(response.getUserId(), response.getDriverId(), response.getName(), response.getRequestId(), "Accept", "1");

            }
        });
        reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptReject(response.getUserId(), response.getDriverId(), response.getName(), response.getRequestId(), "Reject", "2");
            }
        });
        driverDialog.show();
    }

    private void acceptReject(String userId, String driverId, String drivername, String requestId, String notifType, String requestStatus) {
        final Dialog dialog = AppUtils.showProgress(DriverDialogClass.this);
        APIService mAPIService = AppUtils.getAPIService(DriverDialogClass.this);
        Call<AddEvent> getApi;

        getApi = mAPIService.acceptReject(userId, driverId, drivername, requestId, notifType, requestStatus);


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    if (requestStatus.equals("1")) {
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("myResult", "1");
                        resultIntent.putExtra("data", (Serializable) notfresponse);

                        setResult(RESULT_OK, resultIntent);
                        finish();
                    }
                    finish();
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(DriverDialogClass.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(DriverDialogClass.this);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                dialog.dismiss();

                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

package com.wagon.red_wagon.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.fragments.AboutCar;
import com.wagon.red_wagon.ui.fragments.ChildProfileFrag;
import com.wagon.red_wagon.ui.fragments.CreateFamilyActivity;
import com.wagon.red_wagon.ui.fragments.Dashboard;
import com.wagon.red_wagon.ui.fragments.DashboardCoach;
import com.wagon.red_wagon.ui.fragments.Invite_Screen;

import de.hdodenhof.circleimageview.CircleImageView;

public class CoachDashboardAc extends BaseActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        NavigationView.OnNavigationItemSelectedListener {
    FragmentManager fragmentManager;
    public static TextView title;
    FragmentTransaction transaction;
    private static final String TAG = "FamilyDashboardActivity";

    String user_name = "", check = "";


    public static ImageView sidebar;
    CircleImageView imageset;

    DrawerLayout mDrawerLayout;

    View layout;


    FrameLayout frameLayout;
    boolean isSideMain = true;

    TextView username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_dashboard);

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fram_dash_coach, new DashboardCoach(), "Manager");
        transaction.commit();
        sidebar = findViewById(R.id.sidebar);

        layout = findViewById(R.id.bar);

        frameLayout = findViewById(R.id.fram_dash_coach);
        title = findViewById(R.id.text_create);
        mDrawerLayout = findViewById(R.id.mDrawerLayout_coach);

        sidebar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {

                if (isSideMain) {
                    mDrawerLayout.openDrawer(Gravity.START);
                } else {
                    onBackPressed();
                }

            }
        });
        isVisible(true);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigations_coach);

        navigation.setOnNavigationItemSelectedListener(this);
        NavigationView navView = (NavigationView) findViewById(R.id.navigation_view_coach);
        View header = navView.getHeaderView(0);
        imageset = (CircleImageView) header.findViewById(R.id.imagesetuser);
        username = header.findViewById(R.id.username);
        Menu nav_Menu = navView.getMenu();
        nav_Menu.findItem(R.id.admaneger).setVisible(false);
      /*  try {

            check = intent.getStringExtra("check");
            if (Constans.getUserName(FamilyDashboardActivity.this) != null) {
                user_name = Constans.getUserName(FamilyDashboardActivity.this);
                username.setText(user_name);

            }
            if (Constans.getProfilePic(FamilyDashboardActivity.this) != null) {
                Glide.with(FamilyDashboardActivity.this).load(Constans.BASEURL + Constans.getProfilePic(FamilyDashboardActivity.this)).placeholder(R.drawable.ic_launcher).into(imageset);
            }
        } catch (NullPointerException e) {

        }*/


        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @SuppressLint("WrongConstant")
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                // create a Fragment Object
                int itemId = menuItem.getItemId();

                if (itemId == R.id.log_school) {
                    SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor.clear();
                    editor.commit();

                    Intent intent2 = new Intent(CoachDashboardAc.this, LogInActivity.class);
                    startActivity(intent2);
                    finish();
                } else if (itemId == R.id.add_cc) {

                    ChildProfileFrag childProfile_frag = new ChildProfileFrag();
                    Bundle args = new Bundle();
                    args.putString("childd", "1");
                    childProfile_frag.setArguments(args);
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash_coach, childProfile_frag, "Child Profile");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else if (itemId == R.id.add_drt) {

                    AboutCar about_car = new AboutCar();
                    Bundle args = new Bundle();
                    args.putString("about_car", "1");
                    about_car.setArguments(args);

                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash_coach, about_car, "About Car");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else if (itemId == R.id.refer_wagon) {

                    Invite_Screen about_car = new Invite_Screen();
                    Bundle args = new Bundle();
                    args.putString("school_dashboard", "1");
                    about_car.setArguments(args);

                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash_coach, about_car, "Invite");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mDrawerLayout.closeDrawer(Gravity.START);
                }
                return false;
            }
        });
    }
        @Override
        public boolean onNavigationItemSelected (@NonNull MenuItem menuItem){

            switch (menuItem.getItemId()) {
                case R.id.action_home:

                    layout.setVisibility(View.VISIBLE);
                    isVisible(true);
                    FragmentManager fm = getSupportFragmentManager();
                    for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }

                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash_coach, new DashboardCoach(), "Manager");
                    transaction.commit();


                    return true;





            }


            return false;
        }


        @Override
        public void onBackPressed () {
            super.onBackPressed();

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fram_dash_coach);
            title.setText(fragment.getTag());
            if (fragment instanceof Dashboard) {

                isVisible(true);
            }

            if (fragment instanceof Invite_Screen) {
                isVisible(true);
            }


        }

        public void isVisible ( boolean isMain){
            isSideMain = isMain;
            if (!isMain) {
                sidebar.setBackground(getResources().getDrawable(R.drawable.bck_arrow));

            } else {
                sidebar.setBackground(getResources().getDrawable(R.drawable.menu_side));
            }
        }

        @Override
        protected void onActivityResult ( int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fram_dash_coach);
            if (fragment instanceof CreateFamilyActivity) {
                fragment.onActivityResult(requestCode, resultCode, data);

            }
        }

    }


package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class ForgetPassword extends BaseActivity {
    TextView text_title;
    EditText emailfor;
    Button send_btn;
    String emailget;
    ImageView gallback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password);
        emailfor = findViewById(R.id.emailfor);
        send_btn = findViewById(R.id.send_btn);
        text_title = findViewById(R.id.text_title);
        gallback = findViewById(R.id.gallback);
        text_title.setText("Forgot password");
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailget = emailfor.getText().toString().trim();
                if (!emailget.equals("") && emailValidator(emailget)) {
                    apiservice();
                } else {
                    emailfor.setError("Please enter the valid email");
                }

            }
        });


    }


    private void apiservice() {
        final Dialog dialog = AppUtils.showProgress(this);
        String url = Constans.BASEURL + "api/forgetPassword";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                Toast.makeText(ForgetPassword.this, "Email has been sent", Toast.LENGTH_SHORT).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("dsfdfsdf", "onErrorResponse: " + error.toString());
                int code = error.networkResponse.statusCode;
                if (code == 400) {
                    Toast.makeText(ForgetPassword.this, "Wrong Email Address", Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", emailget);
                Log.e("", "getParams: " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                // Removed this line if you dont need it or Use application/json
                // params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

                return super.parseNetworkResponse(response);
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(ForgetPassword.this);
        requestQueue.add(stringRequest);

    }




}

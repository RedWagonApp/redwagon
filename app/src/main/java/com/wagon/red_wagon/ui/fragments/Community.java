package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CommunityAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class Community extends Fragment {

    private RecyclerView commnictylist;
    private TextView no_result_tv, filter;
    String filterStr;
    EditText search_et;
    List<Body> responseBody;
    CommunityAdapter communityAdapter;
    @Nullable

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.community, container, false);
        commnictylist = view.findViewById(R.id.commnictylist);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        search_et = view.findViewById(R.id.search_et);
        filter = view.findViewById(R.id.filter);
        FamilyDashboardActivity.title.setText("Community");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        commnictylist.setLayoutManager(linearLayoutManager);
        getList("");
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), v);
                popup.getMenuInflater().inflate(R.menu.pop_up, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //  Toast.makeText(getContext(), "Some Text" + item.getTitle(), Toast.LENGTH_SHORT).show();
                        filterStr = item.getTitle().toString();
                        getList(filterStr);
                        return true;
                    }
                });
                popup.show();
            }
        });

        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });

        return view;
    }

    private void filter(String text) {
        try {
            List<Body> filterdList = new ArrayList<>();

            if (responseBody.size() > 0) {
                for (Body s : responseBody) {

                    if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                        filterdList.add(s);
                    }
                }
                communityAdapter.filterList(filterdList);
            }
        } catch (NullPointerException e) {

        }
    }

    private void getList(String from) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getEventList();


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    responseBody = response.body().getBody();
//                    communityAdapter = new CommunityAdapter(getActivity(), responseBody, Constans.fromType);
//                    commnictylist.setAdapter(communityAdapter);

                    if (responseBody.size() == 0) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        commnictylist.setVisibility(View.GONE);
                    } else {
                        no_result_tv.setVisibility(View.GONE);
                        commnictylist.setVisibility(View.VISIBLE);

                        CommunityAdapter communityAdapter = new CommunityAdapter(getActivity(), responseBody, Constans.fromType);
                        commnictylist.setAdapter(communityAdapter);

                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                no_result_tv.setVisibility(View.VISIBLE);
                commnictylist.setVisibility(View.GONE);
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }



}

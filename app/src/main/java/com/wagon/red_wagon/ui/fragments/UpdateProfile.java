package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.ProfieModel;
import com.wagon.red_wagon.model.UsersDetail;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

public class UpdateProfile extends Fragment implements View.OnClickListener {
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CAMERA_PIC_REQUEST = 144;
    EditText home_teliphone, flat_number, street, country, state, zipe_code, sequrty_number, driving_number;
    EditText state_issued, united_state;
    TextView text_title, gellary_clickfamilyup;
    Dialog dialog;
    Button update_data;
    CircleImageView imagesetup;
    String home_teliphone_get, flat_number_get, street_get, country_get, state_get, zipe_code_get, united_state_get, sequrty_number_get, driving_number_get, state_issued_get;
    private String filePath = "";
    Uri imageUri;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.update_profile, container, false);
        findview(view);
        FamilyDashboardActivity.title.setText("Update Profile");

        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        update_data.setOnClickListener(this);
        imagesetup.setOnClickListener(this);
        gellary_clickfamilyup.setOnClickListener(this);
        return view;
    }

    private void findview(View view) {

        gellary_clickfamilyup = view.findViewById(R.id.gellary_clickfamilyup);
        imagesetup = view.findViewById(R.id.imagesetup);
        update_data = view.findViewById(R.id.update_data);
        home_teliphone = view.findViewById(R.id.home_teliphoneup);
        flat_number = view.findViewById(R.id.flat_number_up);
        street = view.findViewById(R.id.street_up);
        country = view.findViewById(R.id.county_up);
        state = view.findViewById(R.id.state_up);
        zipe_code = view.findViewById(R.id.zipe_code_up);
        sequrty_number = view.findViewById(R.id.sequrty_numberup);


        getProfileDetail();

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.update_data) {
            Validation();
        }
        if (v.getId() == R.id.imagesetup) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
        }
        if (v.getId() == R.id.gellary_clickfamilyup) {
            pickFromGallery();
        }
    }

    private void pickFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }

    private void Validation() {

        home_teliphone_get = home_teliphone.getText().toString().trim();
        flat_number_get = flat_number.getText().toString().trim();
        street_get = street.getText().toString().trim();
        country_get = country.getText().toString().trim();
        state_get = state.getText().toString().trim();
        zipe_code_get = zipe_code.getText().toString().trim();


        if (!home_teliphone_get.equals("")) {
            if (!flat_number_get.equals("")) {

                if (!street_get.equals("")) {


                    if (!country_get.equals("")) {


                        if (!state_get.equals("")) {

                            if (!zipe_code_get.equals("")) {


//                                        if (!filePath.equals("")) {


                                service();

                                        /*} else {
                                            Toast.makeText(getContext(), "Please add image", Toast.LENGTH_SHORT).show();
                                        }*/


                            } else {

                                zipe_code.setError("Please enter a zip code");
                            }

                        } else {

                            state.setError("Please enter a state");
                        }
                    } else {
                        country.setError("Please enter a state country");
                    }

                } else {
                    street.setError("Please enter the street ");
                }
            } else {
                flat_number.setError("Please enter the flat number");
            }
        } else {
            home_teliphone.setError("Please enter the home telephone");
        }

    }

    private void service() {
        dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/UpdatePersonalDetails";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String meassage = jsonObject.getString("message");
                    //Toast.makeText(getActivity(), meassage, Toast.LENGTH_SHORT).show();

                    uploadPic();


                } catch (JSONException e) {
                    e.printStackTrace();
                }


//                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                FragmentTransaction transaction = fragmentManager.beginTransaction();
//                transaction.replace(R.id.parent_res_frame, new ConguratulationFrag());
//                transaction.addToBackStack(null);
//                transaction.commit();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("homePhone", home_teliphone_get);
                params.put("flatNumber", flat_number_get);
                params.put("street", street_get);
                params.put("country", country_get);
                params.put("state", state_get);
                params.put("zipCode", zipe_code_get);
                params.put("unitedStates", "KJHJH");
                params.put("securityNumber", "454");
                Log.e("params", "params" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }

    private void getProfileDetail() {
        final Dialog dialog1 = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());


        mAPIService.getUserProfile().enqueue(new Callback<ProfieModel>() {
            @Override
            public void onResponse(Call<ProfieModel> call, retrofit2.Response<ProfieModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog1.dismiss();
                if (response.code() == 200) {
                    UsersDetail usersDetail = response.body().getBody().getUsersDetail();

                    home_teliphone.setText(usersDetail.getHomePhone());
                    flat_number.setText(usersDetail.getFlatNumber());
                    street.setText(usersDetail.getStreet());
                    country.setText(usersDetail.getCountry());
                    state.setText(usersDetail.getState());
                    zipe_code.setText(usersDetail.getZipCode());
                    sequrty_number.setText(usersDetail.getSecurityNumber());
                    Glide.with(getActivity()).load(Constans.BASEURL + usersDetail.getProfileImage()).into(imagesetup);
//
                    Log.e("sequrty_number", "onResponse: " + Constans.BASEURL + usersDetail.getProfileImage());
                } else if (response.code() == 400) {
                    dialog1.dismiss();
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<ProfieModel> call, Throwable t) {
                dialog1.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void textnull() {

        home_teliphone.setText("");
        flat_number.setText("");
        street.setText("");
        country.setText("");
        state.setText("");
        zipe_code.setText("");
        sequrty_number.setText("");
        home_teliphone.setText("");


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_PIC_REQUEST) {
            try {


                imagesetup.setImageURI(imageUri);
                filePath = ImageUtils.getCompressedBitmap(getrealPathFromUrl(imageUri));
            } catch (NullPointerException e) {
                Log.e("NullPointerException", "==v+" + e);
            }
        }

        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();

            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                filePath = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
                imagesetup.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }

    public void uploadPic() {


        MultipartBody.Part part = null;
        if (!filePath.equals("")) {
            try {
                File file = new File(filePath);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {

            File file = new File(filePath);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }
        AppUtils.getAPIService(getContext()).prolieUplod(part).

                enqueue(new Callback<AddEvent>() {
                    @Override
                    public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                        dialog.dismiss();
                        try {
                            Toast.makeText(getActivity(), "Profile Updated!", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();

                        } catch (Exception e) {

                        }

                    }

                    @Override
                    public void onFailure(Call<AddEvent> call, Throwable t) {
                        Log.e("Failure", "onFailure: ", t);
                    }
                });
    }

    private String getrealPathFromUrl(Uri uri) {
        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();
        return result;
    }
}

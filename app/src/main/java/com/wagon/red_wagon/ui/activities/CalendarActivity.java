package com.wagon.red_wagon.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.utils.DateUtils;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.model.RideBody;
import com.wagon.red_wagon.model.ScheduleModel;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CalendarActivity extends AppCompatActivity {
    List<EventDay> events;
    ArrayList<String> dates = new ArrayList<>();
    CalendarView calendarView;
    List<Body> responseBody;
    List<Calendar> calendars = new ArrayList<>();
    String from = "", ID = "";
    List<RideBody> body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity);
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        ImageView list_ic = (ImageView) findViewById(R.id.list_ic);
        Calendar min = Calendar.getInstance();
        min.add(Calendar.MONTH, -2);
        Calendar max = Calendar.getInstance();
        max.add(Calendar.MONTH, 2);
        calendarView.setMinimumDate(min);
        calendarView.setMaximumDate(max);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            from = bundle.getString("from", "");

            if (from.equals("School")) {
                getList("");
            } else {
                ID = bundle.getString("childId", "");
                if (ID.equals(Constans.getFamilyId(this))) {
                    getFamilyPoolEvent(ID, "family");
                } else {
                    getFamilyPoolEvent(ID, "child");
                }

            }
        }
        list_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (from.equals("School")) {
                    startActivity(new Intent(CalendarActivity.this, CalendarAct.class)
                            .putExtra("responseBody", (Serializable) responseBody)
                            .putExtra("from", from)
                            .putStringArrayListExtra("dates", dates)
                            .putExtra("selectedDate", Calendar.getInstance().getTime().toString()));
                } else {
                    startActivity(new Intent(CalendarActivity.this, CalendarAct.class)
                            .putExtra("responseBody", (Serializable) body)
                            .putExtra("from", from)
                            .putStringArrayListExtra("dates", dates)
                            .putExtra("selectedDate", Calendar.getInstance().getTime().toString()));
                }
            }
        });

        calendarView.setOnDayClickListener(eventDay -> {

            if (from.equals("School")) {
                startActivity(new Intent(CalendarActivity.this, CalendarAct.class)
                        .putExtra("responseBody", (Serializable) responseBody)
                        .putExtra("from", from)
                        .putStringArrayListExtra("dates", dates)
                        .putExtra("selectedDate", eventDay.getCalendar().getTime().toString()));
            } else {
                startActivity(new Intent(CalendarActivity.this, CalendarAct.class)
                        .putExtra("responseBody", (Serializable) body)
                        .putExtra("from", from)
                        .putStringArrayListExtra("dates", dates)
                        .putExtra("selectedDate", eventDay.getCalendar().getTime().toString()));
            }
        });

    }

    private List<Calendar> getDisabledDays() {
        Calendar firstDisabled = DateUtils.getCalendar();
        firstDisabled.add(Calendar.DAY_OF_MONTH, 2);
        Calendar secondDisabled = DateUtils.getCalendar();
        secondDisabled.add(Calendar.DAY_OF_MONTH, 1);
        Calendar thirdDisabled = DateUtils.getCalendar();
        thirdDisabled.add(Calendar.DAY_OF_MONTH, 18);
        List<Calendar> calendars = new ArrayList<>();
        calendars.add(firstDisabled);
        calendars.add(secondDisabled);
        calendars.add(thirdDisabled);
        return calendars;
    }


    private void getList(final String fromType) {
        final Dialog dialog = AppUtils.showProgress(this);
        APIService mAPIService = AppUtils.getAPIService(this);
        Call<GetEvent> getApi;


        getApi = mAPIService.getEventDetail(Constans.getFamilyId(this), "");


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    responseBody = response.body().getBody();
                    events = new ArrayList<>();
                    for (int i = 0; i < responseBody.size(); i++) {

                        if(responseBody.get(i).getDateRange().equals("")){

                            dates.add(dateTimetoString(responseBody.get(i).getDateTime()));
                        }else {
                            convertString(responseBody.get(i).getDateRange());
                        }
                    }
                    setEvents();

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(CalendarActivity.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(CalendarActivity.this);
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void convertString(String date) {

        String[] elements = date.split(",");
        List<String> fixedLenghtList = Arrays.asList(elements);
        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);

        for (String string : listOfString) {
            dates.add(stringToDate(string));
        }


    }

    private void setEvents() {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (dates != null) {
            for (String string : dates) {
                try {
                    date = sdf.parse(string);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                CalendarActivity obj = new CalendarActivity();
                // Test - Convert Date to Calendar
                Calendar calendar = obj.dateToCalendar(date);
                calendars.add(calendar);
                // calendar.add(Calendar.DAY_OF_MONTH, 7);
                events.add(new EventDay(calendar, R.drawable.red_dot, calendars));
                calendarView.setEvents(events);
                Log.e("Dates", date.toString());
                Log.e("Dates11", string);
            }


        }



    }

    private Calendar dateToCalendar(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;

    }

    //Convert Calendar to Date
    private Date calendarToDate(Calendar calendar) {
        return calendar.getTime();
    }

    private void getFamilyPoolEvent(String Id, String type) {
        final Dialog dialog = AppUtils.showProgress(CalendarActivity.this);
        APIService mAPIService = AppUtils.getAPIService(CalendarActivity.this);
        Call<ScheduleModel> getApi;
        if (type.equals("child")) {
            getApi = mAPIService.getFamilySchedule(Id);
        } else {
            getApi = mAPIService.getDriverPickStatus(Id);
        }
        getApi.enqueue(new Callback<ScheduleModel>() {
            @Override
            public void onResponse(Call<ScheduleModel> call, retrofit2.Response<ScheduleModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    body = response.body().getBody();

                    events = new ArrayList<>();
                    for (int i = 0; i < body.size(); i++) {

                        for (int j = 0; j < body.get(i).getPickDate().size(); j++) {
                            dates.add(body.get(i).getPickDate().get(j));


                            Date date = null;
                            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
                            if (body.get(i).getPickDate().get(j) != null) {

                                try {
                                    date = sdf.parse(body.get(i).getPickDate().get(j));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                CalendarActivity obj = new CalendarActivity();
                                // Test - Convert Date to Calendar
                                Calendar calendar = obj.dateToCalendar(date);
                                calendars.add(calendar);
                                // calendar.add(Calendar.DAY_OF_MONTH, 7);
                                events.add(new EventDay(calendar, R.drawable.red_dot, calendars));
                                calendarView.setEvents(events);
                            }

                            Log.e("Dates11", body.get(i).getPickDate().get(j));
                        }

                    }
                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(CalendarActivity.this, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(CalendarActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ScheduleModel> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(CalendarActivity.this, "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private String stringToDate(String dateStr) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dest = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            Log.d("Exception", e.getMessage());
        }
        String result = dest.format(date);
        return result;
    }

    private String dateTimetoString(String dateStr) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat dest = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            Log.d("Exception", e.getMessage());
        }
        String result = dest.format(date);
        return result;
    }
}
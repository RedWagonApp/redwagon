package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.EventAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class EventListFragment extends Fragment {

    private RecyclerView eventRecyclerView;
    private TextView createeveet_, no_result_tv, text_title;
    private ArrayList<Event_List_model> arrayList;
    // String fromType;
    private ImageView gallback;
    private CardView cardview;
    private EditText search_et;
    private EventAdapter eventAdapter;
    private List<Body> responseBody;
    private String fromType = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_layout, container, false);
        arrayList = new ArrayList<>();
        createeveet_ = view.findViewById(R.id.createeveet_);
        text_title = view.findViewById(R.id.text_title);
        search_et = view.findViewById(R.id.search_et);
        cardview = view.findViewById(R.id.cardview);
        gallback = view.findViewById(R.id.gallback);

        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        eventRecyclerView = view.findViewById(R.id.event_reyclerview);
        no_result_tv = view.findViewById(R.id.no_result_tv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        eventRecyclerView.setLayoutManager(linearLayoutManager);
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });

        createeveet_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (Constans.fromType) {
                    case "Student": {

                        AddPlayer fragment = new AddPlayer();
                        Bundle args = new Bundle();
                        args.putString("fromType", Constans.fromType);
                        args.putString("teamId", "0");
                        args.putString("manegerID", "0");
                        args.putString("value", "1");
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.school_dashbord_fram, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        break;
                    }
                    case "Activities": {

                        CreateActivity fragment = new CreateActivity();
                        Bundle args = new Bundle();
                        args.putString("fromType", "Activities");
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                        transaction.addToBackStack(Constans.fromType);
                        transaction.commit();
                        break;
                    }
                    case "Classes": {

                        CreateClass fragment = new CreateClass();
                        Bundle args = new Bundle();
                        args.putString("fromType", Constans.fromType);
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                        transaction.addToBackStack(Constans.fromType);
                        transaction.commit();
                        break;
                    }
                    case "Games": {

                        AddGameFrag add_game_frag = new AddGameFrag();
                        Bundle args = new Bundle();
                        Constans.fromType = "Games";
                        args.putString("fromType", "Games");
                        add_game_frag.setArguments(args);
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.school_dashbord_fram, add_game_frag);
                        transaction.addToBackStack(Constans.fromType);
                        transaction.commit();
                        break;
                    }
                }
            }
        });
        return view;
    }

    private void filter(String text) {
        try {
            List<Body> filterdList = new ArrayList<>();


            for (Body s : responseBody) {

                if (s.getFirstName().toLowerCase().contains(text.toLowerCase())) {
                    filterdList.add(s);
                }
            }
            eventAdapter.filterList(filterdList);
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Constans.fromType.equals("Student")) {

            text_title.setText("Players");
            createeveet_.setText("Add Player");
            no_result_tv.setText("Sorry! There are no students at the moment. Please add Players.");
            cardview.setVisibility(View.VISIBLE);
        } else if (Constans.fromType.equals("Activities")) {

            text_title.setText("Activities");
            createeveet_.setText("Create Activity");
            no_result_tv.setText("Sorry! There are no activities at the moment. Please add Activities.");
            cardview.setVisibility(View.GONE);
        } else if (Constans.fromType.equals("Classes")) {

            text_title.setText("Classes");
            createeveet_.setText("Create Class");
            no_result_tv.setText("Sorry! There are no classes at the moment. Please add Classes.");
            cardview.setVisibility(View.GONE);
        } else if (Constans.fromType.equals("Games")) {

            text_title.setText("Games");
            createeveet_.setText("Create Game");
            no_result_tv.setText("Sorry! There are no games at the moment. Please add Games.");
            cardview.setVisibility(View.GONE);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "onStart" + Constans.fromType);
        if (Constans.getLoginType(getActivity()).equals("Manager")) {
            getList(Constans.getSchlID(getActivity()));
        } else {
            getList(Constans.getFamilyId(getActivity()));
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.e("onAttach", "onAttach");


    }


    private void getList(final String id) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        switch (Constans.fromType) {
            case "Student":
                getApi = mAPIService.getAllPlayer(id);
                break;
            case "Classes":
                getApi = mAPIService.getclassList(id);
                break;
            case "Games":
                getApi = mAPIService.getEventDetail(id, "Games");
                break;
            default:
                getApi = mAPIService.getEventDetail(id, "Activities");
                break;
        }


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        eventRecyclerView.setVisibility(View.GONE);
                    } else {
                        no_result_tv.setVisibility(View.GONE);
                        eventRecyclerView.setVisibility(View.VISIBLE);
                        eventAdapter = new EventAdapter(getActivity(), responseBody, Constans.fromType);
                        eventRecyclerView.setAdapter(eventAdapter);
                    }


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        no_result_tv.setVisibility(View.VISIBLE);
                        eventRecyclerView.setVisibility(View.GONE);
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                no_result_tv.setVisibility(View.VISIBLE);
                eventRecyclerView.setVisibility(View.GONE);
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }
}

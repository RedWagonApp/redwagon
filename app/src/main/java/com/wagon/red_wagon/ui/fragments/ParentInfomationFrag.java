package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class ParentInfomationFrag extends Fragment implements View.OnClickListener {
    ImageView gallback;
    TextView getgander, text_title;
    EditText firstname_info, surname_info, email_info, edtPhoneNumber_info;
    EditText firstname_info1, surname_info1, email_info1, edtPhoneNumber_info1;
    String firstname_info_get, surname_info_get, email_info_get, edtPhoneNumber_info_get;
    String firstname_info_get1 = "", surname_info_get1 = "", email_info_get1 = "", edtPhoneNumber_info_get1 = "";

    String image_path = "", teamId, first_name_plyr_get, manegerID, last_name_plyr_get, email_plyr_get, phone_plyr_get, edit_gender_get, address_plyr_get, dateofbirth_get = "", jersey_num_get, postion_plyr_get;
    Button next_click_info;
    CountryCodePicker ccp_info, ccp_info1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.preantinfo, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            first_name_plyr_get = bundle.getString("first_name_plyr_get", "");
            last_name_plyr_get = bundle.getString("last_name_plyr_get", "");
            email_plyr_get = bundle.getString("email_plyr_get", "");
            phone_plyr_get = bundle.getString("phone_plyr_get", "");
            edit_gender_get = bundle.getString("edit_gender_get", "");
            address_plyr_get = bundle.getString("address_plyr_get", "");
            dateofbirth_get = bundle.getString("dateofbirth_get", "");
            jersey_num_get = bundle.getString("jersey_num_get", "");
            postion_plyr_get = bundle.getString("postion_plyr_get", "");
            manegerID = bundle.getString("manegerID", "");
            teamId = bundle.getString("teamId", "");
            image_path = bundle.getString("image_path", "");
            Log.e("image_path", "onCreateView: " + image_path);
        }
        findViewID(view);

        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Parent/Guardian Information");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        next_click_info.setOnClickListener(this);
        return view;
    }

    private void findViewID(View view) {
        firstname_info = view.findViewById(R.id.firstname_info);
        surname_info = view.findViewById(R.id.surname_info);
        email_info = view.findViewById(R.id.email_info);
        edtPhoneNumber_info = view.findViewById(R.id.edtPhoneNumber_info);
        ccp_info = view.findViewById(R.id.ccp_info);

        firstname_info1 = view.findViewById(R.id.firstname_info1);
        surname_info1 = view.findViewById(R.id.surname_info1);
        email_info1 = view.findViewById(R.id.email_info1);
        edtPhoneNumber_info1 = view.findViewById(R.id.edtPhoneNumber_info1);
        ccp_info1 = view.findViewById(R.id.ccp_info1);
        next_click_info = view.findViewById(R.id.next_click_info);


    }

    @Override
    public void onClick(View v) {
        firstname_info_get = firstname_info.getText().toString();
        surname_info_get = surname_info.getText().toString();
        firstname_info_get1 = firstname_info1.getText().toString();
        surname_info_get1 = surname_info1.getText().toString();
        email_info_get = email_info.getText().toString();
        email_info_get1 = email_info1.getText().toString();
        edtPhoneNumber_info_get = edtPhoneNumber_info.getText().toString();
        edtPhoneNumber_info_get1 = edtPhoneNumber_info1.getText().toString();


        if (!firstname_info_get.equals("") && firstname_info.length() >= 1) {
            if (!surname_info_get.equals("") && surname_info_get.length() >= 1) {
                if (emailValidator(email_info_get) || emailValidator(email_info_get1)) {

                    AddPlayerAPI();

                } else {
                    Toast.makeText(getContext(), "Please enter valid email address", Toast.LENGTH_SHORT).show();
                }
            } else {
                surname_info.setError("Please enter the full surname");
            }
        } else {
            firstname_info.setError("Please enter the full name");

        }


    }


    private void AddPlayerAPI() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        MultipartBody.Part fileToUpload = null;
        Map<String, RequestBody> map = new HashMap<>();
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            fileToUpload = MultipartBody.Part.createFormData("image", "", requestBody);
          /*  RequestBody image = RequestBody.create(MediaType.parse("text/plain"), url);
            map.put("image", image);*/
        }

//        File file = new File(image_path);
//        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
        RequestBody id12;
        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), Constans.TeamId);
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), first_name_plyr_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), last_name_plyr_get);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), email_plyr_get);
        RequestBody id5 = RequestBody.create(MediaType.parse("text/plain"), phone_plyr_get);
        RequestBody id6 = RequestBody.create(MediaType.parse("text/plain"), edit_gender_get);
        RequestBody id7 = RequestBody.create(MediaType.parse("text/plain"), address_plyr_get);
        RequestBody id8 = RequestBody.create(MediaType.parse("text/plain"), dateofbirth_get);
        RequestBody id9 = RequestBody.create(MediaType.parse("text/plain"), jersey_num_get);
        RequestBody id10 = RequestBody.create(MediaType.parse("text/plain"), postion_plyr_get);
        RequestBody id11 = RequestBody.create(MediaType.parse("text/plain"), Constans.MangerId);
        if(Constans.getLoginType(getActivity()).equals("Manager")){
             id12 = RequestBody.create(MediaType.parse("text/plain"), Constans.getSchlID(getActivity()));
        }
        else {
             id12 = RequestBody.create(MediaType.parse("text/plain"), Constans.getFamilyId(getActivity()));
        }

        RequestBody id13 = RequestBody.create(MediaType.parse("text/plain"), firstname_info_get + " " + surname_info_get);
        RequestBody id14 = RequestBody.create(MediaType.parse("text/plain"), email_info_get);
        RequestBody id15 = RequestBody.create(MediaType.parse("text/plain"), ccp_info.getSelectedCountryCode() + edtPhoneNumber_info_get);
        RequestBody id16 = RequestBody.create(MediaType.parse("text/plain"), firstname_info_get1 + " " + surname_info_get1);
        RequestBody id17 = RequestBody.create(MediaType.parse("text/plain"), email_info_get1);
        RequestBody id18 = RequestBody.create(MediaType.parse("text/plain"), ccp_info1.getSelectedCountryCode() + edtPhoneNumber_info_get1);

        Log.e("ApiPlayer", "ApiPlayer: " + Constans.getFamilyId(getActivity()));
        map.put("teamId", id1);
        map.put("firstName", id2);
        map.put("lastName", id3);
        map.put("email", id4);
        map.put("phoneNumber", id5);
        map.put("gender", id6);
        map.put("address", id7);
        map.put("birthday", id8);
        map.put("jerseyNumber", id9);
        map.put("position", id10);
        map.put("manager", id11);
        map.put("schId", id12);
        map.put("parentName", id13);
        map.put("parentEmail", id14);
        map.put("parentMobile", id15);

        map.put("parentName1", id16);
        map.put("parentEmail1", id17);
        map.put("parentMobile1", id18);


        mAPIService.addTeamPlayer(map, fileToUpload).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    if (Constans.TeamId.equals("0")) {

                        Toast.makeText(getActivity(), "Player added successfully", Toast.LENGTH_SHORT).show();

                        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    } else {

                        Toast.makeText(getActivity(), "Player added successfully", Toast.LENGTH_SHORT).show();

                        PlayerListFrag eventListFragment = new PlayerListFrag();
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.add_plyaer_, eventListFragment);
                        transaction.commit();


                        FragmentManager fm = getFragmentManager();

                        assert fm != null;
                        int value = 1;
                        for (int entry = 0; entry < fm.getBackStackEntryCount(); entry++) {

                            value = fm.getBackStackEntryAt(entry).getId() - 1;
                        }

                        Log.e("Countfragnmnet", "Foundfragment: " + value);
                        try {
                            getActivity().getSupportFragmentManager().popBackStackImmediate(value, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        }
                        catch (IllegalArgumentException e){

                        }
                        //getActivity().getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }


            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {

                Toast.makeText(getActivity(), "Please try again!", Toast.LENGTH_SHORT).show();
                Log.e("Failure", "onFailure: ", t);
                dialog.dismiss();
            }
        });

    }
}

package com.wagon.red_wagon.ui.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.rilixtech.CountryCodePicker;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.fragments.EnterCodeFrag;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class FamilyRegisterActivity extends BaseActivity implements View.OnClickListener {

    // Request sing in code. Could be anything as you required.
    public static final int RequestSignInCode = 7;
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CAMERA_PIC_REQUEST = 144;
    private static final String TAG = "CalendarActivity";
    // Firebase Auth Object.
    public FirebaseAuth firebaseAuth;
    // Google API Client object.
    public GoogleApiClient googleApiClient;
    int mStatusCode;
    EditText famly_rg, surname_rg, email_rg, password_rg, phone_number_rg;
    String famly_rg_get, surname_rg_get, email_rg_get, password_rg_get, phone_number_rg_get, result;
    Button next_click_glry, google_button, next_button;
    FrameLayout frame_parent_register;
    CallbackManager callbackManager;
    LoginButton loginButton;
    TextView gellary_clickfamily, text_title;
    CountryCodePicker ccp;
    ImageView gallback;
    CircleImageView imageset;
    EditText edtPhoneNumber;
    String filePath = "", value = "", driver = "";
    Dialog dialog;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parent_profile_frag);


        IdFindview();
        value = getIntent().getStringExtra("value");

        if (value.equals("2")) {
            text_title.setText("Driver Registration");
            driver = "2";
        } else {
            text_title.setText("Family Registration");
            driver = "1";
//        hashKeyGet();
        }
        gallback.setVisibility(View.GONE);

        try {

            famly_rg.setText(Constans.getDisplayName);
            email_rg.setText(Constans.getEmail);
            phone_number_rg.setText(Constans.getPhoneNumber);
            imageUri = ((Constans.uri) != null ? (Constans.uri) : null);

        } catch (NullPointerException e) {
            Log.e("RRRRRR", "" + e);
        }

        imageset = findViewById(R.id.imageset);
        next_click_glry = findViewById(R.id.next_click_glry);
        gellary_clickfamily = findViewById(R.id.gellary_clickfamily);


        next_click_glry.setOnClickListener(this);
        gellary_clickfamily.setOnClickListener(this);
        gallback.setOnClickListener(this);
        imageset.setOnClickListener(this);
    }

    private void APIGet() {
//

        famly_rg_get = famly_rg.getText().toString().trim();
        surname_rg_get = surname_rg.getText().toString().trim();
        email_rg_get = email_rg.getText().toString().trim();
        password_rg_get = password_rg.getText().toString().trim();
        phone_number_rg_get = edtPhoneNumber.getText().toString().trim();

        if (!famly_rg_get.equals("") && famly_rg_get.length() >= 1) {
            if (!surname_rg_get.equals("") && surname_rg_get.length() >= 1) {
                if (!email_rg_get.equals("") && emailValidator(email_rg_get)) {
                    if (!password_rg_get.equals("")) {

                        if (password_rg_get.length() >= 6) {

                            if (!phone_number_rg_get.equals("")) {

                                Log.e("phone_number_rg_get", phone_number_rg_get);


                                checkEmail();


                            } else {

                                edtPhoneNumber.setError("Please enter a phone number");
                            }
                        } else {
                            password_rg.setError("password must of six characters");
                        }

                    } else {
                        password_rg.setError("Please enter the password ");
                    }
                } else {
                    email_rg.setError("Please enter the email address");
                }
            } else {
                surname_rg.setError("Please enter the full surname");
            }
        } else {
            famly_rg.setError("Please enter the full name");
        }

    }

    private void checkEmail() {
        dialog = AppUtils.showProgress(this);

        String url = Constans.BASEURL + "api/checkEmail";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("message");
                    if (status.equals("0")) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        EnterCodeFrag enterCode_frag = new EnterCodeFrag();
                        Bundle args = new Bundle();
                        args.putString("email", email_rg_get);
                        args.putString("password_rg_get", password_rg_get);
                        args.putString("ccp", ccp.getSelectedCountryCode());
                        args.putString("phone_number_rg_get", phone_number_rg_get);
                        args.putString("firstName", famly_rg_get);
                        args.putString("surname_rg_get", surname_rg_get);
                        args.putString("filePath", filePath);
                        args.putString("value", value);

                        enterCode_frag.setArguments(args);
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.parent_glry_Frame, enterCode_frag);
                        transaction.addToBackStack(null);
                        transaction.commit();
                       /* PrivacyFrag enterCode_frag = new PrivacyFrag();
                        Bundle args = new Bundle();
                        args.putString("email", email_rg_get);
                        args.putString("password_rg_get", password_rg_get);
                        args.putString("ccp", ccp.getSelectedCountryCode());
                        args.putString("phone_number_rg_get", phone_number_rg_get);
                        args.putString("firstName", famly_rg_get);
                        args.putString("surname_rg_get", surname_rg_get);
                        args.putString("filePath", filePath);

                        enterCode_frag.setArguments(args);
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.parent_glry_Frame, enterCode_frag);
                        transaction.addToBackStack(null);
                        transaction.commit();*/

                    } else {
                        String sta = jsonObject.getString("body");
                        Toast.makeText(FamilyRegisterActivity.this, sta, Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception r) {

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                try {
                    mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 400) {
                        Toast.makeText(FamilyRegisterActivity.this, "User already exists", Toast.LENGTH_SHORT).show();

                    } else if (mStatusCode == 401) {
                        AppUtils.sessionExpiredAlert(FamilyRegisterActivity.this);
                    }

                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    Log.e("response", "onErrorResponse" + error.toString());
                } catch (NullPointerException e) {

                }
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email_rg_get);
                params.put("password", password_rg_get);
                params.put("phoneNumber", ccp + phone_number_rg_get);
                params.put("firstName", famly_rg_get);
                params.put("userName", surname_rg_get);
                params.put("gender", Constans.userFind);
                Log.e("params", "params" + params);
                return params;

            }

//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("Content-Type", "application/json; charset=UTF-8");
//                        params.put("token", Constans.Token);
//                        return params;
//                    }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


        //String url = Constants.BaseUrl + Constants.action + "registration&userName=" + userName + "&phoneNumber=" +userPhone+ "&userEmail=" + userEmail + "&userPassword=" + userPass +"&dob="+userdob+ "&userDeviceToken=1234&loginDevice=android";
        //Log.e(TAG, "signUpService: " + url);

//


    }

    private void IdFindview() {

        gallback = findViewById(R.id.gallback);
        text_title = findViewById(R.id.text_title);

        famly_rg = findViewById(R.id.famly_rg);
        surname_rg = findViewById(R.id.surname_rg);
        email_rg = findViewById(R.id.email_rg);
        password_rg = findViewById(R.id.password_rg);
        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        ccp = findViewById(R.id.ccp);


    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        return matcher.matches();
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.next_click_glry) {
            APIGet();
        } else if (view.getId() == R.id.gellary_clickfamily) {

            //imageUpload(filePath);
            pickFromGallery();


        } else if (view.getId() == R.id.gallback) {
            onBackPressed();
        } else if (view.getId() == R.id.imageset) {
            openCamera();
        }
    }

    private void openCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK

        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_PIC_REQUEST) {
            try {
//                Uri uri = data.getData();
//                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                imageset.setImageURI(imageUri);

                filePath = getPath(imageUri);
            } catch (NullPointerException e) {
                Log.e("NullPointerException", "==v+" + e);
            }

        }

        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {


            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                filePath = getPath(uri);
                imageset.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }

    @Override
    public void onBackPressed() {


        if (getSupportFragmentManager().findFragmentById(R.id.parent_glry_Frame) instanceof EnterCodeFrag) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
            Constans.getDisplayName = "";
            Constans.getEmail = "";
            Constans.getPhoneNumber = "";
            finish();
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private String getPath(Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();

        } catch (SecurityException ignored) {

        }
        return result;
    }

    public void uploadPic() {

        dialog = AppUtils.showProgress(this);
        File file = new File(filePath);

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);


        AppUtils.getAPIService(this).prolieUplod(part).

                enqueue(new Callback<AddEvent>() {
                    @Override
                    public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {

                        try {
                            // Toast.makeText(FamilyRegisterActivity.this, "Profile Updated!", Toast.LENGTH_SHORT).show();
                            checkEmail();

                        } catch (Exception e) {

                        }

                    }

                    @Override
                    public void onFailure(Call<AddEvent> call, Throwable t) {
                        Log.e("Failure", "onFailure: ", t);
                    }
                });
    }

}

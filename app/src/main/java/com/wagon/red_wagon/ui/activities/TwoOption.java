package com.wagon.red_wagon.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wagon.red_wagon.R;

public class TwoOption extends BaseActivity implements View.OnClickListener {
    TextView text_title;
    LinearLayout family_set, coach_set;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.two_coach_fam );

        family_set = findViewById( R.id.family_set );
        coach_set = findViewById( R.id.coach_set );
        text_title = findViewById( R.id.text_title );

        text_title.setText( "What would you like to do Today" );
        coach_set.setOnClickListener( this );
        family_set.setOnClickListener( this );
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.coach_set) {

            SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
            editor.putString("userr", "Manager");
            editor.putString("Coach", "1");
            editor.apply();
            Intent intent = new Intent( TwoOption.this, SchoolDashboard.class);
            intent.putExtra("Coach","2");
            intent.putExtra("text","Coach");
            intent.putExtra( "type","1" );
            startActivity(intent);
            finish();

        }
        else if (v.getId() == R.id.family_set) {
            SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
            editor.putString("userr", "Manager");
            editor.apply();
            Intent intent = new Intent( TwoOption.this, FamilyDashboardActivity.class);
            intent.putExtra( "check","1" );
            intent.putExtra( "type","1" );
            intent.putExtra("child", "0");
            startActivity(intent);
            finish();

        }
    }
}

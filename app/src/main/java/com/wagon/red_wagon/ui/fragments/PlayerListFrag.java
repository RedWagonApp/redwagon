package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.PlayerListAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.Event_List_model;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class PlayerListFrag extends Fragment implements View.OnClickListener {

    CardView add_player_lay, cardview;
    RecyclerView players_recyclerview;
    RelativeLayout linear_main_bar;
    ArrayList<Event_List_model> arrayList;
    String teamId = "", typeshow = "", teamname = "", manegerID = "", teamImage = "";
    TextView player_count, text_title, teams, team_name;
    LinearLayout sort;
    ImageView gallback;
    CircleImageView team_img;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.player_list_lay, container, false);
        findView(view);
        add_player_lay.setOnClickListener(this);

        return view;
    }

    private void findView(View view) {
        arrayList = new ArrayList<>();

        add_player_lay = view.findViewById(R.id.add_player_lay);
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        sort = view.findViewById(R.id.sort);

        players_recyclerview = view.findViewById(R.id.players_recyclerview);
        player_count = view.findViewById(R.id.player_count);
        teams = view.findViewById(R.id.teams);
        cardview = view.findViewById(R.id.cardview);
        player_count = view.findViewById(R.id.player_count);

        try {


            Bundle bundle = getArguments();
            teamId = bundle.getString("teamId", "");
            typeshow = bundle.getString("typeshow", "");
            teamname = bundle.getString("teamname", "");
            manegerID = bundle.getString("manegerID", "");
            teamImage = bundle.getString("teamImage", "");
            Log.e("findView", "findView: " + manegerID);

            if (typeshow.equals("1")) {
                FamilyDashboardActivity.title.setText(teamname);
                ((FamilyDashboardActivity) getActivity()).isVisible(false);
                add_player_lay.setVisibility(View.GONE);
                cardview.setVisibility(View.VISIBLE);
                teams.setVisibility(View.VISIBLE);
                sort.setVisibility(View.GONE);
                linear_main_bar.setVisibility(View.GONE);
                team_img = view.findViewById(R.id.team_img);
                team_name = view.findViewById(R.id.team_name);
                Glide.with(getActivity()).load(Constans.BASEURL + teamImage).into(team_img);
                team_name.setText(teamname);
            } else {
//                SchoolDashboard.title.setText("Players");
//                ((SchoolDashboard) getActivity()).isVisible(false);
                linear_main_bar.setVisibility(View.VISIBLE);
                text_title = view.findViewById(R.id.text_title);
                text_title.setText("Players");
                gallback = view.findViewById(R.id.gallback);
                gallback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().onBackPressed();
                    }
                });
            }
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        players_recyclerview.setLayoutManager(linearLayoutManager);


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_player_lay) {
            AddRoasterFrag addPlayer = new AddRoasterFrag();
            Bundle args = new Bundle();
            args.putString("teamId", teamId);
            args.putString("manegerID", manegerID);
            args.putString("value", "2");
            addPlayer.setArguments(args);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.add_plyaer_, addPlayer);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private void getList() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        getApi = mAPIService.getTeamPlayer(Constans.TeamId);
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();
                    if (responseBody.size() != 0) {
                        player_count.setText("Players (" + responseBody.size() + ")");

                    }
                    PlayerListAdapter playerAdapter = new PlayerListAdapter(getActivity(), responseBody, typeshow);
                    players_recyclerview.setAdapter(playerAdapter);


                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                if (response.code() == 401) {
                    Toast.makeText(getContext(), "Session Expired! Please Login Again.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(getContext(), "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onResume", "onStart: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "onResume: ");
        getList();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.e("onResume", "onAttach: ");
    }

}

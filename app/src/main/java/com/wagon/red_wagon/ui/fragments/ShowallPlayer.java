package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ShowallPlayerAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowallPlayer extends Fragment implements ShowallPlayerAdapter.RecipientsCallBack {
    TextView text_title, no_result_tv;
    ImageView gallback;
    RecyclerView showallplyr_recycler;
    ShowallPlayerAdapter.RecipientsCallBack callBack;
    public static Button add_plyerlist;
    String teamId, from = "";
    Call<GetEvent> getApi;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.showallplayer, container, false);
        showallplyr_recycler = view.findViewById(R.id.showallplyr_recycler);
        add_plyerlist = view.findViewById(R.id.add_plyerlist);
        text_title = view.findViewById(R.id.text_title);
        no_result_tv = view.findViewById(R.id.no_result_tv);


        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        Bundle bundle = getArguments();

        if (bundle != null) {
            teamId = bundle.getString("teamId", "");
            from = bundle.getString("from", "");
            if (from.equals("Students")) {
                text_title.setText("Players");
                Get_Players(this, "Students");
                add_plyerlist.setText("Add Players");
            } else {
                text_title.setText("Players");
                Get_Players(this, "Player");
                add_plyerlist.setText("Add Players");
            }


        }
        Log.e("teamId", "onCreateView: " + teamId);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        showallplyr_recycler.setLayoutManager(linearLayoutManager);


        return view;
    }

    private void Get_Players(final ShowallPlayerAdapter.RecipientsCallBack showallPlayer, String from) {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());

        if (from.equals("Students")) {
            getApi = mAPIService.getAllTeamPlayerZero(Constans.getFamilyId(getActivity()));
        } else {
            getApi = mAPIService.getTeamPlyer(Constans.getFamilyId(getActivity()));
        }
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    List<Body> responseBody = response.body().getBody();

                    if (responseBody.size() != 0) {
                        no_result_tv.setVisibility(View.GONE);
                        showallplyr_recycler.setVisibility(View.VISIBLE);
                        ShowallPlayerAdapter recipients_adapter = new ShowallPlayerAdapter(getActivity(), responseBody, showallPlayer, from);
                        showallplyr_recycler.setAdapter(recipients_adapter);
                    } else {
                        no_result_tv.setVisibility(View.VISIBLE);
                        showallplyr_recycler.setVisibility(View.GONE);
                    }

                } else if (response.code() == 400) {
                    dialog.dismiss();
                    no_result_tv.setVisibility(View.VISIBLE);
                    showallplyr_recycler.setVisibility(View.GONE);

                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    @Override
    public void onMethodCallback(ArrayList<String> arrayplyerId) {

        Log.e("arrayplyerId", "onMethodCallback: " + arrayplyerId);

        addRoster(arrayplyerId);

    }


    private void addRoster(ArrayList<String> arrayplyerId) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        getApi = mAPIService.addRoster(teamId, AppUtils.convertToString(arrayplyerId),Constans.getFamilyId(getActivity()),"Players");
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, Response<GetEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        // Toast.makeText(getActivity(), "Team Created Successfully", Toast.LENGTH_SHORT).show();
                        TeamDetailFrag createTeamFrag = new TeamDetailFrag();
                        FragmentManager manager = getActivity().getSupportFragmentManager();
                        FragmentTransaction trans = manager.beginTransaction();
                        trans.replace(R.id.frame_team, createTeamFrag);
                        trans.commit();
                        FragmentManager fm = getFragmentManager();

                        assert fm != null;
                        int value = 0;
                        for (int entry = 0; entry < fm.getBackStackEntryCount(); entry++) {

                            value = fm.getBackStackEntryAt(entry).getId() - 1;

                        }
                        getActivity().getSupportFragmentManager().popBackStackImmediate(value, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

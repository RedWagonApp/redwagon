package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class AddRosterManuallyFrag extends Fragment implements View.OnClickListener {

    Button add_roster, add_anouther;
    TextView addNewTeam, addRosterManually, text_title;
    RecyclerView roster_rv;
    private String teamId;
    String nameuser = "", useremail = "", usernumber = "";
    ImageView gallback;
    EditText roster_num, roster_name, roster_email;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.add_roster_manually, container, false);
        findviewId(view);
        text_title = view.findViewById(R.id.text_title);
        text_title.setText("Add Roster Manually");
        gallback = view.findViewById(R.id.gallback);
        gallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        add_roster.setOnClickListener(this);
        add_anouther.setOnClickListener(this);
        return view;
    }

    private void findviewId(View view) {
        add_roster = view.findViewById(R.id.add_roster);
        add_anouther = view.findViewById(R.id.add_anouther);
        roster_name = view.findViewById(R.id.roster_name);
        roster_email = view.findViewById(R.id.roster_email);
        roster_num = view.findViewById(R.id.roster_num);
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                teamId = bundle.getString("teamId", "");
            }
        } catch (Exception ignored) {

        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.add_roster) {
            nameuser = roster_name.getText().toString().trim();
            useremail = roster_email.getText().toString().trim();
            usernumber = roster_num.getText().toString().trim();
            if (!nameuser.equals("")) {
                if (!useremail.equals("") && emailValidator(useremail)) {
                    if (!usernumber.equals("")) {
                        addRoster("Save");
                    } else {
                        Toast.makeText(getActivity(), "Please enter the number", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please enter the valid email", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Please enter the name", Toast.LENGTH_SHORT).show();
            }
        }
        if (v.getId() == R.id.add_anouther) {
            nameuser = roster_name.getText().toString();
            useremail = roster_email.getText().toString();
            usernumber = roster_num.getText().toString();

            if (!nameuser.equals("")) {
                if (!useremail.equals("") && emailValidator(useremail)) {
                    if (!usernumber.equals("")) {
                        addRoster("Another");
                    } else {
                        Toast.makeText(getActivity(), "Please enter the number", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please enter the valid email", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Please enter the name", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void addRoster(String addFrom) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<AddEvent> getApi;
        getApi = mAPIService.addRosterManually(teamId, nameuser, useremail, usernumber, Constans.getFamilyId(getActivity()));
        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    if (addFrom.equals("Another")) {
                        roster_name.setText("");
                        roster_email.setText("");
                        roster_num.setText("");
                    } else {

                        TeamDetailFrag createTeamFrag = new TeamDetailFrag();
                        FragmentManager manager = getActivity().getSupportFragmentManager();
                        FragmentTransaction trans = manager.beginTransaction();
                        trans.replace(R.id.frame_team, createTeamFrag);
                        trans.commit();
                        FragmentManager fm = getFragmentManager();

                        assert fm != null;
                        int value = 0;
                        for (int entry = 0; entry < fm.getBackStackEntryCount(); entry++) {

                            value = fm.getBackStackEntryAt(entry).getId() - 1;

                        }
                        getActivity().getSupportFragmentManager().popBackStackImmediate(value, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        Log.e("Countfragnmnet", "Foundfragment: " + value);


                    }
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }


}

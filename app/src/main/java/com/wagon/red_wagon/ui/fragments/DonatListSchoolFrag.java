package com.wagon.red_wagon.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.wagon.red_wagon.R;

public class DonatListSchoolFrag extends Fragment {
    EditText school,nolisted,contactname,cityname,statename,zipcode,contaractnumber;
     String school_get,nolisted_get,contactname_get,cityname_get,statename_get,zipcode_get,contractnumber_get;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=LayoutInflater.from(container.getContext()).inflate(R.layout.instude_and_instude_frag,container,false);

        viewidfind(view);
        getvalue();

        return view;
    }

    private void getvalue() {
        school_get = school.getText().toString();
        nolisted_get = nolisted.getText().toString();
        contactname_get = contactname.getText().toString();
        cityname_get = cityname.getText().toString();
        zipcode_get = zipcode.getText().toString();
        contractnumber_get = contaractnumber.getText().toString();
        statename_get = statename.getText().toString();
    }


    private void viewidfind(View view) {
        school=view.findViewById(R.id.school);
        nolisted=view.findViewById(R.id.nolisted);
        contactname=view.findViewById(R.id.contactname);
        cityname=view.findViewById(R.id.cityname);
        zipcode=view.findViewById(R.id.zipecode);
        contaractnumber=view.findViewById(R.id.Contractnumber);
        statename=view.findViewById(R.id.statename);
    }
}


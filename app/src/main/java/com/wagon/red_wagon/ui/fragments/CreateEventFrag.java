package com.wagon.red_wagon.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class CreateEventFrag extends Fragment implements View.OnClickListener {
    private static final int GALLERY_REQUEST_CODE = 122;
    Button create_eventButton;
    EditText eventName, eventLocation, eventTimeZone, eventCountry, eventZipCode, eventDateTime, eventDuration;
    String eventNameStr, eventLocationStr, eventTimeZoneStr, eventCountryStr, eventZipCodeStr, eventDateTimeStr, eventDurationStr;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private final static int PLACE_PICKER_REQUEST = 111;
    //private String fromType;
    LinearLayout upload_logo;
    CircleImageView logo_img;
    private String image_path = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_event, container, false);
        findviewId(view);
        create_eventButton.setOnClickListener(this);
        eventDateTime.setOnClickListener(this);
        eventTimeZone.setOnClickListener(this);
        eventLocation.setOnClickListener(this);
        eventDuration.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        final CountryAdapter.CountryCallback countryCallback = new CountryAdapter.CountryCallback() {
            @Override
            public void onSelectCountryCallback(String name) {
                AppUtils.dismissDialog();
                eventCountry.setText(name);
            }

            @Override
            public void onSelectTimeZoneCallback(String name) {

            }
        };
        eventCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.countryDialog(getContext(), countryCallback);
            }
        });
        eventTimeZone.setText(AppUtils.getCurrentTimeZone());
        return view;
    }

    private void findviewId(View view) {
        logo_img = view.findViewById(R.id.logo_img);
        upload_logo = view.findViewById(R.id.upload_logo);
        create_eventButton = view.findViewById(R.id.create_event_btn);
        eventName = view.findViewById(R.id.event_name);
        eventLocation = view.findViewById(R.id.event_location);
        eventTimeZone = view.findViewById(R.id.event_time_zone);
        eventCountry = view.findViewById(R.id.event_country);
        eventZipCode = view.findViewById(R.id.event_zip);
        eventDateTime = view.findViewById(R.id.event_date_time);
        eventDuration = view.findViewById(R.id.event_duration);
        Bundle args = getArguments();
    }

    @Override
    public void onClick(View view) {


        if (view == eventDateTime) {
            datePicker();
        }
        if (view == create_eventButton) {

            validation();
        }

        if (view == eventDuration) {

            opendurationDialog();
        }
        if (view.getId() == R.id.upload_logo) {
            startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

        }

    }


    private void opendurationDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.durationdialog);
        dialog.setTitle("Select " + " Duration");
        TimePicker simpleTimePicker = (TimePicker) dialog.findViewById(R.id.timepicker);
        simpleTimePicker.setIs24HourView(true);
        simpleTimePicker.setCurrentHour(new Integer(0));
        simpleTimePicker.setCurrentMinute(new Integer(0));
        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String selectedTime = String.format("%02d:%02d", hourOfDay, minute);
                eventDuration.setText(selectedTime);
            }
        });
        dialog.show();
        dialog.findViewById(R.id.set_duration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eventDuration.getText().toString().equals("")) {

                    Toast.makeText(getContext(), "Please set Duration", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    private void datePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String strDate = format.format(calendar.getTime());
                        String selectedDate = strDate;
                        timePicker(selectedDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    private void timePicker(final String selectedDate) {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String selectedTime = String.format("%02d:%02d", hourOfDay, minute);

                        eventDateTime.setText(selectedDate + "  " + selectedTime);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    private void validation() {
        eventNameStr = eventName.getText().toString();
        eventLocationStr = eventLocation.getText().toString();
        eventTimeZoneStr = eventTimeZone.getText().toString();
        eventCountryStr = eventCountry.getText().toString();
        eventZipCodeStr = eventZipCode.getText().toString();
        eventDateTimeStr = eventDateTime.getText().toString();
        eventDurationStr = eventDuration.getText().toString();
        if (!eventNameStr.equals("")) {
            if (!eventLocationStr.equals("")) {
                if (!eventTimeZoneStr.equals("")) {
                    if (!eventCountryStr.equals("")) {
                        if (!eventZipCodeStr.equals("")) {
                            if (!eventDateTimeStr.equals("")) {
                                if (!eventDurationStr.equals("")) {
                                    if (!image_path.equals("")) {
                                        RecurrenceFrag recurrence_frag = new RecurrenceFrag();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.create_event_frame, recurrence_frag);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("eventNameStr", eventNameStr);
                                        bundle.putString("eventLocationStr", eventLocationStr);
                                        bundle.putString("eventTimeZoneStr", eventTimeZoneStr);
                                        bundle.putString("eventCountryStr", eventCountryStr);
                                        bundle.putString("eventZipCodeStr", eventZipCodeStr);
                                        bundle.putString("eventDateTimeStr", eventDateTimeStr);
                                        bundle.putString("eventDurationStr", eventDurationStr);
                                        bundle.putString("eventDurationStr", eventDurationStr);
                                        bundle.putString("image_path", image_path);
                                        bundle.putString("fromType", Constans.fromType);
                                        recurrence_frag.setArguments(bundle);
                                        transaction.commit();
                                    } else {
                                        Toast.makeText(getActivity(), "Please Select Logo", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Please Select Duration", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Please Select Date and Time", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Please Enter Zip Code", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please Select Country", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please Select Time Zone", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Please Select Location", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Please Enter Name", Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                logo_img.setImageBitmap(bitmap);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private String getrealPathFromUrl(Uri uri) {
        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();
        return result;
    }

}

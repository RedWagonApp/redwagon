package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.wagon.red_wagon.utils.AppUtils.emailValidator;

public class Invite_Screen extends Fragment implements View.OnClickListener {
    TextView text_title;
    private EditText invite_name, invite_mobile_num, invite_email_address;
    private String arg = "", invite_name_get, invite_mobile_num_get, invite_email_address_get, Invite_Screen;
    private Button invite_send;
    RelativeLayout linear_main_bar;
    ImageView gallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.invite, container, false);

        FindView(view);

        arg = getArguments().getString("school_dashboard", "");

        if (arg.equals("2")) {
            FamilyDashboardActivity.title.setText("Invite");
            ((FamilyDashboardActivity) getActivity()).isVisible(false);
            linear_main_bar.setVisibility(View.GONE);
        } else {
            text_title.setText("Invite");
            gallback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
//            SchoolDashboard.title.setText("Invite");
//            ((SchoolDashboard) getActivity()).isVisible(false);
        }




   /*     try {
            Bundle args = getArguments();

            Invite_Screen = args.getString("Invite_Screen", "");
            if (Invite_Screen.equals("1")) {
                linear_main_bar.setVisibility(View.GONE);
            } else {
                linear_main_bar.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

        }*/

        invite_send.setOnClickListener(this);

        return view;
    }

    private void FindView(View view) {
        invite_name = view.findViewById(R.id.invite_name);
        gallback = view.findViewById(R.id.gallback);
        invite_mobile_num = view.findViewById(R.id.invite_mobile_num);
        invite_email_address = view.findViewById(R.id.invite_email_address);
        invite_send = view.findViewById(R.id.invite_send);
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        text_title = view.findViewById(R.id.text_title);
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.invite_send) {

            Validation();


        }
    }

    private void Validation() {


        invite_name_get = invite_name.getText().toString();
        invite_mobile_num_get = invite_mobile_num.getText().toString();
        invite_email_address_get = invite_email_address.getText().toString();


        if (!invite_name_get.equals("")) {

            if (!invite_mobile_num_get.equals("")) {
                //getting the progressbar
                if (!invite_email_address_get.equals("") && emailValidator(invite_email_address_get)) {
                    signUpService();
                } else {
                    invite_email_address.setError("Please enter the email");
                }


            } else {
                invite_mobile_num.setError("Please enter the Mobile ");
            }
        } else {
            invite_name.setError("Please enter your Name");
        }
    }


    private void signUpService() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        String url = Constans.BASEURL + "api/Invite";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                try {
                    dialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(getActivity(), "Invite sent successfully", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();

                    invite_name.setText("");
                    invite_mobile_num.setText("");
                    invite_email_address.setText("");
                } catch (Exception r) {

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                NetworkResponse response = error.networkResponse;
                try {
                    int mStatusCode = error.networkResponse.statusCode;
                    if (mStatusCode == 401) {
                        // HTTP Status Code: 401 Unauthorized
                        AppUtils.sessionExpiredAlert(getContext());
                    }
                } catch (NullPointerException e) {
                    Toast.makeText(getContext(), "Server Error", Toast.LENGTH_SHORT).show();

                }
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject obj = new JSONObject(res);
                        String message = obj.getString("message");
                        Log.e("objobjobj", "objobjobj" + message);

                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
            }


        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("firstName", invite_name_get);
                params.put("email", invite_email_address_get);
                params.put("phoneNumber", invite_mobile_num_get);

                Log.e("params", "params" + params);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(getContext()));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }
        };

//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("Content-Type", "application/json; charset=UTF-8");
//                        params.put("token", Constans.Token);
//                        return params;
//                    }

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }
}

package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.ChatsListAdapter;
import com.wagon.red_wagon.adapter.ShowallManegrAdapter;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ShowSchoolManger extends Fragment {
    RecyclerView allmanegerRv;
    public ChatsListAdapter mAdapter;
    int mStatusCode;
    ImageView gallback;
    TextView titleTxt, no_result_tv;
    String arg = "", value = "";

    RelativeLayout linear_main_bar;
    EditText search_et;
    List<Body> responseBody;
    ShowallManegrAdapter recipients_adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.showall_manegar, container, false);
        titleTxt = view.findViewById(R.id.text_title);
        linear_main_bar = view.findViewById(R.id.linear_main_bar);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            arg = bundle.getString("value", "");
        }
        Log.e("argarg", "onCreateView: " + arg);
        if (arg.equals("1")) {
            Get_Family();
            linear_main_bar.setVisibility(View.GONE);
        } else {
            linear_main_bar.setVisibility(View.VISIBLE);
            Get_Maneger();
            titleTxt.setText("New Chat");
            gallback = view.findViewById(R.id.gallback);
            gallback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
//
        }


        search_et = view.findViewById(R.id.search_et);
        allmanegerRv = view.findViewById(R.id.allmaneger);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        allmanegerRv.setLayoutManager(linearLayoutManager);

        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });

        return view;
    }

    private void filter(String text) {

        ArrayList<Body> filterdList = new ArrayList<>();
        if (responseBody.size() != 0) {
            for (Body s : responseBody) {

                if (s.getFirstName().toLowerCase().contains(text.toLowerCase())) {
                    filterdList.add(s);
                }
            }
        }
        recipients_adapter.filterList(filterdList);
    }

    private void Get_Family() {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;

        getApi = mAPIService.getFamily();
        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    responseBody = response.body().getBody();
                    if (responseBody.size() != 0) {
                        no_result_tv.setVisibility(View.GONE);
                        allmanegerRv.setVisibility(View.VISIBLE);
                        recipients_adapter = new ShowallManegrAdapter(getActivity(), responseBody, arg);
                        allmanegerRv.setAdapter(recipients_adapter);
                    } else {
                        no_result_tv.setVisibility(View.VISIBLE);
                        allmanegerRv.setVisibility(View.GONE);
                    }

                    ;


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

//

    private void Get_Maneger() {

        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        Call<GetEvent> getApi;
        if (Constans.getLoginType(getActivity()).equals("Manager")) {
            getApi = mAPIService.getManager(Constans.getSchlID(getActivity()));
        } else {
            getApi = mAPIService.getManager(Constans.getFamilyId(getActivity()));
        }


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    responseBody = response.body().getBody();

                    if (responseBody.size() != 0) {
                        no_result_tv.setVisibility(View.GONE);
                        allmanegerRv.setVisibility(View.VISIBLE);
                        recipients_adapter = new ShowallManegrAdapter(getActivity(), responseBody, arg);
                        allmanegerRv.setAdapter(recipients_adapter);
                    }
                    else {
                        no_result_tv.setVisibility(View.VISIBLE);
                        allmanegerRv.setVisibility(View.GONE);
                    }

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }
}

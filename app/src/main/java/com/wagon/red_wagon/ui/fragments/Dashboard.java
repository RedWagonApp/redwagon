package com.wagon.red_wagon.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.NotificationResponse;
import com.wagon.red_wagon.ui.activities.AddScoolDialog;
import com.wagon.red_wagon.ui.activities.CompleteDialogClass;
import com.wagon.red_wagon.ui.activities.DriverDialogClass;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.ui.activities.JoincarpoolDailoge;
import com.wagon.red_wagon.utils.Constans;

import java.io.Serializable;

import static android.content.Context.MODE_PRIVATE;

public class Dashboard extends Fragment {
    ImageView sidebar;
    DrawerLayout mDrawerLayout;
    LinearLayout community, teamfm, event_list, carpool_lay, chat_family, requestride, child_ride, onride_lay;
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    NotificationResponse response;
    Dialog driverDialog;
    String loginFrom = "";
    SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.dashboard_famliy, container, false);


        event_list = view.findViewById(R.id.event_list);
        chat_family = view.findViewById(R.id.chat_family);
        child_ride = view.findViewById(R.id.child_ride);
        teamfm = view.findViewById(R.id.teamfm);
        onride_lay = view.findViewById(R.id.onride_lay);
        carpool_lay = view.findViewById(R.id.carpool_lay);
        community = view.findViewById(R.id.community);
        requestride = view.findViewById(R.id.requestride);
        sharedPreferences = getActivity().getSharedPreferences("Login", MODE_PRIVATE);

        FamilyDashboardActivity.title.setText("Home");
        ((FamilyDashboardActivity) getActivity()).isVisible(true);
        Bundle extras = getArguments();
        if (extras != null) {
            response = (NotificationResponse) extras.getSerializable("data");
            if (response != null) {
                if (response.getType().equals("ride status")) {
                    if (response.getStatus().equals("1")) {
                        OnGoingRideFrag ongoingrideFrag = new OnGoingRideFrag();
                        fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fram_dash, ongoingrideFrag, "On Going Ride");
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        Toast.makeText(getContext(), "User Rejected your request", Toast.LENGTH_SHORT).show();
                    }
                    extras.clear();
                } else if (response.getType().equals("Request")) {
                    Intent intent = new Intent(getContext(), DriverDialogClass.class);
                    intent.putExtra("data", (Serializable) response);
                    intent.putExtra("check", "1");

                    startActivityForResult(intent, 101);
                    extras.clear();
                } else if (response.getType().equals("completed")) {
                    Intent intent = new Intent(getContext(), CompleteDialogClass.class);
                    intent.putExtra("data", (Serializable) response);
                    intent.putExtra("check", "1");
                    startActivityForResult(intent, 102);
                    extras.clear();
                }else if (response.getType().equals("joinFamily")||response.getType().equals("joinCarPool")||response.getType().equals("join")) {
                    Intent intent = new Intent(getContext(), AddScoolDialog.class);
                    intent.putExtra("data", (Serializable) response);
                    intent.putExtra("check", "1");
                    startActivityForResult(intent, 102);
                    extras.clear();
                }

                else if (response.getType().equals("addFamily")||response.getType().equals("addJoinCarPool"))  {
                    Intent intent = new Intent(getContext(), JoincarpoolDailoge.class);
                    intent.putExtra("data", (Serializable) response);
                    intent.putExtra("check", "1");
                    startActivityForResult(intent, 102);
                    extras.clear();
                }else if (response.getType().equals("Chat"))  {
                    Log.e("useriduserid", "onCreateView: "+response.getUserId() );
                    ChatsFragment chatsFragment = new ChatsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("value", "1");
                    bundle.putString("userid", response.getMeUserId());
                    chatsFragment.setArguments(bundle);
                    fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fram_dash, chatsFragment, "Chat");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    extras.clear();
                }


                Log.e("Response", "onCreateView:" + response.getType());

            }
        }

        requestride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new RequestRide(), "Request a Ride");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        community.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new Community(), "Community");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        child_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, new TrackChild(), "Track Child");
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });


        event_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FamilyScheduleList schedule_list_frag = new FamilyScheduleList();
                Bundle args = new Bundle();
                Constans.fromType = "Schedule";
                args.putString("fromType", "Schedule");
                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, schedule_list_frag, "Schedules");
                transaction.addToBackStack(Constans.fromType);
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });

        carpool_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CarpoolListFrag eventListFragment = new CarpoolListFrag();
               /* Bundle args = new Bundle();
                args.putString("fromType", "Schedule");
                eventListFragment.setArguments(args);*/
                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, eventListFragment, "Carpools");
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });

        chat_family.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatsFragment chatsFragment = new ChatsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("value", "1");
                bundle.putString("userid", "0");
                chatsFragment.setArguments(bundle);
                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, chatsFragment, "Chat");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        teamfm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                TeamFamily teamFamily = new TeamFamily();
                Bundle bundle = new Bundle();
                bundle.putString("value", "1");
                teamFamily.setArguments(bundle);
                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, teamFamily, "Team");
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });
        onride_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginFrom = sharedPreferences.getString("driver", "");
                OnGoingRideFrag ongoingrideFrag = new OnGoingRideFrag();
                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, ongoingrideFrag, "On Going Ride");
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        new CreateEventFrag().onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.crate_activity_scl);
        Fragment fragment1 = getActivity().getSupportFragmentManager().findFragmentById(R.id.frag_frame_eve);
        if (fragment instanceof CostFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);

        }

        if (fragment1 instanceof EditActivityFrag) {
            fragment1.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == 101) {
            if (data.getStringExtra("myResult").equals("1")) {
                OnGoingRideFrag ongoingrideFrag = new OnGoingRideFrag();

                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, ongoingrideFrag, "On Going Ride");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }
    }


}

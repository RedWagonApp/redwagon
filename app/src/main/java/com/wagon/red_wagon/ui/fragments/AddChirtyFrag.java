package com.wagon.red_wagon.ui.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.content.CursorLoader;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;
import com.wagon.red_wagon.utils.ImageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class AddChirtyFrag extends Fragment implements View.OnClickListener {
    Button next_addchirty;
    private static final int GALLERY_REQUEST_CODE = 1;

    private EditText name_charty, name_acount_chirty, bank_name_chirty, bankcode_chirty, account_number_chirty, reacount_number_chirty;
    private String image_path = "", name_charty_get, name_acount_chirty_get, bank_name_chirty_get, bankcode_chirty_get, account_number_chirty_get, reacount_number_chirty_get;
    TextView text_title;
    APIService mAPIService;
    private CircleImageView imageset;
    TextView upload_logo;
    Call<AddEvent> getApi;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.add_your_chirty, container, false);
        FamilyDashboardActivity.title.setText("Add your Charity");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);

        findViewId(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getActivity().getPackageName()));
            getActivity().finish();
            startActivity(intent);
        }

        upload_logo = view.findViewById(R.id.upload_logo);
        next_addchirty.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        upload_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
                Log.e("ImageVieew", "onClick: ");
            }
        });
        return view;
    }

    private void findViewId(View view) {
        mAPIService = AppUtils.getAPIService(getContext());
        next_addchirty = view.findViewById(R.id.next_addchirty);
        name_charty = view.findViewById(R.id.name_charty);
        name_acount_chirty = view.findViewById(R.id.name_acount_chirty);
        bank_name_chirty = view.findViewById(R.id.bank_name_chirty);
        bankcode_chirty = view.findViewById(R.id.bankcode_chirty);
        account_number_chirty = view.findViewById(R.id.account_number_chirty);
        reacount_number_chirty = view.findViewById(R.id.reacount_number_chirty);
        imageset = view.findViewById(R.id.imageset_child);


    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.next_addchirty) {
            validation();
        }
    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI), GALLERY_REQUEST_CODE);

    }

    private void validation() {

        name_charty_get = name_charty.getText().toString().trim();
        name_acount_chirty_get = name_acount_chirty.getText().toString().trim();
        bank_name_chirty_get = bank_name_chirty.getText().toString().trim();
        bankcode_chirty_get = bankcode_chirty.getText().toString().trim();
        account_number_chirty_get = account_number_chirty.getText().toString().trim();
        reacount_number_chirty_get = reacount_number_chirty.getText().toString().trim();


        if (!name_charty_get.equals("")) {
            if (!name_acount_chirty_get.equals("")) {

                if (!bank_name_chirty_get.equals("")) {


                    if (!bankcode_chirty_get.equals("")) {

                        if (!account_number_chirty_get.equals("")) {

                            if (reacount_number_chirty_get.equals(account_number_chirty_get)) {


                                signUpService();

                                //Toast.makeText(getActivity(), "Charity upload", Toast.LENGTH_SHORT).show();
                            } else {

                                Toast.makeText(getActivity(), "Account number does not match", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            reacount_number_chirty.setError("Please enter the Reaccount Number");
                        }
                    } else {
                        account_number_chirty.setError("Please enter the Account Number");
                    }
                } else {
                    bankcode_chirty.setError("Please enter the Bank Code");
                }
            } else {
                name_acount_chirty.setError("Please enter the Bank Name");
            }
        } else {
            name_charty.setError("Please enter the Name Charity");
        }


    }

    private void signUpService() {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());
        dialog.show();
        MultipartBody.Part part = null;
        if (!image_path.equals("")) {
            try {
                File file = new File(image_path);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            part = MultipartBody.Part.createFormData("image", "", requestBody);
        }
        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), name_charty_get);
        RequestBody id2 = RequestBody.create(MediaType.parse("text/plain"), name_acount_chirty_get);
        RequestBody id3 = RequestBody.create(MediaType.parse("text/plain"), bank_name_chirty_get);
        RequestBody id4 = RequestBody.create(MediaType.parse("text/plain"), account_number_chirty_get);
        Map<String, RequestBody> map = new HashMap<>();
        map.put("charityName", id1);
        map.put("charityAccountName", id2);
        map.put("charityBankName", id3);
        map.put("charityAcccountNo", id4);

        mAPIService.AddChirty(map, part).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();

                if (response.code() == 200) {

//
                    String chartyId = response.body().getBody().getId().toString();
                    SharedPreferences.Editor editor1 = getActivity().getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor1.putString("charityId", chartyId);
                    editor1.apply();
                    UpdateCharty(chartyId);
                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void UpdateCharty(String ChartyId) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getContext());

        mAPIService.updateCharityId(Constans.getFamilyId(getActivity()), ChartyId).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {

                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fram_dash, new CongFamily(), "Congratulations");
                        transaction.addToBackStack(null);
                        transaction.commit();

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getContext(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
//            Uri uri = data.getData();
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//
//                image_path = getrealPathFromUrl(uri);
//                imageset.setImageBitmap(image);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                image_path = ImageUtils.getCompressedBitmap(getrealPathFromUrl(uri));
                imageset.setImageBitmap(bitmap);

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }


    private String getrealPathFromUrl(Uri uri) {

        String[] po = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, po, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int count_int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(count_int);
        cursor.close();

        return result;
    }


}

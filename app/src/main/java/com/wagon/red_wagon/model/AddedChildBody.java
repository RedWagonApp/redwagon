package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddedChildBody {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("childId")
    @Expose
    private Integer childId;
    @SerializedName("carPoolId")
    @Expose
    private Integer carPoolId;
    @SerializedName("parentId")
    @Expose
    private Integer parentId;
    @SerializedName("carName")
    @Expose
    private String carName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChildId() {
        return childId;
    }

    public void setChildId(Integer childId) {
        this.childId = childId;
    }

    public Integer getCarPoolId() {
        return carPoolId;
    }

    public void setCarPoolId(Integer carPoolId) {
        this.carPoolId = carPoolId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }


}

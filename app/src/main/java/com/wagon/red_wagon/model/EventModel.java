package com.wagon.red_wagon.model;

public class EventModel {
    private String eventName;
    private String eventImg;
    private String phnNum, weeks;
    public EventModel(String eventName, String eventImg) {
        this.eventName = eventName;
        this.eventImg = eventImg;

    }

    public EventModel(String eventName, String eventImg, String teamId) {
        this.eventName = eventName;
        this.eventImg = eventImg;
        this.teamId = teamId;

    }

    public String getPhnNum() {
        return phnNum;
    }

    public void setPhnNum(String phnNum) {
        this.phnNum = phnNum;
    }

    public String getWeeks() {
        return weeks;
    }

    public void setWeeks(String weeks) {
        this.weeks = weeks;
    }

    public EventModel(String eventName, String eventImg, String phnNum, String weeks) {
        this.eventName = eventName;
        this.eventImg = eventImg;
        this.weeks = weeks;
        this.phnNum = phnNum;

    }
    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventImg() {
        return eventImg;
    }

    public void setEventImg(String eventImg) {
        this.eventImg = eventImg;
    }


    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    private String teamId;

}

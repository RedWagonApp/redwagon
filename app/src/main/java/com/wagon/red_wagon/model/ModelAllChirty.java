package com.wagon.red_wagon.model;

public class ModelAllChirty {
    String charityName,url,charityId;
    public ModelAllChirty(String url, String charityName,String charityId) {

        this.url=url;
        this.charityName=charityName;
    }

    public String getCharityId() {
        return charityId;
    }

    public void setCharityId(String charityId) {
        this.charityId = charityId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

}

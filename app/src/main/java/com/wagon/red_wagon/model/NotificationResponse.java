package com.wagon.red_wagon.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class NotificationResponse implements Serializable {
    String driverId;
    String userId;
    String carPoolId;
    String driverName1;
    String carPoolName;

    String seats;
    String status;
    String meUserId;

    String senduserId;
    String joinFamily;
    String msg;
    String nulll;
    String imageurl;
    String childId, parentId,seat;

    public NotificationResponse(String driverId, String userId, String seat, String pickAddress, String requestId, String dropAddress, String type, String name, String carPoolId, String driverName1, String carPoolName, String imageurl) {
        this.driverId = driverId;
        this.userId = userId;
        this.seat = seat;
        this.type = type;
        this.pickAddress = pickAddress;
        this.dropAddress = dropAddress;
        this.requestId = requestId;
        this.name = name;
        this.carPoolId = carPoolId;
        this.driverName1 = driverName1;
        this.carPoolName = carPoolName;
        this.imageurl = imageurl;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    String requestStatus;

    public NotificationResponse(String s, String meUserId, String senduserId, String type, String msg, String nulll) {
        this.status = s;
        this.meUserId = meUserId;
        this.senduserId = senduserId;
        this.type = type;
        this.msg = msg;
        this.nulll = nulll;

    }

    public String getNulll() {
        return nulll;
    }

    public void setNulll(String nulll) {
        this.nulll = nulll;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMeUserId() {
        return meUserId;
    }

    public void setMeUserId(String meUserId) {
        this.meUserId = meUserId;
    }

    public String getSenduserId() {
        return senduserId;
    }

    public void setSenduserId(String senduserId) {
        this.senduserId = senduserId;
    }

    public String getJoinFamily() {
        return joinFamily;
    }

    public void setJoinFamily(String joinFamily) {
        this.joinFamily = joinFamily;
    }

    public LatLng getPickLtLng() {
        return pickLtLng;
    }

    public void setPickLtLng(LatLng pickLtLng) {
        this.pickLtLng = pickLtLng;
    }

    public LatLng getDropLtLng() {
        return dropLtLng;
    }

    public void setDropLtLng(LatLng dropLtLng) {
        this.dropLtLng = dropLtLng;
    }

    LatLng pickLtLng, dropLtLng;

    public NotificationResponse(String driverId, String userId, String seats, String pickAddress, String requestId, String dropAddress, String type, String name, String carPoolId, String driverName1, String carPoolName) {
        this.driverId = driverId;
        this.userId = userId;
        this.carPoolId = carPoolId;

        this.seats = seats;
        this.driverName1 = driverName1;
        this.pickAddress = pickAddress;
        this.requestId = requestId;
        this.dropAddress = dropAddress;
        this.type = type;
        this.name = name;
        this.carPoolName = carPoolName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NotificationResponse(String status, String type) {
        this.status = status;
        this.type = type;

    }


    public NotificationResponse(String status, String driverId, String userId, String type, String name) {
        this.driverId = driverId;
        this.userId = userId;
        this.type = type;
        this.name = name;
        this.status = status;
    }

    public NotificationResponse(String driverId, String carPoolId, String childId, String parentId, String msg, String type, String requestStatus) {
        this.driverId = driverId;
        this.carPoolId = carPoolId;
        this.type = type;
        this.msg = msg;
        this.childId = childId;
        this.parentId = parentId;
        this.requestStatus = requestStatus;
    }

    String pickAddress;
    String requestId;
    String dropAddress;
    String type;
    String name;

    public String getCarPoolName() {
        return carPoolName;
    }

    public void setCarPoolName(String carPoolName) {
        this.carPoolName = carPoolName;
    }

    public String getDriverName1() {
        return driverName1;
    }

    public void setDriverName1(String driverName1) {
        this.driverName1 = driverName1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarPoolId() {
        return carPoolId;
    }

    public void setCarPoolId(String carPoolId) {
        this.carPoolId = carPoolId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getPickAddress() {
        return pickAddress;
    }

    public void setPickAddress(String pickAddress) {
        this.pickAddress = pickAddress;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getDropAddress() {
        return dropAddress;
    }

    public void setDropAddress(String dropAddress) {
        this.dropAddress = dropAddress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}

package com.wagon.red_wagon.model;

public class FlagDetail {

    String flagName;
    int flagImage;

    public String getFlagName() {
        return flagName;
    }

    public void setFlagName(String flagName) {
        this.flagName = flagName;
    }

    public int getFlagImage() {
        return flagImage;
    }

    public void setFlagImage(int flagImage) {
        this.flagImage = flagImage;
    }

    public FlagDetail(String flagName, int flagImage) {
        this.flagName = flagName;
        this.flagImage = flagImage;

    }

}

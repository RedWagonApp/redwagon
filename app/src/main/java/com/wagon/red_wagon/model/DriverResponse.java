package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DriverResponse {

@SerializedName("users")
@Expose
private List<User> users = null;
@SerializedName("total")
@Expose
private Total total;

public List<User> getUsers() {
return users;
}

public void setUsers(List<User> users) {
this.users = users;
}

public Total getTotal() {
return total;
}

public void setTotal(Total total) {
this.total = total;
}

}
package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Body implements Serializable {

    @SerializedName("carPoolId")
    @Expose
    private String carPoolId;
    @SerializedName("percent")
    @Expose
    private String percent;

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    @SerializedName("chatId")
    @Expose
    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @SerializedName("childId")
    @Expose
    private String childId;
    @SerializedName("carName")
    @Expose
    private String carName;
    @SerializedName("freeSeat")
    @Expose
    private String freeSeat;
    @SerializedName("usedSeat")
    @Expose
    private Integer usedSeat;
    @SerializedName("seatAvailability")
    @Expose
    private Integer seatAvailability;

    @SerializedName("userId")
    @Expose
    private String userId;

    public String getCarPoolId() {
        return carPoolId;
    }

    public void setCarPoolId(String carPoolId) {
        this.carPoolId = carPoolId;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Integer getUsedSeat() {
        return usedSeat;
    }

    public void setUsedSeat(Integer usedSeat) {
        this.usedSeat = usedSeat;
    }

    public Integer getSeatAvailability() {
        return seatAvailability;
    }

    public void setSeatAvailability(Integer seatAvailability) {
        this.seatAvailability = seatAvailability;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("eventName")
    @Expose
    private String eventName;

    @SerializedName("profileimage")
    @Expose
    private String profileimage;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("parentName2")
    @Expose
    private String parentName2;
    @SerializedName("parentEmail2")
    @Expose
    private String parentEmail2;
    @SerializedName("parentMobile2")
    @Expose
    private String parentMobile2;
    @SerializedName("parentName3")
    @Expose
    private String parentName3;
    @SerializedName("carpoolId")
    @Expose
    private String carpoolId;

    @SerializedName("carpoolUserId")
    @Expose
    private String carpoolUserId;
    @SerializedName("managerpic")
    @Expose
    private String managerpic;

    public String getManagerpic() {
        return managerpic;
    }

    public void setManagerpic(String managerpic) {
        this.managerpic = managerpic;
    }

    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("pickLongitude")
    @Expose
    private String pickLongitude;

    @SerializedName("pickLatitude")
    @Expose
    private String pickLatitude;

    @SerializedName("dropLongitude")
    @Expose
    private String dropLongitude;

    public String getPickLongitude() {
        return pickLongitude;
    }

    public void setPickLongitude(String pickLongitude) {
        this.pickLongitude = pickLongitude;
    }

    public String getPickLatitude() {
        return pickLatitude;
    }

    public void setPickLatitude(String pickLatitude) {
        this.pickLatitude = pickLatitude;
    }

    public String getDropLongitude() {
        return dropLongitude;
    }

    public void setDropLongitude(String dropLongitude) {
        this.dropLongitude = dropLongitude;
    }

    public String getDropLatitude() {
        return dropLatitude;
    }

    public void setDropLatitude(String dropLatitude) {
        this.dropLatitude = dropLatitude;
    }

    @SerializedName("dropLatitude")
    @Expose
    private String dropLatitude;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("latitude")
    @Expose
    private String latitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCarpoolUserId() {
        return carpoolUserId;
    }

    public void setCarpoolUserId(String carpoolUserId) {
        this.carpoolUserId = carpoolUserId;
    }

    public String getCarpoolId() {
        return carpoolId;
    }

    public void setCarpoolId(String carpoolId) {
        this.carpoolId = carpoolId;
    }

    @SerializedName("parentEmail3")
    @Expose
    private String parentEmail3;


    @SerializedName("parentMobile3")
    @Expose
    private String parentMobile3;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @SerializedName("classname")
    @Expose
    private String classname;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("managerId")
    @Expose
    private String managerId;

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    @SerializedName("coachName")
    @Expose
    private String coachName;

    public String getCoachId() {
        return coachId;
    }

    public void setCoachId(String coachId) {
        this.coachId = coachId;
    }

    @SerializedName("coachId")
    @Expose
    private String coachId;

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    @SerializedName("managerName")
    @Expose
    private String managerName;

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("timeZone")
    @Expose
    private String timeZone;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @SerializedName("type")
    @Expose
    private String type;

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("Zipcode")
    @Expose
    private String zipcode;

    @SerializedName("zipCode")
    @Expose
    private String zipcodee;

    public String getZipcodee() {
        return zipcodee;
    }

    public void setZipcodee(String zipcodee) {
        this.zipcodee = zipcodee;
    }

    @SerializedName("zip")
    @Expose
    private String zip;

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @SerializedName("dateTime")
    @Expose
    private String dateTime;
    @SerializedName("playerImage")
    @Expose
    private String playerImage;


    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("weekDay")
    @Expose
    private String weekDay;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("modal")
    @Expose
    private String modal;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("regNumber")
    @Expose
    private String regNumber;
    @SerializedName("teamName")
    @Expose
    private String teamName;

    @SerializedName("sport")
    @Expose
    private String sport;

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    @SerializedName("opponent")
    @Expose
    private String opponent;

    public String getHomeAway() {
        return homeAway;
    }

    public void setHomeAway(String homeAway) {
        this.homeAway = homeAway;
    }

    @SerializedName("homeAway")
    @Expose
    private String homeAway;

    public String getFlagColor() {
        return flagColor;
    }

    public void setFlagColor(String flagColor) {
        this.flagColor = flagColor;
    }

    public String getParentName2() {
        return parentName2;
    }

    public void setParentName2(String parentName2) {
        this.parentName2 = parentName2;
    }

    public String getParentEmail2() {
        return parentEmail2;
    }

    public void setParentEmail2(String parentEmail2) {
        this.parentEmail2 = parentEmail2;
    }

    public String getParentMobile2() {
        return parentMobile2;
    }

    public void setParentMobile2(String parentMobile2) {
        this.parentMobile2 = parentMobile2;
    }

    public String getParentName3() {
        return parentName3;
    }

    public void setParentName3(String parentName3) {
        this.parentName3 = parentName3;
    }

    public String getParentEmail3() {
        return parentEmail3;
    }

    public void setParentEmail3(String parentEmail3) {
        this.parentEmail3 = parentEmail3;
    }

    public String getParentMobile3() {
        return parentMobile3;
    }

    public void setParentMobile3(String parentMobile3) {
        this.parentMobile3 = parentMobile3;
    }

    @SerializedName("flagColor")
    @Expose
    private String flagColor;

    public String getArriveEarly() {
        return arriveEarly;
    }

    public void setArriveEarly(String arriveEarly) {
        this.arriveEarly = arriveEarly;
    }

    @SerializedName("arriveEarly")
    @Expose
    private String arriveEarly;


    @SerializedName("studentName")
    @Expose
    private String studentName;

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    @SerializedName("cost")
    @Expose
    private String cost;

    @SerializedName("parentName")
    @Expose
    private String parentName;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("studentEmail")
    @Expose
    private String studentEmail;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("studentMobile")
    @Expose
    private String studentMobile;


    @SerializedName("parentEmail")
    @Expose
    private String parentEmail;

    @SerializedName("profileImage")
    @Expose
    private String profileImage;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("status")
    @Expose
    private String status;

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @SerializedName("schoolName")
    @Expose
    private String schoolName;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @SerializedName("firstName")
    @Expose
    private String firstName;


    @SerializedName("lastName")
    @Expose
    private String lastName;

    public String getLastName() {
        return lastName;
    }


    @SerializedName("jerseyNumber")
    @Expose
    private String jerseyNumber;


    @SerializedName("gender")
    @Expose
    private String gender;


    @SerializedName("position")
    @Expose
    private String position;


    @SerializedName("playerId")
    @Expose
    private String playerId;


    @SerializedName("teamId")
    @Expose
    private String teamId;

    @SerializedName("usersDetail")
    @Expose
    private UsersDetail usersDetail;


    @SerializedName("court")
    @Expose
    private String court;

    @SerializedName("classId")
    @Expose
    private String classId;


    @SerializedName("weeks")
    @Expose
    private List<String> weeks = null;

    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("eventImage")
    @Expose
    private String eventImage;

    @SerializedName("eventId")
    @Expose
    private String eventId;

    @SerializedName("driverId")
    @Expose
    private List<String> driverId = null;

    @SerializedName("driverName")
    @Expose
    private List<String> driverName = null;

    public List<String> getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(List<String> driverPhone) {
        this.driverPhone = driverPhone;
    }

    @SerializedName("driverPhone")
    @Expose
    private List<String> driverPhone = null;

    public List<String> getDriverImage() {
        return driverImage;
    }

    public void setDriverImage(List<String> driverImage) {
        this.driverImage = driverImage;
    }

    @SerializedName("driverImage")
    @Expose
    private List<String> driverImage = null;

    public List<String> getGenderArr() {
        return genderArr;
    }

    public void setGenderArr(List<String> genderArr) {
        this.genderArr = genderArr;
    }

    @SerializedName("genderArr")
    @Expose
    private List<String> genderArr = null;

    @SerializedName("carPoolName")
    @Expose
    private String carPoolName;
    @SerializedName("driName")
    @Expose
    private String driName;

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("pickDate")
    @Expose
    private String pickDate;

    @SerializedName("pickupStatus")
    @Expose
    private String pickupStatus;

    public String getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(String pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public String getPickDate() {
        return pickDate;
    }

    public void setPickDate(String pickDate) {
        this.pickDate = pickDate;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public List<String> getDriverName() {
        return driverName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(String jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public String getPosition() {
        return position;
    }

    public String getTeamId() {
        return teamId;
    }

    public String getEventId() {
        return eventId;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getParentMobile() {
        return parentMobile;
    }

    public void setParentMobile(String parentMobile) {
        this.parentMobile = parentMobile;
    }

    @SerializedName("parentMobile")
    @Expose
    private String parentMobile;

    public String getType_img() {
        return type_img;
    }

    public void setType_img(String type_img) {
        this.type_img = type_img;
    }

    @SerializedName("type_img")
    @Expose
    private String type_img;

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    @SerializedName("dateRange")
    @Expose
    private String dateRange;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentMobile() {
        return studentMobile;
    }

    public void setStudentMobile(String studentMobile) {
        this.studentMobile = studentMobile;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setDriverName(List<String> driverName) {
        this.driverName = driverName;
    }

    public String getEventImage() {
        return eventImage;
    }

    public String getPlayerImage() {
        return playerImage;
    }

    public void setPlayerImage(String playerImage) {
        this.playerImage = playerImage;
    }

    public String getdatetime() {
        return datetime;
    }

    public void setdatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }


    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }


    public UsersDetail getUsersDetail() {
        return usersDetail;
    }

    public void setUsersDetail(UsersDetail usersDetail) {
        this.usersDetail = usersDetail;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCarPoolName() {
        return carPoolName;
    }

    public void setCarPoolName(String carPoolName) {
        this.carPoolName = carPoolName;
    }

    public List<String> getDriverId() {
        return driverId;
    }

    public void setDriverId(List<String> driverId) {
        this.driverId = driverId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }


    public String getCourt() {
        return court;
    }

    public void setCourt(String court) {
        this.court = court;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public List<String> getWeeks() {
        return weeks;
    }

    public void setWeeks(List<String> weeks) {
        this.weeks = weeks;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModal() {
        return modal;
    }

    public void setModal(String modal) {
        this.modal = modal;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getFreeSeat() {
        return freeSeat;
    }

    public void setFreeSeat(String freeSeat) {
        this.freeSeat = freeSeat;
    }

}
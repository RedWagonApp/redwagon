package com.wagon.red_wagon.model;

public class School_Model {
    String schoolname,id;
    public School_Model(String schoolname, String id) {
        this.id=id;
        this.schoolname=schoolname;
    }

    public String getSchoolname() {
        return schoolname;
    }

    public void setSchoolname(String schoolname) {
        this.schoolname = schoolname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

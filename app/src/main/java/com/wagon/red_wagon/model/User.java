package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

@SerializedName("chat_id")
@Expose
private Integer chatId;
@SerializedName("block_status")
@Expose
private Integer blockStatus;
@SerializedName("name")
@Expose
private String name;
@SerializedName("status")
@Expose
private Integer status;
@SerializedName("latitude")
@Expose
private String latitude;
@SerializedName("longitude")
@Expose
private String longitude;
@SerializedName("location")
@Expose
private String location;
@SerializedName("sharingLocation")
@Expose
private Integer sharingLocation;
@SerializedName("profileImage")
@Expose
private String profileImage;
@SerializedName("time")
@Expose
private Integer time;
@SerializedName("id")
@Expose
private Integer id;

public Integer getChatId() {
return chatId;
}

public void setChatId(Integer chatId) {
this.chatId = chatId;
}

public Integer getBlockStatus() {
return blockStatus;
}

public void setBlockStatus(Integer blockStatus) {
this.blockStatus = blockStatus;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public String getLatitude() {
return latitude;
}

public void setLatitude(String latitude) {
this.latitude = latitude;
}

public String getLongitude() {
return longitude;
}

public void setLongitude(String longitude) {
this.longitude = longitude;
}

public String getLocation() {
return location;
}

public void setLocation(String location) {
this.location = location;
}

public Integer getSharingLocation() {
return sharingLocation;
}

public void setSharingLocation(Integer sharingLocation) {
this.sharingLocation = sharingLocation;
}

public String getProfileImage() {
return profileImage;
}

public void setProfileImage(String profileImage) {
this.profileImage = profileImage;
}

public Integer getTime() {
return time;
}

public void setTime(Integer time) {
this.time = time;
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

}
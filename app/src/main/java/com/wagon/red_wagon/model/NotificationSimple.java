package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationSimple {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("body")
    @Expose
    private String body;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

package com.wagon.red_wagon.model;

public class UserMessage {
    String message;

    public UserMessage(String message) {
        this.message = message;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ManagerModel {

@SerializedName("message")
@Expose
private MessageM message;

public MessageM getMessage() {
return message;
}

public void setMessage(MessageM message) {
this.message = message;
}

}
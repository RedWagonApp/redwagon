package com.wagon.red_wagon.model;

public class Model_CarList  {
    private String make, modal, color, regNumber, freeSeat, id, status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Model_CarList(String make, String modal, String color, String regNumber, String freeSeat, String id, String status) {
     this.make=make;
     this.modal=modal;
     this.color=color;
     this.regNumber=regNumber;
     this.freeSeat=freeSeat;
     this.id=id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModal() {
        return modal;
    }

    public void setModal(String modal) {
        this.modal = modal;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getFreeSeat() {
        return freeSeat;
    }

    public void setFreeSeat(String freeSeat) {
        this.freeSeat = freeSeat;
    }
}

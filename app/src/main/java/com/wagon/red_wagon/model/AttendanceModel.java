package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttendanceModel {

@SerializedName("message")
@Expose
private String message;
@SerializedName("body")
@Expose
private List<AttendanceBody> body = null;

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public List<AttendanceBody> getBody() {
return body;
}

public void setBody(List<AttendanceBody> body) {
this.body = body;
}

}
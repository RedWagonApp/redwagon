package com.wagon.red_wagon.model;



import java.io.Serializable;
import java.util.List;

public  class TableList implements Serializable {
    private List<RowHeader> rowHeaderList;
    private List<ColumnHeader> columnHeaderList;
    private List<List<Cell>> cellList;

    public List<RowHeader> getRowHeaderList() {
        return rowHeaderList;
    }

    public void setRowHeaderList(List<RowHeader> rowHeaderList) {
        this.rowHeaderList = rowHeaderList;
    }

    public List<ColumnHeader> getColumnHeaderList() {
        return columnHeaderList;
    }

    public void setColumnHeaderList(List<ColumnHeader> columnHeaderList) {
        this.columnHeaderList = columnHeaderList;
    }

    public List<List<Cell>> getCellList() {
        return cellList;
    }

    public void setCellList(List<List<Cell>> cellList) {
        this.cellList = cellList;
    }
}
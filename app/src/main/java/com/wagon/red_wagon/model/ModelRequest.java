package com.wagon.red_wagon.model;

public class ModelRequest {
    double latitude, longitude;
    String profileImage, name, cost, seats, minutes,driverId;
//    public ModelRequest(double latitude, double longitude, String profileImage, String name) {
//        this.latitude=latitude;
//        this.longitude=longitude;
//        this.profileImage=profileImage;
//        this.name=name;
//
//    }
//
//    public ModelRequest(double latitude, double longitude, String profileImage, String name, String cost, String seats, String minutes) {
//
//
//    }

    public ModelRequest(double latitude, double longitude, String profileImage, String name, String driverId, String cost, String seats) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.profileImage = profileImage;
        this.name = name;
        this.cost = cost;
        this.seats = seats;
        this.minutes = minutes;
        this.driverId = driverId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.wagon.red_wagon.model;

public class ModelUserChat {
    String message, profile_image, name, User2id, chat_id,room,userid;

    public ModelUserChat( String message, String profile_image, String name, String otheruser, String chat_id,String room,String userid) {

//    public ModelUserChat(String User2id, String message, String profile_image, String name) {
        this.message = message;
        this.profile_image = profile_image;
        this.name = name;
        this.User2id = otheruser;
        this.chat_id = chat_id;
        this.room = room;
        this.userid = userid;
//    }
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getMessage() {
        return message;
    }

    public String getUser2id() {
        return User2id;
    }

    public void setUser2id(String user2id) {
        User2id = user2id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

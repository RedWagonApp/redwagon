package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RideModel implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("body")
    @Expose
    private RideBody body;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RideBody getBody() {
        return body;
    }

    public void setBody(RideBody body) {
        this.body = body;
    }

}
package com.wagon.red_wagon.model;

public class ChatAppMsgDTO {

    public final static int MSG_TYPE_RECEIVED = 0;
    public final static int MSG_TYPE_SENT =1;

    public final static int MSG_TYPE_IMAGE_SEND = 2;
    public final static int MSG_TYPE_IMAGE_RECIEVED =3;

    // Message content.
    private String msgContent;

    // Message type.
    private int msgType;
    private String mediaConetent;
    private String name;
    public ChatAppMsgDTO(int msgType, String msgContent, String mediaConetent,String name) {
        this.msgType = msgType;
        this.msgContent = msgContent;
        this.mediaConetent = mediaConetent;
        this.name = name;
    }
    public String getMediaConetent() {
        return mediaConetent;
    }

    public void setMediaConetent(String mediaConetent) {
        this.mediaConetent = mediaConetent;
    }



    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModel {

@SerializedName("message")
@Expose
private String message;
@SerializedName("body")
@Expose
private List<NotificationBody> body = null;

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public List<NotificationBody> getBody() {
return body;
}

public void setBody(List<NotificationBody> body) {
this.body = body;
}

}
package com.wagon.red_wagon.model;

import java.util.ArrayList;

public class DriverModel {
    String driverName;
    String pickupDate;

    String driverId, status, carPoolId,getEventName;

    String profileImg;

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    String driverPhone;
    String type;
    ArrayList<String> weekDays = new ArrayList<>();

    public String getCarPoolId() {
        return carPoolId;
    }

    public void setCarPoolId(String carPoolId) {
        this.carPoolId = carPoolId;
    }

    public DriverModel(String carPoolId, String driverId, String driverName, ArrayList<String> weekDays, String profileImg, String status, String pickupDate, String type, String driverPhone,String getEventName) {
        this.driverName = driverName;
        this.pickupDate = pickupDate;
        this.driverId = driverId;
        this.weekDays = weekDays;
        this.profileImg = profileImg;
        this.type = type;
        this.status = status;
        this.carPoolId = carPoolId;
        this.driverPhone = driverPhone;
        this.getEventName = getEventName;
    }

    public String getGetEventName() {
        return getEventName;
    }

    public void setGetEventName(String getEventName) {
        this.getEventName = getEventName;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }


    public ArrayList<String> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(ArrayList<String> weekDays) {
        this.weekDays = weekDays;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }


    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }


}

package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScheduleModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("body")
    @Expose
    private List<RideBody> body = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RideBody> getBody() {
        return body;
    }

    public void setBody(List<RideBody> body) {
        this.body = body;
    }

}
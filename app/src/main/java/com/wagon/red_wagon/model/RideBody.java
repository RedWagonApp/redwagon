package com.wagon.red_wagon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RideBody implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userid")
    @Expose
    private Integer userId;
    @SerializedName("driverId")
    @Expose
    private Integer driverId;
    @SerializedName("pickLongitude")
    @Expose
    private String pickLongitude;
    @SerializedName("pickLatitude")
    @Expose
    private String pickLatitude;
    @SerializedName("dropLongitude")
    @Expose
    private String dropLongitude;
    @SerializedName("dropLatitude")
    @Expose
    private String dropLatitude;
    @SerializedName("eventId")
    @Expose
    private Integer eventId;
    @SerializedName("pickDate")
    @Expose
    private List<String> pickDate = null;
    @SerializedName("pickupStatus")
    @Expose
    private String pickupStatus;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("eventName")
    @Expose
    private String eventName;

    @SerializedName("weekDay")
    @Expose
    private String weekDay = null;
    @SerializedName("driName")
    @Expose
    private String driName;

    @SerializedName("dateTime")
    @Expose
    private String dateTime;
    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;


    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }


    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }


    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public List<String> getPickDate() {
        return pickDate;
    }

    public void setPickDate(List<String> pickDate) {
        this.pickDate = pickDate;
    }

    public String getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(String pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getPickLongitude() {
        return pickLongitude;
    }

    public void setPickLongitude(String pickLongitude) {
        this.pickLongitude = pickLongitude;
    }

    public String getPickLatitude() {
        return pickLatitude;
    }

    public void setPickLatitude(String pickLatitude) {
        this.pickLatitude = pickLatitude;
    }

    public String getDropLongitude() {
        return dropLongitude;
    }

    public void setDropLongitude(String dropLongitude) {
        this.dropLongitude = dropLongitude;
    }

    public String getDropLatitude() {
        return dropLatitude;
    }

    public void setDropLatitude(String dropLatitude) {
        this.dropLatitude = dropLatitude;
    }
}

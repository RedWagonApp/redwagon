package com.wagon.red_wagon.model;

public class Event_List_model {
    String name,dateTime,address,endDate;
    public Event_List_model(String name, String dateTime, String address, String endDate) {
        this.address=address;
        this.name=name;
        this.dateTime=dateTime;
        this.endDate=endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}

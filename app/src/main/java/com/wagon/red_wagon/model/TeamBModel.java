package com.wagon.red_wagon.model;

public class TeamBModel {
    String teamName, teamId;

    public TeamBModel(String teamName, String teamId) {
     this.teamName=teamName;
 this.teamId=teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}

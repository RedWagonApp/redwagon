package com.wagon.red_wagon.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.google.gson.Gson;
import com.wagon.red_wagon.model.Body;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Constans {
    public static String BASEURL = "http://132.148.166.74:3000/";

    //    //    http://64.202.185.51:3000/api/managerSignUpNew Old URl
    public static String getDisplayName = "";
    public static String getEmail = "";
    public static String getFbGoogleId = "";
    public static Uri uri;
    public static String getPhoneNumber = "";
    public static String Token = "";
    public static String PicUrl = "";
    public static String TeamId = "0";
    public static String Chat_Family = "";
    public static String MangerId = "0";
    public static int userin;

    public static String clubOrgranization_get = "";
    public static String FMID = "";
    public static String userFind = "";
    public static String location = "";
    public static String SchId = "";
    public static double lat;
    public static double longg;
    public static String child_name = "";
    public static String fromType = "";


    public static String getlatitude(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("latitude", "");
    }

    public static String getlongitude(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("longitude", "");
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("Token", "");
    }

    public static String getProfilePic(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("PicUrl", "");
    }

    public static String getUserName(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("userName", "");
    }

    public static String getFamilyId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("FMID", "");
    }

    public static String getCharityId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("charityId", "");
    }

    public static String getphoneNumber(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("phoneNumber", "");
    }

    public static String getLoginType(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("userr", "");
    }

    public static String FCMToken(Activity context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("FCMToken", "");
    }

    public static String getSchlID(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPref.getString("SchlId", "");
    }

    public static Body getprofileData(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPref.getString("profileData", "");
        Body obj = gson.fromJson(json, Body.class);
        return obj;
    }

    public static void clearSp(Activity context) {
        clearNotification(context);
        SharedPreferences.Editor editor = context.getSharedPreferences("Login", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();

    }

    public static void clearNotification(Activity context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
}

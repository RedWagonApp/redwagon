package com.wagon.red_wagon.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.NotificationResponse;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;

import java.io.Serializable;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    static NotificationManager notificationManager;
    private int notificationId = 1;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
// [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
// [START_EXCLUDE]
// There are two types of messages data messages and notification messages. Data messages
// are handled
// here in onMessageReceived whether the app is in the foreground or background. Data
// messages are the type
// traditionally used with GCM. Notification messages are only received here in
// onMessageReceived when the app
// is in the foreground. When the app is in the background an automatically generated
// notification is displayed.
// When the user taps on the notification they are returned to the app. Messages
// containing both notification
// and data payloads are treated as notification messages. The Firebase console always
// sends notification
// messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
// [END_EXCLUDE]

// TODO(developer): Handle FCM messages here.
// Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

// Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData().get("requestId"));

            if (true) {
// For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob();
            } else {
// Handle message within 10 seconds
                handleNow();
            }

        }

// Check if message contains a notification payload.


        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getData());
            if (remoteMessage.getData().get("type") != null) {
                if (remoteMessage.getData().get("type").equals("Request")) {
                    String requestId = remoteMessage.getData().get("requestId");
                    String name = remoteMessage.getData().get("name");
                    String pickAddress = remoteMessage.getData().get("pickAddress");
                    String dropAddress = remoteMessage.getData().get("dropAddress");
                    String seat = remoteMessage.getData().get("seat");
                    String userId = remoteMessage.getData().get("userId");
                    String driverId = remoteMessage.getData().get("driverId");
                    String carPoolId = remoteMessage.getData().get("carPoolId");
                    String type = remoteMessage.getData().get("type");
                    String driverName1 = remoteMessage.getData().get("driverName1");
                    String carPoolName = remoteMessage.getData().get("carPoolName");
                    String imageurl = remoteMessage.getData().get("userPic");
                    double pickLat = Double.parseDouble(remoteMessage.getData().get("pickLatitude"));
                    double pickLong = Double.parseDouble(remoteMessage.getData().get("pickLongitude"));
                    double dropLat = Double.parseDouble(remoteMessage.getData().get("dropLatitude"));
                    double dropLong = Double.parseDouble(remoteMessage.getData().get("dropLongitude"));
                    LatLng pickLtLtng = new LatLng(pickLat, pickLong);
                    LatLng dropLtLtng = new LatLng(dropLat, dropLong);

                    NotificationResponse notificationResponse = new NotificationResponse(driverId, userId, seat, pickAddress, requestId, dropAddress, type, name, carPoolId, driverName1, carPoolName,imageurl);
                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                } else if (remoteMessage.getData().get("type").equals("ride status")) {
                    if ((remoteMessage.getData().get("statusrequest").equals("1"))) {
                        String statusrequest = String.valueOf(remoteMessage.getData().get("statusrequest"));
                        String type = remoteMessage.getData().get("type");
                        NotificationResponse notificationResponse = new NotificationResponse(statusrequest, type);
                        sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                    }
                } else if (remoteMessage.getData().get("type").equals("completed")) {
                    String type = remoteMessage.getData().get("type");
                    String driverIdd = remoteMessage.getData().get("driverId");
                    String userIdd = remoteMessage.getData().get("userId");
                    String username = remoteMessage.getData().get("name");

                    NotificationResponse notificationResponse = new NotificationResponse("1", driverIdd, userIdd, "completed", username);
                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                    Log.e(TAG, "onMessageReceived: " + type);


                } else if (remoteMessage.getData().get("type").equals("joinFamily")) {
                    String type = remoteMessage.getData().get("type");
                    String senduserId = remoteMessage.getData().get("userId1");
                    String meUserId = remoteMessage.getData().get("userId2");
                    String msg = remoteMessage.getData().get("msg");

                    NotificationResponse notificationResponse = new NotificationResponse("1", meUserId, senduserId, "joinFamily", msg, "null");
                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                    Log.e(TAG, "onMessageReceived: " + type);


                } else if (remoteMessage.getData().get("type").equals("addFamily")) {
                    String msg = remoteMessage.getData().get("msg");
                    String type = remoteMessage.getData().get("type");
                    NotificationResponse notificationResponse = new NotificationResponse("1", "", "", type, msg, "null");
                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                } else if (remoteMessage.getData().get("type").equals("joinCarPool")) {
                    String msg = remoteMessage.getData().get("msg");
                    String type = String.valueOf(remoteMessage.getData().get("type"));
                    String carPoolId = String.valueOf(remoteMessage.getData().get("carPoolId"));
                    String driverId = String.valueOf(remoteMessage.getData().get("driverId"));
                    String childId = String.valueOf(remoteMessage.getData().get("childId"));
                    String parentId = remoteMessage.getData().get("parentId");
                    String requestStatus = remoteMessage.getData().get("requestStatus");
                    NotificationResponse notificationResponse = new NotificationResponse(driverId, carPoolId, childId, parentId, msg, type, requestStatus);
                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                } else if (remoteMessage.getData().get("type").equals("join")) {
                    String msg = remoteMessage.getData().get("msg");
                    String type = String.valueOf(remoteMessage.getData().get("type"));
                    String carPoolId = String.valueOf(remoteMessage.getData().get("carPoolId"));
                    String driverId = String.valueOf(remoteMessage.getData().get("driverId"));
                    String driverName = String.valueOf(remoteMessage.getData().get("driverName1"));
                    String carPoolName = remoteMessage.getData().get("carPoolName");
                    String userId = remoteMessage.getData().get("userId");
                    String chatId = remoteMessage.getData().get("chatId");
                    NotificationResponse notificationResponse = new NotificationResponse(driverId, userId, "", "", chatId, "", type, msg, carPoolId, driverName, carPoolName);

                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                } else if (remoteMessage.getData().get("type").equals("addJoinCarPool")) {
                    String msg = remoteMessage.getData().get("msg");
                    String type = remoteMessage.getData().get("type");
                    NotificationResponse notificationResponse = new NotificationResponse("1", "", "", type, msg, "null");
                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                } else if(remoteMessage.getData().get("type").equals("ride start")) {

                }else if(remoteMessage.getData().get("type").equals("Chat")) {
                    String type = remoteMessage.getData().get("type");
                    String msg = remoteMessage.getData().get("msg");
                    String userId = remoteMessage.getData().get("userId");
                    Log.e(TAG, "onMessageReceived: "+userId );
                    NotificationResponse notificationResponse = new NotificationResponse("1", userId, "", type, msg, "null");


                    sendNotification(remoteMessage.getNotification().getBody(), type, notificationResponse);
                }

                else{
                    sendNotification(remoteMessage.getNotification().getBody(), "", new NotificationResponse("1", ""));

                }
            }


        }
    }


    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

// If you want to send messages to this application instance or
// manage this apps subscriptions on the server side, send the
// Instance ID token to your app server.
        sendRegistrationToServer(token);
    }
// [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {
// [START dispatch_job]
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
                .build();
        WorkManager.getInstance().beginWith(work).enqueue();
// [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
// TODO: Implement this method to send token to your app server.
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String type, NotificationResponse data) {
        Intent intent = new Intent(this, FamilyDashboardActivity.class);

        intent.putExtra("data", (Serializable) data);
        intent.putExtra("type", type);
        intent.putExtra("check", "1");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(messageBody))

                        .setContentIntent(pendingIntent);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

// Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(notificationId++, notificationBuilder.build());
    }


}
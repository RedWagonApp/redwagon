package com.wagon.red_wagon.utils;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        String status = NetworkUtil.getConnectivityStatusString(context);

        if (status.equals("Not connected to Internet")) {
            //Toast.makeText(context, status, Toast.LENGTH_LONG).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Connection Failed");
            builder.setMessage("There may be a problem in your internet connection. Please try again.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                           dialog.dismiss();

                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }

    }


}
package com.wagon.red_wagon.utils;

import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.AddedChildModel;
import com.wagon.red_wagon.model.AttendanceModel;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.model.NotificationModel;
import com.wagon.red_wagon.model.NotificationSimple;
import com.wagon.red_wagon.model.PreferDriverModel;
import com.wagon.red_wagon.model.ProfieModel;
import com.wagon.red_wagon.model.RideModel;
import com.wagon.red_wagon.model.ScheduleModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface APIService {

    @POST("api/addCharity")
    @Multipart
    Call<AddEvent> AddChirty(
            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );


    @Multipart
    @POST("api/addchildNew")
    Call<AddEvent> ADD_CHILD_CALL(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );


    @POST("api/GetChildListNew")
    @FormUrlEncoded
    Call<GetEvent> GetChildListNew(
            @Field("fmlyId") String id
    );

    @POST("api/GetChildListAmount")
    @FormUrlEncoded
    Call<GetEvent> GetChildListAmount(
            @Field("fmlyId") String id);

    @POST("api/getJoinCarPoolChild")
    @FormUrlEncoded
    Call<AddedChildModel> getJoinCarPoolChild(@Field("driverId") String driverId);

    @POST("api/oneWaylatlog")
    @FormUrlEncoded
    Call<RideModel> oneWaylatlog(
            @Field("driverId") String id
    );

    @POST("api/rideComplete")
    @FormUrlEncoded
    Call<AddEvent> rideComplete(
            @Field("userId") String userId,
            @Field("driverId") String driverId,
            @Field("requestId") String requestId,
            @Field("requestStatus") String requestStatus

    );

    @POST("api/carPoolRideComplete")
    @FormUrlEncoded
    Call<AddEvent> carPoolRideComplete(
            @Field("driverId") String driverId,
            @Field("childId") String childId
    );

    @POST("api/addDriverPoolList")
    @FormUrlEncoded
    Call<AddEvent> addDriverPoolList(
            @Field("userId") String userId,
            @Field("carPoolId") String carPoolId,
            @Field("driverId") String driverId,
            @Field("requestStatus") String requestStatus,
            @Field("carPoolName") String carPoolName,
            @Field("driverName1") String driverName1,
            @Field("chatId") String chatId
    );

    @POST("api/addDriverPoolListRequest")
    @FormUrlEncoded
    Call<AddEvent> addDriverPoolListRequest(
            @Field("userId") String userId,
            @Field("carPoolId") String carPoolId,
            @Field("driverId") String driverId,
            @Field("carPoolName") String carPoolName,
            @Field("driverName1") String driverName1,
            @Field("chatId") String chatId);


    @POST("api/carDetailsUpdateStatus")
    @FormUrlEncoded
    Call<AddEvent> carDetailsUpdateStatus(
            @Field("id") String id,
            @Field("userId") String userId,
            @Field("status") String status
    );

    @POST("api/SearchTeams")
    @FormUrlEncoded
    Call<GetEvent> SearchTeams(
            @Field("userId") String id
    );

    @POST("api/usersDriverPreferenceList")
    @FormUrlEncoded
    Call<PreferDriverModel> usersDriverPreferenceList(
            @Field("userId") String id
    );

    @Multipart
    @POST("api/addSchoolEvent")
    Call<AddEvent> addSchoolEvent(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/eventImage")
    Call<AddEvent> eventImage(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part[] photo

    );

    @Multipart
    @POST("api/updateEvent")
    Call<AddEvent> updateEvent(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/addGameEvent")
    Call<AddEvent> addGameEvent(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/addTeam")
    Call<AddEvent> addTeam(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/addTeam")
    Call<AddEvent> addTeam(

            @PartMap Map<String, RequestBody>
                    map

    );

    @Multipart

    @POST("api/managerSignUpNew")
    Call<AddEvent> addmanger(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );


    // params.put("firstName", manager_name_get);
    //                params.put("email", manager_email_get);
//                params.put("phoneNumber", manager_mobile_get);
//                params.put("schId",Constans.getFamilyId(getActivity()));
//                params.put("gender", "Manager");
    @Multipart
    @POST("api/updateTeamDetails")
    Call<AddEvent> updateTeamDetails(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/updateTeamDetails")
    Call<AddEvent> updateTeamDetails(

            @PartMap Map<String, RequestBody>
                    map

    );

    @Multipart
    @POST("api/addTeamPlayer")
    Call<AddEvent> addTeamPlayer(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/updatePlayerDetails")
    Call<AddEvent> updatePlayerDetails(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/updatePlayerDetails")
    Call<AddEvent> updatePlayerDetails(

            @PartMap Map<String, RequestBody>
                    map

    );

    @Multipart
    @POST("api/addStudent")
    Call<AddEvent> addStudent(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/updateProfileStudent")
    Call<AddEvent> updateProfileStudent(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/updateManagerDetails")
    Call<AddEvent> updateProfileManager(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/addClass")
    Call<AddEvent> addClass(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );


    @POST("api/editCarList")
    @FormUrlEncoded
    Call<AddEvent> getCarDetail(@Field("id") String id);

    @POST("api/addFamilyRequest")
    @FormUrlEncoded
    Call<AddEvent> addFamilyRequest(
            @Field("userId") String userId,
            @Field("name") String username,
            @Field("email") String email);

    @POST("api/addFamilyRequestAccept")
    @FormUrlEncoded
    Call<AddEvent> addFamilyRequestAccept(
            @Field("userId1") String userId1,
            @Field("userId2") String userId2,
            @Field("requestStatus") String requestStatus);

    @POST("api/requestRide")
    @FormUrlEncoded
    Call<AddEvent> requestRide(@Field("userId") String id,
                               @Field("driverId") String driverId,
                               @Field("pickAddress") String pickAddress,
                               @Field("dropAddress") String dropAddress,
                               @Field("pickLongitude") String pickLongitude,
                               @Field("pickLatitude") String pickLatitude,
                               @Field("dropLongitude") String dropLongitude,
                               @Field("dropLatitude") String dropLatitude,
                               @Field("noOfSeat") String noOfSeat,
                               @Field("note") String note);

    @POST("api/signUp")
    @FormUrlEncoded
    Call<AddEvent> signUp(@Field("email") String email,
                          @Field("password") String password,
                          @Field("phoneNumber") String phoneNumber,
                          @Field("firstName") String firstName,
                          @Field("userName") String userName,
                          @Field("gender") String gender,
                          @Field("fbGoogleId") String fbGoogleId
    );

    @POST("api/getSortSchool")
    @FormUrlEncoded
    Call<GetEvent> getSortList(@Field("firstName") String id);

    @POST("api/GetChildFamilyList")
    @FormUrlEncoded
    Call<GetEvent> GetChildFamilyList(@Field("fmlyId") String fmlyId);


    @POST("api/getTypeEvent")
    @FormUrlEncoded
    Call<GetEvent> getEventDetail(@Field("userId") String userid,
                                  @Field("type") String type);

    @POST("api/update_latlong_users")
    @FormUrlEncoded
    Call<AddEvent> update_latlong(@Field("userId") String userid,
                                  @Field("longitude") String longitude,
                                  @Field("latitude") String latitude);

    @POST("api/groupCarpoolChat")
    @FormUrlEncoded
    Call<AddEvent> groupCarpoolChat(@Field("driverId") String driverId,
                                    @Field("carPoolName") String carPoolName,
                                    @Field("carPoolId") String carPoolId
    );

    @GET("api/getFamily")
    Call<GetEvent> getFamily();

    @GET("api/getEventList")
    Call<GetEvent> getEventList();

    @POST("api/getManager")
    @FormUrlEncoded
    Call<GetEvent> getManager(@Field("schId") String teamId);

    @POST("api/getDriverPoolList")
    @FormUrlEncoded
    Call<GetEvent> getDriverPoolList(@Field("eventId") String eventId);

    @POST("api/accepetReject")
    @FormUrlEncoded
    Call<AddEvent> accepetReject(@Field("userId") String teamId,
                                 @Field("driverId") String driverId,
                                 @Field("requestId") String requestId,
                                 @Field("requestStatus") String requestStatus);


    @POST("api/getAllPlayer")
    @FormUrlEncoded
    Call<GetEvent> getAllPlayer(@Field("schId") String userid);

    @POST("api/getAllTeamPlayerZero")
    @FormUrlEncoded
    Call<GetEvent> getAllTeamPlayerZero(@Field("schId") String userid);

    @POST("api/getTeam")
    @FormUrlEncoded
    Call<GetEvent> getTeamList(@Field("userId") String userid);

    @POST("api/getSortTeam")
    @FormUrlEncoded
    Call<GetEvent> getSortTeamList(@Field("userId") String userid);

    @POST("api/getTeamPlayer")
    @FormUrlEncoded
    Call<GetEvent> getTeamPlayer(@Field("teamId") String teamId);

    @GET("api/getUserProfile")
    Call<ProfieModel> getUserProfile();

    @POST("api/getManager")
    @FormUrlEncoded
    Call<GetEvent> getCoachList(@Field("schId") String schId);

    @POST("api/getAllManager")
    @FormUrlEncoded
    Call<GetEvent> getAllCoachList(@Field("schId") String schId);

    @POST("api/schoolDetails")
    @FormUrlEncoded
    Call<AddEvent> getschoolprofile(@Field("userId") String schId);

    @POST("api/logout")
    @FormUrlEncoded
    Call<AddEvent> logout(@Field("id") String id);

//    @GET("api/getManager")
//    Call<GetEvent> getCoachList();

    @POST("api/updateProfileImage")
    @Multipart
    Call<AddEvent> prolieUplod(
            @Part MultipartBody.Part photo

    );


    @POST("api/addRosterManually")
    @FormUrlEncoded
    Call<AddEvent> addRosterManually(

            @Field("teamId") String userId,
            @Field("firstName") String firstName,
            @Field("email") String email,
            @Field("phoneNumber") String phoneNumber,
            @Field("schId") String schId

    );

    @POST("api/teamAttendance")
    @FormUrlEncoded
    Call<AttendanceModel> teamAttendance(

            @Field("teamId") String teamId,
            @Field("playerId") String playerId,
            @Field("eventId") String eventId,
            @Field("attendanceStatus") String attendanceStatus,
            @Field("dateRange") String date

    );

    @POST("api/getAttendanceTeam")
    @FormUrlEncoded
    Call<AttendanceModel> getAttendanceTeam(
            @Field("teamId") String teamId,
            @Field("eventId") String eventId,
            @Field("dateRange") String date

    );

    @POST("api/getClass")
    @FormUrlEncoded
    Call<GetEvent> getclassList(@Field("userId") String userId);

    @POST("api/getTeamDetails")
    @FormUrlEncoded
    Call<AddEvent> getTeamDetails(@Field("id") String id);

    @POST("api/GetGroupUsers")
    @FormUrlEncoded
    Call<GetEvent> GetGroupUsers(@Field("roomId") String roomId);

    @POST("api/getPlayerDetails")
    @FormUrlEncoded
    Call<AddEvent> getPlayerDetails(@Field("id") String id);

    @POST("api/getClassDetails")
    @FormUrlEncoded
    Call<AddEvent> getClassDetails(@Field("id") String id);

    @POST("api/getEventDetails")
    @FormUrlEncoded
    Call<AddEvent> getEventDetails(@Field("id") String id);

    @POST("api/getStudentDetails")
    @FormUrlEncoded
    Call<AddEvent> getStudentDetails(@Field("id") String id);

    @POST("api/getStudentDetails")
    @FormUrlEncoded
    Call<AddEvent> getManagerDetails(@Field("userId") String id);

    @Multipart
    @POST("api/updateClass")
    Call<AddEvent> updateClassImage(

            @PartMap Map<String, RequestBody>
                    map,
            @Part MultipartBody.Part photo

    );

    @Multipart
    @POST("api/updateClass")
    Call<AddEvent> updateClass(

            @PartMap Map<String, RequestBody>
                    map);

    @DELETE("api/deleteStudent/{id}")
    Call<AddEvent> delteStudent(@Path("id") String itemId);

    @DELETE("api/deleteClass/{id}")
    Call<AddEvent> deleteClass(@Path("id") String itemId);

    @DELETE("api/deleteEvent/{id}")
    Call<AddEvent> delteItem(@Path("id") String itemId);

    @DELETE("api/deleteManager/{id}")
    Call<AddEvent> deleteManager(@Path("id") String itemId);

    @DELETE("api/deletePlayer/{id}")
    Call<AddEvent> deletePlayer(@Path("id") String Id);

    @DELETE("api/deleteTeam/{id}")
    Call<AddEvent> deleteTeam(@Path("id") String Id);


    @DELETE("api/deleteJoinCarPoolChild/{id}")
    Call<AddEvent> deleteJoinCarPoolChild(@Path("id") String Id);

    @DELETE("api/car/{id}/{userId}")
    Call<AddEvent> deletecar(@Path("id") String Id, @Path("userId") String userId);


    @POST("api/teamPlayerList")
    @FormUrlEncoded
    Call<GetEvent> getTeamPlyer(@Field("userId") String userId);

    @POST("api/getInvoice")
    @FormUrlEncoded
    Call<GetEvent> getInvoice(@Field("userId") String userid);

    @POST("api/invoiceEmail")
    @FormUrlEncoded
    Call<AddEvent> invoiceEmail(@Field("userId") String userId,
                                @Field("email") String email,
                                @Field("amount") String amount,
                                @Field("dueDate") String dueDate,
                                @Field("invoiceTitle") String invoiceTitle,
                                @Field("name") String name);


    @POST("api/addRoster")
    @FormUrlEncoded
    Call<GetEvent> addRoster(@Field("teamId") String teamId,
                             @Field("playerId") String playerId,
                             @Field("schId") String schId,
                             @Field("type") String type);

    @POST("api/groupTeamChat")
    @FormUrlEncoded
    Call<AddEvent> addGroup(@Field("teamId") String user_id);

    @POST("api/addSameAmountPayment")
    @FormUrlEncoded
    Call<AddEvent> addSameAmountPayment(@Field("userId") String user_id,
                                        @Field("amount") String amount,
                                        @Field("stripeToken") String stripeToken
    );

    @POST("api/usersFundTransfers")
    @FormUrlEncoded
    Call<AddEvent> usersFundTransfers(@Field("userId") String user_id,
                                      @Field("amount") String amount
    );

    @POST("api/charityFundTransfers")
    @FormUrlEncoded
    Call<AddEvent> charityFundTransfers(@Field("userId") String user_id,
                                        @Field("amount") String amount,
                                        @Field("charityId") String charityId);

    @POST("api/updateCharityId")
    @FormUrlEncoded
    Call<AddEvent> updateCharityId(@Field("userId") String user_id,
                                   @Field("charityId") String charityId);


    @POST("api/getUsersAmount")
    @FormUrlEncoded
    Call<AddEvent> getUsersAmount(@Field("userId") String user_id
    );

    @POST("api/addFamilyAmountPayment")
    @FormUrlEncoded
    Call<AddEvent> addFamilyAmountPayment(@Field("userId") String user_id,
                                          @Field("amount") String amount,
                                          @Field("childId") String childId,
                                          @Field("stripeToken") String stripeToken
    );

    @POST("api/addFamilyToChildWallet")
    @FormUrlEncoded
    Call<AddEvent> addFamilyToChildWallet(@Field("userId") String user_id,
                                          @Field("amount1") String amount,
                                          @Field("childId") String childId
    );


//    @Multipart
//    @POST("api/addGroup")+ 04

//    Call<AddEvent> addGroup(
//
//            @PartMap Map<String, RequestBody>
//                    map,
//            @Part MultipartBody.Part photo
//
//    );

    @POST("api/addCarpool")
    @FormUrlEncoded
    Call<AddEvent> addCarpool(@Field("userId") String userId,
                              @Field("eventId") String eventId,
                              @Field("driverId") String driverId,
                              @Field("driverName") String driverName,
                              @Field("carPoolName") String carPoolName,
                              @Field("driverImage") String driverImage,
                              @Field("driverPhone") String driverPhone,
                              @Field("genderArr") String genderArr);

    @POST("api/addRide")
    @FormUrlEncoded
    Call<AddEvent> addRide(@Field("carPoolId") String carPoolId,
                           @Field("eventId") String eventId,
                           @Field("driverId") String driverId,
                           @Field("pickDate") String pickDate,
                           @Field("pickupStatus") String pickupStatus);

    @POST("api/getCarpool")
    @FormUrlEncoded
    Call<GetEvent> getCarpool(@Field("userId") String userId);

    @POST("api/getJoinChildList")
    @FormUrlEncoded
    Call<GetEvent> getJoinChildList(@Field("childId") String childId);

    @POST("api/getEventDriverList")
    @FormUrlEncoded
    Call<GetEvent> getEventDriverList(@Field("carPoolId") String carPoolId);

    @POST("api/getDriverPoolEvent ")
    @FormUrlEncoded
    Call<ScheduleModel> getDriverPoolEvent(@Field("driverId") String carPoolId);

    @POST("api/getFamilySchedule")
    @FormUrlEncoded
    Call<ScheduleModel> getFamilySchedule(@Field("childId") String childId);

    @POST("api/getDriverPickStatus")
    @FormUrlEncoded
    Call<ScheduleModel> getDriverPickStatus(@Field("driverId") String driverId);

    @POST("api/getNotification")
    @FormUrlEncoded
    Call<NotificationModel> getNotification(@Field("receiver_id") String receiver_id);

    @GET("api/getPoolEvent")
    Call<GetEvent> getPoolEvent();


    @POST("api/getDriverNew")
    @FormUrlEncoded
    Call<GetEvent>getDriver(
            @Field("latitude") String latitude,
            @Field("longitude") String longitude

    );

    @GET("api/getSchool")
    Call<GetEvent> getSchool();

    @POST("api/accepetReject")
    @FormUrlEncoded
    Call<AddEvent> acceptReject(@Field("userId") String userId,
                                @Field("driverId") String driverId,
                                @Field("driverName") String driverName,
                                @Field("requestId") String requestId,
                                @Field("notifications_type") String notifications_type,
                                @Field("requestStatus") String requestStatus);

    @POST("api/rideStart")
    @FormUrlEncoded
    Call<AddEvent> rideStart(@Field("userId") String userId,
                             @Field("driverId") String driverId,
                             @Field("requestId") String requestId,
                             @Field("notifications_type") String notifications_type,
                             @Field("requestStatus") String requestStatus);


    @POST("api/usersDriverRating")
    @FormUrlEncoded
    Call<AddEvent> usersDriverRating(@Field("driverId") String driverId,
                                     @Field("userId") String userId,
                                     @Field("rating") String rating);

    @PUT("api/UpdateSettings")
    @FormUrlEncoded
    Call<NotificationSimple> UpdateSettings(@Field("userId") String userId,
                                            @Field("messageNotification") String messageNotification);


    @POST("api/joinCarPoolChildAccept")
    @FormUrlEncoded
    Call<AddEvent> joinCarPoolChildAccept(@Field("carPoolId") String carPoolId,
                                          @Field("driverId") String driverId,
                                          @Field("parentId") String parentId,
                                          @Field("childId") String childId,
                                          @Field("requestStatus") String requestStatus);

    @POST("api/joinCarPoolChildRequest")
    @FormUrlEncoded
    Call<AddEvent> joinCarPoolChildRequest(@Field("carPoolId") String carPoolId,
                                           @Field("driverId") String driverId,
                                           @Field("parentId") String parentId,
                                           @Field("childId") String childId);

    @PUT("api/carlatlogUpdate")
    @FormUrlEncoded
    Call<AddEvent> carlatlogUpdate(@Field("latitude") String latitude,
                                   @Field("longitude") String longitude,
                                   @Field("location") String location);

}
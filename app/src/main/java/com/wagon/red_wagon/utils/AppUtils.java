package com.wagon.red_wagon.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.applandeo.materialcalendarview.utils.DateUtils;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.adapter.CountryAdapter;
import com.wagon.red_wagon.ui.activities.LogInActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtils {

    static CountryAdapter countryAdapter;
    static Dialog countrydialog;


    private AppUtils() {

    }

    public static APIService getAPIService(Context context) {

        return RetrofitClient.getClient(context).create(APIService.class);

    }

    public static Dialog showProgress(Activity activity) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(activity.getResources().getColor(R.color.white_trans)));
        dialog.setContentView(R.layout.dialog_progress);
        ProgressBar progressBar = dialog.findViewById(R.id.progress);
        Sprite doubleBounce = new Circle();
        doubleBounce.setColor(Color.WHITE);
        // doubleBounce.setBounds(0, 0, 100, 100);
        progressBar.setIndeterminateDrawable(doubleBounce);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }

    public static String convertToString(ArrayList<String> numbers) {
        StringBuilder builder = new StringBuilder();
        // Append all Integers in StringBuilder to the StringBuilder.
        for (String number : numbers) {
            builder.append(number);
            builder.append(",");
        }
        // Remove last delimiter with setLength.
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }

    public static ArrayList<String> getCountryList() {
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for (Locale loc : locale) {
            country = loc.getDisplayCountry();
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);
        return countries;

    }

    public static ArrayList<String> getTimeZoneList() {
        String[] idArray = TimeZone.getAvailableIDs();
        String[] ids;


        ArrayList<String> timezones = new ArrayList<String>();
        String timezone = null;
        for (String loc : idArray) {
            if (!timezones.contains(timezone)) {
                timezones.add(loc);
            }
        }
        Collections.sort(timezones, String.CASE_INSENSITIVE_ORDER);


        return timezones;

    }

    public static void filter(String text, ArrayList<String> countries, CountryAdapter countryAdapter) {

        ArrayList<String> filterdList = new ArrayList<>();


        for (String s : countries) {

            if (s.toLowerCase().contains(text.toLowerCase())) {
                filterdList.add(s);
            }
        }

        countryAdapter.filterList(filterdList);
    }

    public static void countryDialog(Context context, CountryAdapter.CountryCallback countriesCall) {
        {
            countrydialog = new Dialog(context);
            countrydialog.setContentView(R.layout.country_dialog);
            RecyclerView country_rv = (RecyclerView) countrydialog.findViewById(R.id.country_rv);
            EditText search_et = countrydialog.findViewById(R.id.search_et);


            search_et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    //after the change calling the method and passing the search input

                    AppUtils.filter(editable.toString(), AppUtils.getCountryList(), countryAdapter);
                }
            });
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            country_rv.setLayoutManager(linearLayoutManager);
            countryAdapter = new CountryAdapter(context, AppUtils.getCountryList(), countriesCall, "Country");
            country_rv.setAdapter(countryAdapter);
            countrydialog.show();
        }

    }


    public static void timeZoneDialog(Context context, CountryAdapter.CountryCallback countriesCall) {
        {
            countrydialog = new Dialog(context);
            countrydialog.setContentView(R.layout.country_dialog);
            RecyclerView country_rv = (RecyclerView) countrydialog.findViewById(R.id.country_rv);
            EditText search_et = countrydialog.findViewById(R.id.search_et);


            search_et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    //after the change calling the method and passing the search input

                    AppUtils.filter(editable.toString(), AppUtils.getTimeZoneList(), countryAdapter);
                }
            });
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            country_rv.setLayoutManager(linearLayoutManager);
            countryAdapter = new CountryAdapter(context, AppUtils.getTimeZoneList(), countriesCall, "Time");
            country_rv.setAdapter(countryAdapter);
            countrydialog.show();
        }

    }

    public static void dismissDialog() {
        countrydialog.dismiss();
    }

    public static String formatTime(long time) {
        // income time
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(time);

        // current time
        Calendar curDate = Calendar.getInstance();
        curDate.setTimeInMillis(System.currentTimeMillis());

        SimpleDateFormat dateFormat = null;
        if (date.get(Calendar.YEAR) == curDate.get(Calendar.YEAR)) {
            if (date.get(Calendar.DAY_OF_YEAR) == curDate.get(Calendar.DAY_OF_YEAR)) {
                dateFormat = new SimpleDateFormat("h:mm a", Locale.US);
            } else {
                dateFormat = new SimpleDateFormat("MMM d", Locale.US);
            }
        } else {
            dateFormat = new SimpleDateFormat("MMM yyyy", Locale.US);
        }
        return dateFormat.format(time);
    }

    public static List<Calendar> getDisabledDays(ArrayList<String> weeksArr) {

        int weeks = 50;
        int week = 0;
        int i = 0;
        Calendar calendar;
        List<Calendar> calendars = new ArrayList<>();


        while (week < weeks * 7) {


            if (!weeksArr.contains("MON")) {

                calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, (Calendar.MONDAY - calendar.get(Calendar.DAY_OF_WEEK) + 7 * i));
                calendars.add(calendar);

            }
            if (!weeksArr.contains("TUE")) {
                Calendar calendar1 = DateUtils.getCalendar();
                calendar1.add(Calendar.DAY_OF_YEAR, (Calendar.TUESDAY - calendar1.get(Calendar.DAY_OF_WEEK) + 7 * i));
                calendars.add(calendar1);
            }
            if (!weeksArr.contains("WED")) {
                Calendar calendar2 = DateUtils.getCalendar();
                calendar2.add(Calendar.DAY_OF_YEAR, (Calendar.WEDNESDAY - calendar2.get(Calendar.DAY_OF_WEEK) + 7 * i));
                calendars.add(calendar2);
            }
            if (!weeksArr.contains("THU")) {
                Calendar calendar3 = DateUtils.getCalendar();
                calendar3.add(Calendar.DAY_OF_YEAR, (Calendar.THURSDAY - calendar3.get(Calendar.DAY_OF_WEEK) + 7 * i));
                calendars.add(calendar3);
            }
            if (!weeksArr.contains("FRI")) {
                Calendar calendar4 = DateUtils.getCalendar();
                calendar4.add(Calendar.DAY_OF_YEAR, (Calendar.FRIDAY - calendar4.get(Calendar.DAY_OF_WEEK) + 7 * i));
                calendars.add(calendar4);
            }
            if (!weeksArr.contains("SAT")) {
                Calendar calendar5 = DateUtils.getCalendar();
                calendar5.add(Calendar.DAY_OF_YEAR, (Calendar.SATURDAY - calendar5.get(Calendar.DAY_OF_WEEK) + 7 * i));
                calendars.add(calendar5);
            }
            if (!weeksArr.contains("SUN")) {
                Calendar calendar6 = DateUtils.getCalendar();
                calendar6.add(Calendar.DAY_OF_YEAR, (Calendar.SUNDAY - calendar6.get(Calendar.DAY_OF_WEEK) + 7 * i));
                calendars.add(calendar6);
            }
            i += 1;
            week += 7;
        }

        return calendars;
    }

    public static String formatDateToString(Date date, String format,
                                            String timeZone) {
        // null check
        if (date == null) return null;
        // create SimpleDateFormat object with input format
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        // default system timezone if passed null or empty
        if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }
        // set timezone to SimpleDateFormat
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        // return Date in required format with timezone as String
        return sdf.format(date);
    }

    public static String getCurrentTimeZone() {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        return tz.getID();
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public static void sessionExpiredAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Session Expired");
        builder.setMessage("Your current session has expired. Please re-login to renew your session.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Constans.clearSp((Activity) context);
                        Intent intent2 = new Intent((Activity) context, LogInActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent2);
                        ((Activity) context).finishAffinity();


                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
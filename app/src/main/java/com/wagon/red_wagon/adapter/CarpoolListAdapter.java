package com.wagon.red_wagon.adapter;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.DriverListFrag;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CarpoolListAdapter extends RecyclerView.Adapter<CarpoolListAdapter.CarpoolListHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String value = "";
    View view;

    public CarpoolListAdapter(FragmentActivity activity, List<Body> arrayList, String value) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.value = value;
    }

    @NonNull
    @Override
    public CarpoolListAdapter.CarpoolListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.carpool_list_row, parent, false);


        CarpoolListAdapter.CarpoolListHolder community_adapter = new CarpoolListAdapter.CarpoolListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull CarpoolListAdapter.CarpoolListHolder holder, final int position) {
        holder.groupName.setText(arrayList.get(position).getCarPoolName());
        Log.e("onBindViewHolder", "onBindViewHolder: "+arrayList.get(position).getEventName() );
        for (String genre : arrayList.get(position).getDriverImage()) {
            CircleImageView imageView = new CircleImageView(activity);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(dpToPx(25), dpToPx(25));
            imageView.setLayoutParams(p);
            p.gravity = Gravity.CENTER;
            p.setMargins(10, 0, 10, 0);
            Glide.with(activity).load(Constans.BASEURL + genre).placeholder(R.drawable.profile_ic).into(imageView);
            holder.driver_list_lay.addView(imageView);
        }


            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("getChatId", "onClick: "+arrayList.get(position).getChatId() );
                    DriverListFrag drivertList = new DriverListFrag();
                    Bundle bundle = new Bundle();
                    bundle.putString("value", value);
                    bundle.putString("getCarPoolName", arrayList.get(position).getCarPoolName());
                    bundle.putString("getEventName", arrayList.get(position).getEventName() );
                    bundle.putString("eventId", arrayList.get(position).getEventId());
                    bundle.putString("carpoolid", String.valueOf(arrayList.get(position).getId()));
                    bundle.putStringArrayList("driverIds", (ArrayList<String>) arrayList.get(position).getDriverId());
                    bundle.putStringArrayList("driverNames", (ArrayList<String>) arrayList.get(position).getDriverName());
                    bundle.putStringArrayList("weeks", (ArrayList<String>) arrayList.get(position).getWeeks());
                    bundle.putStringArrayList("gender", (ArrayList<String>) arrayList.get(position).getGenderArr());
                    bundle.putStringArrayList("driverImage", (ArrayList<String>) arrayList.get(position).getDriverImage());
                    bundle.putStringArrayList("driverPhone", (ArrayList<String>) arrayList.get(position).getDriverPhone());
                    drivertList.setArguments(bundle);
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fram_dash, drivertList, "Drivers");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class CarpoolListHolder extends RecyclerView.ViewHolder {
        TextView groupName, sport_name;
        CardView cardview;
        ImageView imageView;
        LinearLayout driver_list_lay, driver_name_lay;
        CircleImageView group_img;

        public CarpoolListHolder(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            groupName = view.findViewById(R.id.group_name);
            driver_list_lay = view.findViewById(R.id.driver_list_lay);
            driver_name_lay = view.findViewById(R.id.driver_name_lay);

            group_img = view.findViewById(R.id.group_img);
        }
    }

    public int dpToPx(int dp) {
        float density = activity.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
}


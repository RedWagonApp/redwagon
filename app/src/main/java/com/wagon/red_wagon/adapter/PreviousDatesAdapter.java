package com.wagon.red_wagon.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.fragments.PreviousAttendaceFrag;

import java.util.List;

public class PreviousDatesAdapter extends RecyclerView.Adapter<PreviousDatesAdapter.PreviousDatesAdapterHolder> {
    FragmentActivity activity;
    List<String> arrayList;
    String from, nameStr;
    private PreviousDatesAdapter.AdapterCallback mAdapterCallback;
    String teamId;
    String eventId;

    public PreviousDatesAdapter(FragmentActivity activity, List<String> arrayList, String teamId, String eventId) {

        this.teamId = teamId;
        this.eventId = eventId;
        this.activity = activity;
        this.arrayList = arrayList;


    }

    @NonNull
    @Override
    public PreviousDatesAdapter.PreviousDatesAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attend
                , parent, false);

        PreviousDatesAdapter.PreviousDatesAdapterHolder community_adapter = new PreviousDatesAdapter.PreviousDatesAdapterHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final PreviousDatesAdapter.PreviousDatesAdapterHolder holder, final int position) {

        holder.date_event.setText(arrayList.get(position));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreviousAttendaceFrag eventListFragment = new PreviousAttendaceFrag();
                Bundle args = new Bundle();

                args.putString("teamId", teamId);
                args.putString("date", arrayList.get(position));
                args.putString("eventId", eventId);
                eventListFragment.setArguments(args);

                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.school_dashbord_fram, eventListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    public interface AdapterCallback {
        void onMethodCallback(String name, int img);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class PreviousDatesAdapterHolder extends RecyclerView.ViewHolder {
        TextView date_event, sport_name;
        CardView cardview;
        ImageView flag_img;

        public PreviousDatesAdapterHolder(View view) {
            super(view);
            date_event = view.findViewById(R.id.date_event);


//            event_time_d = view.findViewById(R.id.event_time_d);
//
//            event_time_devent_location_time = view.findViewById(R.id.event_location_time);


        }
    }

}

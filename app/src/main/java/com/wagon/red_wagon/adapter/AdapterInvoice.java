package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterInvoice extends RecyclerView.Adapter<AdapterInvoice.MYHolderInvoice> {
    FragmentActivity activity;
    List<Body> responseBody;
    public AdapterInvoice(FragmentActivity activity, List<Body> responseBody) {
        this.activity = activity;
        this.responseBody = responseBody;
    }

    @NonNull
    @Override
    public MYHolderInvoice onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoce_adapter, parent, false);
        MYHolderInvoice invoce_adapter = new MYHolderInvoice(view);
        return invoce_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull MYHolderInvoice holder, int position) {
//holder.playr_pro
        holder.plyr_name.setText(responseBody.get(position).getName());
        holder.plyr_paymenmt.setText("$0.00 of " + responseBody.get(position).getAmount());
    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MYHolderInvoice extends RecyclerView.ViewHolder {
        CircleImageView playr_profile;
        TextView plyr_name, plyr_paymenmt;
        public MYHolderInvoice(View view) {
            super(view);

            plyr_paymenmt = view.findViewById(R.id.plyr_paymenmt);
            playr_profile = view.findViewById(R.id.playr_profile);
            plyr_name = view.findViewById(R.id.plyr_name);

        }
    }
}

package com.wagon.red_wagon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AttendanceBody;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PreviousAttendaceAdapter extends RecyclerView.Adapter<PreviousAttendaceAdapter.MyRecipients> {
    FragmentActivity activity;
    List<AttendanceBody> responseBody;
    ArrayList<String> playersId = new ArrayList();
    ArrayList<String> attendacestatus = new ArrayList();
    private String image_path = "";
    ArrayList<String> getPlayerName, getProfileImage, getAttendanceStatus;
    PreviousAttendaceAdapter.AttendanceCallBack callBack;

//    public PreviousAttendaceAdapter(FragmentActivity activity, List<AttendanceBody> responseBody) {
//
//    }

    public PreviousAttendaceAdapter(FragmentActivity activity, ArrayList<String> getPlayerName, ArrayList<String> getProfileImage, ArrayList<String> getAttendanceStatus) {
        this.activity = activity;
        this.getPlayerName = getPlayerName;
        this.getProfileImage = getProfileImage;
        this.getAttendanceStatus = getAttendanceStatus;

    }

    @NonNull
    @Override
    public PreviousAttendaceAdapter.MyRecipients onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pevious_list_adapter, parent, false);

        PreviousAttendaceAdapter.MyRecipients myRecipients = new PreviousAttendaceAdapter.MyRecipients(view);
        return myRecipients;
    }


    @Override
    public void onBindViewHolder(@NonNull final PreviousAttendaceAdapter.MyRecipients holder, final int position) {


        holder.nameplayr.setText(getPlayerName.get(position));
        Glide.with(activity).load(Constans.BASEURL + getProfileImage.get(position)).placeholder(R.drawable.ic_profile).into(holder.image_team);
        Log.e("getAttendanceStatus", "onBindViewHolder: " + getAttendanceStatus.get(position));
        if (getAttendanceStatus.get(position).equals("2")) {

            holder.attendence_status.setText("Absent");

        } else {
            holder.attendence_status.setText("Present");
        }


    }

    public interface AttendanceCallBack {
        void onMethodCallback(ArrayList<String> playersId, ArrayList<String> attendacestatus);
    }

    @Override
    public int getItemCount() {
        return getPlayerName.size();
    }

    public class MyRecipients extends RecyclerView.ViewHolder {
        CheckBox select_invite;
        TextView nameplayr, attendence_status;
        CircleImageView image_team;
        RadioGroup radio_group;

        public MyRecipients(View view) {
            super(view);


            nameplayr = view.findViewById(R.id.nameplayr);
            attendence_status = view.findViewById(R.id.attendence_status);

            image_team = view.findViewById(R.id.image_team);


        }
    }
}


package com.wagon.red_wagon.adapter;

import android.app.Dialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AttendanceBody;
import com.wagon.red_wagon.model.AttendanceModel;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class AttendacePagerAdapter extends PagerAdapter implements AttendancePlayerAdapter.AttendanceCallBack, View.OnClickListener {

    private List<String> mItems = new ArrayList<>();
    private ArrayList<String> playerIds = new ArrayList<>();
    private ArrayList<String> status = new ArrayList<>();
    FragmentActivity context;
    RecyclerView players_recyclerview;
    AttendancePlayerAdapter.AttendanceCallBack callBack;
    String teamID, eventID, date;
    Button attandence;
    PreviousAttendaceAdapter playerAdapter;
    int pos;

    public AttendacePagerAdapter(FragmentActivity context) {
        this.context = context;
    }

    @NotNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_page, container, false);

        players_recyclerview = view.findViewById(R.id.players_recyclerview);
        attandence = view.findViewById(R.id.attandence);
        attandence.setVisibility(View.GONE);
        pos = position;
        date=mItems.get(position);
        getList(date);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        players_recyclerview.setLayoutManager(linearLayoutManager);

        callBack = this;
        container.addView(view);

        return view;
    }


    @Override
    public int getItemPosition(@NonNull Object object) {
        Log.e(TAG, "instantiateItem: " + object);
        return super.getItemPosition(object);
    }

    @Override
    public void startUpdate(@NonNull ViewGroup container) {
        super.startUpdate(container);

    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NotNull Object object) {
        container.removeView((View) object);
    }



    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public boolean isViewFromObject(@NotNull View view, @NotNull Object object) {
        return view == object;
    }

    @Override
    public String getPageTitle(int position) {
        return mItems.get(position);

    }

    public void addAll(List<String> items, String teamId, String eventId) {
        mItems = new ArrayList<>(items);
        teamID = teamId;
        eventID = eventId;
    }

    private void getList(String date) {
        final Dialog dialog = AppUtils.showProgress(context);
        APIService mAPIService = AppUtils.getAPIService(context);
        Call<AttendanceModel> getApi;
        getApi = mAPIService.getAttendanceTeam(teamID, eventID, date);
        getApi.enqueue(new Callback<AttendanceModel>() {
            @Override
            public void onResponse(@NotNull Call<AttendanceModel> call, @NotNull retrofit2.Response<AttendanceModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    assert response.body() != null;
                    List<AttendanceBody> responseBody = response.body().getBody();
                    if (responseBody.size() != 0) {

//                        playerAdapter = new PreviousAttendaceAdapter(context, responseBody);
//                        players_recyclerview.setAdapter(playerAdapter);
                    }

                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(context, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(context);
                }
            }

            @Override
            public void onFailure(@NotNull Call<AttendanceModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(context, "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onMethodCallback(ArrayList<String> ids, ArrayList<String> attstatus) {
        playerIds = ids;
        status = attstatus;

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.attandence) {
            if (playerIds.size() != 0 && status.size() != 0) {
                setAttendance();
            }
        }
    }

    private void setAttendance() {
        final Dialog dialog = AppUtils.showProgress(context);
        APIService mAPIService = AppUtils.getAPIService(context);
        Call<AttendanceModel> getApi;
        String attendanceStatus = AppUtils.convertToString(status);
        String playerId = AppUtils.convertToString(playerIds);
        getApi = mAPIService.teamAttendance(teamID, playerId, eventID, attendanceStatus, date);
        getApi.enqueue(new Callback<AttendanceModel>() {
            @Override
            public void onResponse(@NotNull Call<AttendanceModel> call, @NotNull retrofit2.Response<AttendanceModel> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    assert response.body() != null;
                    List<AttendanceBody> responseBody = response.body().getBody();
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                } else if (response.code() == 400) {

                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(context, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(context);
                }
            }

            @Override
            public void onFailure(@NotNull Call<AttendanceModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
                Toast.makeText(context, "Network connection error", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
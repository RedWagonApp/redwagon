package com.wagon.red_wagon.adapter;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.ChatsFragment;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ShowallManegrAdapter extends RecyclerView.Adapter<ShowallManegrAdapter.MyInnerClass> {
    FragmentActivity activity;
    List<Body> responseBody;
    Socket socket;
    String value="";

    public ShowallManegrAdapter(FragmentActivity activity, List<Body> responseBody, String value) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.value = value;
    }

    @NonNull
    @Override
    public MyInnerClass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chats, parent, false);

        MyInnerClass myhodel = new MyInnerClass(view);
        return myhodel;
    }

    @Override
    public void onBindViewHolder(@NonNull MyInnerClass holder, final int position) {
        holder.team_name.setText(responseBody.get(position).getFirstName());
        Log.e("onBindViewHolder", "onBindViewHolder: "+responseBody.get(position).getFirstName());
        //holder.sport_name.setText(responseBody.get(position).getMessage());
        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).placeholder(R.drawable.profile_ic).into(holder.team_img);
//        holder.team_img.setText(arrayList.get(position).getName());
        holder.hellosay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ChatsFragment.socket.disconnect();
                Constans.userin = responseBody.get(position).getId();
                String userId = String.valueOf(Constans.userin);
                //  activity.startActivity(new Intent(activity, ChatDetailsActivity.class).putExtra("name", responseBody.get(position).getFirstName()).putExtra("UserId2", "maneger"));
//                activity.finish();


                try {
                    socket = IO.socket(Constans.BASEURL);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject jsonData = new JSONObject();
                    jsonData.put("user_id", Constans.getFamilyId(activity));
                    jsonData.put("user2id", userId);
                    jsonData.put("message", "Hello!!");
                    jsonData.put("message_type", "1");
                    socket.emit("send_message", jsonData);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                socket.connect();
                ChatsFragment chatsFragment = new ChatsFragment();
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putString("value", value);
                bundle.putString("userid", "0");
                chatsFragment.setArguments(bundle);
                transaction.replace(R.id.chat_frag, chatsFragment, "Chat");
                transaction.commit();


            }
        });

    }

    public void filterList(ArrayList<Body> arrayList) {
        this.responseBody = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MyInnerClass extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        LinearLayout lyt_parent;
        CircleImageView team_img;
        Button hellosay;

        public MyInnerClass(View view) {
            super(view);
            sport_name = view.findViewById(R.id.content);
            team_img = view.findViewById(R.id.image);
            team_name = view.findViewById(R.id.title);
            lyt_parent = view.findViewById(R.id.lyt_parent);
            hellosay = view.findViewById(R.id.hellosay);
            hellosay.setVisibility(View.VISIBLE);
        }
    }
}

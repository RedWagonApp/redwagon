package com.wagon.red_wagon.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.utils.Constans;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class SliderPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    Activity activity;
    ArrayList<String> image_arraylist;
    //ArrayList<String> slider_type_list;
    String slider_type_list;
    String type;
    JCVideoPlayerStandard videoView;
    ImageView video_play_button;
    int resId = -1;

    public SliderPagerAdapter(Activity activity, ArrayList<String> image_arraylist, String slider_type_list) {
        this.activity = activity;
        this.image_arraylist = image_arraylist;
        this.slider_type_list = slider_type_list;
        this.type = type;
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup container, int position) {
        View view = null;
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (Integer.parseInt(slider_type_list)) {

            case 2:
                view = layoutInflater.inflate(R.layout.layout_video_slider, container, false);
                videoView = (JCVideoPlayerStandard) view.findViewById(R.id.videoplayer);
                setVideo(Constans.BASEURL + image_arraylist.get(position));
                break;
            case 1:

            default:
                view = layoutInflater.inflate(R.layout.layout_slider, container, false);
                ImageView im_slider1 = (ImageView) view.findViewById(R.id.im_slider);
                Glide.with(activity.getApplicationContext())
                        .load(Constans.BASEURL + image_arraylist.get(position))
                        .placeholder(R.drawable.imagesloder) // optional
                        .error(R.drawable.imagesloder)         // optional
                        .into(im_slider1);

                break;
        }
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        // Just to be safe, check also if we have an valid list of items - never return invalid size.
        return null == image_arraylist ? 0 : image_arraylist.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        // The object returned by instantiateItem() is a key/identifier. This method checks whether

        return view == object;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Removes the page from the container for the given position. We simply removed object using removeView()
        // but could’ve also used removeViewAt() by passing it the position.
        try {
            // Remove the view from the container
            container.removeView((View) object);

            // Remove any resources used by this view
            unbindDrawables((View) object);
            videoView.releaseAllVideos();
            // Invalidate the object
            object = null;
        } catch (Exception e) {
            Log.w("LOd", "destroyItem: failed to destroy item and clear it's used resources", e);
        }
    }

    protected void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    private void setVideo(final String videoUrl) {

        videoView.setUp(videoUrl, JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, "");

    }


}
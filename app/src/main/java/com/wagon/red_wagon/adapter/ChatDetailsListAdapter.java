package com.wagon.red_wagon.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.ChatAppMsgDTO;
import com.wagon.red_wagon.ui.activities.ChatDetailsActivity;
import com.wagon.red_wagon.utils.Constans;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.util.List;

public class ChatDetailsListAdapter extends RecyclerView.Adapter {
    Context activity;

    List<AddEvent> arrayLists;
    String from, nameStr;
    private List<ChatAppMsgDTO> msgDtoList = null;
    File imgFile;
//    public ChatDetailsListAdapter(Context activity, List<ChatAppMsgDTO> msgDtoList) {
//
//        this.activity = activity;
//        this.msgDtoList = msgDtoList;
//
//
//    }

    public ChatDetailsListAdapter(ChatDetailsActivity chatDetailsActivity, List<ChatAppMsgDTO> msgDtoList) {
        this.activity = chatDetailsActivity;
        this.msgDtoList = msgDtoList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        ChatAppMsgDTO msgDto;
        switch (viewType) {
            case ChatAppMsgDTO.MSG_TYPE_RECEIVED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_details, parent, false);


                return new TextLeftViewHolder(view);

            case ChatAppMsgDTO.MSG_TYPE_SENT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_sent, parent, false);
                return new TextRightViewHolder(view);
            case ChatAppMsgDTO.MSG_TYPE_IMAGE_SEND:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_right_image_sent, parent, false);
                return new ImageRightViewHolder(view);
            case ChatAppMsgDTO.MSG_TYPE_IMAGE_RECIEVED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_left_image_sent, parent, false);
                return new ImageLeftViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {


        final ChatAppMsgDTO object = msgDtoList.get(listPosition);
        if (object != null) {
            switch (object.getMsgType()) {
                case ChatAppMsgDTO.MSG_TYPE_RECEIVED:
                    ((TextLeftViewHolder) holder).leftMsgTextView.setText(StringEscapeUtils.unescapeJava(object.getMsgContent()));
                    ((TextLeftViewHolder) holder).Leftname.setText(StringEscapeUtils.unescapeJava(object.getName()));

                    break;
                case ChatAppMsgDTO.MSG_TYPE_SENT:

                    ((TextRightViewHolder) holder).rightMsgTextView.setText(StringEscapeUtils.unescapeJava(object.getMsgContent()));
                    ((TextRightViewHolder) holder).rightnameuserchat.setText(StringEscapeUtils.unescapeJava(object.getName()));
                    break;

                case ChatAppMsgDTO.MSG_TYPE_IMAGE_SEND:

                    final String image = Constans.BASEURL + "public/" + object.getMediaConetent();
                    Glide.with(activity).load(image).override(500, 500).into(((ImageRightViewHolder) holder).rightMsgImageView);

                    ((ImageRightViewHolder) holder).rightMsgImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showImage(image);
                        }
                    });
                    ((TextRightViewHolder) holder).rightnameuserchat.setText(StringEscapeUtils.unescapeJava(object.getName()));
                   /* imgFile = new File(object.getMediaConetent());
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        ((ImageRightViewHolder) holder).rightMsgTextView.setImageBitmap(myBitmap);
                    }*/
                    break;
                case ChatAppMsgDTO.MSG_TYPE_IMAGE_RECIEVED:
                    final String image1 = Constans.BASEURL + "public/" + object.getMediaConetent();
                    Glide.with(activity).load(Constans.BASEURL + "public/" + object.getMediaConetent()).override(500, 500).into(((ImageLeftViewHolder) holder).leftMsgImageView);
                    ((ImageLeftViewHolder) holder).leftMsgImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showImage(image1);
                        }
                    });
                    ((TextLeftViewHolder) holder).Leftname.setText(StringEscapeUtils.unescapeJava(object.getName()));

                    /*imgFile = new File(object.getMediaConetent());
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        ((ImageLeftViewHolder) holder).leftMsgTextView.setImageBitmap(myBitmap);
                    }*/

                    break;

            }

        }
    }

    @Override
    public int getItemViewType(int position) {

        switch (msgDtoList.get(position).getMsgType()) {
            case 0:
                return ChatAppMsgDTO.MSG_TYPE_RECEIVED;
            case 1:
                return ChatAppMsgDTO.MSG_TYPE_SENT;
            case 2:
                return ChatAppMsgDTO.MSG_TYPE_IMAGE_SEND;
            case 3:
                return ChatAppMsgDTO.MSG_TYPE_IMAGE_RECIEVED;
            default:
                return -1;
        }
    }

    @Override
    public int getItemCount() {
        return msgDtoList.size();
    }

    public static class TextLeftViewHolder extends RecyclerView.ViewHolder {
        LinearLayout leftMsgLayout;
        LinearLayout rightMsgLayout;
        TextView leftMsgTextView;
        TextView Leftname;
        TextView rightMsgTextView;

        public TextLeftViewHolder(View itemView) {
            super(itemView);

            if (itemView != null) {
                //  leftMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_left_msg_layout);
                leftMsgTextView = (TextView) itemView.findViewById(R.id.chat_left_msg_text_view);
                Leftname = (TextView) itemView.findViewById(R.id.nameuserchat);

            }
        }
    }

    public static class TextRightViewHolder extends RecyclerView.ViewHolder {
        LinearLayout rightMsgLayout;
        TextView rightMsgTextView;
        TextView rightnameuserchat;

        public TextRightViewHolder(View itemView) {
            super(itemView);

            if (itemView != null) {

                //  rightMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_right_msg_layout);
                rightMsgTextView = (TextView) itemView.findViewById(R.id.chat_right_msg_text_view);
                rightnameuserchat = (TextView) itemView.findViewById(R.id.rightnameuserchat);
            }
        }
    }

    public static class ImageLeftViewHolder extends RecyclerView.ViewHolder {
        LinearLayout leftMsgLayout;
        ImageView leftMsgImageView;
TextView Leftname;

        public ImageLeftViewHolder(View itemView) {
            super(itemView);

            if (itemView != null) {
                //   leftMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_left_msg_layout);
                leftMsgImageView = (ImageView) itemView.findViewById(R.id.chat_left_msg_image_view);
                Leftname = (TextView) itemView.findViewById(R.id.nameuserchat);

            }
        }
    }

    public static class ImageRightViewHolder extends RecyclerView.ViewHolder {
        LinearLayout rightMsgLayout;
        ImageView rightMsgImageView;
TextView rightnameuserchat;
        public ImageRightViewHolder(View itemView) {
            super(itemView);

            if (itemView != null) {

                // rightMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_right_msg_layout);
                rightMsgImageView = (ImageView) itemView.findViewById(R.id.chat_right_msg_Image_view);
                rightnameuserchat = (TextView) itemView.findViewById(R.id.rightnameuserchat);
            }
        }
    }

    private void showImage(String imageStr) {
        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
        WindowManager manager = (WindowManager) activity.getSystemService(Activity.WINDOW_SERVICE);
        int width, height;
        LinearLayout.LayoutParams params;
        dialog.setContentView(R.layout.full_image_view);
        ImageView imageView = dialog.findViewById(R.id.imageView);
        ImageView close_dialog = dialog.findViewById(R.id.close_dialog);
        Glide.with(activity).load(imageStr).into(imageView);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
       /* if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
            width = manager.getDefaultDisplay().getWidth();
            height = manager.getDefaultDisplay().getHeight();
        } else {
            Point point = new Point();
            manager.getDefaultDisplay().getSize(point);
            width = point.x;
            height = point.y;
        }*/
    }

}

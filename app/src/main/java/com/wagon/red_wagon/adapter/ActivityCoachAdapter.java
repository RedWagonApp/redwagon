package com.wagon.red_wagon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityCoachAdapter extends RecyclerView.Adapter<ActivityCoachAdapter.CoachListHolder> {
    Context activity;
    List<Body> arrayList;
    String from, nameStr = "", teamId = "0", coachName = "", coachId = "0";
    private AdapterCallback mAdapterCallback;

    public ActivityCoachAdapter(Context activity, List<Body> arrayList, AdapterCallback callback, String from) {
        this.mAdapterCallback = callback;
        this.activity = activity;
        this.arrayList = arrayList;
        this.from = from;

    }

    @NonNull
    @Override
    public CoachListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coach_row, parent, false);

        CoachListHolder community_adapter = new CoachListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final CoachListHolder holder, final int position) {
        if (from.equals("Team")) {
            holder.team_name.setText(arrayList.get(position).getTeamName());
        } else {

            holder.team_name.setText(arrayList.get(position).getFirstName());
        }

        holder.team_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (from.equals("Team")) {
                    nameStr = arrayList.get(position).getTeamName();
                    coachName = arrayList.get(position).getManagerName();
                    teamId = arrayList.get(position).getId().toString();
                    coachId = arrayList.get(position).getManagerId();

                } else {
                    coachName = arrayList.get(position).getFirstName();
                    coachId = arrayList.get(position).getId().toString();
                    nameStr = "";
                    teamId = "0";
                }
                mAdapterCallback.onMethodCallback(coachName, coachId, nameStr, teamId);
            }
        });
    }

    public interface AdapterCallback {
        void onMethodCallback(String coachName, String coachid, String teamName, String teamid);
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class CoachListHolder extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        CardView cardview;
        CircleImageView team_img;

        public CoachListHolder(View view) {
            super(view);
            team_name = view.findViewById(R.id.textView);

        }
    }
}
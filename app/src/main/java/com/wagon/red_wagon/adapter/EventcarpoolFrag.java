package com.wagon.red_wagon.adapter;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.ui.activities.FamilyDashboardActivity;
import com.wagon.red_wagon.ui.fragments.CreateCarPoolFrag;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class EventcarpoolFrag extends Fragment {
    String getId = "",eventId="";
    List<Body> responseBody;
    RecyclerView carpool_recyclerview;
    TextView no_result_tv, create_carpool;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.eventcarpool, container, false);
        carpool_recyclerview = view.findViewById(R.id.carpool_recyclervieww);
        no_result_tv = view.findViewById(R.id.no_result_tv);
        create_carpool = view.findViewById(R.id.create_carpool);

        FamilyDashboardActivity.title.setText("Community");
        ((FamilyDashboardActivity) getActivity()).isVisible(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        carpool_recyclerview.setLayoutManager(linearLayoutManager);
        Bundle bundle = getArguments();
        getId = bundle.getString("getId", "");

        ApigetDriverPoolList(getId);

        create_carpool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateCarPoolFrag eventList = new CreateCarPoolFrag();
                Bundle args = new Bundle();
                args.putString("eventId", getId);
                eventList.setArguments(args);

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fram_dash, eventList);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return view;
    }

    private void ApigetDriverPoolList(String EventID) {
        final Dialog dialog = AppUtils.showProgress(getActivity());
        APIService mAPIService = AppUtils.getAPIService(getActivity());
        Call<GetEvent> getApi;

        getApi = mAPIService.getDriverPoolList(EventID);
        Log.e("getDriverPoolList", "onResponse: " + EventID);

        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    responseBody = response.body().getBody();

                    if (responseBody.size() == 0) {
                        no_result_tv.setVisibility(View.VISIBLE);

                        carpool_recyclerview.setVisibility(View.GONE);
                    } else {
                        no_result_tv.setVisibility(View.GONE);
                        carpool_recyclerview.setVisibility(View.VISIBLE);

                        CarpoolCommnityAdapter commnityAdapter = new CarpoolCommnityAdapter(getActivity(), responseBody);
                        carpool_recyclerview.setAdapter(commnityAdapter);

                    }


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(getActivity(), userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

}

package com.wagon.red_wagon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.TeamFamily;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChildTrackAdapter extends RecyclerView.Adapter<ChildTrackAdapter.MyTrack> {
    FragmentActivity activity;
    List<Body> responseBody;
    String value;
    View view;

    ChildTrackAdapter.AdapterCallback callback;

    public ChildTrackAdapter(FragmentActivity activity, List<Body> responseBody, String value, ChildTrackAdapter.AdapterCallback callback) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.value = value;
        this.callback = callback;
    }

    @NonNull
    @Override
    public MyTrack onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (value.equals("1")) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chidteam_adapter, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chidtrack_adapter, parent, false);
        }
        MyTrack chidtrack_adapter = new MyTrack(view);

        return chidtrack_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull MyTrack holder, int position) {
        Log.e("onBindViewHolder", "onBindViewHolder: " + responseBody.get(position).getId());
        holder.childName.setText(responseBody.get(position).getFirstName());

        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.childImg);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onMethodCallback(responseBody.get(position).getProfileImage(), responseBody.get(position).getId().toString());
                if (value.equals("1")) {
                    TeamFamily.dialog1.dismiss();
                } else {

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public interface AdapterCallback {
        void onMethodCallback(String name, String id);

    }

    public class MyTrack extends RecyclerView.ViewHolder {
        CircleImageView childImg;
        TextView childName;

        public MyTrack(View view) {
            super(view);

            childImg = view.findViewById(R.id.child_img);
            childName = view.findViewById(R.id.child_name);
        }
    }
}


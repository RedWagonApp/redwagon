package com.wagon.red_wagon.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.nkzawa.socketio.client.Socket;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class TeamChatAdapter extends RecyclerView.Adapter<TeamChatAdapter.MyTeamHolder> {
    FragmentActivity activity;
    List<Body> responseBody;
    Socket socket;
    String filePath = "", result;
    Bitmap bitmap;
    ArrayList<String> arrayplyerId = new ArrayList();
    TeamChatAdapter.RecipientsCallBack callback;
    CircleImageView imageset_g;

    public TeamChatAdapter(FragmentActivity activity, List<Body> responseBody, RecipientsCallBack showallTeam) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.callback = showallTeam;
    }

    @NonNull
    @Override
    public MyTeamHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chats, parent, false);
        MyTeamHolder myTeamHolder = new MyTeamHolder(view);
        return myTeamHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyTeamHolder holder, int position) {
        holder.team_name.setText(responseBody.get(position).getTeamName());

        //holder.sport_name.setText(responseBody.get(position).getMessage());

        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).placeholder(R.drawable.profile_ic).into(holder.team_img);


//        holder.select_user.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (holder.select_user.isChecked()) {
//
//                        arrayplyerId.add(responseBody.get(position).getId().toString());
//
//
//                    Log.e("arrayList", "onBindViewHolder: " + arrayplyerId);
//                } else {
//
//                        if (arrayplyerId.contains(responseBody.get(position).getId().toString())) {
//
//                            arrayplyerId.remove(responseBody.get(position).getId().toString());
//
//                        }
//
//                    }
//
//
//
//            }
//        });
        holder.hellosay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String teamId = String.valueOf(responseBody.get(position).getId());
                    callback.onMethodCallback(teamId, responseBody.get(position).getTeamName());

            }
        });
    }

    public void filterList(ArrayList<Body> arrayList) {
        this.responseBody = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public interface RecipientsCallBack {

        void onMethodCallback(String teamId, String gropname);

    }


    public class MyTeamHolder extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        LinearLayout lyt_parent;
        CircleImageView team_img;
        CheckBox select_user;
        Button hellosay;

        public MyTeamHolder(@NonNull View view) {
            super(view);
            sport_name = view.findViewById(R.id.content);
            team_img = view.findViewById(R.id.image);
            team_name = view.findViewById(R.id.title);
            lyt_parent = view.findViewById(R.id.lyt_parent);
            select_user = view.findViewById(R.id.select_user);
            hellosay = view.findViewById(R.id.hellosay);
            select_user.setVisibility(View.GONE);
            hellosay.setVisibility(View.VISIBLE);

        }
    }

    private void pickFromGallery(Dialog dialog) {
        //Create an Intent with action as ACTION_PICK

        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(i, 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image


        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {


            try {

                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
                filePath = getPath(uri);
                Log.e("onActivityResult", "onActivityResult: " + bitmap);
                imageset_g.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();

            }

        } else {
            Log.e("else", "onActivityResult: ");
        }

    }

    private String getPath(Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(activity, contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();

        } catch (SecurityException ignored) {

        }
        return result;
    }

}

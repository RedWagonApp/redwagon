package com.wagon.red_wagon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;

import java.util.List;

public class StatsHorizontalAdapter extends RecyclerView.Adapter<StatsHorizontalAdapter.MyViewHolder> {
    private Context context;
    private List<String> statsHorizontalBeanArrayList;

    public StatsHorizontalAdapter(Context context, List<String> statsHorizontalBeanArrayList) {
        this.context = context;
        this.statsHorizontalBeanArrayList = statsHorizontalBeanArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.stats_horizontal_item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        String  statsHorizontalBean = statsHorizontalBeanArrayList.get(i);
        myViewHolder.txt_content.setText(statsHorizontalBean);
    }

    @Override
    public int getItemCount() {
        return statsHorizontalBeanArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_content;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_content = itemView.findViewById(R.id.txt_content);
        }
    }


}

package com.wagon.red_wagon.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.EditPlayerFrag;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.PlayerListHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String value = "";

    public PlayerListAdapter(FragmentActivity activity, List<Body> arrayList, String value) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.value = value;
    }

    @NonNull
    @Override
    public PlayerListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_row, parent, false);

        PlayerListHolder community_adapter = new PlayerListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerListHolder holder, final int position) {
        //holder.player_name.setText(team_name);
        String firstName =arrayList.get(position).getFirstName() !=null ? arrayList.get(position).getFirstName() :"";
        String lastName =arrayList.get(position).getLastName() !=null ? arrayList.get(position).getLastName() :"";
        holder.player_name.setText( firstName+ " " + lastName);
        Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).placeholder(R.drawable.ic_profile).into(holder.team_img);

        holder.sport_name.setText(arrayList.get(position).getEmail());
        holder.moblie_num.setText(arrayList.get(position).getPhoneNumber());
//        holder.event_time_d.setText(arrayList.get(position).getDateTime());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jersey = arrayList.get(position).getJerseyNumber() != null ? arrayList.get(position).getJerseyNumber() : "0";
                EditPlayerFrag eventListFragment = new EditPlayerFrag();
                Bundle args = new Bundle();
                args.putString("fromType", "Player");

                args.putString("playerId", arrayList.get(position).getId().toString());
                args.putString("value", jersey);
                eventListFragment.setArguments(args);

                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_team, eventListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        if (value.equals("2")) {


            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String jersey = arrayList.get(position).getJerseyNumber() != null ? arrayList.get(position).getJerseyNumber() : "0";
                    EditPlayerFrag eventListFragment = new EditPlayerFrag();
                    Bundle args = new Bundle();
                    args.putString("fromType", "show");
                    args.putString("playerId", arrayList.get(position).getId().toString());
                    args.putString("value",jersey );
                    eventListFragment.setArguments(args);

                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_team, eventListFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });

        }
        holder.delete_btn_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    ApiDelete(arrayList.get(position).getId().toString(), holder.getAdapterPosition());
                                } catch (Exception e) {

                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class PlayerListHolder extends RecyclerView.ViewHolder {
        TextView player_name, sport_name, moblie_num, event_close_time, event_time_d;
        CardView cardview;
        ImageView imageView;
        LinearLayout delete_btn_;
        CircleImageView team_img;

        public PlayerListHolder(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            player_name = view.findViewById(R.id.team_name);
            sport_name = view.findViewById(R.id.sport_name);
            imageView = view.findViewById(R.id.edit_btn);
            delete_btn_ = view.findViewById(R.id.delete_btn_);
            team_img = view.findViewById(R.id.team_img);
            moblie_num = view.findViewById(R.id.moblie_num);
            moblie_num.setVisibility(View.VISIBLE);
            if (value.equals("1")) {
                delete_btn_.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
            }

        }
    }

    private void ApiDelete(String id, final int position) {
        final Dialog dialog = AppUtils.showProgress(activity);
        Call<AddEvent> addEventCall;
        addEventCall = AppUtils.getAPIService(activity).deletePlayer(id);


        addEventCall.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                if (response.code() == 200) {
                    delete(position);
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();

//                    EventListFragment createTeamFrag = new EventListFragment();
//                    FragmentManager manager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction trans = manager.beginTransaction();
//                    trans.replace(R.id.crate_activity_scl, createTeamFrag);
//
//                    trans.commit();
                    //manager.popBackStack();


                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void delete(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }

}

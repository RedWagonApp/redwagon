package com.wagon.red_wagon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;

import java.util.ArrayList;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryListHolder> {
    Context activity;
    ArrayList<String> arrayList;

    private CountryAdapter.CountryCallback mAdapterCallback;
    String type;
    public CountryAdapter(Context activity, ArrayList<String> arrayList, CountryCallback callback,String type) {
        this.mAdapterCallback = callback;
        this.activity = activity;
        this.arrayList = arrayList;
        this.type = type;


    }

    @NonNull
    @Override
    public CountryAdapter.CountryListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_row, parent, false);

        CountryAdapter.CountryListHolder community_adapter = new CountryAdapter.CountryListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final CountryAdapter.CountryListHolder holder, final int position) {

        holder.country_name.setText(arrayList.get(position));
        holder.country_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("Country")) {
                    mAdapterCallback.onSelectCountryCallback(arrayList.get(position));
                }else{
                    mAdapterCallback.onSelectTimeZoneCallback(arrayList.get(position));
                }
            }
        });
    }

    public interface CountryCallback {
        void onSelectCountryCallback(String name);
        void onSelectTimeZoneCallback(String name);
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public void filterList(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }
    public class CountryListHolder extends RecyclerView.ViewHolder {
        TextView country_name;


        public CountryListHolder(View view) {
            super(view);
            country_name = view.findViewById(R.id.textView);

//            event_time_d = view.findViewById(R.id.event_time_d);
//
//            event_time_devent_location_time = view.findViewById(R.id.event_location_time);


        }
    }

}
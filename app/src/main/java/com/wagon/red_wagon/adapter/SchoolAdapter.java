package com.wagon.red_wagon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.CoachListHolder> {
    Context activity;
    List<Body> arrayList;
    String from, nameStr;
    private SchoolAdapter.AdapterCallback mAdapterCallback;

    public SchoolAdapter(Context activity, List<Body> arrayList, SchoolAdapter.AdapterCallback callback, String from) {
        this.mAdapterCallback = callback;
        this.activity = activity;
        this.arrayList = arrayList;
        this.from = from;

    }

    @NonNull
    @Override
    public SchoolAdapter.CoachListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coach_row, parent, false);

        SchoolAdapter.CoachListHolder community_adapter = new SchoolAdapter.CoachListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final SchoolAdapter.CoachListHolder holder, final int position) {


        if (from.equals("Game")) {
            holder.team_name.setText(arrayList.get(position).getTeamName());


        } else {
            holder.team_name.setText(arrayList.get(position).getFirstName());

        }

        holder.team_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (from.equals("Game")) {
                    nameStr = arrayList.get(position).getTeamName();
                    int pos= position;
                    mAdapterCallback.onMethodCallback(nameStr, arrayList.get(position).getId().toString(),pos);

                } else {
                  //  int pos= Integer.parseInt(arrayList.get(position).getPosition());
                    int pos= position;
                    nameStr = arrayList.get(position).getFirstName();
                    mAdapterCallback.onMethodCallback(nameStr, arrayList.get(position).getId().toString(),pos);
                }


            }
        });
    }

    public interface AdapterCallback {
        void onMethodCallback(String name, String id,int position);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class CoachListHolder extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        CardView cardview;
        CircleImageView team_img;

        public CoachListHolder(View view) {
            super(view);
            team_name = view.findViewById(R.id.textView);

//            event_time_d = view.findViewById(R.id.event_time_d);
//
//            event_time_devent_location_time = view.findViewById(R.id.event_location_time);


        }
    }
}
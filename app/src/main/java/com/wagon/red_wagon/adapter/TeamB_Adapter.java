package com.wagon.red_wagon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.TeamBModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TeamB_Adapter extends RecyclerView.Adapter<TeamB_Adapter.TeamB_Holer> {
    Context context;
    ArrayList<TeamBModel> teamBData;
    String from, nameStr;
    private TeamB_Adapter.AdapterCallback mAdapterCallback;
    public TeamB_Adapter(Context context, ArrayList<TeamBModel> teamBData, TeamB_Adapter.AdapterCallback adapterCallback) {
       this.context=context;
       this.teamBData=teamBData;
       this.mAdapterCallback=adapterCallback;

    }

    @NonNull
    @Override
    public TeamB_Holer onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coach_row, parent, false);
        TeamB_Holer teamB_holer = new TeamB_Holer(view);

        return teamB_holer;
    }

    @Override
    public void onBindViewHolder(@NonNull TeamB_Holer holder, int position) {
        holder.team_name.setText(teamBData.get(position).getTeamName());
        holder.team_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    nameStr = teamBData.get(position).getTeamName();
                    mAdapterCallback.onMethodCallback(nameStr, teamBData.get(position).getTeamId());


            }
        });
    }

    @Override
    public int getItemCount() {
        return teamBData.size();
    }
    public interface AdapterCallback {
        void onMethodCallback(String name, String id);

    }


    public class TeamB_Holer extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        CardView cardview;
        CircleImageView team_img;

        public TeamB_Holer(View view) {
            super(view);
            team_name = view.findViewById(R.id.textView);

        }
    }
}

package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SchedulePlayerAdapter extends RecyclerView.Adapter<SchedulePlayerAdapter.PlayerListHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String value = "";

    public SchedulePlayerAdapter(FragmentActivity activity, List<Body> arrayList, String value) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.value = value;
    }

    @NonNull
    @Override
    public SchedulePlayerAdapter.PlayerListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_player_list, parent, false);

        SchedulePlayerAdapter.PlayerListHolder community_adapter = new SchedulePlayerAdapter.PlayerListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull SchedulePlayerAdapter.PlayerListHolder holder, final int position) {
        //holder.player_name.setText(team_name);
        String firstName =arrayList.get(position).getFirstName() !=null ? arrayList.get(position).getFirstName() :"";
        String lastName =arrayList.get(position).getLastName() !=null ? arrayList.get(position).getLastName() :"";
        holder.player_name.setText( firstName+ " " + lastName);

        holder.contact_number.setText(arrayList.get(position).getPhoneNumber());
        Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).placeholder(R.drawable.ic_profile).into(holder.team_img);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class PlayerListHolder extends RecyclerView.ViewHolder {
        TextView player_name, contact_number;
        CardView cardview;
        CircleImageView team_img;

        public PlayerListHolder(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            player_name = view.findViewById(R.id.player_name);
            contact_number = view.findViewById(R.id.contact_number);
            team_img = view.findViewById(R.id.team_img);


        }
    }


}

package com.wagon.red_wagon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddChildInPoolAdapter extends RecyclerView.Adapter<AddChildInPoolAdapter.MyTrack> {
    FragmentActivity activity;
    List<Body> responseBody;
    ArrayList<String> childIds ;
    String value;
    View view;

    AddChildInPoolAdapter.AdapterCallback callback;

    public AddChildInPoolAdapter(FragmentActivity activity, List<Body> responseBody, ArrayList<String> childIds, AddChildInPoolAdapter.AdapterCallback callback) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.childIds = childIds;
        this.callback = callback;
    }

    @NonNull
    @Override
    public AddChildInPoolAdapter.MyTrack onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_child_to_pool_row, parent, false);

        AddChildInPoolAdapter.MyTrack chidtrack_adapter = new AddChildInPoolAdapter.MyTrack(view);

        return chidtrack_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull AddChildInPoolAdapter.MyTrack holder, int position) {
        Log.e("onBindViewHolder", "onBindViewHolder: " + responseBody.get(position).getId());
        holder.childName.setText(responseBody.get(position).getFirstName());

        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.childImg);
        if(childIds.contains(responseBody.get(position).getId().toString())){
            holder.add_btn.setText("Added");
            holder.add_btn.setTextColor(activity.getResources().getColor(R.color.gray));
            holder.add_btn.setEnabled(false);
        }
        holder.add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onMethodCallback(responseBody.get(position).getFirstName(), responseBody.get(position).getId().toString(), holder.add_btn);
            }
        });

    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public interface AdapterCallback {
        void onMethodCallback(String name, String id, Button v);

    }

    public class MyTrack extends RecyclerView.ViewHolder {
        CircleImageView childImg;
        TextView childName;
        Button add_btn;

        public MyTrack(View view) {
            super(view);

            childImg = view.findViewById(R.id.child_img);
            childName = view.findViewById(R.id.child_name);
            add_btn = view.findViewById(R.id.add_btn);
        }
    }
}



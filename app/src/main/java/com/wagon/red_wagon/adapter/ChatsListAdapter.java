package com.wagon.red_wagon.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.GetEvent;
import com.wagon.red_wagon.model.ModelUserChat;
import com.wagon.red_wagon.model.NotificationSimple;
import com.wagon.red_wagon.ui.activities.ChatDetailsActivity;
import com.wagon.red_wagon.ui.fragments.ChatsFragment;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatsListAdapter extends RecyclerView.Adapter<ChatsListAdapter.ChatsListHolder> {
    Context activity;
    ArrayList<ModelUserChat> arrayList;
    String from, nameStr;
    ArrayList<String> User_idarry = new ArrayList<>();

    public ChatsListAdapter(Activity activity, ArrayList<ModelUserChat> arrayList) {

        this.activity = activity;
        this.arrayList = arrayList;


    }

    @NonNull
    @Override
    public ChatsListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chats, parent, false);

        ChatsListHolder community_adapter = new ChatsListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatsListHolder holder, final int position) {

        if (!arrayList.get(position).getName().equals("")) {
            holder.team_name.setText(arrayList.get(position).getName());
        }
        holder.sport_name.setText(StringEscapeUtils.unescapeJava(arrayList.get(position).getMessage()));
        Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfile_image()).placeholder(R.drawable.profile_ic).into(holder.team_img);
//        holder.team_img.setText(arrayList.get(position).getName());

         Log.e("ifif", "onBindViewHolder: " + arrayList.get(position).getUserid());
//        if (arrayList.get(position).getUserid().equals(arrayList.get(position).getUser2id())) {
//            Log.e("ifif", "onBindViewHolder: ");
//        } else {
//            Log.e("elseeee", "onBindViewHolder: ");
//        }


        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatsFragment.socket.disconnect();
                if (arrayList.get(position).getRoom().equals("1")) {
                    UpdateSettings("0");
                    activity.startActivity(new Intent(activity, ChatDetailsActivity.class).putExtra("name", arrayList.get(position).getName()).putExtra("UserId2", arrayList.get(position).getUser2id()).putExtra("chat_id", arrayList.get(position).getChat_id()).putExtra("Room", arrayList.get(position).getRoom()));


                } else {
                    String roomId = arrayList.get(position).getChat_id();
                    ApiService(roomId, position);
                }

            }
        });
    }

    private void ApiService(String roomId, int position) {
        Log.e("ApiService", "ApiService: " + roomId);
        final Dialog dialog = AppUtils.showProgress((Activity) activity);
        APIService mAPIService = AppUtils.getAPIService(activity);
        Call<GetEvent> getApi;

        getApi = mAPIService.GetGroupUsers(roomId);


        getApi.enqueue(new Callback<GetEvent>() {
            @Override
            public void onResponse(Call<GetEvent> call, retrofit2.Response<GetEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {
                    ArrayList<String> getUser_idget = new ArrayList<>();
                    List<Body> responseBody = response.body().getBody();
                    for (int i = 0; i < responseBody.size(); i++) {
                        String userid = responseBody.get(i).getUser_id();
                        User_idarry.add(userid);

                    }
                    UpdateSettings("0");
                    activity.startActivity(new Intent(activity, ChatDetailsActivity.class).putExtra("name", arrayList.get(position).getName()).putExtra("UserId2", arrayList.get(position).getUser2id()).putExtra("chat_id", arrayList.get(position).getChat_id()).putExtra("Room", arrayList.get(position).getRoom()).putExtra("User_idarry", User_idarry));
                    Log.e("getProfileImage", "onResponse: " + User_idarry);

                } else if (response.code() == 400) {
//                    if (!response.isSuccessful()) {
//
//                        try {
//
//                            activity.startActivity(new Intent(activity, ChatDetailsActivity.class).putExtra("name", arrayList.get(position).getName()).putExtra("UserId2", arrayList.get(position).getUser2id()).putExtra("chat_id", arrayList.get(position).getChat_id()).putExtra("Room", arrayList.get(position).getRoom()));
//
//                        } catch (JSONException | IOException e) {
//                            e.printStackTrace();
//                        }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(activity);
                }
            }

            @Override
            public void onFailure(Call<GetEvent> call, Throwable t) {

                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void UpdateSettings(String s) {

        APIService mAPIService = AppUtils.getAPIService(activity);
        Call<NotificationSimple> getApi;

        getApi = mAPIService.UpdateSettings(Constans.getFamilyId(activity),s);


        getApi.enqueue(new Callback<NotificationSimple>() {
            @Override
            public void onResponse(Call<NotificationSimple> call, retrofit2.Response<NotificationSimple> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());



            }

            @Override
            public void onFailure(Call<NotificationSimple> call, Throwable t) {


                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    public void filterList(ArrayList<ModelUserChat> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ChatsListHolder extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        LinearLayout lyt_parent;
        CircleImageView team_img;

        public ChatsListHolder(View view) {
            super(view);
            sport_name = view.findViewById(R.id.content);
            team_img = view.findViewById(R.id.image);
            team_name = view.findViewById(R.id.title);
            lyt_parent = view.findViewById(R.id.lyt_parent);


        }
    }
}

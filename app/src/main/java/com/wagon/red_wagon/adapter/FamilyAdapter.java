package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

public class FamilyAdapter extends RecyclerView.Adapter<FamilyAdapter.FamilyHolder> {
    FragmentActivity activity;
    List<Body> responseBody;
    ArrayList<String> arrayList = new ArrayList();
    ArrayList<String> arrayListName = new ArrayList();
    private String image_path = "";

    public FamilyAdapter(FragmentActivity activity, List<Body> responseBody) {
        this.activity = activity;
        this.responseBody = responseBody;

    }

    @NonNull
    @Override
    public FamilyAdapter.FamilyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_list_row, parent, false);

        FamilyAdapter.FamilyHolder familyHolder = new FamilyAdapter.FamilyHolder(view);
        return familyHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final FamilyAdapter.FamilyHolder holder, final int position) {


        holder.event_name.setText(responseBody.get(position).getFirstName());
        holder.driver_phn.setText(responseBody.get(position).getEmail());

        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.group_img);


    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class FamilyHolder extends RecyclerView.ViewHolder {
        TextView event_name, sport_name, weekss, timestatus, driver_phn;
        CardView cardview;
        ImageView group_img;
        Button edit_driver_btn, join_btn;

        public FamilyHolder(View view) {
            super(view);

            /*select_invite = view.findViewById(R.id.select_invite);
            nameplayr = view.findViewById(R.id.nameplayr);
            nameteam_ = view.findViewById(R.id.nameteam_);
            image_team = view.findViewById(R.id.image_team);*/
            edit_driver_btn = view.findViewById(R.id.edit_driver_btn);
            event_name = view.findViewById(R.id.event_names);
            cardview = view.findViewById(R.id.cardview);
            weekss = view.findViewById(R.id.weeks);
            timestatus = view.findViewById(R.id.timestatus);
            group_img = view.findViewById(R.id.group_img);
            join_btn = view.findViewById(R.id.join_btn);
            driver_phn = view.findViewById(R.id.driver_phn);
            join_btn.setVisibility(View.GONE);
            edit_driver_btn.setVisibility(View.GONE);

        }
    }
}

package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;

public class StudentAtDetilAdapter extends RecyclerView.Adapter {
    FragmentActivity activity;

    public StudentAtDetilAdapter(FragmentActivity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipients_adapter, parent, false);

        MyHolderStudent myHolderStudent = new MyHolderStudent(view);
        return myHolderStudent;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    private class MyHolderStudent extends RecyclerView.ViewHolder {
        public MyHolderStudent(View view) {
            super(view);
        }
    }
}

package com.wagon.red_wagon.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Model_CarList;
import com.wagon.red_wagon.ui.fragments.EditCarDetails;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyVichleAdapter extends RecyclerView.Adapter<MyVichleAdapter.MyHolder> {
    FragmentActivity activity;
    ArrayList<Model_CarList> arrayList;
    String status;
    VehicleCallback callback;

    public MyVichleAdapter(FragmentActivity activity, ArrayList<Model_CarList> arrayList, VehicleCallback callback) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.callback = callback;

    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myvichel_adapter, parent, false);
        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, final int position) {
        holder.makename.setText(arrayList.get(position).getMake());
        holder.reg_number.setText("REG: " + arrayList.get(position).getRegNumber().toUpperCase());
        status = arrayList.get(position).getStatus();
        if (status.equals("1")) {
            holder.available.setText("Available");

            holder.available.setTextColor(activity.getResources().getColor(R.color.appclrcodew));
        } else {
            holder.available.setText("Unavailable");

            holder.available.setTextColor(activity.getResources().getColor(R.color.gray));
        }
        holder.available.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (arrayList.get(position).getStatus().equals("1")) {
                   /* arrayList.get(position).setStatus("2");
                    notifyItemChanged(position);*/
                    callback.makeAvailable(arrayList.get(position).getId(), "2", position);

                } else {
                   /* arrayList.get(position).setStatus("1");
                    notifyItemChanged(position);*/
                    callback.makeAvailable(arrayList.get(position).getId(), "1", position);
                }
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    ApiDelete(arrayList.get(position).getId().toString(), holder.getAdapterPosition());
                                } catch (Exception w3) {

                                }

//                                    getActivity().finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditCarDetails edit_carDetails = new EditCarDetails();
                Bundle bundle = new Bundle();
                bundle.putString("Id", arrayList.get(position).getId());
                edit_carDetails.setArguments(bundle);
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, edit_carDetails, "Edit Details");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView makename, reg_number, remove, available;

        public MyHolder(View view) {
            super(view);
            makename = view.findViewById(R.id.makename);
            reg_number = view.findViewById(R.id.reg_number);
            available = view.findViewById(R.id.available);
            remove = view.findViewById(R.id.remove);

        }
    }

    public interface VehicleCallback{

         void makeAvailable(String id, String status, int pos);
    }


    private void ApiDelete(String id, final int position) {
        final Dialog dialog = AppUtils.showProgress(activity);
        Call<AddEvent> addEventCall;
        addEventCall = AppUtils.getAPIService(activity).deletecar(id, Constans.getFamilyId(activity));
        addEventCall.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                if (response.code() == 200) {
                    delete(position);
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void delete(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }
}

package com.wagon.red_wagon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.wagon.red_wagon.ui.fragments.ShowallPlayer.add_plyerlist;

public class ShowallPlayerAdapter extends RecyclerView.Adapter<ShowallPlayerAdapter.ShowHolder> {

    List<Body> responseBody;

    FragmentActivity activity;

    ArrayList<String> arrayplyerId = new ArrayList();
    ArrayList<String> arrayteamId = new ArrayList();
    RecipientsCallBack callback;
    String from;

    public ShowallPlayerAdapter(FragmentActivity activity, List<Body> responseBody, RecipientsCallBack showallPlayer, String from) {

        this.activity = activity;
        this.from = from;

        this.responseBody = responseBody;
        this.callback = showallPlayer;

    }

    @NonNull
    @Override
    public ShowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipients_adapter, parent, false);

        ShowHolder showHolder = new ShowHolder(view);

        return showHolder;


    }

    @Override
    public void onBindViewHolder(@NonNull final ShowHolder holder, final int position) {

        if (from.equals("Students")) {
            holder.nameteam_.setText(responseBody.get(position).getEmail());
            Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.image_team);
            holder.nameplayr.setText(responseBody.get(position).getFirstName());

        } else {
            holder.nameteam_.setText(responseBody.get(position).getTeamName());

            holder.nameplayr.setText(responseBody.get(position).getFirstName());

            Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getPlayerImage()).into(holder.image_team);
        }

        holder.select_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.select_invite.isChecked()) {
                    if (from.equals("Students")) {
                        arrayplyerId.add(responseBody.get(position).getId().toString());

                    } else {
                        arrayplyerId.add(responseBody.get(position).getPlayerId());

                    }
                    Log.e("arrayList", "onBindViewHolder: " + arrayplyerId);
                } else {


                    if (from.equals("Students")) {

                        if (arrayplyerId.contains(responseBody.get(position).getId().toString())) {
                            arrayplyerId.remove(responseBody.get(position).getId().toString());
                        }
                    } else {
                        if (arrayplyerId.contains(responseBody.get(position).getPlayerId())) {
                            arrayplyerId.remove(responseBody.get(position).getPlayerId());
                        }

                    }


                }

            }
        });


        add_plyerlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrayplyerId.size() != 0) {
                    callback.onMethodCallback(arrayplyerId);
                } else {
                    Toast.makeText(activity, "Please select Players", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public interface RecipientsCallBack {
        void onMethodCallback(ArrayList<String> arrayplyerId);


    }

    @Override
    public int getItemCount() {

        return responseBody.size();

    }

    public class ShowHolder extends RecyclerView.ViewHolder {

        CheckBox select_invite;

        TextView nameplayr, nameteam_;

        CircleImageView image_team;

        public ShowHolder(View view) {

            super(view);

            select_invite = view.findViewById(R.id.select_invite);

            nameplayr = view.findViewById(R.id.nameplayr);

            nameteam_ = view.findViewById(R.id.nameteam_);

            image_team = view.findViewById(R.id.image_team);
        }
    }
}


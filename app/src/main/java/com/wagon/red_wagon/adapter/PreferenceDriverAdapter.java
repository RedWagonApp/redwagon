package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.model.DriverBody;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PreferenceDriverAdapter extends RecyclerView.Adapter<PreferenceDriverAdapter.MyHolderDriverPrefrence> {
    FragmentActivity activity;
    List<DriverBody> bodyList;

    public PreferenceDriverAdapter(FragmentActivity activity, List<DriverBody> bodyList) {
        this.activity = activity;
        this.bodyList = bodyList;
    }

    @NonNull
    @Override
    public MyHolderDriverPrefrence onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.carpool_list_adapter, parent, false);

        MyHolderDriverPrefrence myHolderDriverPrefrence = new MyHolderDriverPrefrence(view);

        return myHolderDriverPrefrence;

    }

    @Override
    public void onBindViewHolder(@NonNull MyHolderDriverPrefrence holder, int position) {
        holder.groupName.setText(bodyList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return bodyList.size();
    }

    public class MyHolderDriverPrefrence extends RecyclerView.ViewHolder {
        TextView groupName, sport_name;
        CardView cardview;
        ImageView imageView;
        LinearLayout driver_list_lay, driver_name_lay, join_com;
        CircleImageView group_img;

        public MyHolderDriverPrefrence(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            groupName = view.findViewById(R.id.group_name);
            driver_list_lay = view.findViewById(R.id.driver_list_lay);
            driver_name_lay = view.findViewById(R.id.driver_name_lay);
            join_com = view.findViewById(R.id.join_com);
            join_com.setVisibility(View.GONE);
        }
    }
}

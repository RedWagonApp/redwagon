package com.wagon.red_wagon.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.StudentAttendanceDetails;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AttendenceListAdapter extends RecyclerView.Adapter<AttendenceListAdapter.AttendenceListHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String from;

    public AttendenceListAdapter(FragmentActivity activity, List<Body> arrayList, String from) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.from = from;
    }

    @NonNull
    @Override
    public AttendenceListAdapter.AttendenceListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendence_list_row, parent, false);

        AttendenceListAdapter.AttendenceListHolder community_adapter = new AttendenceListAdapter.AttendenceListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull AttendenceListAdapter.AttendenceListHolder holder, final int position) {
        //holder.player_name.setText(team_name);
        if (from.equals("Classes")) {
            holder.view_name.setText(arrayList.get(position).getClassname());
        }
        else {
            holder.view_name.setText(arrayList.get(position).getName());
        }
        Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.image_atten);
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudentAttendanceDetails eventListFragment = new StudentAttendanceDetails();
                Bundle args = new Bundle();
                args.putString("fromType", "show");
                args.putString("dateRange", arrayList.get(position).getDateRange());
                args.putString("teamId", arrayList.get(position).getTeamId());
                args.putString("teamName", arrayList.get(position).getName());
                args.putString("eventId", arrayList.get(position).getId().toString());
                eventListFragment.setArguments(args);

                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.school_dashbord_fram, eventListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class AttendenceListHolder extends RecyclerView.ViewHolder {
        TextView view_name, sport_name;
        CardView cardview;
        CircleImageView image_atten;

        public AttendenceListHolder(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            view_name = view.findViewById(R.id.view_name);
            image_atten = view.findViewById(R.id.image_atten);


        }
    }


}

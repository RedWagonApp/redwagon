package com.wagon.red_wagon.adapter;

import android.app.Dialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.ModelAllChirty;
import com.wagon.red_wagon.ui.fragments.CongFamily;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyChirtyListAdapter extends RecyclerView.Adapter<MyChirtyListAdapter.MyHolderView> {

    FragmentActivity fragmentActivity;

    List<ModelAllChirty> modelAllChirties;

    public MyChirtyListAdapter(FragmentActivity activity, List<ModelAllChirty> modelAllChirties) {
        this.fragmentActivity = activity;
        this.modelAllChirties = modelAllChirties;
    }

    @NonNull
    @Override
    public MyHolderView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mychirty_adapter, parent, false);

        return new MyHolderView(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyHolderView holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateCharty(modelAllChirties.get(position).getCharityId());
            }
        });

        holder.chartity_name.setText(modelAllChirties.get(position).getUrl());


        Glide.with(fragmentActivity)
                .load(Constans.BASEURL+ modelAllChirties.get(position).getCharityName())
                .into(holder.logo_chirty);
    }

    @Override
    public int getItemCount() {
        return modelAllChirties.size();
    }

    public class MyHolderView extends RecyclerView.ViewHolder {
        TextView chartity_name;
        ImageView logo_chirty;

        MyHolderView(View view) {
            super(view);
            logo_chirty = view.findViewById(R.id.logo_chirty);
            chartity_name = view.findViewById(R.id.chartity_name);
        }
    }
    private void UpdateCharty(String ChartyId) {
        final Dialog dialog = AppUtils.showProgress(fragmentActivity);
        APIService mAPIService = AppUtils.getAPIService(fragmentActivity);

        mAPIService.updateCharityId(Constans.getFamilyId(fragmentActivity), ChartyId).enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {

                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                try {
                    if (response.code() == 200) {
                        FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.chirtylist, new CongFamily());
                        transaction.addToBackStack(null);
                        transaction.commit();

                    }
                } catch (Exception w) {

                }
                if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(fragmentActivity, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    }
                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(fragmentActivity);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", "onFailure: " + t);
                Toast.makeText(fragmentActivity, "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

}

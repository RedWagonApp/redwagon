package com.wagon.red_wagon.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.wagon.red_wagon.ui.activities.LogInActivity.TAG;

public class CommunityAdapter extends RecyclerView.Adapter<CommunityAdapter.MyHolderC> {
    FragmentActivity activity;
    List<Body> arrayList;
    String from, type;
    List<Body> responseBody;
    ArrayList<String> slider_image_list = new ArrayList<>();
    ArrayList<String> slider_type_list = new ArrayList<>();
    private static final String DEEP_LINK_URL = "https://wagon.page.link/community";
    int page = 0;
    public HashMap<Integer, Integer> viewPageStates = new HashMap<>();
    private String ProductLinkInBrowser = "";

    public CommunityAdapter(FragmentActivity activity, List<Body> responseBody, String fromType) {
        this.activity = activity;
        this.arrayList = responseBody;
        this.from = fromType;

    }


    @NonNull
    @Override
    public MyHolderC onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.community_image_adapter, parent, false);
        MyHolderC community_adapter = new MyHolderC(view);
        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolderC holder, int position) {

        holder.name.setText(arrayList.get(position).getName());

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        Date date = null;
        try {
            date = dateFormat.parse(arrayList.get(position).getDateTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm"); //If you need time just put specific format for time like 'HH:mm:ss'
        String dateStr = formatter.format(date);
        holder.time.setText("Please join us : " + dateStr + "\n" + "Address: " + arrayList.get(position).getAddress());


        //Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.image_event);
        String images = (arrayList.get(holder.getAdapterPosition()).getEventImage() != null ? arrayList.get(holder.getAdapterPosition()).getEventImage() : "");
        if (!images.equals("")) {
            slider_image_list = new ArrayList<String>(Arrays.asList(images.split(",")));
            slider_type_list = new ArrayList<String>(Arrays.asList(arrayList.get(position).getType_img().split(",")));
            type = slider_type_list.get(0);
        } else {
            slider_image_list = new ArrayList<>();
            slider_image_list.add(arrayList.get(holder.getAdapterPosition()).getProfileImage() != null ? arrayList.get(holder.getAdapterPosition()).getProfileImage() : "");

            type = arrayList.get(position).getType_img() != null ? arrayList.get(position).getType_img() : "1";
        }
        final SliderPagerAdapter sliderPagerAdapter = new SliderPagerAdapter(activity, slider_image_list, type);
        holder.image_event.setAdapter(sliderPagerAdapter);
        holder.image_event.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                page = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        addBottomDots(page, holder.ll_dots);
        //Glide.with(activity).load(arrayList.get(position).getProfileImage()).into(holder.image_event);

        holder.share_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildDeepLink(Uri.parse(DEEP_LINK_URL), arrayList.get(position).getId().toString(), 0);
            }
        });


        holder.joinpool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventcarpoolFrag eventcarpoolFrag = new EventcarpoolFrag();
                Bundle bundle = new Bundle();
                bundle.putString("getId", String.valueOf(arrayList.get(position).getId()));
                eventcarpoolFrag.setArguments(bundle);
                FragmentManager fragmentManager;
                fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash, eventcarpoolFrag, "Carpools");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void filterList(List<Body> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    public void buildDeepLink(@NonNull Uri deepLink, String userid, int minVersion) {
//build link normally and add queries like a normal href link would
        String permLink = DEEP_LINK_URL + "/?userid=" + userid;


        String uriPrefix = "/community";

        // Set dynamic link parameters:
        //  * URI prefix (required)
        //  * Android Parameters (required)
        //  * Deep link
        // [START build_dynamic_link]
        Task<ShortDynamicLink> builder = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix("https://wagon.page.link")
                .setLink(Uri.parse("https://www.redwagon.com/?userid=" + userid))
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder("com.wagon.red_wagon")
                                .setMinimumVersion(0)
                                .build())
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT).addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {

                        if (task.isSuccessful()) {
                            Uri shortURL = task.getResult().getShortLink();
                            shareDeepLink(shortURL.toString());
                        } else {
                            Toast.makeText(activity, "error", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        //  shareDeepLink(permLink);
        // Build the dynamic link

    }

    private Uri createShareUri(String saladId) {

        Uri.Builder builder = new Uri.Builder();

        builder.scheme("http") // "http"

                .authority("redwagon.com") // "365salads.xyz"

                .appendPath("/community") // "salads"

                .appendQueryParameter("userid", saladId);

        return builder.build();

    }

    private void shareDeepLink(String deepLink) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link");
        intent.putExtra(Intent.EXTRA_TEXT, deepLink);

        activity.startActivity(intent);
    }


    private void attachParameter(String userid) {
        Uri link = Uri.parse(DEEP_LINK_URL)
                .buildUpon()
                .appendQueryParameter("userid", userid)
                .build();
        generateDynamicLink(link.toString());
    }

    private void generateDynamicLink(String link) {
        Uri dynamicLink = Uri.parse("http://xodev.net/RW/")
                .buildUpon()
                .appendQueryParameter("link", link)
                //.appendQueryParameter("apn", "c.kristofer.jax2")
                .build();

        Log.e(TAG, "generateDynamicLink: " + dynamicLink.toString());
    }


    public class MyHolderC extends RecyclerView.ViewHolder {
        private VideoView vv;
        private MediaController mediacontroller;
        ViewPager image_event;
        private LinearLayout ll_dots, share_com, joinpool;
        TextView name, time;

        public MyHolderC(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            time = view.findViewById(R.id.time);
            image_event = view.findViewById(R.id.image_event);
            share_com = view.findViewById(R.id.share_com);
            ll_dots = view.findViewById(R.id.ll_dots);
            joinpool = view.findViewById(R.id.joinpool);

        }
    }

    //showing dots on screen
    private void addBottomDots(int currentPage, LinearLayout ll_dots) {
        TextView[] dots = new TextView[slider_image_list.size()];
        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(activity);
            dots[i].setText(Html.fromHtml("•"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#A2A2A2"));
            ll_dots.addView(dots[i]);
        }
        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#343434"));
    }


}

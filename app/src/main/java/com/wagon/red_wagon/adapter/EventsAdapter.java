package com.wagon.red_wagon.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.EventModel;
import com.wagon.red_wagon.ui.activities.CalendarTeamDetailAcrivity;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsAdapterHolder> {
    FragmentActivity activity;
    List<EventModel> eventModels;
    String from, nameStr;
    private EventsAdapter.AdapterCallback mAdapterCallback;

    public EventsAdapter(FragmentActivity activity, List<EventModel> eventModels, String from) {

        this.activity = activity;
        this.eventModels = eventModels;
        this.from = from;


    }

    @NonNull
    @Override
    public EventsAdapter.EventsAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendence_list_row
                , parent, false);

        EventsAdapter.EventsAdapterHolder community_adapter = new EventsAdapter.EventsAdapterHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final EventsAdapter.EventsAdapterHolder holder, final int position) {

        holder.view_name.setText(eventModels.get(position).getEventName());
        Glide.with(activity).load(Constans.BASEURL + eventModels.get(position).getEventImg()).into(holder.image);

        if (from.equals("Family")) {
            holder.contact_number.setVisibility(View.VISIBLE);
            holder.weeks_days.setVisibility(View.VISIBLE);
            holder.contact_number.setText(eventModels.get(position).getPhnNum());

            switch (eventModels.get(position).getWeeks()) {
                case "1":
                    holder.weeks_days.setText("Pickup");
                    break;
                case "2":
                    holder.weeks_days.setText("Drop");
                    break;
                case "3":
                    holder.weeks_days.setText("Return");
                    break;

            }
        }

        if (eventModels.get(position).getTeamId() != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.startActivity(new Intent(activity, CalendarTeamDetailAcrivity.class)
                            .putExtra("teamName", eventModels.get(position).getEventName())
                            .putExtra("teamId", eventModels.get(position).getTeamId()));
                }
            });

        }
    }

    public interface AdapterCallback {
        void onMethodCallback(String name, int img);
    }

    @Override
    public int getItemCount() {
        return eventModels.size();
    }

    public class EventsAdapterHolder extends RecyclerView.ViewHolder {
        TextView view_name, contact_number, weeks_days;
        CardView cardview;
        CircleImageView image;


        public EventsAdapterHolder(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            view_name = view.findViewById(R.id.view_name);
            image = view.findViewById(R.id.image_atten);
            contact_number = view.findViewById(R.id.contact_number);
            weeks_days = view.findViewById(R.id.weeks_days);


        }
    }

}

package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChildCarpoolAdapter extends RecyclerView.Adapter<ChildCarpoolAdapter.MyChildPool> {
    FragmentActivity activity;
    List<Body> responseBody;
    String getGetEventName;

    public ChildCarpoolAdapter(FragmentActivity activity, List<Body> responseBody, String getGetEventName) {

        this.getGetEventName = getGetEventName;
        this.activity = activity;
        this.responseBody = responseBody;


    }

    @NonNull
    @Override
    public MyChildPool onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_fm, parent, false);

        MyChildPool myChildPool = new MyChildPool(view);

        return myChildPool;
    }

    @Override
    public void onBindViewHolder(@NonNull MyChildPool holder, int position) {
        holder.team_name.setText(responseBody.get(position).getFirstName());
        holder.sport_name.setText(getGetEventName);
        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.team_img);

    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MyChildPool extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        CardView cardview;
        CircleImageView team_img;
        ImageView edit_btn;
        LinearLayout delete_btn_;

        public MyChildPool(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            team_name = view.findViewById(R.id.team_name);
            sport_name = view.findViewById(R.id.sport_name);
            team_img = view.findViewById(R.id.team_img);
            edit_btn = view.findViewById(R.id.edit_btn);
            delete_btn_ = view.findViewById(R.id.delete_btn_);
        }
    }
}

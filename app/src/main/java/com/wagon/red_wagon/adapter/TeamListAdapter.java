package com.wagon.red_wagon.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.EditTeamFragment;
import com.wagon.red_wagon.ui.fragments.PlayerListFrag;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamListAdapter extends RecyclerView.Adapter<TeamListAdapter.TeamListHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String value = "";
    View view;

    public TeamListAdapter(FragmentActivity activity, List<Body> arrayList, String value) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.value = value;
    }

    @NonNull
    @Override
    public TeamListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (value.equals("1")) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_fm, parent, false);

        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_row, parent, false);
        }
        TeamListHolder teamListAdapter = new TeamListHolder(view);

        return teamListAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull TeamListHolder holder, final int position) {
        holder.team_name.setText(arrayList.get(position).getTeamName());
        holder.sport_name.setText(arrayList.get(position).getSport());
        if (!arrayList.get(position).getProfileImage().equals("")) {
            Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.team_img);
        }

        holder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTeamFragment fragment = new EditTeamFragment();
                Bundle args = new Bundle();
                args.putString("fromType", "edit");
                args.putString("id", arrayList.get(position).getId().toString());
                fragment.setArguments(args);
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frame_team, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(value.equals("2")) {
                        PlayerListFrag eventListFragment = new PlayerListFrag();
                        Bundle args = new Bundle();
                        Constans.TeamId = arrayList.get(position).getId().toString();
                        Constans.MangerId = arrayList.get(position).getManagerId().toString();
                        args.putString("fromType", "Player");
                        args.putString("typeshow", "2");
                        args.putString("manegerID", Constans.MangerId);
                        args.putString("teamId", Constans.TeamId);
                        eventListFragment.setArguments(args);
                        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_team, eventListFragment, "Team");
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }else {
                        PlayerListFrag eventListFragment = new PlayerListFrag();
                        Bundle args = new Bundle();
                        Constans.TeamId = arrayList.get(position).getId().toString();
                        Constans.MangerId = arrayList.get(position).getManagerId().toString();
                        args.putString("fromType", "Player");
                        args.putString("typeshow", "1");
                        args.putString("teamname",arrayList.get(position).getTeamName());
                        args.putString("teamId",Constans.TeamId);
                        args.putString("manegerID",Constans.MangerId);
                        args.putString("teamImage",arrayList.get(position).getProfileImage());
                        eventListFragment.setArguments(args);
                        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fram_dash, eventListFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
            });



        holder.delete_btn_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    ApiDelete(arrayList.get(position).getId().toString(), holder.getAdapterPosition());
                                } catch (Exception w3) {

                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();


            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class TeamListHolder extends RecyclerView.ViewHolder {
        TextView team_name, sport_name;
        CardView cardview;
        CircleImageView team_img;
        ImageView edit_btn;
        LinearLayout delete_btn_;

        public TeamListHolder(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            team_name = view.findViewById(R.id.team_name);
            sport_name = view.findViewById(R.id.sport_name);
            team_img = view.findViewById(R.id.team_img);
            edit_btn = view.findViewById(R.id.edit_btn);
            delete_btn_ = view.findViewById(R.id.delete_btn_);

        }
    }

    private void ApiDelete(String id, final int position) {
        final Dialog dialog = AppUtils.showProgress(activity);
        Call<AddEvent> addEventCall;
        addEventCall = AppUtils.getAPIService(activity).deleteTeam(id);


        addEventCall.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                if (response.code() == 200) {
                    delete(position);
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void delete(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }


}
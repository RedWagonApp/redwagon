package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DriverAllAdapter extends RecyclerView.Adapter<DriverAllAdapter.MyHolderDriver> {
    FragmentActivity activity;
    ArrayList<String> driverNames;
    ArrayList<String> driverImages;

    public DriverAllAdapter(FragmentActivity activity, ArrayList<String> driverNames, ArrayList<String> driverImages) {
        this.activity = activity;
        this.driverNames = driverNames;
        this.driverImages = driverImages;

    }

    @NonNull
    @Override
    public DriverAllAdapter.MyHolderDriver onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alldriver_show, parent, false);
        MyHolderDriver myHolderDriver = new MyHolderDriver(view);

        return myHolderDriver;
    }

    @Override
    public void onBindViewHolder(@NonNull DriverAllAdapter.MyHolderDriver holder, int position) {
        holder.driver_name.setText(driverNames.get(position));
        Glide.with(activity).load(Constans.BASEURL + driverImages.get(position)).placeholder(R.drawable.profile_ic).into(holder.driver_img);
    }

    @Override
    public int getItemCount() {
        return driverNames.size();
    }

    public class MyHolderDriver extends RecyclerView.ViewHolder {
        TextView driver_name;
        CircleImageView driver_img;

        public MyHolderDriver(View view) {
            super(view);
            driver_name = view.findViewById(R.id.driver_name);
            driver_img = view.findViewById(R.id.driver_img);
        }
    }
}

package com.wagon.red_wagon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.FlagDetail;

import java.util.List;

public class FlagsAdapter extends RecyclerView.Adapter<FlagsAdapter.FlagsAdapterHolder> {
    Context activity;
    List<FlagDetail> arrayList;
    String from, nameStr;
    private FlagsAdapter.AdapterCallback mAdapterCallback;

    public FlagsAdapter(Context activity, List<FlagDetail> arrayList, FlagsAdapter.AdapterCallback callback) {
        this.mAdapterCallback = callback;
        this.activity = activity;
        this.arrayList = arrayList;


    }

    @NonNull
    @Override
    public FlagsAdapter.FlagsAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.flag_row
                , parent, false);

        FlagsAdapter.FlagsAdapterHolder community_adapter = new FlagsAdapter.FlagsAdapterHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final FlagsAdapter.FlagsAdapterHolder holder, final int position) {

        holder.flag_name.setText(arrayList.get(position).getFlagName());
        holder.flag_img.setImageResource(arrayList.get(position).getFlagImage());


        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onMethodCallback(arrayList.get(position).getFlagName(), arrayList.get(position).getFlagImage());
            }
        });
    }

    public interface AdapterCallback {
        void onMethodCallback(String name, int img);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class FlagsAdapterHolder extends RecyclerView.ViewHolder {
        TextView flag_name, sport_name;
        CardView cardview;
        ImageView flag_img;

        public FlagsAdapterHolder(View view) {
            super(view);
            flag_name = view.findViewById(R.id.flag_name);
            flag_img = view.findViewById(R.id.flag_image);
            cardview = view.findViewById(R.id.flag_lay);

//            event_time_d = view.findViewById(R.id.event_time_d);
//
//            event_time_devent_location_time = view.findViewById(R.id.event_location_time);


        }
    }
}
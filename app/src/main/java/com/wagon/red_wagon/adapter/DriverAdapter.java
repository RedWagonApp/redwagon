package com.wagon.red_wagon.adapter;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.DriverModel;
import com.wagon.red_wagon.ui.fragments.AddChildToPoolFrag;
import com.wagon.red_wagon.ui.fragments.ChildListCarpoolFrag;
import com.wagon.red_wagon.ui.fragments.EditCarPoolDriverFrag;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

import static com.wagon.red_wagon.ui.fragments.DriverListFrag.eventId;

public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.DriverAdapterHolder> {
    FragmentActivity activity;
    ArrayList<String> namearrayList, idArrayList, weeks;
    String from, value = "";
    List<DriverModel> responseBody;

    public DriverAdapter(FragmentActivity activity, List<DriverModel> responseBody, String value) {

        this.activity = activity;
        this.value = value;
        this.idArrayList = idArrayList;
        this.weeks = weeks;
        this.responseBody = responseBody;


    }

    @NonNull
    @Override
    public DriverAdapter.DriverAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_list_row
                , parent, false);

        DriverAdapter.DriverAdapterHolder community_adapter = new DriverAdapter.DriverAdapterHolder(view);

        return community_adapter;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull final DriverAdapter.DriverAdapterHolder holder, final int position) {


        Log.e("idArrayList", "onBindViewHolder: " +responseBody.get(position).getGetEventName());

        if (value.equals("1")) {
            holder.join_btn.setVisibility(View.GONE);
            holder.edit_driver_btn.setVisibility(View.GONE);
            holder.btn_lay.setVisibility(View.GONE);
        }
        else{
            holder.join_btn.setVisibility(View.VISIBLE);
            holder.edit_driver_btn.setVisibility(View.VISIBLE);
            holder.btn_lay.setVisibility(View.VISIBLE);
        }
        String getUserId = responseBody.get(position).getDriverId();
        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImg()).placeholder(R.drawable.profile_ic).into(holder.group_img);
        holder.event_name.setText(responseBody.get(position).getDriverName());


        holder.driver_phn.setText(responseBody.get(position).getDriverPhone());
        holder.weekss.setText(responseBody.get(position).getPickupDate());


        if (responseBody.get(position).getType().equals("Driver")||responseBody.get(position).getType().equals("Manager") && value.equals("2")) {
            holder.join_btn.setVisibility(View.VISIBLE);
            holder.edit_driver_btn.setVisibility(View.VISIBLE);
        } else {
            holder.join_btn.setVisibility(View.GONE);
            holder.edit_driver_btn.setVisibility(View.GONE);
        }
        switch (responseBody.get(position).getStatus()) {
            case "1":
                holder.timestatus.setText("Pickup");
                break;
            case "2":
                holder.timestatus.setText("Drop");
                break;
            case "3":
                holder.timestatus.setText("Return");
                break;

        }


        holder.edit_driver_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditCarPoolDriverFrag drivertList = new EditCarPoolDriverFrag();
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("weeks", responseBody.get(position).getWeekDays());
                bundle.putString("driverId", responseBody.get(position).getDriverId());
                bundle.putString("driverName", responseBody.get(position).getDriverName());
                bundle.putString("eventID", eventId);
                bundle.putString("carpoolid", responseBody.get(position).getCarPoolId());
                drivertList.setArguments(bundle);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fram_dash, drivertList, "Edit");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChildListCarpoolFrag childListCarpoolFrag = new ChildListCarpoolFrag();
                Bundle bundle = new Bundle();
                bundle.putString("driverId", responseBody.get(position).getDriverId());
                bundle.putString("driverName", responseBody.get(position).getDriverName());
                bundle.putString("getGetEventName", responseBody.get(position).getGetEventName());
                bundle.putString("eventID", eventId);
                bundle.putString("date", responseBody.get(position).getPickupDate());
                bundle.putString("profileImage", responseBody.get(position).getProfileImg());
                bundle.putString("carpoolid", responseBody.get(position).getCarPoolId());
                childListCarpoolFrag.setArguments(bundle);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fram_dash, childListCarpoolFrag, "Child to Pool");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });





        holder.join_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddChildToPoolFrag drivertList = new AddChildToPoolFrag();
                Bundle bundle = new Bundle();
                bundle.putString("driverId", responseBody.get(position).getDriverId());
                bundle.putString("driverName", responseBody.get(position).getDriverName());
                bundle.putString("eventID", eventId);
                bundle.putString("date", responseBody.get(position).getPickupDate());
                bundle.putString("profileImage", responseBody.get(position).getProfileImg());
                bundle.putString("carpoolid", responseBody.get(position).getCarPoolId());
                drivertList.setArguments(bundle);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fram_dash, drivertList, "Add Child to Pool");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }


    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class DriverAdapterHolder extends RecyclerView.ViewHolder {
        TextView event_name, sport_name, weekss, timestatus, driver_phn;
        CardView cardview;
        ImageView group_img;
        Button edit_driver_btn, join_btn;
LinearLayout btn_lay;
        public DriverAdapterHolder(View view) {
            super(view);
            edit_driver_btn = view.findViewById(R.id.edit_driver_btn);
            event_name = view.findViewById(R.id.event_names);
            cardview = view.findViewById(R.id.cardview);
            weekss = view.findViewById(R.id.weeks);
            timestatus = view.findViewById(R.id.timestatus);
            group_img = view.findViewById(R.id.group_img);
            join_btn = view.findViewById(R.id.join_btn);
            driver_phn = view.findViewById(R.id.driver_phn);
            btn_lay = view.findViewById(R.id.btn_lay);


//            event_time_d = view.findViewById(R.id.event_time_d);
//
//            event_time_devent_location_time = view.findViewById(R.id.event_location_time);


        }
    }
}
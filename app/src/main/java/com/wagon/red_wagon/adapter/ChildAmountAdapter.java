package com.wagon.red_wagon.adapter;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.stripe.android.view.CardInputWidget;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.ChildFundTransferFrag;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChildAmountAdapter extends RecyclerView.Adapter<ChildAmountAdapter.MychildHOlder> {
    FragmentActivity activity;
    List<Body> responseBody;
    EditText payment, cardname;
    AlertDialog dialoge;
    CardInputWidget mCardInputWidget;
    String card_number = "", stripeToken = "";
    private int exp_date, exp_month;

    public ChildAmountAdapter(FragmentActivity activity, List<Body> responseBody) {
        this.activity = activity;
        this.responseBody = responseBody;
    }

    @NonNull
    @Override
    public MychildHOlder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_fm, parent, false);
        MychildHOlder mychildHOlder = new MychildHOlder(view);

        return mychildHOlder;

    }

    @Override
    public void onBindViewHolder(@NonNull MychildHOlder holder, int position) {
        holder.team_name.setText(responseBody.get(position).getFirstName() + " " + responseBody.get(position).getLastName());
        holder.sport_name.setText("Amount: $" + responseBody.get(position).getAmount());
        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.team_img);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Apiservice(responseBody.get(position).getId().toString());
                ChildFundTransferFrag childFoundTransferFrag=new ChildFundTransferFrag();
                Bundle bundle=new Bundle();
                bundle.putString("childId",responseBody.get(position).getId().toString());
                bundle.putString("amount",responseBody.get(position).getAmount());
                childFoundTransferFrag.setArguments(bundle);
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fram_dash,childFoundTransferFrag,"Child Found Amount");

                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }




    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MychildHOlder extends RecyclerView.ViewHolder {
        CircleImageView team_img;
        TextView team_name, sport_name;

        public MychildHOlder(View view) {
            super(view);

            sport_name = view.findViewById(R.id.sport_name);
            team_name = view.findViewById(R.id.team_name);
            team_img = view.findViewById(R.id.team_img);
        }
    }
}

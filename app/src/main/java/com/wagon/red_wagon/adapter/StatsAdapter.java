package com.wagon.red_wagon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;

import java.util.ArrayList;
import java.util.List;

public class StatsAdapter extends RecyclerView.Adapter<StatsAdapter.ViewHolder> {

    RecyclerView.RecycledViewPool sharedPool = new RecyclerView.RecycledViewPool();
    private ArrayList<List<String>> arrayList;
    private Context context;

    public StatsAdapter(ArrayList<List<String>> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stats_main_adapter_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        StatsHorizontalAdapter statsHorizontalAdapter = new StatsHorizontalAdapter(context, arrayList.get(i));
//        viewHolder.rcv_main_stats.setLayoutManager(new CustomGridLayoutManager(context, LinearLayoutManager.HORIZONTAL, false, true));
//        viewHolder.rcv_main_stats.setAdapter(statsHorizontalAdapter);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView rcv_main_stats;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rcv_main_stats = itemView.findViewById(R.id.rcv_main_stats);
        }
    }
}

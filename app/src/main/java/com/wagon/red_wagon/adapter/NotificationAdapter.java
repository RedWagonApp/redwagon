package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.NotificationBody;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.Notification_Holder> {
    FragmentActivity activity;
    List<NotificationBody> responseBody;

    public NotificationAdapter(FragmentActivity activity, List<NotificationBody> responseBody) {
        this.activity = activity;
        this.responseBody = responseBody;
    }


    @NonNull
    @Override
    public Notification_Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_adapter, parent, false);

        Notification_Holder notification_adapter = new Notification_Holder(view);
        return notification_adapter;
    }


    @Override
    public void onBindViewHolder(@NonNull Notification_Holder holder, int position) {
        holder.notification_msg.setText(responseBody.get(position).getReason());
    }


    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class Notification_Holder extends RecyclerView.ViewHolder {

        TextView notification_msg;

        public Notification_Holder(View view) {
            super(view);
            notification_msg = view.findViewById(R.id.notification_msg);

        }
    }
}

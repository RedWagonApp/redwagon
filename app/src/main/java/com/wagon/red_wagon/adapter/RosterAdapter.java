package com.wagon.red_wagon.adapter;

import android.app.Dialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

import static com.wagon.red_wagon.utils.AppUtils.emailValidator;


public class RosterAdapter extends RecyclerView.Adapter<RosterAdapter.RosterHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    List<String> name = new ArrayList<>();
    String nameuser, useremail, usernumber;
    RosterHolder community_adapter;
    String id;

    public RosterAdapter(FragmentActivity activity, String id) {
        this.activity = activity;
        this.id = id;
    }


    @NonNull
    @Override
    public RosterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.roster_manual_row, parent, false);

        community_adapter = new RosterHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull final RosterHolder holder, int position) {


        holder.send_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameuser = holder.roster_name.getText().toString();
                useremail = holder.roster_email.getText().toString();
                usernumber = holder.roster_num.getText().toString();

                if (!nameuser.equals("")) {

                    if (!useremail.equals("") && emailValidator(useremail)) {


                        if (!usernumber.equals("")) {
                            addRoster();

                        } else {

                            Toast.makeText(activity, "Please enter the number", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Toast.makeText(activity, "Please enter the valid email address", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(activity, "Please enter the name", Toast.LENGTH_SHORT).show();

                }
            }


        });


    }


    private void addRoster() {
        final Dialog dialog = AppUtils.showProgress(activity);
        APIService mAPIService = AppUtils.getAPIService(activity);
        Call<AddEvent> getApi;

        getApi = mAPIService.addRosterManually(id, nameuser, useremail, usernumber,Constans.getFamilyId(activity));


        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {

                    activity.onBackPressed();
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(activity);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                Log.e("Failure", "onFailure: ", t);
            }
        });


    }

    private void ApisendEmail() {

        final Dialog dialog = AppUtils.showProgress(activity);

        String url = Constans.BASEURL + "api/addRosterManually";
        Log.e("urllll", "Constans.BASEURL+" + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "response" + response);
                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(activity, "Invite sent successfully", Toast.LENGTH_SHORT).show();

                } catch (Exception r) {
                    Log.e("Exceptio", "Exccchekwe");
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);

                        Toast.makeText(activity, obj.getString("message"), Toast.LENGTH_SHORT).show();

                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Log.e("response", "onErrorResponse" + error.toString());
            }


        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("teamId", id);
                params.put("firstName", nameuser);
                params.put("email", useremail);
                params.put("phoneNumber", usernumber);

                Log.e("params", "params" + params);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                String bearer = "Bearer ".concat(Constans.getToken(activity));
                Map<String, String> headersSys = null;
                try {
                    headersSys = super.getHeaders();
                } catch (AuthFailureError authFailureError) {
                    authFailureError.printStackTrace();
                }
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                Log.e("headersSys", "sdsd" + headersSys);
                return headers;

            }
        };

//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("Content-Type", "application/json; charset=UTF-8");
//                        params.put("token", Constans.Token);
//                        return params;
//                    }

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }




    @Override
    public int getItemCount() {
        return 1;
    }

    public class RosterHolder extends RecyclerView.ViewHolder {
        EditText roster_num, roster_name, roster_email, event_close_time, event_time_d;
        Button send_email;

        public RosterHolder(View view) {
            super(view);
            roster_name = view.findViewById(R.id.roster_name);
            roster_email = view.findViewById(R.id.roster_email);

            send_email = view.findViewById(R.id.send_email);
            roster_num = view.findViewById(R.id.roster_num);


        }
    }
}

package com.wagon.red_wagon.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.ViewManagerFrag;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManagerAdapter extends RecyclerView.Adapter<ManagerAdapter.ManagerAdapterHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String value;

    public ManagerAdapter(FragmentActivity activity, List<Body> arrayList, String value) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.value = value;
    }


    @NonNull
    @Override
    public ManagerAdapter.ManagerAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.manager_adapter, parent, false);

        ManagerAdapter.ManagerAdapterHolder notification_adapter = new ManagerAdapter.ManagerAdapterHolder(view);
        return notification_adapter;
    }


    @Override
    public void onBindViewHolder(@NonNull ManagerAdapter.ManagerAdapterHolder holder, final int position) {
        holder.manager_name.setText(arrayList.get(position).getFirstName());
        holder.school_name.setText("School: "+arrayList.get(position).getSchoolName());
        Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).placeholder(R.drawable.imagesloder).into(holder.manager_img);
        holder.item_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewManagerFrag fragment = new ViewManagerFrag();
                Bundle args = new Bundle();
                args.putString("fromType", arrayList.get(position).getFirstName());
                args.putString("id", arrayList.get(position).getId().toString());
                fragment.setArguments(args);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frag_frame_man, fragment).addToBackStack(null).commit();


            }
        });

        holder.edit_btn_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewManagerFrag fragment = new ViewManagerFrag();
                Bundle args = new Bundle();
                args.putString("fromType", arrayList.get(position).getFirstName());
                args.putString("id", arrayList.get(position).getId().toString());
                args.putString("edit", "1");
                fragment.setArguments(args);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frag_frame_man, fragment).addToBackStack(null).commit();

            }
        });

        holder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ApiDelete(arrayList.get(position).getId().toString(), holder.getAdapterPosition());

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ManagerAdapterHolder extends RecyclerView.ViewHolder {

        TextView manager_name;
        TextView school_name;
        CircleImageView manager_img;
        LinearLayout item_manager, delete_btn;
        ImageView edit_btn_manager;

        public ManagerAdapterHolder(View view) {
            super(view);
            manager_name = view.findViewById(R.id.manager_name);
            school_name = view.findViewById(R.id.school_name);
            manager_img = view.findViewById(R.id.manager_img);
            item_manager = view.findViewById(R.id.item_manager);
            edit_btn_manager = view.findViewById(R.id.edit_btn_manager);
            delete_btn = view.findViewById(R.id.delete_btn);

            if (value.equals("2")) {
                edit_btn_manager.setVisibility(View.GONE);
                delete_btn.setVisibility(View.GONE);
            } else {
                edit_btn_manager.setVisibility(View.VISIBLE);
                delete_btn.setVisibility(View.VISIBLE);
            }
        }
    }

    private void delete(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }

    private void ApiDelete(String id, int position) {
        final Dialog dialog = AppUtils.showProgress(activity);
        Call<AddEvent> deleteApi;


        deleteApi = AppUtils.getAPIService(activity).deleteManager(id);


        deleteApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                if (response.code() == 200) {
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    delete(position);
                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

}


package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.wagon.red_wagon.ui.fragments.StudentAttendanceDetails.attandence;

public class AttendancePlayerAdapter extends RecyclerView.Adapter<AttendancePlayerAdapter.MyRecipients> {
    FragmentActivity activity;
    List<Body> responseBody;
    ArrayList<String> playersId;
    ArrayList<String> attendacestatus;
    private String image_path = "";
    AttendancePlayerAdapter.AttendanceCallBack callBack;

    public AttendancePlayerAdapter(FragmentActivity activity, List<Body> responseBody, ArrayList<String> playersId, ArrayList<String> attendacestatus, AttendancePlayerAdapter.AttendanceCallBack callBack) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.playersId = playersId;
        this.attendacestatus = attendacestatus;
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public AttendancePlayerAdapter.MyRecipients onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipients_adapter, parent, false);

        AttendancePlayerAdapter.MyRecipients myRecipients = new AttendancePlayerAdapter.MyRecipients(view);
        return myRecipients;
    }


    @Override
    public void onBindViewHolder(@NonNull final AttendancePlayerAdapter.MyRecipients holder, final int position) {


        holder.nameplayr.setText(responseBody.get(position).getFirstName());

        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).placeholder(R.drawable.ic_profile).into(holder.image_team);

        if (responseBody.get(position).getStatus().equals("2")) {
            holder.select_invite.setChecked(true);
        } else {
            holder.select_invite.setChecked(false);
        }
        holder.select_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.select_invite.isChecked()) {


                    attendacestatus.set(position, "1");


                } else {
                    attendacestatus.set(position, "2");
                }


            }
        });
        attandence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onMethodCallback(playersId, attendacestatus);
            }
        });

    }

    public interface AttendanceCallBack {
        void onMethodCallback(ArrayList<String> playersId, ArrayList<String> attendacestatus);
    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MyRecipients extends RecyclerView.ViewHolder {
        CheckBox select_invite;
        TextView nameplayr, nameteam_;
        CircleImageView image_team;
        RadioGroup radio_group;

        public MyRecipients(View view) {
            super(view);


            select_invite = view.findViewById(R.id.select_invite);
            nameplayr = view.findViewById(R.id.nameplayr);
            nameteam_ = view.findViewById(R.id.nameteam_);
            image_team = view.findViewById(R.id.image_team);


        }
    }
}

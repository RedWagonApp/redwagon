package com.wagon.red_wagon.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.CreateEventFrag;
import com.wagon.red_wagon.ui.fragments.EditActivityFrag;
import com.wagon.red_wagon.ui.fragments.EditClass;
import com.wagon.red_wagon.ui.fragments.EditGameFrag;
import com.wagon.red_wagon.ui.fragments.EditPlayerFrag;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String from;

    public EventAdapter(FragmentActivity activity, List<Body> arrayList, String from) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.from = from;
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_row, parent, false);

        return new EventHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, final int position) {
        holder.edit_btn.setBackgroundResource(R.drawable.edit);
        Log.e("Games", "onBindViewHolder:+ " + from);
        if (from.equals("Student")) {
            String firstName =arrayList.get(position).getFirstName() !=null ? arrayList.get(position).getFirstName() :"";
            String lastName =arrayList.get(position).getLastName() !=null ? arrayList.get(position).getLastName() :"";
            holder.name.setText( firstName+ " " + lastName);
            Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).placeholder(R.drawable.ic_profile).into(holder.event_img);
            holder.location.setText(arrayList.get(position).getEmail());
            holder.cost.setText(arrayList.get(position).getPhoneNumber());
            holder.pause_btn.setVisibility(View.GONE);
            holder.pause_btn.setBackgroundResource(R.drawable.pause);

            holder.edit_btn.setVisibility(View.VISIBLE);
            holder.delete_btn_.setVisibility(View.VISIBLE);

        } else if (from.equals("Classes")) {
            holder.name.setText(arrayList.get(position).getClassname());
            Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileimage()).into(holder.event_img);
            holder.location.setText(arrayList.get(position).getAddress());
            holder.cost.setText(arrayList.get(position).getCourt());
            holder.pause_btn.setBackgroundResource(R.drawable.add_ic);
            holder.edit_btn.setVisibility(View.VISIBLE);
            holder.delete_btn_.setVisibility(View.VISIBLE);
        } else if (from.equals("Schedule")) {
            holder.name.setText(arrayList.get(position).getName());
            Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.event_img);
            holder.location.setText(arrayList.get(position).getAddress());
            holder.cost.setText(arrayList.get(position).getCost());
            holder.pause_btn.setBackgroundResource(R.drawable.pause);
            holder.edit_btn.setVisibility(View.GONE);
            holder.delete_btn_.setVisibility(View.GONE);
        } else if (from.equals("Activities")) {
            holder.name.setText(arrayList.get(position).getName() + arrayList.get(position).getOpponent());
            Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.event_img);
            holder.location.setText(arrayList.get(position).getAddress());
            //holder.cost.setText(arrayList.get(position).getCost());
            holder.pause_btn.setBackgroundResource(R.drawable.pause);
        } else if (from.equals("Games")) {
            holder.name.setText(arrayList.get(position).getName() + " vs " + arrayList.get(position).getOpponent());
            Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.event_img);
            holder.location.setText(arrayList.get(position).getAddress());
            //holder.cost.setText(arrayList.get(position).getCost());
            holder.pause_btn.setBackgroundResource(R.drawable.pause);
        }


        holder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("arrayList", "onClick:  " + from);
                if (from.equals("Student")) {
                    String jersey = arrayList.get(position).getJerseyNumber() != null ? arrayList.get(position).getJerseyNumber() : "0";

                    EditPlayerFrag fragment = new EditPlayerFrag();
                    Bundle args = new Bundle();
                    args.putString("fromType", "edit");
                    args.putString("value", jersey);
                    args.putString("playerId", arrayList.get(position).getId().toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else if (from.equals("Activities")) {

                    EditActivityFrag fragment = new EditActivityFrag();
                    Bundle args = new Bundle();
                    args.putString("fromType", "edit");
                    args.putString("id", arrayList.get(position).getId().toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else if (from.equals("Classes")) {

                    EditClass fragment = new EditClass();
                    Bundle args = new Bundle();
                    args.putString("fromType", "edit");
                    args.putString("classId", arrayList.get(position).getId().toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else if (from.equals("Games")) {

                    EditGameFrag fragment = new EditGameFrag();
                    Bundle args = new Bundle();
                    args.putString("fromType", "edit");
                    args.putString("id", arrayList.get(position).getId().toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.school_dashbord_fram, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });


        holder.delete_btn_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ApiDelete(arrayList.get(position).getId().toString(), Constans.fromType, holder.getAdapterPosition());

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();


            }
        });

        holder.items_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (from.equals("Student")) {
                        String jersey = arrayList.get(position).getJerseyNumber() != null ? arrayList.get(position).getJerseyNumber() : "0";

                        EditPlayerFrag fragment = new EditPlayerFrag();
                        Bundle args = new Bundle();
                        args.putString("fromType", "show");
                        args.putString("value", jersey);
                        args.putString("playerId", arrayList.get(position).getId().toString());
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else if (from.equals("Activities")) {

                    /*    EditActivityFrag fragment = new EditActivityFrag();
                        Bundle args = new Bundle();
                        args.putString("fromType", "edit");
                        args.putString("viewType", "show");
                        args.putString("id", arrayList.get(position).getId().toString());
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                         transaction.addToBackStack(null);
                        transaction.commit();*/

                        EditActivityFrag fragment = new EditActivityFrag();
                        Bundle args = new Bundle();
                        args.putString("viewType", "show");
                        args.putString("id", arrayList.get(position).getId().toString());
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();


                    } else if (from.equals("Classes")) {

                        EditClass fragment = new EditClass();
                        Bundle args = new Bundle();
                        args.putString("fromType", "show");
                        args.putString("classId", arrayList.get(position).getId().toString());
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else if (from.equals("Games")) {
//
                        EditGameFrag fragment = new EditGameFrag();
                        Bundle args = new Bundle();
                        args.putString("fromType", "show");
                        args.putString("id", arrayList.get(position).getId().toString());
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        CreateEventFrag fragment = new CreateEventFrag();
                        Bundle args = new Bundle();
                        args.putString("fromType", "edit");
                        args.putSerializable("body", arrayList.get(position));
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.frag_frame_eve, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                } catch (Exception e) {

                }

            }
        });
    }

    public void filterList(List<Body> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    private void ApiDelete(String id, String from, int position) {
        final Dialog dialog = AppUtils.showProgress(activity);
        Call<AddEvent> addEventCall;

        if (from.equals("Student")) {
            addEventCall = AppUtils.getAPIService(activity).deletePlayer(id);

        } else if (from.equals("Classes")) {
            addEventCall = AppUtils.getAPIService(activity).deleteClass(id);

        } else {
            addEventCall = AppUtils.getAPIService(activity).delteItem(id);
        }


        addEventCall.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, Response<AddEvent> response) {
                dialog.dismiss();
                Log.e("onResponse", "lf;d-== " + response.toString());
                if (response.code() == 200) {

                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    delete(position);
//                    EventListFragment createTeamFrag = new EventListFragment();
//                    FragmentManager manager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction trans = manager.beginTransaction();
//                    trans.replace(R.id.crate_activity_scl, createTeamFrag);
//
//                    trans.commit();
                    //manager.popBackStack();


                } else {
                    if (response.code() == 400) {
                        if (!response.isSuccessful()) {

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String userMessage = jsonObject.getString("message");
                                Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void delete(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class EventHolder extends RecyclerView.ViewHolder {
        TextView name, location, cost, manager;
        CircleImageView event_img;
        ImageView pause_btn, edit_btn;
        LinearLayout delete_btn_;
        CardView items_click;

        public EventHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            location = view.findViewById(R.id.location);
            cost = view.findViewById(R.id.cost);
            manager = view.findViewById(R.id.manager);
            event_img = view.findViewById(R.id.event_img);
            pause_btn = view.findViewById(R.id.pause_btn);
            edit_btn = view.findViewById(R.id.edit_btn);
            delete_btn_ = view.findViewById(R.id.delete_btn_);
            items_click = view.findViewById(R.id.items_click);


        }
    }
}
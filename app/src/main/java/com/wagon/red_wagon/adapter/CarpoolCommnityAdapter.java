package com.wagon.red_wagon.adapter;

import android.app.Dialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddEvent;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.APIService;
import com.wagon.red_wagon.utils.AppUtils;
import com.wagon.red_wagon.utils.Constans;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class CarpoolCommnityAdapter extends RecyclerView.Adapter<CarpoolCommnityAdapter.MyHolderCar> {
    FragmentActivity activity;
    List<Body> arrayList;
    String value = "";
    View view;

    public CarpoolCommnityAdapter(FragmentActivity activity, List<Body> responseBody) {

        this.activity = activity;
        this.arrayList = responseBody;

    }

    @NonNull
    @Override
    public MyHolderCar onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.carpool_list_adapter, parent, false);
        MyHolderCar myHolderCar = new MyHolderCar(view);

        return myHolderCar;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolderCar holder, int position) {
        holder.groupName.setText(arrayList.get(position).getCarPoolName());


        for (String genre : arrayList.get(position).getDriverName()) {

            ImageView imageView = new ImageView(activity);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(30, 30);

            imageView.setLayoutParams(p);
            p.gravity = Gravity.CENTER;
            p.setMargins(10, 0, 10, 0);
            imageView.setBackgroundResource(R.drawable.profile_ic);

            TextView textview = new TextView(activity);
            textview.setTextSize(10);
            LinearLayout.LayoutParams lpTextView = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            int width = 40;
            int height = 40;
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
            parms.setMargins(5, 0, 5, 0);
            parms.gravity = Gravity.CENTER;


            textview.setLayoutParams(parms);
            textview.setText(genre);
            holder.driver_list_lay.addView(imageView);
            holder.driver_name_lay.addView(textview);

        }

        holder.join_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // APIServiceJoin(arrayList.get(position).getUserId());
                List<String> driverId = arrayList.get(position).getDriverId();
                Log.e("getFamilyId", "onClick: " + driverId);
                Log.e("getFamilyId", "onClick: " + arrayList.get(position).getChatId());


                if (driverId.contains(Constans.getFamilyId(activity))) {
                    Toast.makeText(activity, "You have already join this pool", Toast.LENGTH_SHORT).show();
                } else {
                    String drivername = arrayList.get(position).getDriverName().toString();
                    APIServiceJoin(Constans.getFamilyId(activity), arrayList.get(position).getCarpoolId(),arrayList.get(position).getCarpoolUserId().toString(), arrayList.get(position).getCarPoolName(), Constans.getUserName(activity),arrayList.get(position).getChatId());

                }

            /*    if (driverId.contains(arrayList.get(position).getCarpoolUserId())) {
                    Log.e("LinearLayout", "onBindViewHolder: ");
                    Toast.makeText(activity, "You have already join this pool", Toast.LENGTH_SHORT).show();

                } else {
                    String drivername =arrayList.get(position).getDriverName().toString();
                    Log.e("Doesnotmacth", "onBindViewHolder: ");

                    APIServiceJoin(arrayList.get(position).getCarpoolUserId(), arrayList.get(position).getCarpoolId(),arrayList.get(position).getCarPoolName(),drivername);
                }
*/

            }
        });
    }

    private void APIServiceJoin(String carpoolUserId, String getCarpoolId, String driverid, String carpoolname, String driverName,String chatID) {

        final Dialog dialog = AppUtils.showProgress(activity);
        APIService mAPIService = AppUtils.getAPIService(activity);
        Call<AddEvent> getApi = mAPIService.addDriverPoolListRequest(carpoolUserId, getCarpoolId, driverid, carpoolname, driverName,chatID);

        getApi.enqueue(new Callback<AddEvent>() {
            @Override
            public void onResponse(Call<AddEvent> call, retrofit2.Response<AddEvent> response) {
                Log.e("onResponse", "lf;d-== " + response.toString());
                dialog.dismiss();
                if (response.code() == 200) {


                    activity.onBackPressed();

                } else if (response.code() == 400) {
                    if (!response.isSuccessful()) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");

                            Log.e("MessageM ", userMessage);
                            Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (response.code() == 401) {
                    AppUtils.sessionExpiredAlert(activity);
                }
            }

            @Override
            public void onFailure(Call<AddEvent> call, Throwable t) {
                dialog.dismiss();
                Log.e("Failure", "onFailure: ", t);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyHolderCar extends RecyclerView.ViewHolder {
        TextView groupName, sport_name;
        CardView cardview;
        ImageView imageView;
        LinearLayout driver_list_lay, driver_name_lay, join_com;
        CircleImageView group_img;

        public MyHolderCar(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            groupName = view.findViewById(R.id.group_name);
            driver_list_lay = view.findViewById(R.id.driver_list_lay);
            driver_name_lay = view.findViewById(R.id.driver_name_lay);
            join_com = view.findViewById(R.id.join_com);


        }
    }
}

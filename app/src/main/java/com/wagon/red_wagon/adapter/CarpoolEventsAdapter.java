package com.wagon.red_wagon.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.fragments.CreateCarPoolFrag;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.wagon.red_wagon.ui.fragments.CarpoolSelectEventList.eventId;

public class CarpoolEventsAdapter extends RecyclerView.Adapter<CarpoolEventsAdapter.CarpoolListHolder> {
    FragmentActivity activity;
    List<Body> arrayList;

    public CarpoolEventsAdapter(FragmentActivity activity, List<Body> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public CarpoolEventsAdapter.CarpoolListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.carpool_list_row, parent, false);

        CarpoolEventsAdapter.CarpoolListHolder community_adapter = new CarpoolEventsAdapter.CarpoolListHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull CarpoolEventsAdapter.CarpoolListHolder holder, final int position) {
        holder.groupName.setText(arrayList.get(position).getName());


//        holder.player_name.setText(arrayList.get(position).getFirstName());
        Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).placeholder(R.drawable.profile_ic).into(holder.group_img);
//

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventId = arrayList.get(position).getId().toString();
                CreateCarPoolFrag eventList = new CreateCarPoolFrag();
                Bundle args = new Bundle();
                args.putString("eventId", eventId);
                eventList.setArguments(args);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fram_dash, eventList, "Create a Pool");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    public void filterList(List<Body> arrayList) {

        this.arrayList = arrayList;
        notifyDataSetChanged();

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class CarpoolListHolder extends RecyclerView.ViewHolder {
        TextView groupName, sport_name;
        CardView cardview;
        ImageView imageView;
        LinearLayout driver_list_lay, driver_name_lay;
        CircleImageView group_img;

        public CarpoolListHolder(View view) {
            super(view);
            cardview = view.findViewById(R.id.cardview);
            groupName = view.findViewById(R.id.group_name);
            driver_list_lay = view.findViewById(R.id.driver_list_lay);
            driver_name_lay = view.findViewById(R.id.driver_name_lay);

            group_img = view.findViewById(R.id.group_img);


        }
    }


}


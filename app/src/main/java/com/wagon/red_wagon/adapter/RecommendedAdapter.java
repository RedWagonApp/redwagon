package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecommendedAdapter extends RecyclerView.Adapter<RecommendedAdapter.MyRecmdHolder> {
    FragmentActivity activity;
    List<Body> responseBody;
    AddToPoolCallback callback;

    public RecommendedAdapter(FragmentActivity activity, List<Body> responseBody, AddToPoolCallback callback) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.callback = callback;
    }

    @NonNull
    @Override
    public RecommendedAdapter.MyRecmdHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommend_adapter, parent, false);
        MyRecmdHolder myRecmdHolder = new MyRecmdHolder(view);
        return myRecmdHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecommendedAdapter.MyRecmdHolder holder, final int position) {
        holder.driver_name.setText(responseBody.get(position).getFirstName());
        holder.content.setVisibility(View.VISIBLE);
        holder.progressBar.setProgress(Integer.parseInt(responseBody.get(position).getPercent()));
        holder.progressBar.getIndeterminateDrawable().setColorFilter(activity.
                getResources().getColor(R.color.yellow),
                android.graphics.PorterDuff.Mode.SRC_IN);
        holder.content.setText("Contact : " + responseBody.get(position).getPhoneNumber());
        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).placeholder(R.drawable.profile_ic).into(holder.image);
        holder.add_pool_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onAddDriver(responseBody, position, holder.add_pool_btn);
            }
        });

    }

    public void filterList(List<Body> arrayList) {

        this.responseBody = arrayList;
        notifyDataSetChanged();

    }

   public interface AddToPoolCallback {

        void onAddDriver(List<Body> list, int pos, Button btn);
    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MyRecmdHolder extends RecyclerView.ViewHolder {
        TextView driver_name, content;
        Button add_pool_btn;
        CircleImageView image;
        ProgressBar progressBar;

        public MyRecmdHolder(View view) {
            super(view);
            driver_name = view.findViewById(R.id.driver_name);
            content = view.findViewById(R.id.content);
            add_pool_btn = view.findViewById(R.id.add_pool_btn);
            image = view.findViewById(R.id.image);
            progressBar = view.findViewById(R.id.progress_bar);
        }
    }
}

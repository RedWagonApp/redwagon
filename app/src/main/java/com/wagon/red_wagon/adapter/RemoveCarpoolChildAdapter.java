package com.wagon.red_wagon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.AddedChildBody;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RemoveCarpoolChildAdapter extends RecyclerView.Adapter<RemoveCarpoolChildAdapter.MyTrack> {
    FragmentActivity activity;
    List<AddedChildBody> responseBody;
    String value;
    View view;

    RemoveCarpoolChildAdapter.AdapterCallback callback;

    public RemoveCarpoolChildAdapter(FragmentActivity activity, List<AddedChildBody> responseBody, String value, RemoveCarpoolChildAdapter.AdapterCallback callback) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.value = value;
        this.callback = callback;
    }

    @NonNull
    @Override
    public RemoveCarpoolChildAdapter.MyTrack onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.remove_child_pool_row, parent, false);

        RemoveCarpoolChildAdapter.MyTrack chidtrack_adapter = new RemoveCarpoolChildAdapter.MyTrack(view);

        return chidtrack_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull RemoveCarpoolChildAdapter.MyTrack holder, int position) {
        Log.e("onBindViewHolder", "onBindViewHolder: " + responseBody.get(position).getId());
        holder.childName.setText(responseBody.get(position).getName());

        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.childImg);

        holder.add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onMethodCallback( responseBody.get(position).getId().toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public interface AdapterCallback {
        void onMethodCallback(String id);

    }

    public class MyTrack extends RecyclerView.ViewHolder {
        CircleImageView childImg;
        TextView childName;
        Button add_btn;

        public MyTrack(View view) {
            super(view);

            childImg = view.findViewById(R.id.child_img);
            childName = view.findViewById(R.id.child_name);
            add_btn = view.findViewById(R.id.add_btn);
        }
    }
}



package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wagon.red_wagon.R;
import com.wagon.red_wagon.ui.activities.ContractSelectAc;

import java.util.ArrayList;

public class Contract_SelectAdapter extends RecyclerView.Adapter<Contract_SelectAdapter.MyHolderContract>{
    ContractSelectAc activity;
    ArrayList<String> userContract, userName;
    public Contract_SelectAdapter(ContractSelectAc contractSelectAc, ArrayList<String> userContract, ArrayList<String> userName) {
        activity=contractSelectAc;
        this.userContract=userContract;
        this.userName=userName;
    }

    @NonNull
    @Override
    public MyHolderContract onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.contract_adapter,parent,false);
        MyHolderContract myHolderView=new MyHolderContract(view);
        return myHolderView;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolderContract holder, int position) {
        holder.numcontract.setText(userContract.get(position));
        holder.namecontract.setText(userName.get(position));
    }

    @Override
    public int getItemCount() {
        return userContract.size();
    }

    public class MyHolderContract extends RecyclerView.ViewHolder
    {
        TextView numcontract,namecontract;
        public MyHolderContract(View view) {
            super(view);
            numcontract=view.findViewById(R.id.numcontract);
            namecontract=view.findViewById(R.id.namecontract);
        }
    }
}

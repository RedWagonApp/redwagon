package com.wagon.red_wagon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.utils.Constans;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.wagon.red_wagon.ui.fragments.RecipientsFrag.ok_invoice;
import static com.wagon.red_wagon.ui.fragments.RecipientsFrag.select_all;

public class Recipients_Adapter extends RecyclerView.Adapter<Recipients_Adapter.MyRecipients> {
    FragmentActivity activity;
    List<Body> responseBody;
    ArrayList<String> arrayList = new ArrayList();
    ArrayList<String> arrayListName = new ArrayList();
    private String image_path = "";
    RecipientsCallBack callBack;
    boolean isChecked ;

    public Recipients_Adapter(FragmentActivity activity, List<Body> responseBody, RecipientsCallBack callBack) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.callBack = callBack;
    }



    @NonNull
    @Override
    public MyRecipients onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipients_adapter, parent, false);

        MyRecipients myRecipients = new MyRecipients(view);
        return myRecipients;
    }


    @Override
    public void onBindViewHolder(@NonNull final MyRecipients holder, final int position) {

        holder.nameteam_.setText(responseBody.get(position).getTeamName());
        holder.nameplayr.setText(responseBody.get(position).getFirstName());
        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).placeholder(R.drawable.ic_profile).into(holder.image_team);

        if (isChecked) {
            holder.select_invite.setChecked(true);
            arrayList.add(responseBody.get(position).getEmail());
            arrayListName.add(responseBody.get(position).getFirstName());
        }else{
            holder.select_invite.setChecked(false);
            arrayList.clear();
            arrayListName.clear();
        }

        holder.select_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.select_invite.isChecked()) {
                    arrayList.add(responseBody.get(position).getEmail());
                    arrayListName.add(responseBody.get(position).getFirstName());
                    Log.e("arrayList", "onBindViewHolder: " + arrayList);
                    Log.e("arrayListName", "onBindViewHolder: " + arrayListName);

                } else {
                    if (arrayList.contains(responseBody.get(position).getEmail())) {
                        arrayList.remove(responseBody.get(position).getEmail());
                        arrayListName.remove(responseBody.get(position).getFirstName());
                    }


                }

            }
        });

        select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select_all.isChecked()) {
                    isChecked = true;
                } else {
                    isChecked = false;
                }
               notifyDataSetChanged();
            }
        });

        ok_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (arrayList.size() != 0) {
                    callBack.onMethodCallback(arrayList, arrayListName);
                } else {
                    Toast.makeText(activity, "Please select Players", Toast.LENGTH_SHORT).show();
                }
            }

        });


    }

    public interface RecipientsCallBack {
        void onMethodCallback(ArrayList<String> arrayList, ArrayList<String> arrayListName);
    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MyRecipients extends RecyclerView.ViewHolder {
        CheckBox select_invite;
        TextView nameplayr, nameteam_;
        CircleImageView image_team;
        RadioGroup radio_group;

        public MyRecipients(View view) {
            super(view);


            select_invite = view.findViewById(R.id.select_invite);
            nameplayr = view.findViewById(R.id.nameplayr);
            nameteam_ = view.findViewById(R.id.nameteam_);
            image_team = view.findViewById(R.id.image_team);


        }
    }
}

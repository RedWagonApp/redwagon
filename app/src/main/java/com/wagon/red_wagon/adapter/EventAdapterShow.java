package com.wagon.red_wagon.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.Body;
import com.wagon.red_wagon.ui.activities.CalendarTeamDetailAcrivity;
import com.wagon.red_wagon.ui.fragments.EditActivityFrag;
import com.wagon.red_wagon.ui.fragments.EditGameFrag;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EventAdapterShow extends RecyclerView.Adapter<EventAdapterShow.EventHolder> {
    FragmentActivity activity;
    List<Body> arrayList;
    String from, datafrom = "", value = "";

    public EventAdapterShow(FragmentActivity activity, List<Body> responseBody, String fromType, String value) {
        this.activity = activity;
        this.arrayList = responseBody;
        this.from = fromType;
        this.value = value;

    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_row, parent, false);

        EventHolder community_adapter = new EventHolder(view);

        return community_adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, final int position) {

        holder.pause_btn.setVisibility(View.VISIBLE);
        holder.edit_btn.setVisibility(View.VISIBLE);
        holder.delete_btn_.setVisibility(View.GONE);
        datafrom = arrayList.get(position).getType();
        holder.pause_btn.setBackgroundResource(R.drawable.gray_cal);
        holder.edit_btn.setBackgroundResource(R.drawable.players_gray);

        if (datafrom != null) {
            if (datafrom.equals("Activities")) {
                holder.name.setText(arrayList.get(position).getName());
                holder.location.setText(arrayList.get(position).getTeamName());
                holder.cost.setText(arrayList.get(position).getAddress());
                if (from.equals("Teams")) {
                    holder.manager.setVisibility(View.VISIBLE);
                    holder.manager.setText(arrayList.get(position).getCoachName() != null ? arrayList.get(position).getCoachName() : "");
                }
                if (!arrayList.get(position).getProfileImage().equals("")) {
                    Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.event_img);
                }
                //holder.pause_btn.setBackgroundResource(R.drawable.pause);
            } else if (datafrom.equals("Games")) {

                holder.name.setText(arrayList.get(position).getName() + " vs " + arrayList.get(position).getOpponent());
                holder.location.setText(arrayList.get(position).getAddress());
                if (arrayList.get(position).getProfileImage() != "") {
                    Glide.with(activity).load(Constans.BASEURL + arrayList.get(position).getProfileImage()).into(holder.event_img);
                }
            }
        }


        holder.pause_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", arrayList.get(position).getStartDate());
                intent.putExtra("endTime", arrayList.get(position).getStartDate());
                intent.putExtra("title", arrayList.get(position).getName());
                intent.putExtra("allDay", false);
                intent.putExtra("rule", "FREQ=YEARLY");
                activity.startActivity(intent);
            }


        });
        holder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, CalendarTeamDetailAcrivity.class)
                        .putExtra("teamName", arrayList.get(position).getTeamName())
                        .putExtra("teamId", arrayList.get(position).getTeamId()));

            }

        });
        holder.items_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String type = "";
                if (arrayList.get(position).getType() != null) {
                    type = arrayList.get(position).getType();
                    Log.e("typetype", "onClick: " + type);
                }
                if (type.equals("Activities")) {

                    EditActivityFrag fragment = new EditActivityFrag();
                    Bundle args = new Bundle();
                    args.putString("fromType", "edit");
                    args.putString("viewType", "show");
                    args.putString("value", value);
                    args.putString("id", arrayList.get(position).getId().toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.shedule_frag, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                if (type.equals("Games")) {
//
                    EditGameFrag fragment = new EditGameFrag();
                    Bundle args = new Bundle();
                    args.putString("fromType", "show");
                    args.putString("value", value);
                    args.putString("id", arrayList.get(position).getId().toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction().replace(R.id.shedule_frag, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    private void delete(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }

    public class EventHolder extends RecyclerView.ViewHolder {
        TextView name, location, cost, manager;
        CircleImageView event_img;
        ImageView pause_btn, edit_btn;
        LinearLayout delete_btn_;
        CardView items_click;

        public EventHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            location = view.findViewById(R.id.location);
            cost = view.findViewById(R.id.cost);
            manager = view.findViewById(R.id.manager);
            event_img = view.findViewById(R.id.event_img);
            pause_btn = view.findViewById(R.id.pause_btn);
            edit_btn = view.findViewById(R.id.edit_btn);
            delete_btn_ = view.findViewById(R.id.delete_btn_);
            items_click = view.findViewById(R.id.items_click);


        }
    }


}

package com.wagon.red_wagon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wagon.red_wagon.R;
import com.wagon.red_wagon.model.ModelRequest;
import com.wagon.red_wagon.utils.Constans;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RidesDriverAdapter extends RecyclerView.Adapter<RidesDriverAdapter.MyHolderDriver> {
    FragmentActivity activity;
    List<ModelRequest> responseBody;
    String drivernote = "";
    private int num = 0;
    boolean isSelected = false;

    onRequestCallback callback;

    public RidesDriverAdapter(FragmentActivity activity, List<ModelRequest> responseBody, onRequestCallback callback) {
        this.activity = activity;
        this.responseBody = responseBody;
        this.callback = callback;

    }

    @NonNull
    @Override
    public RidesDriverAdapter.MyHolderDriver onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ride_driver_row, parent, false);
        RidesDriverAdapter.MyHolderDriver myHolderDriver = new RidesDriverAdapter.MyHolderDriver(view);

        return myHolderDriver;
    }

    @Override
    public void onBindViewHolder(@NonNull RidesDriverAdapter.MyHolderDriver holder, int position) {
        holder.driver_name.setText(responseBody.get(position).getName());
        holder.availseats.setText(responseBody.get(position).getSeats());
        holder.cost.setText("Cost: $" + responseBody.get(position).getCost());
        Glide.with(activity).load(Constans.BASEURL + responseBody.get(position).getProfileImage()).into(holder.driver_img);
        holder.send_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (responseBody.get(position).getSeats().equals("0")) {
                    Toast.makeText(activity, "Please add seats ", Toast.LENGTH_SHORT).show();
                } else {
                    callback.onRequest(responseBody.get(position).getDriverId(), responseBody.get(position).getSeats());
                }
            }
        });


        holder.bt_puls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (num < 2) {
                    num++;
                    responseBody.get(position).setSeats(String.valueOf(num));
                    notifyDataSetChanged();
                }

            }
        });
        holder.bt_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (num > 0) {
                    num--;
                    responseBody.get(position).setSeats(String.valueOf(num));
                    notifyDataSetChanged();
                }


            }
        });


    }

    public interface onRequestCallback {

        void onRequest(String driverId, String seat);
    }

    @Override
    public int getItemCount() {
        return responseBody.size();
    }

    public class MyHolderDriver extends RecyclerView.ViewHolder {
        TextView driver_name, minutes, number_seats, cost;
        CircleImageView driver_img;
        ImageView bt_puls, bt_less;
        TextView availseats;
        Button send_request;

        public MyHolderDriver(View view) {
            super(view);
            driver_name = view.findViewById(R.id.driver_name);
            number_seats = view.findViewById(R.id.number_seats);
            cost = view.findViewById(R.id.cost);
            driver_img = view.findViewById(R.id.driver_img);
            bt_puls = view.findViewById(R.id.bt_puls);
            bt_less = view.findViewById(R.id.bt_less);
            availseats = view.findViewById(R.id.availseats);
            send_request = view.findViewById(R.id.send_request);
        }
    }

}
